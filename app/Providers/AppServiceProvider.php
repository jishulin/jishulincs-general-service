<?php

namespace App\Providers;

use DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // DB::listen(function ($query) {
        //     $sql = str_replace('?', '%s', $query->sql);
        //     echo sprintf($sql, ...$query->bindings);
        //     echo PHP_EOL;
        // });
    }
}
