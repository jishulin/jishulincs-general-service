<?php

namespace App\Validate\Zhihu;

use Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ZhihuValidator
{
    // 用户注册
    public static function register($input)
    {
        $rules = [
            'name' => [
                'required',
                'unique:zhihu_users',
                function ($attribute, $value, $fail) {
                    $limitMinVal = 2;
                    $limitMaxVal = 20;
                    if ((strlen($value) > $limitMaxVal || strlen($value) < $limitMinVal) && $value != '') {
                        $fail(trans('zhihu.The-Name-Can-Not-More-Than-Point-Character', ['limitMinVal' => $limitMinVal, 'limitMaxVal' => $limitMaxVal]));
                    }
                },
                'alpha_num',
                'regex:/^[a-zA-Z]\w{1,21}$/'
            ],
            'email' => [
                'required',
                'unique:zhihu_users',
                'email',
                function ($attribute, $value, $fail) {
                    $limitVal = 60;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Email-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'password' => [
                'required',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,32}$/',
                'confirmed'
            ],
            'password_confirmation' => [
                'required',
                'same:password'
            ],
        ];

        $messages = [
            'name.required' => trans('zhihu.The-Name-Can-Not-Be-Empty'),
            'name.unique' => trans('zhihu.The-Name-has-been-register', ['name' => $input['name']]),
            'name.alpha_num' => trans('zhihu.The-Name-Format-Error'),
            'name.regex' => trans('zhihu.The-Name-Format-Error'),
            'email.required' => trans('zhihu.The-Email-Can-Not-Be-Empty'),
            'email.unique' => trans('zhihu.The-Email-has-been-register', ['name' => $input['email']]),
            'email.email' => trans('zhihu.The-Email-Format-Error'),
            'password.required' => trans('zhihu.The-Password-Can-Not-Be-Empty'),
            'password.regex' => trans('zhihu.The-Password-Format-Error'),
            'password.confirmed' => trans('zhihu.The-Password-Not-Match-Confirmed'),
            'password_confirmation.required' => trans('zhihu.The-Password-Confirmation-Can-Not-Be-Empty'),
            'password_confirmation.same' => trans('zhihu.The-Password-Confirmation-Not-Match-Password'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 通过邮箱找回密码
    public static function retrievePassword($input)
    {
        $rules = [
            'email' => [
                'required',
                'email',
                'exists:zhihu_users',
            ],
        ];

        $messages = [
            'email.required' => trans('zhihu.The-Email-Can-Not-Be-Empty'),
            'email.email' => trans('zhihu.The-Email-Format-Error'),
            'email.exists' => trans('zhihu.The-Email-Is-Not-Exists'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 找回密码-链接有效校验-重置密码
    public static function retrievePasswordLinkVerifyActiveAndReset($input)
    {
        $rules = [
            'password' => [
                'required',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,32}$/',
                'confirmed'
            ],
            'password_confirmation' => [
                'required',
                'same:password'
            ],
        ];

        $messages = [
            'password.required' => trans('zhihu.The-Password-Can-Not-Be-Empty'),
            'password.regex' => trans('zhihu.The-Password-Format-Error'),
            'password.confirmed' => trans('zhihu.The-Password-Not-Match-Confirmed'),
            'password_confirmation.required' => trans('zhihu.The-Password-Confirmation-Can-Not-Be-Empty'),
            'password_confirmation.same' => trans('zhihu.The-Password-Confirmation-Not-Match-Password'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 发布问题
    public static function publishQuestion($input)
    {
        $rules = [
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Question-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'lead' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Question-Lead-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'content' => [
                'required',
            ],
            'topic_id' => [
                'required',
            ],
        ];

        $messages = [
            'topic_id.required' => trans('zhihu.The-Question-Topic-Can-Not-Be-Empty'),
            'title.required' => trans('zhihu.The-Question-Title-Can-Not-Be-Empty'),
            'content.required' => trans('zhihu.The-Question-Content-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 设置问题
    public static function configQuestion($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Question-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'lead' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Question-Lead-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'content' => [
                'required',
            ],
            'close_comment' => [
                'required',
                'integer',
            ],
            // 'is_hidden' => [
            //     'required',
            //     'integer',
            // ],
            'is_guest_view' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'title.required' => trans('zhihu.The-Question-Title-Can-Not-Be-Empty'),
            'content.required' => trans('zhihu.The-Question-Content-Can-Not-Be-Empty'),
            'close_comment.required' => trans('zhihu.The-Close-Comment-Can-Not-Be-Empty-Friend'),
            'close_comment.integer' => trans('zhihu.The-Close-Comment-Incorrect-Format-Friend'),
            // 'is_hidden.required' => trans('zhihu.The-Is-Hidden-Can-Not-Be-Empty-Friend'),
            // 'is_hidden.integer' => trans('zhihu.The-Is-Hidden-Incorrect-Format-Friend'),
            'is_guest_view.required' => trans('zhihu.The-Is-Guest-View-Can-Not-Be-Empty-Friend'),
            'is_guest_view.integer' => trans('zhihu.The-Is-Guest-View-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 创建话题
    public static function createTopic($input)
    {
        $rules = [
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Topic-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
        ];

        $messages = [
            'name.required' => trans('zhihu.The-Topic-Name-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 设置话题
    public static function configTopic($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Topic-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'status' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'name.required' => trans('zhihu.The-Topic-Name-Can-Not-Be-Empty'),
            'content.required' => trans('zhihu.The-Question-Content-Can-Not-Be-Empty'),
            'status.required' => trans('zhihu.The-Status-Can-Not-Be-Empty-Friend'),
            'status.integer' => trans('zhihu.The-Status-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 设置配置源【添加】
    public static function sourceCreateConfig($input)
    {
        $rules = [
            'key_title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'key_name' => [
                'required',
                'unique:zhihu_config_sources',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'key_field' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Field-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'is_system' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'key_title.required' => trans('zhihu.The-Source-Config-Key-Title-Can-Not-Be-Empty'),
            'key_name.required' => trans('zhihu.The-Source-Config-Key-Name-Can-Not-Be-Empty'),
            'key_name.unique' => trans('zhihu.The-Source-Config-Key-Name-has-Exists', ['name' => $input['key_name']]),
            'key_field.required' => trans('zhihu.The-Source-Config-Key-Field-Can-Not-Be-Empty'),
            'is_system.required' => trans('zhihu.The-Is-System-Can-Not-Be-Empty-Friend'),
            'is_system.integer' => trans('zhihu.The-Is-System-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 设置配置源【修改】
    public static function sourceEditConfig($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'key_title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'key_name' => [
                'required',
                // 'unique:zhihu_config_sources',
                Rule::unique('zhihu_config_sources')->ignore($input['id']),
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'key_field' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Source-Config-Key-Field-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'status' => [
                'required',
                'integer',
            ],
            'is_system' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'key_title.required' => trans('zhihu.The-Source-Config-Key-Title-Can-Not-Be-Empty'),
            'key_name.required' => trans('zhihu.The-Source-Config-Key-Name-Can-Not-Be-Empty'),
            'key_name.unique' => trans('zhihu.The-Source-Config-Key-Name-has-Exists', ['name' => $input['key_name']]),
            'key_field.required' => trans('zhihu.The-Source-Config-Key-Field-Can-Not-Be-Empty'),
            'status.required' => trans('zhihu.The-Status-Can-Not-Be-Empty-Friend'),
            'status.integer' => trans('zhihu.The-Status-Incorrect-Format-Friend'),
            'is_system.required' => trans('zhihu.The-Is-System-Can-Not-Be-Empty-Friend'),
            'is_system.integer' => trans('zhihu.The-Is-System-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 回答问题-评论
    public static function answerQuestion($input)
    {
        $rules = [
            'content' => [
                'required',
            ],
            'question_id' => [
                'required',
            ],
        ];

        $messages = [
            'question_id.required' => trans('zhihu.The-Question-ID-Can-Not-Be-Empty'),
            'content.required' => trans('zhihu.The-Answer-Question-Content-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 广告设置【添加】
    public static function advsSettingCreateOperation($input)
    {
        $rules = [
            'advcomname' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'advcomcontactname' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Contact-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'advcomcontact' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 13;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Contact-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 20;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'description' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 20;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'imagelink' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Imagelink-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'blanklinkurl' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Blanklinkurl-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'type' => [
                'required',
            ],
            'start_date' => [
                'required',
                'date'
            ],
            'end_date' => [
                'required',
                'date'
            ],
        ];

        $messages = [
            'advcomname.required' => trans('zhihu.The-Advs-Company-Name-Can-Not-Be-Empty'),
            'advcomcontactname.required' => trans('zhihu.The-Advs-Company-Contact-Name-Can-Not-Be-Empty'),
            'advcomcontact.required' => trans('zhihu.The-Advs-Company-Contact-Can-Not-Be-Empty'),
            'title.required' => trans('zhihu.The-Advs-Title-Can-Not-Be-Empty'),
            'type.required' => trans('zhihu.The-Advs-Type-Can-Not-Be-Empty'),
            'start_date.required' => trans('zhihu.The-Advs-Start-Date-Can-Not-Be-Empty'),
            'start_date.date' => trans('zhihu.The-Advs-Start-Date-Incorrect-Format'),
            'end_date.required' => trans('zhihu.The-Advs-End-Date-Can-Not-Be-Empty'),
            'end_date.date' => trans('zhihu.The-Advs-End-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 广告设置【修改】
    public static function advsSettingEditOperation($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'advcomname' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'advcomcontactname' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Contact-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'advcomcontact' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 13;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Company-Contact-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 20;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'description' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 20;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'imagelink' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Imagelink-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'blanklinkurl' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 200;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Advs-Blanklinkurl-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'type' => [
                'required',
            ],
            'start_date' => [
                'required',
                'date'
            ],
            'end_date' => [
                'required',
                'date'
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'advcomname.required' => trans('zhihu.The-Advs-Company-Name-Can-Not-Be-Empty'),
            'advcomcontactname.required' => trans('zhihu.The-Advs-Company-Contact-Name-Can-Not-Be-Empty'),
            'advcomcontact.required' => trans('zhihu.The-Advs-Company-Contact-Can-Not-Be-Empty'),
            'title.required' => trans('zhihu.The-Advs-Title-Can-Not-Be-Empty'),
            'type.required' => trans('zhihu.The-Advs-Type-Can-Not-Be-Empty'),
            'start_date.required' => trans('zhihu.The-Advs-Start-Date-Can-Not-Be-Empty'),
            'start_date.date' => trans('zhihu.The-Advs-Start-Date-Incorrect-Format'),
            'end_date.required' => trans('zhihu.The-Advs-End-Date-Can-Not-Be-Empty'),
            'end_date.date' => trans('zhihu.The-Advs-End-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 封面管理【添加】
    public static function systemCoverCreateOperation($input)
    {
        $rules = [
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Cover-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'description' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Cover-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'coverimg' => [
                'required',
                // function ($attribute, $value, $fail) {
                //     $limitVal = 200;
                //     if (strlen($value) > $limitVal && $value != '') {
                //         $fail(trans('zhihu.The-Cover-Coverimg-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                //     }
                // },
            ],
            'start_date' => [
                'date'
            ],
            'end_date' => [
                'date'
            ],
        ];

        $messages = [
            'title.required' => trans('zhihu.The-Cover-Title-Can-Not-Be-Empty'),
            'coverimg.required' => trans('zhihu.The-Cover-Coverimg-Can-Not-Be-Empty'),
            'start_date.date' => trans('zhihu.The-Cover-Start-Date-Incorrect-Format'),
            'end_date.date' => trans('zhihu.The-Cover-End-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 封面管理【修改】
    public static function systemCoverEditOperation($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Cover-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'description' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Cover-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'coverimg' => [
                'required',
                // function ($attribute, $value, $fail) {
                //     $limitVal = 200;
                //     if (strlen($value) > $limitVal && $value != '') {
                //         $fail(trans('zhihu.The-Cover-Coverimg-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                //     }
                // },
            ],
            'start_date' => [
                'date'
            ],
            'end_date' => [
                'date'
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'title.required' => trans('zhihu.The-Cover-Title-Can-Not-Be-Empty'),
            'coverimg.required' => trans('zhihu.The-Cover-Coverimg-Can-Not-Be-Empty'),
            'start_date.date' => trans('zhihu.The-Cover-Start-Date-Incorrect-Format'),
            'end_date.date' => trans('zhihu.The-Cover-End-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 管理员消息发布【添加】
    public static function systemPublishCreateOperation($input)
    {
        $rules = [
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-System-Publish-Message-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'message' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-System-Publish-Message-Message-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'types' => [
                'required',
            ],
            'times' => [
                'required_unless:types,0',
                function ($attribute, $value, $fail) use ($input) {
                    if ($input['types'] == 1 && !is_numeric($value)) {
                        $fail(trans('zhihu.The-System-Publish-Message-Times-formater-Error-Integer'));
                    }
                },
            ]
        ];

        $messages = [
            'title.required' => trans('zhihu.The-System-Publish-Message-Title-Can-Not-Be-Empty'),
            'message.required' => trans('zhihu.The-System-Publish-Message-Message-Can-Not-Be-Empty'),
            'types.required' => trans('zhihu.The-System-Publish-Message-Types-Can-Not-Be-Empty'),
            'times.required_unless' => trans('zhihu.The-System-Publish-Message-Times-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 管理员消息发布【修改】
    public static function systemPublishEditOperation($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-System-Publish-Message-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'message' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-System-Publish-Message-Message-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'types' => [
                'required',
            ],
            'times' => [
                'required_unless:types,0',
                function ($attribute, $value, $fail) use ($input) {
                    if ($input['types'] == 1 && !is_numeric($value)) {
                        $fail(trans('zhihu.The-System-Publish-Message-Times-formater-Error-Integer'));
                    }
                },
            ]
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'title.required' => trans('zhihu.The-System-Publish-Message-Title-Can-Not-Be-Empty'),
            'message.required' => trans('zhihu.The-System-Publish-Message-Message-Can-Not-Be-Empty'),
            'types.required' => trans('zhihu.The-System-Publish-Message-Types-Can-Not-Be-Empty'),
            'times.required_unless' => trans('zhihu.The-System-Publish-Message-Times-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 问题举报
    public static function reportQuestion($input)
    {
        $rules = [
            'question_id' => [
                'required',
                'integer',
            ],
            'report_type' => [
                'required',
            ],
            'report_descrption' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 600;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Report-Descrption-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'report_type.required' => trans('zhihu.The-Report-Type-Can-Not-Be-Empty'),
            'report_descrption.required' => trans('zhihu.The-Report-Descrption-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 评论举报
    public static function reportAnswer($input)
    {
        $rules = [
            'answer_id' => [
                'required',
                'integer',
            ],
            'report_type' => [
                'required',
            ],
            'report_descrption' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 600;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Report-Descrption-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'report_type.required' => trans('zhihu.The-Report-Type-Can-Not-Be-Empty'),
            'report_descrption.required' => trans('zhihu.The-Report-Descrption-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 声望分值配置【添加】
    public static function prestigeScoreCreateOperation($input)
    {
        $rules = [
            'preskey' => [
                'required',
                'unique:zhihu_prestiges',
                function ($attribute, $value, $fail) {
                    $limitVal = 64;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Prestige-Preskey-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Prestige-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'score' => [
                'required',
                'integer',
            ],
            'status' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'preskey.required' => trans('zhihu.The-Prestige-Preskey-Can-Not-Be-Empty'),
            'preskey.unique' => trans('zhihu.The-Prestige-Preskey-has-Exists', ['name' => $input['preskey']]),
            'name.required' => trans('zhihu.The-Prestige-Name-Can-Not-Be-Empty'),
            'score.required' => trans('zhihu.The-Prestige-Score-Can-Not-Be-Empty-Friend'),
            'score.integer' => trans('zhihu.The-Prestige-Score-Incorrect-Format-Friend'),
            'status.required' => trans('zhihu.The-Prestige-Status-Can-Not-Be-Empty-Friend'),
            'status.integer' => trans('zhihu.The-Prestige-Status-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 声望分值配置【修改】
    public static function prestigeScoreEditOperation($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'preskey' => [
                'required',
                Rule::unique('zhihu_prestiges')->ignore($input['id']),
                function ($attribute, $value, $fail) {
                    $limitVal = 64;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Prestige-Preskey-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Prestige-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'score' => [
                'required',
                'integer',
            ],
            'status' => [
                'required',
                'integer',
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'preskey.required' => trans('zhihu.The-Prestige-Preskey-Can-Not-Be-Empty'),
            'preskey.unique' => trans('zhihu.The-Prestige-Preskey-has-Exists', ['name' => $input['preskey']]),
            'name.required' => trans('zhihu.The-Prestige-Name-Can-Not-Be-Empty'),
            'score.required' => trans('zhihu.The-Prestige-Score-Can-Not-Be-Empty-Friend'),
            'score.integer' => trans('zhihu.The-Prestige-Score-Incorrect-Format-Friend'),
            'status.required' => trans('zhihu.The-Prestige-Status-Can-Not-Be-Empty-Friend'),
            'status.integer' => trans('zhihu.The-Prestige-Status-Incorrect-Format-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 建议反馈管理【反馈，添加】
    public static function feedbackManagerFeedbackOperation($input)
    {
        $rules = [
            'type' => [
                'required',
            ],
            'content' => [
                'required',
            ],
        ];

        $messages = [
            'type.required' => trans('zhihu.The-Feedback-Type-Can-Not-Be-Empty-Friend'),
            'content.required' => trans('zhihu.The-Feedback-Content-Can-Not-Be-Empty-Friend'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 友情链接【添加】
    public static function friendLinkCreateOperation($input)
    {
        $rules = [
            'tags' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Tags-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'links' => [
                'required',
                'url',
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Links-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'startdate' => [
                'required',
                'date'
            ],
        ];

        $messages = [
            'tags.required' => trans('zhihu.The-Friend-Link-Tags-Can-Not-Be-Empty'),
            'name.required' => trans('zhihu.The-Friend-Link-Name-Can-Not-Be-Empty'),
            'links.required' => trans('zhihu.The-Friend-Link-Links-Can-Not-Be-Empty'),
            'links.url' => trans('zhihu.The-Friend-Link-Links-Is-Not-Url'),
            'startdate.required' => trans('zhihu.The-Friend-Link-Start-Date-Can-Not-Be-Empty'),
            'startdate.date' => trans('zhihu.The-Friend-Link-Start-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 友情链接【编辑】
    public static function friendLinkEditOperation($input)
    {
        $rules = [
            'id' => [
                'required',
                'integer',
            ],
            'tags' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Tags-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'links' => [
                'required',
                'url',
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('zhihu.The-Friend-Link-Links-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'status' => [
                'required',
                'integer',
            ],
            'startdate' => [
                'required',
                'date'
            ],
        ];

        $messages = [
            'id.required' => trans('zhihu.The-ID-Can-Not-Be-Empty-Friend'),
            'id.integer' => trans('zhihu.The-ID-Incorrect-Format-Friend'),
            'tags.required' => trans('zhihu.The-Friend-Link-Tags-Can-Not-Be-Empty'),
            'name.required' => trans('zhihu.The-Friend-Link-Name-Can-Not-Be-Empty'),
            'links.required' => trans('zhihu.The-Friend-Link-Links-Can-Not-Be-Empty'),
            'links.url' => trans('zhihu.The-Friend-Link-Links-Is-Not-Url'),
            'status.required' => trans('zhihu.The-Status-Can-Not-Be-Empty-Friend'),
            'status.integer' => trans('zhihu.The-Status-Incorrect-Format-Friend'),
            'startdate.required' => trans('zhihu.The-Friend-Link-Start-Date-Can-Not-Be-Empty'),
            'startdate.date' => trans('zhihu.The-Friend-Link-Start-Date-Incorrect-Format'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }
}
