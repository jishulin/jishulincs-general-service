<?php

namespace App\Validate\Book;

use Config;
use Illuminate\Support\Facades\Validator;

class BookValidator
{
    // 书籍创建校验
    public static function bookCreated($input)
    {
        $rules = [
            'bk_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'bk_desc' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 300;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'bk_name.required' => trans('alg.The-Book-Name-Can-Not-Be-Empty'),
            'bk_desc.required' => trans('alg.The-Book-Description-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 书籍修改校验
    public static function bookModified($input)
    {
        $rules = [
            'id' => 'required',
            'bk_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'bk_desc' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 300;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'id.required' => trans('alg.The-Id-Can-Not-Be-Empty'),
            'bk_name.required' => trans('alg.The-Book-Name-Can-Not-Be-Empty'),
            'bk_desc.required' => trans('alg.The-Book-Description-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 书籍目录创建
    public static function artCateCreated($input)
    {
        $rules = [
            'book_id' => [
                'required',
                'integer',
            ],
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Cates-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'book_id.required' => trans('alg.The-Book-ID-Can-Not-Be-Empty'),
            'book_id.integer' => trans('alg.The-Book-ID-Incorrect-Format'),
            'name.required' => trans('alg.The-Book-Cates-Name-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 书籍目录修改
    public static function artCateModified($input)
    {
        $rules = [
            'id' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Cates-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'id.required' => trans('alg.The-Id-Can-Not-Be-Empty'),
            'name.required' => trans('alg.The-Book-Cates-Name-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 文章添加
    public static function articleCreated($input)
    {
        $rules = [
            'cate_id' => 'required',
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Article-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            // 'content' => 'required',
            'art-editormd-html-code' => 'required',
            'author' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Cates-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
        ];
        $messages = [
            'cate_id.required' => trans('alg.The-Article-Cate-Can-Not-Be-Empty'),
            'title.required' => trans('alg.The-Article-Title-Can-Not-Be-Empty'),
            // 'content.required' => trans('alg.The-Article-Content-Can-Not-Be-Empty'),
            'art-editormd-html-code.required' => trans('alg.The-Article-Content-Can-Not-Be-Empty'),
            'author.required' => trans('alg.The-Article-Author-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 文章修改
    public static function articleModified($input)
    {
        $rules = [
            'id' => 'required',
            'cate_id' => 'required',
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Article-Title-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            // 'content' => 'required',
            'art-editormd-html-code' => 'required',
            'author' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-Book-Cates-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
        ];
        $messages = [
            'id.required' => trans('alg.The-Id-Can-Not-Be-Empty'),
            'cate_id.required' => trans('alg.The-Article-Cate-Can-Not-Be-Empty'),
            'title.required' => trans('alg.The-Article-Title-Can-Not-Be-Empty'),
            // 'content.required' => trans('alg.The-Article-Content-Can-Not-Be-Empty'),
            'art-editormd-html-code.required' => trans('alg.The-Article-Content-Can-Not-Be-Empty'),
            'author.required' => trans('alg.The-Article-Author-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 平台接入基本参数校验
    public static function BookStatelessPlatformAccessOfBooks($input)
    {
        $rules = [
            'platNumber' => 'required',
            'platSecret' => 'required',
        ];
        $messages = [
            'platNumber.required' => trans('alg.The-Plate-Number-Can-Not-Be-Empty'),
            'platSecret.required' => trans('alg.The-Plate-Secret-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }
}
