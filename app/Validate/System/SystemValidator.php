<?php

namespace App\Validate\System;

use Config;
use Illuminate\Support\Facades\Validator;

class SystemValidator
{
    // 添加平台校验
    public static function platformCreated($input)
    {
        $rules = [
            'plate_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-platform-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'plate_code' => [
                'required',
                'alpha_num',
                function ($attribute, $value, $fail) {
                    $limitVal = 6;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-platform-Code-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'plate_desc' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-platform-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'plate_name.required' => trans('alg.The-platform-Name-Can-Not-Be-Empty'),
            'plate_code.required' => trans('alg.The-platform-Code-Can-Not-Be-Empty'),
            'plate_code.alpha_num' => trans('alg.The-platform-Code-Style-Error-Just-Number-And-Char'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 修改平台校验
    public static function platformModified($input)
    {
        $rules = [
            'id' => 'required',
            'plate_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-platform-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'plate_desc' => [
                function ($attribute, $value, $fail) {
                    $limitVal = 500;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-platform-Description-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ]
        ];
        $messages = [
            'id.required' => trans('alg.The-Id-Can-Not-Be-Empty'),
            'plate_name.required' => trans('alg.The-platform-Name-Can-Not-Be-Empty'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }

    // 创建用户校验
    public static function platformUserCreated($input)
    {
        $rules = [
            'user_name' => [
                'required',
                'alpha_num',
                function ($attribute, $value, $fail) {
                    $limitMinVal = 2;
                    $limitMaxVal = 20;
                    if ((strlen($value) > $limitMaxVal || strlen($value) < $limitMinVal) && $value != '') {
                        $fail(trans('alg.The-User-Name-Can-Not-More-Than-Point-Character', ['limitMinVal' => $limitMinVal, 'limitMaxVal' => $limitMaxVal]));
                    }
                },
                'unique:App\Eloqumd\System\PlateUser',
            ],
            'user_real_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limitVal = 100;
                    if (strlen($value) > $limitVal && $value != '') {
                        $fail(trans('alg.The-User-Real-Name-Can-Not-More-Than-Point-Character', ['limitVal' => $limitVal]));
                    }
                },
            ],
            'user_phone' => [
                'required',
                'regex:/^1[345789][0-9]{9}$/',
                'unique:App\Eloqumd\System\PlateUser',
            ],
            'user_email' => [
                'required',
                'email',
                'unique:App\Eloqumd\System\PlateUser',
            ]
        ];
        $messages = [
            'user_name.required' => trans('alg.The-User-Name-Can-Not-Be-Empty'),
            'user_name.alpha_num' => trans('alg.The-User-Name-Style-Error-Just-Number-And-Char'),
            'user_name.unique' => trans('alg.The-User-Name-Has-Exsits'),
            'user_phone.required' => trans('alg.The-User-Phone-Can-Not-Be-Empty'),
            'user_phone.regex' => trans('alg.The-User-Phone-Style-Error'),
            'user_phone.unique' => trans('alg.The-User-Phone-Has-Exsits'),
            'user_email.required' => trans('alg.The-User-Email-Can-Not-Be-Empty'),
            'user_email.email' => trans('alg.The-User-Email-Style-Error'),
            'user_email.unique' => trans('alg.The-User-Email-Has-Exsits'),
        ];

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorMessage = '';
            foreach ($errors->all() as $message) {
                $errorMessage .= $message . Config::get('api.broken_line');
            }
            return $errorMessage;
        }

        return true;
    }
}
