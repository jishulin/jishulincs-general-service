<?php

namespace App\Http\Requests\Traits;

trait Validations
{
    // 中文长度控制
    public function mbLenMaxLimitValidate($value, $limit)
    {
        if (strlen($value) > $limit && $value != '') {
            return false;
        }
        return true;
    }
    
    // 手机号码验证
    public function verifyPhone($value)
    {
        if (preg_match("/^1[345678]{1}\d{9}$/", $value)) {
            return true;
        }
        return false;
    }
    
    /**
     * 身份证校验
     *
     * @param   string      $id_card        身份证号码
     * @return  bool                        返回校验结果
     */
    public function validateFilterIdcard($id_card)
    {
        return  strlen($id_card) == 18 ?
                $this->idcard_checksum18($id_card) :
                $this->idcard_checksum18($this->idcard_15to18($id_card));
    }
    
    /**
     * 计算身份证校验码，根据国家标准GB 11643-1999
     *
     * @param   string      $idcard_base        身份证号码
     * @return  string                          返回身份证校验码
     */
    protected function idcard_verify_number($idcard_base)
    {
        if (strlen($idcard_base) != 17) :
            return false;
        endif;
    
        // 加权因子
        $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
    
        // 校验码对应值
        $verify_number_list = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
    
        $checksum = 0;
    
        for ($i = 0; $i < strlen($idcard_base); $i++) {
            $checksum += substr($idcard_base, $i, 1) * $factor[$i];
        }
    
        $mod = $checksum % 11;
    
        $verify_number = $verify_number_list[$mod];
    
        return $verify_number;
    }
    
    /**
     * 将15位身份证升级到18位
     *
     * @param   string      $idcard         身份证号码
     * @return  string                      处理后的身份证
     */
    protected function idcard_15to18($idcard)
    {
        if (strlen($idcard) != 15) :
            return false;
        else :
            // 如果身份证顺序码是996 997 998 999，这些是为百岁以上老人的特殊编码
            $idcard = array_search(substr($idcard, 12, 3), ['996', '997', '998', '999']) !== false ? (
                substr($idcard, 0, 6) . '18' . substr($idcard, 6, 9)
            ) : (
                substr($idcard, 0, 6) . '19' . substr($idcard, 6, 9)
            );
        endif;
    
        $idcard = $idcard . $this->idcard_verify_number($idcard);
    
        return $idcard;
    }
    
    /**
     * 18位身份证校验码有效性检查
     *
     * @param   string          $idcard         身份证号码
     * @return  bool                            返回校验结果
     */
    protected function idcard_checksum18($idcard)
    {
        if (strlen($idcard) != 18) :
            return false;
        endif;
    
        $idcard_base = substr($idcard, 0, 17);
    
        $rest = $this->idcard_verify_number($idcard_base) != strtoupper(substr($idcard, 17, 1)) ?
                false : true;
    
        return $rest;
    }
    
    // 字母和数字,下划线及破折号
    public function checkAlphadash($val)
    {
        if ($val == '') {
            return true;
        }
    
        $reg = "/^[A-Za-z0-9\-\_]+$/";
        return preg_match($reg, $val) ? true : false;
    }
}