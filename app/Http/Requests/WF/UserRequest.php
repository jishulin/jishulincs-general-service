<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UserRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['parent_id', 'name', 'realname', 'status', 'idcard', 'mobile', 'email'],
        'edt' => ['id', 'realname', 'status'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'parent_id' => 'required',
            'name' => [
                'required',
                'unique:wf_users',
                function ($attribute, $value, $fail) {
                    $limitMinVal = 2;
                    $limitMaxVal = 20;
                    if ((strlen($value) > $limitMaxVal || strlen($value) < $limitMinVal) && $value != '') {
                        $fail(trans('wf.WF-User-Name-Can-Not-More-Than-Point-Character', ['limitMinVal' => $limitMinVal, 'limitMaxVal' => $limitMaxVal]));
                    }
                },
                'alpha_num',
                'regex:/^[a-zA-Z]\w{1,21}$/'
            ],
            'realname' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-User-Realname-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'mobile' => [
                'required',
                'unique:wf_users',
                function ($attribute, $value, $fail) {
                    $this->verifyPhone($value) === true ? null :
                    $fail(trans('wf.WF-User-Mobile-Formater-Error'));
                }
            ],
            'idcard' => [
                'required',
                'unique:wf_users',
                function ($attribute, $value, $fail) {
                    $this->validateFilterIdcard($value) === true ? null :
                    $fail(trans('wf.WF-User-Idcard-Formater-Error'));
                }
            ],
            'email' => [
                'required',
                'unique:wf_users',
                'email'
            ],
            'status' => 'required|integer',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'parent_id.required' => trans('wf.WF-User-Parent-Id-Cannot-Empty'),
            'name.required' => trans('wf.WF-User-Name-Cannot-Empty'),
            'name.unique' => trans('wf.WF-User-Name-Has-Exists'),
            'name.alpha_num' => trans('wf.WF-User-Name-Format-Error'),
            'name.regex' => trans('wf.WF-User-Name-Format-Error'),
            'realname.required' => trans('wf.WF-User-Realname-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
            'mobile.required' => trans('wf.WF-User-Mobile-Cannot-Empty'),
            'mobile.unique' => trans('wf.WF-User-Mobile-Has-Exists'),
            'idcard.required' => trans('wf.WF-User-Idcard-Cannot-Empty'),
            'idcard.unique' => trans('wf.WF-User-Idcard-Has-Exists'),
            'email.required' => trans('wf.WF-User-Email-Cannot-Empty'),
            'email.email' => trans('wf.WF-User-Email-Formater-Error'),
            'email.unique' => trans('wf.WF-User-Email-Has-Exists'),
        ];
    }
}
