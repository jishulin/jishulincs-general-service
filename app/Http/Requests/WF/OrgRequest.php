<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class OrgRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['name', 'short_name', 'status', 'companyID'],
        'edt' => ['id', 'name', 'short_name', 'status'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'companyID' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Org-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'short_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Org-Short-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'status' => 'required|integer',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'companyID.required' => trans('wf.WF-Org-Parent-Id-Cannot-Empty'),
            'name.required' => trans('wf.WF-Org-Name-Cannot-Empty'),
            'short_name.required' => trans('wf.WF-Org-Short-Name-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
        ];
    }
}
