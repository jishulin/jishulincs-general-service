<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class MenuRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['title', 'status', 'type', 'authtype', 'order_num', 'show', 'component', 'path', 'icon', 'tag'],
        'edt' => ['id', 'title', 'status', 'type', 'authtype', 'order_num', 'show', 'component', 'path', 'icon', 'tag'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'title' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Menu-Title-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'status' => 'required|integer',
            'type' => [
                'required',
                Rule::in(['menu', 'button']),
            ],
            'authtype' => 'nullable|alpha_dash|required_if:type,button',
            'order_num' => 'required|integer',
            'show' => 'required|integer',
            'tag' => function ($attribute, $value, $fail) {
                $limit = 100;
                $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                $fail(trans('wf.WF-Menu-Tag-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
            },
            'component' => function ($attribute, $value, $fail) {
                $limit = 100;
                $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                $fail(trans('wf.WF-Menu-Component-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
            },
            'path' => function ($attribute, $value, $fail) {
                $limit = 100;
                $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                $fail(trans('wf.WF-Menu-Path-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
            },
            'icon' => function ($attribute, $value, $fail) {
                $limit = 100;
                $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                $fail(trans('wf.WF-Menu-Icon-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
            },
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'title.required' => trans('wf.WF-Menu-Title-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
            'type.required' => trans('wf.WF-Menu-Type-Cannot-Empty'),
            'type.in' => trans('wf.WF-Menu-Type-Has-Error-Value'),
            'authtype.required_if' => trans('wf.WF-Menu-Authtype-Cannot-Empty'),
            'authtype.alpha_dash' => trans('wf.WF-Menu-Authtype-Must-Be-Alpha-Dash'),
            'order_num.required' => trans('wf.Order-Cannot-empty'),
            'order_num.integer' => trans('wf.Order-Must-Be-Integer'),
            'show.required' => trans('wf.WF-Menu-Show-Empty'),
            'show.integer' => trans('wf.WF-Menu-Show-Formater-Error'),
        ];
    }
}
