<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public $scenes = [
        'crd' => ['name', 'tag', 'status', 'order_num', 'remark'],
        'edt' => ['id', 'name', 'tag', 'status', 'order_num', 'remark'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Role-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'tag' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->checkAlphadash($value) === true ? null :
                    $fail(trans('wf.WF-Role-Tag-Must-Be-Alpha-Dash'));
                },
                function ($attribute, $value, $fail) {
                    $limit = 60;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Role-Tag-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'remark' => [
                function ($attribute, $value, $fail) {
                    $limit = 500;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Role-Remark-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'status' => 'required|integer',
            'order_num' => 'required|integer',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'name.required' => trans('wf.WF-Role-Name-Cannot-Empty'),
            'tag.required' => trans('wf.WF-Role-Tag-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
            'order_num.required' => trans('wf.Order-Cannot-empty'),
            'order_num.integer' => trans('wf.Order-Must-Be-Integer'),
        ];
    }
}
