<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['name', 'short_name', 'code', 'tag', 'status', 'address', 'legal', 'idcard', 'mobile', 'email'],
        'edt' => ['id', 'short_name', 'code', 'status', 'address', 'legal', 'idcard', 'mobile', 'email'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'short_name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Short-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'code' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->checkAlphadash($value) === true ? null :
                    $fail(trans('wf.WF-Company-Code-Must-Be-Alpha-Dash'));
                },
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Code-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'tag' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->checkAlphadash($value) === true ? null :
                    $fail(trans('wf.WF-Company-Tag-Must-Be-Alpha-Dash'));
                },
                function ($attribute, $value, $fail) {
                    $limit = 60;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Tag-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'address' => [
                function ($attribute, $value, $fail) {
                    $limit = 500;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Address-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'status' => 'required|integer',
            'legal' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Company-Legal-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'mobile' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->verifyPhone($value) === true ? null :
                    $fail(trans('wf.WF-Company-Mobile-Formater-Error'));
                }
            ],
            'idcard' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->validateFilterIdcard($value) === true ? null :
                    $fail(trans('wf.WF-Company-Idcard-Formater-Error'));
                }
            ],
            'email' => [
                'required',
                'email'
            ],
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'name.required' => trans('wf.WF-Company-Name-Cannot-Empty'),
            'short_name.required' => trans('wf.WF-Company-Short-Name-Cannot-Empty'),
            'code.required' => trans('wf.WF-Company-Code-Cannot-Empty'),
            'tag.required' => trans('wf.WF-Company-Tag-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
            'legal.required' => trans('wf.WF-Company-Legal-Cannot-Empty'),
            'mobile.required' => trans('wf.WF-Company-Mobile-Cannot-Empty'),
            'idcard.required' => trans('wf.WF-Company-Idcard-Cannot-Empty'),
            'email.required' => trans('wf.WF-Company-Email-Cannot-Empty'),
            'email.email' => trans('wf.WF-Company-Email-Formater-Error'),
        ];
    }
}
