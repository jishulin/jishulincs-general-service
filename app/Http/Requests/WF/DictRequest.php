<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class DictRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['name', 'status', 'orders', 'parent_id'],
        'edt' => ['id', 'name', 'status', 'orders'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'parent_id' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Dict-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'status' => 'required|integer',
            'orders' => 'required|integer',
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'name.required' => trans('wf.WF-Dict-Name-Cannot-Empty'),
            'parent_id.required' => trans('wf.WF-Dict-Parent-Id-Cannot-Empty'),
            'status.required' => trans('wf.Status-empty'),
            'status.integer' => trans('wf.Status-Formater-Error'),
            'orders.required' => trans('wf.Order-Cannot-empty'),
            'orders.integer' => trans('wf.Order-Must-Be-Integer'),
        ];
    }
}
