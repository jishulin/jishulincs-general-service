<?php

namespace App\Http\Requests\WF;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;
use App\Eloqumd\Wf\WfDictType;

class DictTypeRequest extends BaseRequest
{
    public $scenes = [
        'crd' => ['name', 'code', 'remark'],
        'edt' => ['id', 'name', 'remark'],
        'rmv' => ['id']
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Dict-Type-Name-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
            ],
            'code' => [
                'required',
                'alpha',
                function ($attribute, $value, $fail) {
                    $limit = 100;
                    $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                    $fail(trans('wf.WF-Dict-Type-Code-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
                },
                function ($attribute, $value, $fail) {
                    if (WfDictType::where('code', strtoupper($value))->count()) {
                        $fail(trans('wf.WF-Dict-Type-Code-Has-Exists'));
                    }
                }
            ],
            'remark' => function ($attribute, $value, $fail) {
                $limit = 500;
                $this->mbLenMaxLimitValidate($value, $limit) === true ? null :
                $fail(trans('wf.WF-Dict-Type-Remark-Cannot-More-Than-Limit-Max-Character', ['limit' => $limit]));
            }
        ];
    }

    /**
     * 获取已定义验证规则的错误消息
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => trans('wf.ID-empty'),
            'name.required' => trans('wf.WF-Dict-Type-Name-Cannot-Empty'),
            'code.required' => trans('wf.WF-Dict-Type-Code-Cannot-Empty'),
            'code.alpha' => trans('wf.WF-Dict-Type-Must-Be-Alpha'),
        ];
    }
}
