<?php

namespace App\Http\Controllers\API\Books;

use DB;
use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use App\Eloqumd\Book\BookArticleCate;
use App\Validate\Book\BookValidator;
use App\Eloqumd\Book\BookArticleLatest;

class BookArticleCateController extends ApiController
{
    // 获取书籍选择目录
    public function getArtCatesList(Request $request)
    {
        $bookId = $request->input('book_id', 0);

        $artCates = BookArticleCate::getArticleCates($bookId, 0, 0);

        return $this->jsonReturns(10000, $this->success, $artCates, 'list', trans('alg.Get-Art-Cate-Select-Info'));
    }

    // 根据ID获取书籍目录信息
    public function getArtCateById(Request $request)
    {
        $id = $request->input('id', 0);
        $cateInfo = BookArticleCate::find($id);

        return $this->jsonReturns(10000, $this->success, $cateInfo, 'get', trans('alg.Get-Art-Cate-Info', ['name' => $cateInfo['name'], 'id' => $id]));
    }

    // 创建书籍目录
    public function artCateCreated(Request $request)
    {
        // 数据校验
        $param = $request->all();
        if (true !== $validateRes = BookValidator::artCateCreated($param)) {
            return $this->jsonReturns(99999, $validateRes, [], 'create/verror', trans('alg.Create-Art-Cate-And-Validate-Error'));
        }

        // 数据处理
        DB::beginTransaction();
        try {
            $model = new BookArticleCate;
            $model->book_id = $param['book_id'];
            $model->isdel = 1;
            $model->name = $param['name'];
            $model->parent_id = $param['parent_id'] ?? 0;
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();

            DB::commit();

            return $this->jsonReturns(
                10000,
                $this->success,
                ['id' => $model->id],
                'create',
                trans('alg.Create-Art-Cate-Info-Success', [
                    'name' => $param['name'],
                ])
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage(), [], 'create/error', trans('alg.Create-Art-Cate-System-Error'));
        }
    }

    // 修改书籍目录
    public function artCateModified(Request $request)
    {
        // 数据校验
        $param = $request->all();
        if (true !== $validateRes = BookValidator::artCateModified($param)) {
            return $this->jsonReturns(99999, $validateRes, [], 'modify/verror', trans('alg.Modify-Art-Cate-And-Validate-Error'));
        }

        // 数据处理
        DB::beginTransaction();
        try {
            $model = BookArticleCate::find($param['id']);
            $model->name = $param['name'];
            $model->save();

            DB::commit();

            return $this->jsonReturns(
                10000,
                $this->success,
                ['id' => $param['id']],
                'modify',
                trans('alg.Modify-Art-Cate-Success', ['name' => $param['name']])
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage(), [], 'modify/error', trans('alg.Modify-Art-Cate-System-Error'));
        }
    }

    // 删除书籍目录
    public function artCateDeleted(Request $request)
    {
        // 数据校验
        $id = $request->input('id', 0);

        if (!$id) {
            return $this->jsonReturns(99999, trans('alg.Illegal-Parameter', ['p' => 'id']), [], 'delete/verror', trans('alg.Delete-Art-Cate-And-Validate-Error'));
        }

        // 数据处理
        DB::beginTransaction();
        try {
            $model = BookArticleCate::find($id);
            $model->isdel = 0;
            $model->save();

            DB::commit();

            return $this->jsonReturns(
                10000,
                $this->success,
                [],
                'delete',
                trans('alg.Delete-Art-Cate-Success', ['name' => $model->name])
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage(), [], 'delete/error', trans('alg.Delete-Art-Cate-System-Error'));
        }
    }

    // 获取书籍目录树
    public function getArticleCateBookTree(Request $request)
    {
        $bookId = $request->input('book_id', '');
        $jsonData = BookArticleCate::getArticleCates($bookId, 0, 1);

        return $this->jsonReturns(10000, $this->success, $jsonData, 'get', trans('alg.Get-Art-And-Cate-Tree-Info'));
    }
}
