<?php

namespace App\Http\Controllers\API;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ApiController extends Controller
{
    // 分页限制
    public $limit = 20;
    // 用户信息
    public $apiUser = [];
    // 初始默认值
    public $success = '';
    public $error = '';
    public $unknow = '';
    public $noauth = '';

    public function __construct(Request $request)
    {
        $token = $request->header('token');
        $uid = $request->header('uid');

        $redisKey = $uid . '_' . $token;
        $userData = Redis::get($redisKey);
        $this->apiUser = unserialize($userData);

        // 初始操作
        $this->success = trans('alg.Success');
        $this->error = trans('alg.Error');
        $this->unknow = trans('alg.Unknow');
        $this->noauth = trans('alg.Noauth');
    }

    // 统一返回
    public function jsonReturns($code, $message = '', $data = [], $type = 'unknow', $logMessage = '')
    {
        return Response::json([
            'code' => $code,
            'message' => $message == '' ? $this->unknow : $message,
            'data' => $data,
            'type' => $type,
            'logMessage' => $logMessage
        ]);
    }
}
