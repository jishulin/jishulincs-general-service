<?php

namespace App\Http\Controllers\API\Stateless;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redis;
use Ramsey\Uuid\Uuid;
use App\Eloqumd\Book\BookArticleLatest;
use App\Eloqumd\Book\BookArticlePublish;
use App\Validate\Book\BookValidator;
use App\Eloqumd\System\Plate;

class BookStatelessController extends Controller
{
    // 文章解析
    public function analysisArticleShare(Request $request)
    {
        $artId = $request->input('art_id', 0);
        $_timestamp = $request->input('_timestamp', '');
        $_signastype = $request->input('_signastype', '');

        $deArtId = simple_encrypt($artId, 'D', $_timestamp);
        $deArtType = simple_encrypt($_signastype, 'D', $_timestamp);

        $artData = $deArtType == 0 ? BookArticleLatest::find($deArtId) : BookArticlePublish::find($deArtId);

        $res = [
            'code' => 10000,
            'message' => trans('alg.Success'),
            'data' => $artData
        ];

        return Response::json($res);
    }

    // 平台书籍接入口
    public function platformAccessOfBooks(Request $request)
    {
        // 简单数据校验
        $param = $request->all();
        if (true !== $validateRes = BookValidator::BookStatelessPlatformAccessOfBooks($param)) {
            return $this->jsonReturns(99999, $validateRes);
        }
        // 获取平台号，平台密钥
        $platNumber = $param['platNumber'];
        $platSecret = $param['platSecret'];
        // 校验平台是否存在
        if (!$plateData = Plate::where('plate_code', $platNumber)->where('plate_secret', $platSecret)->first()->toArray()) {
            return Response::json([
                'code' => 99999,
                'message' => trans('alg.The-Plate-Access-Failed'),
                'data' => []
            ]);
        }
        // 判断平台是否被系统禁用
        if (!$plateData['plate_status']) {
            return Response::json([
                'code' => 99999,
                'message' => trans('alg.The-Plate-Access-Has-Be-Disabled'),
                'data' => []
            ]);
        }

        // 其他加解密校验-暂不考虑
        // 其他白名单限制校验-暂不考虑

        // 创建临时访问用户-虚拟
        $minusPlateId = - $plateData['id'];
        unset($plateData['plate_secret']);
        unset($plateData['id']);

        $columns = DB::getDoctrineSchemaManager()->listTableColumns('yw_plate_users');
        $fictitiousUserData = [];
        foreach ($columns as $column) {
            $name = $column->getName();
            // switch case 替换
            switch ($name) {
                case 'id':
                    $fictitiousUserData[$name] = $minusPlateId;
                    break;
                case 'user_status':
                case 'user_type':
                    $fictitiousUserData[$name] = 1;
                    break;
                case 'user_plate':
                    $fictitiousUserData[$name] = $platNumber;
                    break;
                case 'user_pass':
                case 'user_solt':
                    break;
                case 'user_name':
                    $fictitiousUserData[$name] = Uuid::uuid1()->getHex();
                    break;
                case 'user_real_name':
                    $fictitiousUserData[$name] = trans('alg.Virtual-User');
                    break;
                default:
                    $fictitiousUserData[$name] = '';
                    break;
            }
        }

        $originUserData = array_merge($fictitiousUserData, $plateData);
        $originUserData['token'] = Uuid::uuid1()->getHex();

        // 缓存设置，默认缓存登录一周
        $redisKey = $originUserData['id'] . '_' . $originUserData['token'];

        Redis::setex($redisKey, 60 * 60 * 24 * 7, serialize($originUserData));

        $res = [
            'code' => 10000,
            'message' => trans('alg.Success'),
            'data' => $originUserData
        ];

        return Response::json($res);
    }
}
