<?php
// +----------------------------------------------------------------------
// | SignController.php
// +----------------------------------------------------------------------
// | 登录控制器
// +----------------------------------------------------------------------

namespace App\Http\Controllers\API;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Eloqumd\System\PlateUser;

class SignController extends Controller
{
    // 用户登录
    public function signIn(Request $resuest)
    {
        $userName = $resuest->input('user_name', '');
        $userPass = $resuest->input('user_pass', '');

        // 用户校验
        $checkUserData = PlateUser::userCheckOfSign($userName, $userPass);
        if (!$checkUserData['status']) {
            return Response::json(['code' => 50000, 'message' => $checkUserData['message']]);
        }

        $logMessageData = [
            'logType' => 'signin',
            'optMessage' => trans('alg.Success'),
            'logMessage' => trans('alg.The-User-Has-Signin-System-Success', ['name' => $checkUserData['data']['user_real_name']]),
        ];
        $logMessageData = array_merge($logMessageData, $checkUserData['data']);

        event(new \App\Events\BrowseLogsEvent($logMessageData));
        
        return Response::json([
            'code' => 10000,
            'message' => trans('alg.Success'),
            'data' => $checkUserData['data']
        ]);
    }
}
