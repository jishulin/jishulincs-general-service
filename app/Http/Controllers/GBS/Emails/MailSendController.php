<?php

namespace App\Http\Controllers\GBS\Emails;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Eloqumd\System\PlateUser;

class MailSendController extends Controller
{
    public static $queueName = 'emails';

    // 数据发布公开通知
    public static function bookPublishedMail($bookData)
    {
        $when = Carbon::now()->addMinutes(1);
        // 获取需要发送的信息
        $platform = $bookData['bk_plate'];
        $adminUserData = PlateUser::where('user_plate', $platform)->where('user_type', 1)->first();

        $subject = trans('alg.Book-Publish-Mail-Subject');

        if (!$adminUserData['user_email']) {
            return;
        }

        Mail::to($adminUserData['user_email'])->later($when, new \App\Mail\BookPublishedMail($bookData, $subject));
    }

    // 知乎应用-用户邮箱校验
    public static function zhihuRegisterEmailVerifyMail($data)
    {
        $when = Carbon::now()->addSeconds(20);

        $subject = trans('zhihu.User-Register-Email-Verfiy-Mail-Subject');

        Mail::to($data['email'])->later($when, new \App\Mail\ZhihuRegisterEmailVerifyMail($data, $subject));
    }

    // 知乎应用-用户邮箱找回密码
    public static function zhihuRetrievePasswordMail($data)
    {
        $when = Carbon::now()->addSeconds(20);

        $subject = trans('zhihu.User-Retrieve-Password-Mail-Subject');

        Mail::to($data['email'])->later($when, new \App\Mail\ZhihuRetrievePasswordMail($data, $subject));
    }
}
