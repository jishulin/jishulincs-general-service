<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Eloqumd\System\PlateUser;
use Illuminate\Support\Facades\Redis;
use App\Jobs\SendReminderEmail;
use Mail;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Log;
use Jishulin\WorkFlowEngine\WFEngine;
use Jishulin\WorkFlowEngine\Tools\Tools;
use Jishulin\WorkFlowEngine\Tools\Birts;
use Jishulin\RulesEngine\Lrules\Lrules;

class TestController extends Controller
{
    public function testr()
    {
        $val = '20201212 23:12:11';
        if ($val == '') return true;
        if (false === strtotime($val)) {
        	return ['err' => false, 'content' => '不是一个合法的日期格式'];
        }
        $limitVal = 'Ymd H:i:s';
        $info = date_parse_from_format($limitVal, $val);
        if (0 == $info['warning_count'] && 0 == $info['error_count']) {
        	return true;
        }

        return ['err' => false, 'content' => '指定的日期格式不正确'];


        dump(111);
        exit;

        $list = [
            'main' => [
                'zgjob02' => [
                    'NAME' => '呵呵',
                    'ZGJOB020001' => '采购部门',
                    'ZGJOB020002' => '362330199208122410',
                    'ZGJOB020003' => '0',
                ],
            ],
            'zgjob03' => [
                'ZGJOB02030001' => '12',
                'ZGJOB02030002' => '12.36',
                'ZGJOB02030003' => '12.003',
                'ZGJOB02030004' => '20.3',
                'ZGJOB02030005' => '',
            ],
            'zgjob08' => [
                ['aaa' => '111.2112', 'bbb' => '333.12'],
                ['aaa' => '222.022', 'bbb' => '444'],
            ],
        ];
        $dd = Lrules::getInstance()->satisfies(12, $list, ['id' => 13], true, false);
        dump($dd);
    }
    public function testp()
    {
        // echo 'dddd';
        // exit;
        $birts = Birts::getInstance();
        $str = "((92 + (5 + 5) * 27 / 3) - (93 - 12*3 ) / (4 + 26)) * 2^2";
        return $birts->strBC($str);

        exit;
        $tools = Tools::getInstance();
        // return $tools->workFlowCrd('aaaa', 'test');
        // return $tools->workFlowEnabled('aaaa');
        return $tools->getWorkFlowByBelong('001');
        die;
        $wf = new WFEngine;
        echo $wf->test();
    }

    public function test(Request $request)
    {
        if (Redis::get('limitkey') == 5000) {
            Log::info('活动已结束');
            echo "活动已结束";
            exit;
        }
        Redis::incr('limitkey');
        try {
            Redis::lpush('user_list', (string)Uuid::uuid1()->getHex());
            Log::info('抢购成功');
            Log::info('总长度：' . Redis::llen('user_list'));
            Log::info('开始消费');
            $job = (new SendReminderEmail())->delay(1);
            $this->dispatch($job);
        } catch (\Exception $e) {
            Redis::incr('limitkey');
            Log::info($e->getMessage());
        }

        /* $user = PlateUser::find(21);
        for ($i=0;$i<100000;$i++) {
            // if (Redis::decr('limitkey') < 0) {
            //     break;
            // }
            // $user = PlateUser::findOrFail($v['id']);
            // $job = (new SendReminderEmail($user))->delay(5);
            $job = (new SendReminderEmail($user));
            $this->dispatch($job);
        } */
    }
}
