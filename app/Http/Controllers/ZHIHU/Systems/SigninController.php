<?php

namespace App\Http\Controllers\ZHIHU\Systems;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Eloqumd\Zhihu\ZhihuUser;

class SigninController extends Controller
{
    // 用户登录
    public function signIn(Request $request)
    {
        // 支持邮箱密码及用户名密码登录
        $userName = $request->input('name', '');
        $userPass = $request->input('password', '');

        // 用户校验
        $checkUserData = ZhihuUser::userCheckOfSignIn($userName, $userPass);
        if (!$checkUserData['status']) {
            return Response::json(['code' => 50000, 'message' => $checkUserData['message']]);
        }
        
        
        // $logMessageData = [
        //     'logType' => 'signin',
        //     'optMessage' => trans('alg.Success'),
        //     'logMessage' => trans('alg.The-User-Has-Signin-System-Success', ['name' => $checkUserData['data']['user_real_name']]),
        // ];
        // $logMessageData = array_merge($logMessageData, $checkUserData['data']);
        // event(new \App\Events\BrowseLogsEvent($logMessageData));
        
        return Response::json([
            'code' => 10000,
            'message' => trans('zhihu.Sign-In-Success'),
            'data' => $checkUserData['data']
        ]);
    }

    // 用户退出
    public function signOut(Request $request)
    {
        $token = $request->header('token') ?? '';
        $uid = $request->header('uid') ?? '';

        $redisKey = 'zhihu_' . $uid . '_' . $token;

        if (Redis::exists($redisKey)) {
            Redis::del($redisKey);
        }

        return Response::json(['code' => 10000, 'message' => trans('zhihu.Sign-Out-Success')]);
    }
}
