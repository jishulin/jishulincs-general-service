<?php

namespace App\Http\Controllers\ZHIHU;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Jobs\SPUV;

class ApiController extends Controller
{
    // 分页限制
    public $limit = 20;
    // 用户信息
    public $apiUser = [];
    // 初始默认值
    public $success = '';
    public $error = '';
    public $unknow = '';
    public $noauth = '';

    public function __construct(Request $request)
    {
        $token = $request->header('token');
        $uid = $request->header('uid');

        $redisKey = 'zhihu_' . $uid . '_' . $token;
        $userData = Redis::get($redisKey);
        $this->apiUser = unserialize($userData);

        // 初始操作
        $this->success = trans('zhihu.Success');
        $this->error = trans('zhihu.Error');
        $this->unknow = trans('zhihu.Unknow');
        $this->noauth = trans('alg.Noauth-Please-Sign-In-First');

        // 网站统计
        $job = (new SPUV('zhihu'))->delay(1);
        $this->dispatch($job);
    }

    // 统一返回
    public function jsonReturns($code, $message = '', $data = [], $type = 'unknow', $logMessage = '')
    {
        return Response::json([
            'code' => $code,
            'message' => $message == '' ? $this->unknow : $message,
            'data' => $data,
            'type' => $type,
            'logMessage' => $logMessage
        ]);
    }
}
