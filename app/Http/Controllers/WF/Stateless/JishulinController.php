<?php

namespace App\Http\Controllers\WF\Stateless;

use App\Http\Controllers\Controller;

class JishulinController extends Controller
{
    public function testBeforeSave($params)
    {
        try {
            // 参数信息
            // 流程ID
            $flowId = $params['flowId'];
            // 节点ID
            $nodeId = $params['nodeId'];
            // 业务ID
            $jobId = $params['jobId'];
            // 业务数据
            $jobData = $params['data'];
            // 用户信息
            $apiUser = $params['apiUser'];

            // do something

            // 成功
            return ['error' => 0];
        } catch (\Exception $e) {
            // 自定义日志写入
            return ['error' => 1, 'message' => 'failed'];
        }
    }

    public function testAfterSave($params)
    {
        try {
            // 参数信息
            // 流程ID
            $flowId = $params['flowId'];
            // 节点ID
            $nodeId = $params['nodeId'];
            // 业务ID
            $jobId = $params['jobId'];
            // 业务数据
            $jobData = $params['data'];
            // 用户信息
            $apiUser = $params['apiUser'];

            // do something

            // 成功
            return ['error' => 0];
        } catch (\Exception $e) {
            // 自定义日志写入
            return ['error' => 1, 'message' => 'failed'];
        }
    }

    // 数据校验
    public function testValidate01($originData, $extra)
    {
        try {
            // 被校验数据
            // dump($originData);
            // 自定义传入数据
            // dump($extra);

            // 成功
            return ['error' => 0];
        } catch (\Exception $e) {
            // 返回格式，必须存在
            return ['error' => 1, 'message' => 'failed'];
        }
    }
}
