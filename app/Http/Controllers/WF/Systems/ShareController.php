<?php

namespace App\Http\Controllers\WF\Systems;

use DB;
use App\Http\Controllers\WF\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Wf\Common\Share;
use App\Eloqumd\Wf\WfDictType;
use App\Eloqumd\Wf\WfDict;
use App\Eloqumd\Wf\WfDictAccess;
use App\Eloqumd\Wf\WfCodeType;
use App\Eloqumd\Wf\WfCode;

class ShareController extends ApiController
{
    /**
     * 对象存储上传图片
     *
     * @name    putObjectChannelImages
     *
     * @return 	array                       返回结果
     */
    public function putObjectChannelImages(Request $request)
    {
        return Share::getInstance()->imageUpload($request);
    }
    
    /**
     * 获取对象存储文件
     *
     * @name    getObjectChannel
     *
     * @return 	array                       返回结果
     */
    public function getObjectChannel(Request $request)
    {
        $key = $request->input('key', '');
        $src = Share::getInstance()->getSrcByKey($key);

        return $this->jsonReturns(10000, $this->success, ['src' => $src]);
    }

    /**
     * 上传本地文件
     *
     * @name    putObjectChannelLocalFile
     *
     * @return 	array                       返回结果
     */
    public function putObjectChannelLocalFile(Request $request)
    {
        return Share::getInstance()->fileUploadStore($request);
    }

    /**
     * 获取公司字典
     *
     * @name    getComDicts
     *
     * @return 	array                       返回结果
     */
    public function getComDicts(Request $request)
    {
        $whereIn = [];
        if ($this->apiUser['user_type'] != 0) {
            $dictAuths = WfDictAccess::where('company_id', $this->apiUser['company_id'])->value('dicts');
            if (!$dictAuths) {
                return [];
            }
            $whereIn = ['id', explode(',', $dictAuths)];
        }
        
        $jsonData = WfDictType::select('name as label', 'code as value')
            ->where(function ($query) use ($whereIn) {
                if ($whereIn) $query->whereIn($whereIn[0], $whereIn[1]);
            })
            ->orderBy('created_at', 'asc')
            ->get()
            ->toArray();

        return $jsonData;
    }

    /**
     * 获取系统字典代码
     *
     * @name    getSystemDicts
     *
     * @return 	array                       返回结果
     */
    public function getSystemDicts(Request $request)
    {
        $jsonData = WfCodeType::select('name as label', 'code as value')
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->get()
            ->toArray();

        return $jsonData;
    }

    /**
     * 通过代码KEY获取系统字典信息
     *
     * @name    getSystemDictByKey
     *
     * @return 	array                       返回结果
     */
    public function getSystemDictByKey(Request $request)
    {
        $key = $request->input('key', '');
        $jsonData = WfCode::select('name as label', 'vals as value')
            ->where('code', $key)
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->get()
            ->toArray();

        return $jsonData;
    }
    
    /**
     * 通过字典KEY获取公司字典信息
     *
     * @name    getComDictByKey
     *
     * @return 	array                       返回结果
     */
    public function getComDictByKey(Request $request)
    {
        $key = $request->input('key', '');
        $dictId = WfDictType::where('code', $key)->value('id');
        $jsonData = WfDict::select('name as label', 'name as value')
            ->where('parent_id', $dictId)
            ->where('company_id', $this->apiUser['company_id'])
            ->where('status', 1)
            ->orderBy('orders', 'asc')
            ->get()
            ->toArray();
    
        return $jsonData;
    }
}
