<?php

namespace App\Http\Controllers\WF\Systems;

use DB;
use Carbon\Carbon;
use App\Http\Controllers\WF\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Wf\Common\Share;
use App\Eloqumd\Wf\WfMenu;
use App\Http\Requests\WF\MenuRequest;

class MenuController extends ApiController
{
    /**
     * 获取授权菜单
     *
     * @name    getAuthMenus
     *
     * @return 	array                       返回结果
     */
    public function getAuthMenus()
    {
        $menus = Share::getMenuSub($this->apiUser, 1);
        $buttons = Share::getMenuSub($this->apiUser, 0);
        $jsonData = [
            'menus' => $menus,
            'authbtns' => $buttons
        ];

        return $this->jsonReturns(10000, $this->success, $jsonData);
    }

    /**
     * 获取菜单树
     *
     * @name    getMenuListTree
     *
     * @return 	array                       返回结果
     */
    public function getMenuListTree()
    {
        $fields = [
            'id', 'title', 'title as label', 'status', 'order_num', 'parent_id', 'show',
            'path', 'component', 'icon', 'type', 'remark', 'authtype', 'tag'
        ];

        $treeArray = WfMenu::select($fields)
            ->orderBy('order_num', 'asc')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();
        // dump($treeArray);exit;
        $tree = Share::arrayToTree($treeArray, 'id', 'parent_id', 'children', 0);

        return $this->jsonReturns(10000, $this->success, $tree);
    }

    /**
     * 添加菜单
     *
     * @name    menuCrd
     *
     * @return 	array                       返回结果
     */
    public function menuCrd(MenuRequest $request)
    {
        $request->validate('crd');
        $param = $request->all();

        DB::beginTransaction();
        try {
            $model = new WfMenu;
            $model->title = $param['title'];
            $model->parent_id = empty($param['parent_id']) ? 0 : $param['parent_id'];
            $model->status = $param['status'] ?? 0;
            $model->order_num = $param['order_num'] ?? 10;
            $model->show = $param['show'] ?? 0;
            $model->path = $param['path'] ?? '';
            $model->component = $param['component'] ?? '';
            $model->icon = $param['icon'] ?? '';
            $model->type = $param['type'] ?? '';
            $model->remark = $param['remark'] ?? '';
            $model->authtype = $param['authtype'] ?? '';
            $model->tag = $param['tag'] ?? '';
            $model->created_at = Carbon::now();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }

    /**
     * 修改菜单
     *
     * @name    menuEdt
     *
     * @return 	array                       返回结果
     */
    public function menuEdt(MenuRequest $request)
    {
        $request->validate('edt');
        $param = $request->all();

        DB::beginTransaction();
        try {
            $model = WfMenu::find($param['id']);
            $model->title = $param['title'];
            $model->status = $param['status'] ?? 0;
            $model->order_num = $param['order_num'] ?? 10;
            $model->show = $param['show'] ?? 0;
            $model->path = $param['path'] ?? '';
            $model->component = $param['component'] ?? '';
            $model->icon = $param['icon'] ?? '';
            $model->type = $param['type'] ?? '';
            $model->remark = $param['remark'] ?? '';
            $model->authtype = $param['authtype'] ?? '';
            $model->tag = $param['tag'] ?? '';
            $model->updated_at = Carbon::now();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }

    /**
     * 删除菜单
     *
     * @name    menuRmv
     *
     * @return 	array                       返回结果
     */
    public function menuRmv(MenuRequest $request)
    {
        $request->validate('rmv');
        $id = $request->input('id', 0);

        DB::beginTransaction();
        try {
            //校验是否存在子菜单
            if (WfMenu::where('parent_id', $id)->count()) {
                return $this->jsonReturns(60000, trans('wf.WF-The-Menu-Has-Children-Cannot-Remove'));
            }

            $model = WfMenu::find($id);
            $model->delete();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }
}
