<?php

namespace App\Http\Controllers\WF\Systems;

use DB;
use Config;
use Carbon\Carbon;
use App\Http\Controllers\WF\ApiController;
use Illuminate\Http\Request;
use App\Eloqumd\Wf\WfRole;
use App\Http\Requests\WF\RoleRequest;

class RoleController extends ApiController
{
    /**
     * 获取角色字典
     *
     * @name    getRoleDicts
     *
     * @return 	array                       返回结果
     */
    public function getRoleDicts(Request $request)
    {
        $companyId = $request->input('companyId', 0);
        $jsonData = WfRole::where('company_id', $companyId)
            ->orderBy('order_num', 'asc')
            ->get()
            ->toArray();

        return $this->jsonReturns(10000, $this->success, $jsonData);
    }

    /**
     * 获取角色列表
     *
     * @name    getRoleList
     *
     * @return 	array                       返回结果
     */
    public function getRoleList(Request $request)
    {
        $page = (int)$request->input('page', 1);
        $limit = $request->input('limit', $this->limit);
        $offest = ($page - 1) * $limit;
        // 关键字
        $keyword = $request->input('keyword', '');
        $orWhere = [];
        $where[] = ['company_id', '=', $this->apiUser['company_id'], 'AND'];
        $where[] = ['isdel', '=', 0, 'AND'];
        if ($keyword) {
            $orWhere[] = ['name', 'like', "%{$keyword}%", 'AND'];
            $orWhere[] = ['tag', 'like', "%{$keyword}%", 'OR'];
        }

        $totalCount = WfRole::where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })->count();
        $listData = WfRole::where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })
            ->offset($offest)
            ->limit($limit)
            ->orderBy('order_num', 'asc')
            ->get()
            ->toArray();

        $jsonData = [
            'list' => $listData,
            'count' => $totalCount
        ];

        return $this->jsonReturns(10000, $this->success, $jsonData);
    }

    /**
     * 角色添加
     *
     * @name    roleCrd
     *
     * @return 	array                       返回结果
     */
    public function roleCrd(RoleRequest $request)
    {
        $request->validate('crd');
        $param = $request->all();

        // 唯一校验
        if (WfRole::where('tag', strtoupper($param['tag']))->count()) {
            return $this->jsonReturns(50000, trans('wf.WF-Role-Tag-Has-Exists'));
        }

        DB::beginTransaction();
        try {
            $model = new WfRole;
            $model->name = $param['name'];
            $model->tag = strtoupper($param['tag']);
            $model->status = $param['status'];
            $model->order_num = $param['order_num'] ?? 10;
            $model->remark = $param['remark'] ?? '';
            $model->company_id = $this->apiUser['company_id'];
            $model->created_at = Carbon::now();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }

    /**
     * 角色详情获取
     *
     * @name    getRoleById
     *
     * @return 	array                       返回结果
     */
    public function getRoleById(Request $request)
    {
        $id = $request->input('id', 0);
        $data = WfRole::find($id)->toArray();

        return $this->jsonReturns(10000, $this->success, $data);
    }

    /**
     * 角色删除
     *
     * @name    roleRmv
     *
     * @return 	array                       返回结果
     */
    public function roleRmv(Request $request)
    {
        $id = $request->input('id');

        DB::beginTransaction();
        try {
            $model = WfRole::find($id);
            $model->isdel = 1;
            $model->deleted_at = time();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }

    /**
     * 角色启用禁用
     *
     * @name    roleSts
     *
     * @return 	array                       返回结果
     */
    public function roleSts(Request $request)
    {
        $id = $request->input('id');
        $status = $request->input('status', 0);

        DB::beginTransaction();
        try {
            $model = WfRole::find($id);
            $model->status = $status;
            $model->updated_at = Carbon::now();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }

    /**
     * 角色编辑
     *
     * @name    roleEdt
     *
     * @return 	array                       返回结果
     */
    public function roleEdt(RoleRequest $request)
    {
        $request->validate('edt');
        $param = $request->all();

        if (WfRole::where('tag', strtoupper($param['tag']))->where('id', '<>', $param['id'])->count()) {
            return $this->jsonReturns(50000, trans('wf.WF-Role-Tag-Has-Exists'));
        }

        DB::beginTransaction();
        try {
            $model = WfRole::find($param['id']);
            $model->name = $param['name'];
            $model->tag = strtoupper($param['tag']);
            $model->status = $param['status'];
            $model->order_num = $param['order_num'] ?? 10;
            $model->remark = $param['remark'] ?? '';
            $model->updated_at = Carbon::now();
            $model->save();

            DB::commit();

            return $this->jsonReturns(10000, $this->success);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->jsonReturns(50000, $e->getMessage());
        }
    }
}
