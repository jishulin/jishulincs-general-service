<?php

namespace App\Http\Controllers\WF\Systems;

use Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Eloqumd\Wf\WfUser;

class SigninController extends Controller
{
    /**
     * 用户登录
     *
     * @name    signIn
     *
     * @return 	array                       返回结果
     */
    public function signIn(Request $request)
    {
        // 支持邮箱密码及用户名密码登录
        $userName = $request->input('name', '');
        $userPass = $request->input('password', '');

        // 用户校验
        $checkUserData = WfUser::userCheckOfSignIn($userName, $userPass);
        if (!$checkUserData['status']) {
            return Response::json(['code' => 50000, 'message' => $checkUserData['message']]);
        }

        return Response::json([
            'code' => 10000,
            'message' => trans('wf.Sign-In-Success'),
            'data' => ['userInfo' => $checkUserData['data']]
        ]);
    }

    /**
     * 用户退出
     *
     * @name    signOut
     *
     * @return 	array                       返回结果
     */
    public function signOut(Request $request)
    {
        $token = $request->header('token') ?? '';
        $uid = $request->header('uid') ?? '';

        $redisKey = 'wf_' . $uid . '_' . $token;

        if (Redis::exists($redisKey)) {
            Redis::del($redisKey);
        }

        return Response::json(['code' => 10000, 'message' => trans('wf.Sign-Out-Success')]);
    }
}
