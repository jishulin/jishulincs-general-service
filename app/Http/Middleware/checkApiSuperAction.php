<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redis;
use App\Eloqumd\System\PlateUser;

class checkApiSuperAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uid = $request->header('uid') ?? '';

        $noauth = ['code' => 50000, 'message' => trans('alg.Noauth')];
        $originUserData = PlateUser::find($uid)->toArray();
        if ($originUserData['user_type'] != 0) {
            return Response::json($noauth);
        }

        return $next($request);
    }
}
