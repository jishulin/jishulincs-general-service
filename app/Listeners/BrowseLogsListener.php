<?php

namespace App\Listeners;

use App\Events\BrowseLogsEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\BrowseLogsJobs;

class BrowseLogsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BrowseLogsEvent  $event
     * @return void
     */
    public function handle(BrowseLogsEvent $event)
    {
        BrowseLogsJobs::dispatch($event->logs);
    }
}
