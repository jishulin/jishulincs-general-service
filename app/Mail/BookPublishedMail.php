<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookPublishedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bookData = null;
    public $subject = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookData, $subject = '')
    {
        $this->bookData = $bookData;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view('emails.books.published');
    }
}
