<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ZhihuRetrievePasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data = null;
    public $subject = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $subject = '')
    {
        $this->data = $data;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view('emails.zhihu.retrievepwd');
    }
}
