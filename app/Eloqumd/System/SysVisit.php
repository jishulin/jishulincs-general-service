<?php

namespace App\Eloqumd\System;

use Illuminate\Database\Eloquent\Model;

class SysVisit extends Model
{
    public $timestamps = false;
}
