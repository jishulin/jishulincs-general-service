<?php

namespace App\Eloqumd\System;

use Illuminate\Database\Eloquent\Model;

class BrowseLog extends Model
{
    public $timestamps = false;
}
