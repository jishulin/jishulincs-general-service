<?php

namespace App\Eloqumd\System;

use Illuminate\Database\Eloquent\Model;

class Plate extends Model
{
    public $timestamps = false;
}
