<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfDict extends Model
{
    public $timestamps = false;
}
