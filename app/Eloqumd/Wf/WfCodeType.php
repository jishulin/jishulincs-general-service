<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfCodeType extends Model
{
    public $timestamps = false;
}
