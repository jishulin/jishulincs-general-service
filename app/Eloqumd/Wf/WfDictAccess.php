<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfDictAccess extends Model
{
    public $timestamps = false;
}
