<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfMenu extends Model
{
    public $timestamps = false;
}
