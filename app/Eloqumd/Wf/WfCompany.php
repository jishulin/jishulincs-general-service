<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfCompany extends Model
{
    protected $table = 'wf_companys';
    public $timestamps = false;
}
