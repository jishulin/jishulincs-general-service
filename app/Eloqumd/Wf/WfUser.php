<?php

namespace App\Eloqumd\Wf;

use Config;
use Hash;
use App\Casts\Json;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;

class WfUser extends Model
{
    public $timestamps = false;

    public static function userCheckOfSignIn($name, $pass)
    {
        // $queryField = 'email';
        // 判断登录账号-邮箱or用户名
        // if (strpos($name, '@') === false) {
        $queryField = 'name';
        // }

        if (!$originUserData = self::where($queryField, $name)->first()) {
            return ['status' => false, 'message' => trans('wf.The-User-Not-Exist')];
        }

        if (Hash::check($pass, $originUserData['password'], ['solt' => $originUserData['solt']])) {
            // 判断用户是否被禁用
            if (!$originUserData['status']) {
                return ['status' => false, 'message' => trans('wf.The-User-Has-Disabled')];
            }

            // 密码加盐加密处理
            $userPwdCache = simple_encrypt($originUserData['password'], 'E', Config::get('api.pwd_cache'));
            // 剔除安全隐患字段
            $originUserData = Arr::except($originUserData, ['password', 'solt']);
            // 添加token字段
            $originUserData['token'] = Uuid::uuid1()->getHex();
            $originUserData = $originUserData->toArray();

            // 默认加入系统管理员ID及名称
            $admin = self::where('user_type', 0)->first();
            $originUserData['systemId'] = $admin['id'];
            $originUserData['systemName'] = $admin['nickname'];

            // 缓存设置，默认缓存登录一周
            $redisKey = 'wf_' . $originUserData['id'] . '_' . $originUserData['token'];
            if ($loginCacheTtl = Config::get('api.login_cache')) {
                Redis::setex($redisKey, $loginCacheTtl, serialize($originUserData));
                Redis::setex($redisKey . '_ps', $loginCacheTtl, $userPwdCache);
            } else {
                Redis::set($redisKey, serialize($originUserData));
                Redis::set($redisKey . '_ps', $userPwdCache);
            }

            return ['status' => true, 'data' => $originUserData];
        }

        return ['status' => false, 'message' => trans('wf.Username-Or-Userpass-Error')];
    }

    // public static function changePwd($input, $userId, $token)
    // {
    //     $originpwd = $input['originpwd'] ?? '';
    //     if (!$originpwd) {
    //         return ['status' => false, 'message' => trans('zhihu.Origin-Pwd-Can-Not-Be-Empty')];
    //     }
    //     $model = self::find($userId);

    //     if (!Hash::check($originpwd, $model['password'], ['solt' => $model['solt']])) {
    //         return ['status' => false, 'message' => trans('zhihu.Origin-Pwd-Can-Not-Correct')];
    //     }
    //     if (true !== $validateRes = ZhihuValidator::retrievePasswordLinkVerifyActiveAndReset($input)) {
    //         return ['status' => false, 'message' => $validateRes];
    //     }

    //     // 修改密码开始
    //     $password = $input['password'];
    //     // 删除缓存
    //     $redisKey = 'zhihu_' . $userId . '_' . $token;
    //     if (Redis::exists($redisKey)) {
    //         Redis::del($redisKey);
    //         Redis::del($redisKey . '_ps');
    //     }

    //     $solt = Uuid::uuid1()->getHex();
    //     $model->solt = $solt;
    //     $model->password = Hash::make($password, ['solt' => $solt]);
    //     $model->updated_at = Carbon::now();
    //     $model->save();

    //     return ['status' => true];
    // }
}
