<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfDictType extends Model
{
    public $timestamps = false;
}
