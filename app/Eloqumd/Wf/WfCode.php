<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfCode extends Model
{
    public $timestamps = false;
}
