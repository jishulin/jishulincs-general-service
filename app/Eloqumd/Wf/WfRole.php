<?php

namespace App\Eloqumd\Wf;

use Illuminate\Database\Eloquent\Model;

class WfRole extends Model
{
    public $timestamps = false;
}
