<?php

namespace App\Eloqumd\Book;

use Illuminate\Database\Eloquent\Model;

class BookArticlePublish extends Model
{
    public $timestamps = false;
}
