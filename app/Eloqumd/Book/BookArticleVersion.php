<?php

namespace App\Eloqumd\Book;

use Illuminate\Database\Eloquent\Model;

class BookArticleVersion extends Model
{
    public $timestamps = false;
}
