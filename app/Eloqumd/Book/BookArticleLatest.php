<?php

namespace App\Eloqumd\Book;

use Illuminate\Database\Eloquent\Model;

class BookArticleLatest extends Model
{
    public $timestamps = false;
}
