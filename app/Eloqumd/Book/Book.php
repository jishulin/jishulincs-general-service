<?php

namespace App\Eloqumd\Book;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $timestamps = false;
}
