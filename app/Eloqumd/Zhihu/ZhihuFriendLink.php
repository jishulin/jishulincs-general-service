<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuFriendLink extends Model
{
    public $timestamps = false;
}
