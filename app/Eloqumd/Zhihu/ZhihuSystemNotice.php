<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuSystemNotice extends Model
{
    public $timestamps = false;
}
