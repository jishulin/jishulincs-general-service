<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuTopic extends Model
{
    public $timestamps = false;
}
