<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuCollection extends Model
{
    public $timestamps = false;
}
