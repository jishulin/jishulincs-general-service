<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuQuestion extends Model
{
    public $timestamps = false;
}
