<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuSystemCover extends Model
{
    public $timestamps = false;
}
