<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuFollower extends Model
{
    public $timestamps = false;
}
