<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuVoteAnswer extends Model
{
    public $timestamps = false;
}
