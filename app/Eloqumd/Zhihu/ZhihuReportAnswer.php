<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuReportAnswer extends Model
{
    public $timestamps = false;
}
