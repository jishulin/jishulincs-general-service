<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuSystemPublishMessage extends Model
{
    public $timestamps = false;
}
