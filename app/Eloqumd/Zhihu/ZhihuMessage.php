<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuMessage extends Model
{
    public $timestamps = false;
}
