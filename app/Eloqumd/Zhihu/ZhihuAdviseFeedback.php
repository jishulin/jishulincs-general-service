<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuAdviseFeedback extends Model
{
    protected $table = 'zhihu_advise_feedbacks';
    public $timestamps = false;
}
