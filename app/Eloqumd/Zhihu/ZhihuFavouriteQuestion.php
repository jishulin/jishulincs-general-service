<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuFavouriteQuestion extends Model
{
    public $timestamps = false;
}
