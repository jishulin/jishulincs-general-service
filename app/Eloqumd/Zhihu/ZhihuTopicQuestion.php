<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuTopicQuestion extends Model
{
    public $timestamps = false;
}
