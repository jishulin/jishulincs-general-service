<?php

namespace App\Eloqumd\Zhihu;

use Illuminate\Database\Eloquent\Model;

class ZhihuReportQuestion extends Model
{
    public $timestamps = false;
}
