<?php

namespace App\Jobs;

use DB;
use Exception;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Eloqumd\System\BrowseLog;

class BrowseLogsJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    public $timeout = 120;

    public $logsData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($logsData)
    {
        $this->logsData = $logsData;
        // dump($this->logsData);exit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $ip = request()->getClientIp();
            // 本地IP跳过记录
            // 数据记录
            $logDataSave = $this->logDataDealSet();
            DB::table('browse_logs')->insert($logDataSave);
        } catch (Exception $exception) {
            Log::debug($exception->getMessage());
            $this->failed();
        }
    }

    public function logDataDealSet()
    {
        $date = date('Y-m-d H:i:s');
        $jsonData = [];
        $type = $this->logsData['logType'] ?? 'unknow';

        if ($type == 'unknow') {
            return;
        }

        $typeArr = explode('/', $type);

        $jsonData['log_k'] = $this->logsData['log_k'] ?? '';
        $jsonData['log_type'] = $typeArr[0];
        $jsonData['log_type_origin'] = $type;
        $jsonData['log_type_trans'] = trans('sys.' . ucfirst($jsonData['log_type']) . '-Trans');
        $jsonData['log_user_id'] = $this->logsData['id'] ?? 0;
        $jsonData['log_user_name'] = $this->logsData['user_real_name'] ?? '';
        $jsonData['log_plate_id'] = $this->logsData['plate_id'] ?? 0;
        $jsonData['log_plate_name'] = $this->logsData['plate_name'] ?? 0;
        // $actionData = app('request')->route()->getAction();
        $jsonData['log_app'] = $this->logsData['log_app'] ?? '';
        $jsonData['log_controller'] = $this->logsData['log_controller'] ?? '';
        $jsonData['log_action'] = $this->logsData['log_action'] ?? '';
        $jsonData['log_ip'] = ip2long(request()->getClientIp());
        $jsonData['log_created_at'] = $date;
        $jsonData['log_opt_message'] = $this->logsData['optMessage'] ?? '';
        $jsonData['log_red_message'] = $this->logsData['logMessage'] ?? '';

        $sysMessage = '';

        $sysMessage .= '平台[' . $jsonData['log_plate_name'] . ']用户[' . $jsonData['log_user_name'] . ']在[' . $date . ']对系统进行[' . $jsonData['log_type_trans'] . ']操作!';
        $sysMessage .= '操作结果：' . $jsonData['log_opt_message'] . '!';
        if ($jsonData['log_red_message'] != '') {
            $sysMessage .= '详情信息：' . $jsonData['log_red_message'] . '!';
        }

        $jsonData['log_sys_message'] = $sysMessage;

        return $jsonData;
    }

    public function failed(Exception $exception)
    {
        DB::table('job_failed_detail_records')->insert([
            'tags' => 'logs',
            'content' => '',
            'exception' => $exception->getMessage()
        ]);
    }
}
