<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\Middleware\RateLimited;
use App\Eloqumd\System\PlateUser;
use Illuminate\Contracts\Mail\Mailer;
use DB;
use Exception;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

class SendReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    public $tries = 5;
    public $maxExceptions = 3;
    /**
     * 获取任务应该通过的中间件
     *
     * @return array
     */
    // public function middleware()
    // {
    //     return [new RateLimited];
    // }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $num = Redis::lpop('user_list');
        // $mailer->send('emails.reminder',['user' => $user],function($message) use ($user){
        //     $message->to($user->user_email)->subject('李建林对列测试66666');
        // });
        Log::info('消费信息' . $num);
        DB::table('job_failed_detail_records')->insert([
            'tags' => $num,
            'content' => '消费成功',
            'exception' => '11111'
        ]);
    }

    /**
     * 任务未能处理
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        DB::table('job_failed_detail_records')->insert([
            'tags' => 'error',
            'content' => '消费失败',
            'exception' => $exception->getMessage()
        ]);

    }
}
