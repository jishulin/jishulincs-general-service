<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReportNews implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $title;
    public $messageType;
    public $data = [];
    public $type;
    public $position;
    public $duration;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message, $title = null, $messageType = null, $position = null, $duration = null, $type = null, $data = null)
    {
        $this->message = $message;
        $this->title = empty($title) ? trans('sys.Sys-System-Message-Title') : $title;
        $this->messageType = empty($messageType) ? 'info' : $messageType;
        $this->position = empty($position) ? 'bottom-right' : $position;
        $this->duration = empty($duration) ? 0 : $duration * 1000;
        $this->type = empty($type) ? 'default' : $type;
        $this->data = empty($data) ? [] : $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('sysnews');
    }

    public function broadcastWith()
    {
        return [
            'title' => $this->title,
            'message' => $this->message,
            'messageType' => $this->messageType,
            'position' => $this->position,
            'duration' => (int)$this->duration,
            'type' => $this->type,
            'data' => $this->data,
        ];
    }
}
