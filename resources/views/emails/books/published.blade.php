<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $bookData->bk_name }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    您的平台书籍【{{ $bookData->bk_name }}】已经被系统公开
                </div>
                <div>
                    <h4>书籍信息</h4>
                    <p>名称：{{ $bookData->bk_name }}</p>
                    <p>创建时间：{{ $bookData->bk_created_at }}</p>
                    <p>简介：{{ $bookData->bk_desc }}</p>
                    <p>封面</p>
                    <img src="{{ $message->embed(get_domain(request()->server()) . $bookData->bk_img) }}">
                </div>
            </div>
        </div>
    </body>
</html>
