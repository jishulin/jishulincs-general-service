<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $subject }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <h3>您在技术林应用操作找回密码，请输入新的密码进行提交，有效10分钟</h3>
                </div>
                <div>
                    <h4>找回密码</h4>
                    <input type="hidden" name="_uuid" value="{{ $data['_uuid'] }}" />
                    <input type="hidden" name="_timestamp" value="{{ $data['_timestamp'] }}" />
                    新密码：<input type="password" name="password" value="" /><br/>
                    确认密码：<input type="password" name="password_confirmation" value="" /><br/>
                    <button onclick="ajax_submit()">确认提交</button>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            //调用ajax函数
            function ajax_submit()
            {
                ajax({
                    url: "{{ $data['retrieve_pwd_url'] }}",
                    type: 'POST',
                    dataType:'json',
                    data:{name:"马各马它",age:18},
                    success:function(response,xml){
                        console.log(response)
                    },
                    error:function(status){
                        //失败后执行的代码
                    }
                });
            }

            //创建ajax函数
            function ajax(options){
                options=options||{};
                optoins.type=(options.type||'GET').toUpperCase();
                options.dataType=options.dataType||'json';
                params=formatParams(options.data);

                //创建-第一步
                var xhr;
                //非IE6
                if(window.XMLHttpRequest){
                    xhr=new XMLHttpRequest();
                }else{
                    //ie6及其以下版本浏览器
                    xhr=ActiveXObject('Microsoft.XMLHTTP');
                }

                //接收-第三步
                xhr.onreadystatechange=function(){
                    if(xhr.readyState==4){
                        var status=xhr.status;
                        if(status>=200&&status<300){
                            options.success&&options.success(xhr.responseText,xhr.responseXML);
                        }else{
                            options.error&&options.error(status);
                        }
                    }
                }

                //连接和发送-第二步
                if(options.type=='GET'){
                    xhr.open('GET',options.url+'?'+params,true);
                    xhr.send(null);
                }else if(options.type=='POST'){
                    xhr.open('POST',options.url,true);
                    //设置表单提交时的内容类型
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.send(params);
                }
            }

            //格式化参数
            function formatParams(data){
                var arr=[];
                for(var name in data){
                    arr.push(encodeURIComponent(name)+'='+encodeURIComponent(data[name]));
                }
                arr.push(('v='Math.random()).replace('.',''));
                return arr.join('&');
            }
        </script>
    </body>
</html>
