<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $subject }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    您在技术林应用上注册了用户信息，请点击下面链接进行激活，有效15分钟
                </div>
                <div>
                    <p>激活地址：<a href="{{ $data['email_verify_url'] }}">{{ $data['email_verify_url'] }}</a></p>
                </div>
            </div>
        </div>
    </body>
</html>
