<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="@{{ csrf_token }}">
    <title>技术林</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" href="https://at.alicdn.com/t/font_1822955_wjae58nz3r8.css"></link>
    <!-- <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script> -->
</head>
<body style="background: url({{ $coverimg }});">
<div id="app">

</div>
<script type="text/javascript" src="/js/app.js"></script>
<style>
    body {
        margin: 0rem;
        padding: 0rem;
        font-family: -apple-system, BlinkMacSystemFont, Helvetica Neue, PingFang SC, Microsoft YaHei, Source Han Sans SC, Noto Sans CJK SC, WenQuanYi Micro Hei, sans-serif;
        font-size: 0.9375rem;
        color: #1a1a1a;
        background: #f6f6f6;
        /* background: url(../../vendor/swiftmailer/swiftmailer/tests/_samples/files/swiftmailer.png); */
        -webkit-tap-highlight-color: rgba(26, 26, 26, 0);
    }
    a {
        text-decoration: none;
    }

    .mian-aside {
        padding-left: 0rem;
        padding-right: 0rem;
        font-size: 14px;
    }
    .aside-div {
        margin-bottom: 10px;
    }
    .el-header {
        position: relative;
        z-index: 100;
        min-width: 64.5rem;
        overflow: hidden;
        background: #fff;
        -webkit-box-shadow: 0 1px 3px rgba(26, 26, 26, .1);
        box-shadow: 0 1px 3px rgba(26, 26, 26, .1);
        line-height: 3.25rem;
        height: 3.25rem;
        width: 100%;
    }

    .space {
        width: 20px;
        display: inline-block;
    }

    .cursor-header {
        cursor: pointer;
    }

    .container-main {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        width: 1000px;
        padding: 0 16px;
        margin: 10px auto;
        font-size: 16px;
        background: #f7f9fd;
        border-radius: 4px;
        box-sizing: border-box;
        min-height: 840px;
        position: absolute;
        top:60px;
        left: 17%;
    }

    .el-main {
        display: block;
        flex: 1;
        flex-basis: auto;
        overflow: hidden;
        box-sizing: border-box;
        padding: 10px;
        height: 100%;
    }

    .config-manage-form {
        padding-top: 10px;
        background: #fff;
    }
    .el-switch__label * {
        line-height: 1;
        font-size: 13px;
        display: inline-block;
    }
    .el-badge__content.is-fixed{
        top:10px;
    }
    .avue-article__title {
        margin-bottom: 15px;
        font-size: 20px;
        line-height: 20px;
        font-weight: 400;
    }
    .question-box{
        padding-top: 10px;
        padding-bottom: 20px;
        /* border-bottom: #FDFDFE 1px solid; */
    }
    .el-divider--horizontal {
        display: block;
        height: 1px;
        width: 100%;
        margin: 10px 0;
    }
    .el-divider--vertical {
        display: inline-block;
        width: 1px;
        height: 1em;
        margin: 0px 0px;
        vertical-align: middle;
        position: relative;
    }
</style>
</body>
</html>
