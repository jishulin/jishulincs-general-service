INSERT INTO `yw_zhihu_config_sources` (`status`, `is_removed`, `key_name`, `key_title`, `key_field`, `removed_at`, `created_at`, `updated_at`, `is_system`) VALUES ('1', '0', 'dirty', '敏感词过滤', 'dirtyName,dirtyTimes', NULL, '2020-05-23 07:34:55', '2020-06-14 09:07:47', '1');
INSERT INTO `yw_zhihu_config_sources` (`status`, `is_removed`, `key_name`, `key_title`, `key_field`, `removed_at`, `created_at`, `updated_at`, `is_system`) VALUES ('1', '0', 'personal', '个人设置', 'mobile,intro,labels', NULL, '2020-05-23 07:36:34', '2020-05-30 09:32:05', '0');
INSERT INTO `yw_zhihu_config_sources` (`status`, `is_removed`, `key_name`, `key_title`, `key_field`, `removed_at`, `created_at`, `updated_at`, `is_system`) VALUES ('1', '0', 'advs', '广告限制配置', 'vip,general,welfare', NULL, '2020-05-26 21:35:24', NULL, '1');

INSERT INTO `yw_zhihu_system_configs` (`k`, `v`, `created_at`, `updated_at`) VALUES ('dirty', 'a:2:{s:9:\"dirtyName\";s:20:\"约炮,淫荡,援交\";s:10:\"dirtyTimes\";s:2:\"20\";}', '2020-05-20 23:21:26', '2020-05-23 07:38:34');
INSERT INTO `yw_zhihu_system_configs` (`k`, `v`, `created_at`, `updated_at`) VALUES ('personal', 'a:3:{s:6:\"mobile\";s:9:\"手机号\";s:5:\"intro\";s:12:\"个人简介\";s:6:\"labels\";s:12:\"我的标签\";}', '2020-05-23 07:39:37', NULL);
INSERT INTO `yw_zhihu_system_configs` (`k`, `v`, `created_at`, `updated_at`) VALUES ('advs', 'a:3:{s:3:\"vip\";s:1:\"6\";s:7:\"general\";s:2:\"10\";s:7:\"welfare\";s:1:\"5\";}', '2020-05-26 21:36:33', NULL);

INSERT INTO `yw_sys_visits` (`puv`, `pv`, `uv`) VALUES ('zhihu', '0', '0');
INSERT INTO `yw_sys_visits` (`puv`, `pv`, `uv`) VALUES ('error', '0', '0');

INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('cream', '设为精华', '10', '1', '2020-06-27 10:43:45', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('blog', '创建博文', '5', '1', '2020-06-27 11:43:05', '2020-06-27 11:51:11');
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('adopt', '被采纳', '5', '1', '2020-06-27 11:56:54', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('discuss', '发起讨论', '2', '1', '2020-06-27 11:57:37', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('link', '分享链接', '1', '1', '2020-06-27 11:57:52', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('question', '提问', '2', '1', '2020-06-27 11:58:07', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('report', '举报成功', '1', '1', '2020-06-27 11:58:34', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('bereport', '被举报成功', '-2', '1', '2020-06-27 11:58:55', NULL);
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('dirty', '脏问题', '-2', '1', '2020-06-27 18:02:31', NULL);

-- 20200703
INSERT INTO `yw_zhihu_prestiges` (`preskey`, `name`, `score`, `status`, `created_at`, `updated_at`) VALUES ('feedback', '反馈被接收', '2', '1', '2020-07-03 13:36:29', NULL);
