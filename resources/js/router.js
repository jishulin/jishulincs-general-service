import Vue from 'vue'
import VueRouter from 'vue-router'
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}

Vue.use(VueRouter)

const routes = [
    {
        title: '测试流程图',
        path: '/flowchart',
        name: 'flowchart',
        component: () => import('./views/Flowchart.vue')
    },
    {
        title: '游客端',
        path: '/',
        name: 'index',
        component: () => import('./views/Index.vue'),
        redirect : '/main',
        children: [
            {
                title: '评论区/问题区',
                path: 'main',
                name: 'main',
                component: () => import('./views/QC/Main.vue')
            },
            {
                title: '评论区/问题区',
                path: 'questionComment',
                name: 'questionComment',
                component: () => import('./views/QC/QuestionComment.vue')
            },
            {
                title: '文章详情',
                path: 'question/:questionId/:userId',
                name: 'question',
                component: () => import('./views/QC/Question.vue')
            },
            {
                title: '关注/粉丝用户详情',
                path: 'custuser/:userId',
                name: 'custuser',
                component: () => import('./views/QC/Custuser.vue')
            },
            {
                title: '话题专题',
                path: 'topic/:topicId',
                name: 'topic',
                component: () => import('./views/QC/Topic.vue')
            },
            {
                title: '私信',
                path: 'messages',
                name: 'messages',
                component: () => import('./components/messages.vue')
            },
            {
                title: '个人信息',
                path: 'personal',
                name: 'personal',
                component: () => import('./components/personal.vue')
            },
            {
                title: '用户注册',
                path: 'register',
                name: 'register',
                component: () => import('./components/register.vue')
            },
        ],
    },
    {
        title: '客户端',
        path: '/customer',
        name: 'customer',
        component: () => import('./views/Customer/Index.vue'),
        redirect : '/customer/customerHome',
        children: [
            {
                title: '个人中心',
                path: 'customerHome',
                name: 'customerHome',
                component: () => import('./views/Customer/Home/CustomerHome.vue'),
            },
            {
                title: '我的声望',
                path: 'myPrestige',
                name: 'myPrestige',
                component: () => import('./views/Customer/Home/MyPrestige.vue'),
            },
            {
                title: '我的收藏',
                path: 'myCollection',
                name: 'myCollection',
                component: () => import('./views/Customer/Home/MyCollection.vue'),
            },
            {
                title: '我的博文',
                path: 'myBlogs',
                name: 'myBlogs',
                component: () => import('./views/Customer/Home/MyBlogs.vue'),
            },
            {
                title: '我的问题',
                path: 'myQuestion',
                name: 'myQuestion',
                component: () => import('./views/Customer/Home/MyQuestion.vue'),
            },
            {
                title: '我的链接',
                path: 'myLink',
                name: 'myLink',
                component: () => import('./views/Customer/Home/MyLink.vue'),
            },
            {
                title: '我的讨论',
                path: 'myDiscuss',
                name: 'myDiscuss',
                component: () => import('./views/Customer/Home/MyDiscuss.vue'),
            },
            {
                title: '我的关注',
                path: 'myFollowers',
                name: 'myFollowers',
                component: () => import('./views/Customer/Home/MyFollowers.vue'),
            },
            {
                title: '我的粉丝',
                path: 'myFans',
                name: 'myFans',
                component: () => import('./views/Customer/Home/MyFans.vue'),
            },
            {
                title: '新建博文',
                path: 'blogsCrd',
                name: 'blogsCrd',
                component: () => import('./views/Customer/Home/BlogsCrd.vue'),
            },
            {
                title: '发起讨论',
                path: 'discussCrd',
                name: 'discussCrd',
                component: () => import('./views/Customer/Home/DiscussCrd.vue'),
            },
            {
                title: '分享链接',
                path: 'linksCrd',
                name: 'linksCrd',
                component: () => import('./views/Customer/Home/LinksCrd.vue'),
            },
            {
                title: '提个问题',
                path: 'questionsCrd',
                name: 'questionsCrd',
                component: () => import('./views/Customer/Home/QuestionsCrd.vue'),
            },
            {
                title: '修改问题',
                path: 'questionEdt/:questionId',
                name: 'questionEdt',
                component: () => import('./views/Customer/Home/QuestionEdt.vue'),
            },
            {
                title: '建议反馈',
                path: 'adviseFeedback',
                name: 'adviseFeedback',
                component: () => import('./views/Customer/Home/AdviseFeedback.vue'),
            },

        ]
    },
    {
        title: '管理员端',
        path: '/admin',
        name: 'admin',
        component: () => import('./views/Admin/Index.vue'),
        redirect : '/admin/adminHome',
        children: [
            {
                title: '我的主页',
                path: 'adminHome',
                name: 'adminHome',
                component: () => import('./views/Admin/System/AdminHome.vue'),
            },
            {
                title: '话题设置',
                path: 'topicSetting',
                name: 'topicSetting',
                component: () => import('./views/Admin/System/TopicSetting.vue'),
            },
            {
                title: '问题举报管理',
                path: 'reportManager',
                name: 'reportManager',
                component: () => import('./views/Admin/System/ReportManager.vue'),
            },
            {
                title: '评论举报管理',
                path: 'reportCommentManager',
                name: 'reportCommentManager',
                component: () => import('./views/Admin/System/ReportCommentManager.vue'),
            },
            {
                title: '问题列表',
                path: 'questionListManager',
                name: 'questionListManager',
                component: () => import('./views/Admin/System/QuestionListManager.vue'),
            },
            {
                title: '脏问题列表',
                path: 'questionDirtyListManager',
                name: 'questionDirtyListManager',
                component: () => import('./views/Admin/System/QuestionDirtyListManager.vue'),
            },
            {
                title: '注册量',
                path: 'regStatis',
                name: 'regStatis',
                component: () => import('./views/Admin/System/RegStatis.vue'),
            },
            {
                title: '访问量',
                path: 'visitStatis',
                name: 'visitStatis',
                component: () => import('./views/Admin/System/VisitStatis.vue'),
            },
            {
                title: '话题量',
                path: 'topicStatis',
                name: 'topicStatis',
                component: () => import('./views/Admin/System/TopicStatis.vue'),
            },
            {
                title: '活跃度分析',
                path: 'activityStatis',
                name: 'activityStatis',
                component: () => import('./views/Admin/System/ActivityStatis.vue'),
            },
            {
                title: '站内用户',
                path: 'userManager',
                name: 'userManager',
                component: () => import('./views/Admin/System/UserManager.vue'),
            },
            {
                title: '建议反馈处理',
                path: 'feedBack',
                name: 'feedBack',
                component: () => import('./views/Admin/System/FeedBack.vue'),
            },
            {
                title: '配置源管理',
                path: 'sourceConfig',
                name: 'sourceConfig',
                component: () => import('./views/Admin/System/SourceConfig.vue'),
            },
            {
                title: '配置管理',
                path: 'configManager',
                name: 'configManager',
                component: () => import('./views/Admin/System/ConfigManager.vue'),
                children: [{
                        title: '敏感词过滤',
                        path: 'dirty',
                        name: 'dirty',
                        component: () => import('./views/Admin/System/ConfigManager/Dirty.vue'),
                    },
                    {
                        title: '广告限制配置',
                        path: 'advs',
                        name: 'advs',
                        component: () => import('./views/Admin/System/ConfigManager/Advs.vue'),
                    },
                ]
            },
            {
                title: '声望分值配置',
                path: 'prestigeScore',
                name: 'prestigeScore',
                component: () => import('./views/Admin/System/PrestigeScore.vue'),
            },
            {
                title: '封面管理',
                path: 'websiteCover',
                name: 'websiteCover',
                component: () => import('./views/Admin/System/WebsiteCover.vue'),
            },
            {
                title: '系统公告',
                path: 'websiteNotice',
                name: 'websiteNotice',
                component: () => import('./views/Admin/System/WebsiteNotice.vue'),
            },
            {
                title: '广告设置',
                path: 'advsSetting',
                name: 'advsSetting',
                component: () => import('./views/Admin/System/AdvsSetting.vue'),
            },
            {
                title: '消息发布',
                path: 'sysmsgPublish',
                name: 'sysmsgPublish',
                component: () => import('./views/Admin/System/SysmsgPublish.vue'),
            },
            {
                title: '系统设置',
                path: 'sysSetting',
                name: 'sysSetting',
                component: () => import('./views/Admin/System/SysSetting.vue'),
            },
            {
                title: '友情链接',
                path: 'friendLink',
                name: 'friendLink',
                component: () => import('./views/Admin/System/FriendLink.vue'),
            },
        ]
    },
]

const router = new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
