require('./bootstrap');

window.Vue = require('vue');

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import {
    axios
} from './router/axios';
import VueAxios from 'vue-axios';

import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'

import VueRouter from 'vue-router';

import qs from 'qs';

import App from './App.vue';

import Highcharts from 'highcharts';
import HighchartsVue from 'highcharts-vue';
import HighchartsMore from 'highcharts/highcharts-more.js';
import oldieInit from 'highcharts/modules/oldie';

import mavonEditor from 'mavon-editor';
import 'mavon-editor/dist/css/index.css';

Vue.use(mavonEditor);

Vue.use(HighchartsVue, {
    tagName: 'charts'
});
Highcharts.setOptions({
    credits: false
});
HighchartsMore(Highcharts)
oldieInit(Highcharts)

Vue.use(ElementUI);
Vue.use(Avue)
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.prototype.qs = qs;

import {
    ZHIHUAPI
} from './api.js';
Vue.prototype.$API = {
    ZHIHUAPI
};
import store from './store/';
import router from './router.js';

var vm = new Vue({
    el: "#app",
    store,
    router,
    render: h => h(App)
});
