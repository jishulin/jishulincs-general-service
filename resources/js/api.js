const ZHIHUAPI = {
    // 登录
    "signIn": "/api/zhihu/signIn",
    "signOut": "/api/zhihu/signOut",
    "getWebLatestNotice": "/api/zhihu/getWebLatestNotice",
    "getWebAdvs": "/api/zhihu/getWebAdvs",
    "getWebFriendLinks": "/api/zhihu/getWebFriendLinks",
    "imageUpload": "/api/zhihu/imageUpload",
    
    // 设置配置源-edit=修改,status=启用禁用,remove=删除,create=添加,list=列表,active=有效配置源
    "configSourceList": "/api/zhihu/configSource/list",
    "configSourceCreate": "/api/zhihu/configSource/create",
    "configSourceEdit": "/api/zhihu/configSource/edit",
    "configSourceRemove": "/api/zhihu/configSource/remove",
    "configSourceStatus": "/api/zhihu/configSource/status",
    "configSourceActive": "/api/zhihu/configSource/active",

    // 声望分值配置-list=列表,edit=修改,status=启用禁用,create=添加
    "prestigeScoreList": "/api/zhihu/prestigeScore/list",
    "prestigeScoreCreate": "/api/zhihu/prestigeScore/create",
    "prestigeScoreEdit": "/api/zhihu/prestigeScore/edit",
    "prestigeScoreStatus": "/api/zhihu/prestigeScore/status",

    // 系统公告设置-edit=修改,remove=删除,create=添加,list=列表,history=历史
    "systemNoticeList": "/api/zhihu/systemNotice/list",
    // "systemNoticeHistory": "/api/zhihu/systemNotice/history",
    "systemNoticeCreate": "/api/zhihu/systemNotice/create",
    "systemNoticeEdit": "/api/zhihu/systemNotice/edit",
    "systemNoticeRemove": "/api/zhihu/systemNotice/remove",

    // 广告设置-list=列表，create=添加，remove=删除，edit=修改
    "advsSettingList": "/api/zhihu/advsSetting/list",
    "advsSettingCreate": "/api/zhihu/advsSetting/create",
    "advsSettingEdit": "/api/zhihu/advsSetting/edit",
    "advsSettingRemove": "/api/zhihu/advsSetting/remove",

    // 封面管理-list=列表，create=添加，edit=修改，setting=设置为封面
    "coverManagerList": "/api/zhihu/coverManager/list",
    "coverManagerCreate": "/api/zhihu/coverManager/create",
    "coverManagerEdit": "/api/zhihu/coverManager/edit",
    "coverManagerSetting": "/api/zhihu/coverManager/setting",

    // 站内用户--list=列表，status=启用禁用
    "userManagerList": "/api/zhihu/userManager/list",
    "userManagerStatus": "/api/zhihu/userManager/status",

    // 站内统计
    // 用户注册量
    "userRegisterCharts": "/api/zhihu/userRegisterCharts",
    // PV,UV
    "puvCharts": "/api/zhihu/puvCharts",
    // 话题量
    "topicCharts": "/api/zhihu/topicCharts",
    // 活跃度分析
    "activityCharts": "/api/zhihu/activityCharts",

    // 话题设置--列表，创建，设置，启用禁用
    "topicList": "/api/zhihu/listTopics",
    "topicCreate": "/api/zhihu/createTopic",
    "topicSetting": "/api/zhihu/configTopic",
    "topicStatus": "/api/zhihu/statusTopic",
    // 获取话题列表字典
    "topicSelectDict": "/api/zhihu/topicSelectDict",

    // 问题举报管理-list=列表，exam=审核
    "reportManagerList": "/api/zhihu/reportManager/list",
    "reportManagerExam": "/api/zhihu/reportManager/exam",

    // 评论举报管理-list=列表，exam=审核
    "reportCommentManagerList": "/api/zhihu/reportCommentManager/list",
    "reportCommentManagerExam": "/api/zhihu/reportCommentManager/exam",
    // 建议反馈管理-list=列表，deal=处理，反馈=feedback
    "feedbackManagerList": "/api/zhihu/feedbackManager/list",
    "feedbackManagerDeal": "/api/zhihu/feedbackManager/deal",
    "feedbackManagerFeedback": "/api/zhihu/feedbackManager/feedback",

    // 友情链接-list=列表，create=添加，edit=修改,status=启用禁用
    "friendLinkList": "/api/zhihu/friendLink/list",
    "friendLinkCreate": "/api/zhihu/friendLink/create",
    "friendLinkEdit": "/api/zhihu/friendLink/edit",
    "friendLinkStatus": "/api/zhihu/friendLink/status",

    // 站内信-私信列表-all=全部-message=最新未读,readed=已读,deleted=已删除,sended=我发送的,system=系统信息,totals=获取标签未读数
    "listMessageTotals": "/api/zhihu/listMessage/totals",
    "getListMessage": "/api/zhihu/listMessage/",
    // 站内信息设置操作-toread=设为已读[可批量],todelete=删除[可批量],tomessage=设为未读[可批量]
    "configMessage": "/api/zhihu/configMessage/",

    // 管理员消息发布-list=消息发布列表-create=创建,edit=修改,remove=删除,publish=发布,recopy=复制
    "systemPublishMessageList": "/api/zhihu/systemPublishMessage/list",
    "systemPublishMessageCreate": "/api/zhihu/systemPublishMessage/create",
    "systemPublishMessageEdit": "/api/zhihu/systemPublishMessage/edit",
    "systemPublishMessageRemove": "/api/zhihu/systemPublishMessage/remove",
    "systemPublishMessagePublish": "/api/zhihu/systemPublishMessage/publish",
    "systemPublishMessageRecopy": "/api/zhihu/systemPublishMessage/recopy",

    // 系统配置获取
    "systemConfigGetting": "/api/zhihu/gettingByKey",
    // 系统配置保存
    "systemConfigSetting": "/api/zhihu/setting",
    // 个人信息
    "personalSettings": "/api/zhihu/personalSettings",
    // 通过用户ID获取用户信息
    "getPersonalInfoByUserId": "/api/zhihu/getPersonalInfoByUserId",
    // 用户组件信息
    "getPersonalComponent": "/api/zhihu/getPersonalComponent",
    // 获取最新，最受欢迎问题列表
    "getShowQuestionLimitList": "/api/zhihu/getShowQuestionLimitList",
    // 获取封面
    "getWebCover": "/api/zhihu/getWebCover",
    // 获取话题统计
    "getWebTopics": "/api/zhihu/getWebTopics",
    // 获取有效话题信息
    "getWebTopicsEffect": "/api/zhihu/getWebTopicsEffect",

    // 问题相关处理-问题列表(首页)，(取消)关注问题，(取消)喜欢问题，举报问题，查看问题，问题评论，评论删除，评论采纳，评论点赞，评论取消点赞
    // 举报答案,热门问题列表(首页),关注问题列表(首页),问题列表(管理/个人),设置/取消精华(管理),脏问题列表(管理),删除脏问题(管理),博文收藏
    // 博文中心列表(首页)，获取话题列表（话题专题）,获取精华榜列表（首页）
    "getWebLatestQuestion": "/api/zhihu/getWebLatestQuestion",
    "followersQuestion": "/api/zhihu/followersQuestion",
    "favouriteQuestion": "/api/zhihu/favouriteQuestion",
    "reportQuestion": "/api/zhihu/reportQuestion",
    "viewShowQuestion": "/api/zhihu/viewShowQuestion",
    "getQuestionCommentByQuestionID": "/api/zhihu/getQuestionCommentByQuestionID",
    "answerQuestion": "/api/zhihu/answerQuestion",
    "removeAnswerQuestion": "/api/zhihu/removeAnswerQuestion",
    "answerQuestionAdopt": "/api/zhihu/answerQuestionAdopt",
    "answerQuestionVote": "/api/zhihu/answerQuestionVote",
    "answerQuestionCancelVote": "/api/zhihu/answerQuestionCancelVote",
    "reportAnswer": "/api/zhihu/reportAnswer",
    "getWebHotQuestion": "/api/zhihu/getWebHotQuestion",
    "getFollowerQuestionList": "/api/zhihu/getFollowerQuestionList",
    "listQuestion": "/api/zhihu/listQuestion",
    "configQuestionCream": "/api/zhihu/configQuestionCream",
    "listDirtyQuestion": "/api/zhihu/listDirtyQuestion",
    "removeDirtyQuestion": "/api/zhihu/removeDirtyQuestion",
    "collectionQuestion": "/api/zhihu/collectionQuestion",
    "getBlogQuestionList": "/api/zhihu/getBlogQuestionList",
    "getTopicListData": "/api/zhihu/getTopicListData",
    "getTopicCreamList": "/api/zhihu/getTopicCreamList",

    // 关注用户
    "followerUser": "/api/zhihu/followerUser",
    // 取消关注用户(支持批量及全部)
    "followerCancelUser": "/api/zhihu/followerCancelUser",
    // 发送私信
    "sendPrivateMessage": "/api/zhihu/sendPrivateMessage",

    // 管理员首页
    "getSystemRotate": "/api/zhihu/getSystemRotate",
    "getSystemTabs": "/api/zhihu/getSystemTabs",
    "getSystemCard": "/api/zhihu/getSystemCard",
    "getSystemHotQuestion": "/api/zhihu/getSystemHotQuestion",

    // 获取脏词
    "getDirtyWords": "/api/zhihu/getDirtyWords",
    // 获取声望记录
    "getListPrestige": "/api/zhihu/getListPrestige",
    // 我关注的用户
    "followerList": "/api/zhihu/followerList",
    // 我的粉丝
    "followerFanceList": "/api/zhihu/followerFanceList",
    // 我的收藏
    "getCollectionList": "/api/zhihu/getCollectionList",
    // 获取用户个人中心统计数据
    "getPersonalTotalNums": "/api/zhihu/getPersonalTotalNums",
    // 我关注的问题
    "myFollowerQuestionList": "/api/zhihu/myFollowerQuestionList",
    // 我喜欢的问题
    "myFavouriteQuestionList": "/api/zhihu/myFavouriteQuestionList",
    // 我评论的问题
    "myCommentQuestionList": "/api/zhihu/myCommentQuestionList",
    // 修改密码
    "changePassWord": "/api/zhihu/changePassWord",
    // 用户注册
    "register": "/api/zhihu/register",
    // 发布问题
    "publishQuestion": "/api/zhihu/publishQuestion",
    // 删除问题
    "removeQustion": "/api/zhihu/removeQustion",
    // 设置问题
    "configQuestion": "/api/zhihu/configQuestion",
    // 获取查看问题
    "viewQuestion": "/api/zhihu/viewQuestion",
}

export {
    ZHIHUAPI
}
