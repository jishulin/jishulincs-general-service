/**
 * 全站http配置
 *
 * axios参数说明
 * isSerialize是否开启form表单提交
 * isToken是否需要token
 */
import axios from 'axios'
import qs from 'qs'
import { Message } from 'element-ui'
import { getToken, getUid, removeToken } from '../util/auth'
import router from '../router.js'
// 之前我们没有安装qs模块，这里我们要在项目里先安装 cnpm install qs --save
// 安装完成之后才能引入到这里，否则会报错。
// qs模式的作用是：当我们用post的请求接口时候，后台可能会接收不到前端发送的data数据，
// 这时我们就要用到qs模块对post请求发送的数据进行处理，
// qs.stringify(data),然后把处理后的数据发送给后台，
// 注意这里post请求headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}要这么配置才行
// let baseURL = process.env.API_BASE.toString() // 获取配置好的基础路径，直接使用process.env.API_BASE并不是字符串，所以这里要转换
// axios.defaults.baseURL = baseURL // baseURL用于自动切换本地环境接口和生产环境接口
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
axios.interceptors.request.use(config => {
  const token = getToken()
  const uid = getUid()
  config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
  if (token) {
    config.headers.token = token // token
  }
  if (uid) {
    config.headers.uid = uid // uid
  }
  config.headers.Authorization = "d4078d3c5a8e012ceea02e49628819a8" // uid
  
  return config
}, error => {
  return Promise.reject(error)
})
// HTTPresponse拦截
axios.interceptors.response.use(res => {
  const status = Number(res.status) || 200
  const message = res.data.message || '未知错误'
  // 如果是401则跳转到登录页面
  // if (status === 401) {
  //   router.push({ 'path': '/login' })
  // }
  // 如果请求为非200否者默认统一处理
  if (status !== 200) {
    Message({
      message: message,
      type: 'error'
    })
    return Promise.reject(new Error(message))
  }
  if (res.data.code && res.data.code === -10000) {
    removeToken()
    Message({
      message: res.data.info,
      duration: 4000
    })
    setTimeout(() => {
      router.push('/')
    }, 2000)
  }
  return res
}, error => {
  return Promise.reject(error)
})

// 封装axios,用于异步操作的同步写法，与async结合使用
const api = {
  get (url, data) {
    return new Promise((resolve, reject) => {
      axios.get(url, { params: data }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  post (url, data) {
    return new Promise((resolve, reject) => {
      axios.post(url, qs.stringify(data)).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}

export { api, axios }
