import Vue from 'vue'
// 校验规则
/**
 * @param check [校验规则]
 * @param value [需要校验的值]
 * @param posInfo [定位信息以及提示信息]
 */
export function regularCheck (check, value, formIndex, posInfo) {
  // 当value 为null || undefined || '' 时将不做校验 后续会做必填校验
  if (window.vm.error === undefined) window.vm.error = {}

  if (!value) {
    delete window.vm.error[posInfo.field]
    return true
  }
  var reg
  switch (check) {
    case 'phone': // 移动电话
      reg = /^1[3456789]\d{9}$/
      break
    case 'is0int': // 开头包括0的全数字
      reg = /^[0-9]\d*$/
      break
    case 'idcard': // 身份证
      reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
      break
    case 'float2point': // 浮点数且只能包含两位小数
      reg = /(^[0-9]\d*$)|(^[0-9]\d*[\.]{1}[0-9]{1,2}$)/
      break
    case 'float2pointminus': // 浮点数且只能包含两位小数(含负数)
      reg = /(^[+-]?[0-9]\d*$)|(^[+-]?[0-9]\d*[\.]{1}[0-9]{1,2}$)/
      break
    case 'float1point': // 浮点数且只能包含一位小数
      reg = /(^[0-9]\d*$)|(^[0-9]\d*[\.]{1}[0-9]{1}$)/
      break
    case 'int1to9': // 正整数且 >0 <10
      reg = /^[1-9]{1}$/
      break
    case 'chineseOrletter': // 中文或字母
      reg = /^[\u4e00-\u9fa5a-zA-Z]+$/
      break
    case 'chinese': // 中文
      reg = /^[\u4e00-\u9fa5]+$/
      break
  }
  // 存储校验失败信息
  var res = reg.test(value)

  if (!res) {
    Vue.set(window.vm.error, `${posInfo.field}_${formIndex}`, {
      fail: true,
      info: `${posInfo.info}数据格式有误, 请保证数据格式务必正确`,
      tabIndex: posInfo.navIndex
    })
    // window.vm.error[posInfo.field] = 1
  } else {
    delete window.vm.error[`${posInfo.field}_${formIndex}`]
  }
  return res
}
