import Cookies from 'js-cookie'
const TokenKey = 'x-access-token'
const UIDKey = 'x-access-uid'
// var inFifteenMinutes = new Date(new Date().getTime() + 120 * 60 * 1000)
export function getToken () {
  return Cookies.get(TokenKey)
}

export function getUid () {
  return Cookies.get(UIDKey)
}

export function setUid (uid) {
  // return Cookies.set(UIDKey, uid, { expires: inFifteenMinutes })
  return Cookies.set(UIDKey, uid)
}

export function setToken (token) {
  // return Cookies.set(TokenKey, token, { expires: inFifteenMinutes })
  return Cookies.set(TokenKey, token)
}

export function removeToken () {
  return Cookies.remove(TokenKey)
}
