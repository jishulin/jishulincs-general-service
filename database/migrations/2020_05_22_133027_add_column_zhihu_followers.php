<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnZhihuFollowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_followers', function (Blueprint $table) {
            $table->tinyInteger('status')->default(1)->comment('关注及取消[0=取消，1=关注]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_followers', function (Blueprint $table) {
            //
        });
    }
}
