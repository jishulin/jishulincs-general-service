<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteZhihuUsersColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            $table->dropColumn('questions_count');
            $table->dropColumn('answers_count');
            $table->dropColumn('comments_count');
            $table->dropColumn('favorites_count');
            $table->dropColumn('likes_count');
            $table->dropColumn('followers_count');
            $table->dropColumn('followings_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            //
        });
    }
}
