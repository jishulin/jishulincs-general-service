<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->unsignedBigInteger('from_id')->comment("发送私信id");
                $table->string('from_name', 100)->default('')->comment("发送私信人");
                $table->unsignedBigInteger('to_id')->comment("接收私信id");
                $table->text('content')->comment("私信的内容");
                $table->timestamp('readed_at')->nullable()->comment("接收方阅读时间");
                $table->timestamp('removed_at')->nullable()->comment("接收方删除时间");
                $table->timestamps();
            });
            DB::statement("ALTER TABLE `yw_zhihu_messages` comment '知乎应用-私信信息表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_messages');
    }
}
