<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_roles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->default(0)->comment('单位(平台)ID');
            $table->string('name', 100)->default('')->comment('角色名称');
            $table->string('tag', 60)->unique()->default('')->comment('角色唯一标识');
            $table->integer('order_num')->default(10)->comment('排序');
            $table->string('remark', 500)->default('')->comment('备注');
            $table->tinyInteger('status')->default(0)->comment('状态[1=启用，0=禁用]');
            $table->tinyInteger('isdel')->default(0)->comment('是否删除[1=删除，0=正常]');
            $table->integer('deleted_at')->default(0)->comment('删除时间');
            $table->text('auth')->nullable()->comment('授权信息');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_roles` comment '技术林-流程引擎-角色信息表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_roles');
    }
}
