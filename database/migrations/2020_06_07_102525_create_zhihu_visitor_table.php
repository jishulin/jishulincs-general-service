<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuVisitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_visits', function (Blueprint $table) {
			$table->string('puv')->unique()->comment('访问量统计标识');
			$table->integer('pv')->default(0)->comment('访问量');
			$table->integer('uv')->default(0)->comment('访客量');
        });
        DB::statement("ALTER TABLE `yw_sys_visits` comment '访问量统计-pvuv'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_visits');
    }
}
