<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnExamInfoOfReportQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_report_questions', function (Blueprint $table) {
            $table->string('report_exam_message', 500)->default('')->comment("审核意见")->after('report_descrption');
            $table->timestamp('examed_at')->nullable()->comment('审核时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_report_questions', function (Blueprint $table) {
            //
        });
    }
}
