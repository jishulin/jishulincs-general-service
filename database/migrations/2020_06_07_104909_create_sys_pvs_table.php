<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysPvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_pvs', function (Blueprint $table) {
        $table->bigIncrements('id');
            $table->string('puv')->comment('访问量统计标识');
            $table->string('ip')->default('')->comment('IP地址');
            $table->integer('last_time')->default(0)->comment('记录时间');
        });
        DB::statement("ALTER TABLE `yw_sys_pvs` comment '访问量记录'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_pvs');
    }
}
