<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsOfAnswerAdopts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_answer_adopts', function (Blueprint $table) {
            $table->bigInteger('user_id')->default(0)->comment("用户ID")->after('question_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_answer_adopts', function (Blueprint $table) {
            //
        });
    }
}
