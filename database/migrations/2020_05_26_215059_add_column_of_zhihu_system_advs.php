<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOfZhihuSystemAdvs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_system_advs', function (Blueprint $table) {
            $table->integer('start_date')->default(0)->comment('广告开始时间');
            $table->integer('end_date')->default(0)->comment('广告截至时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_system_advs', function (Blueprint $table) {
            //
        });
    }
}
