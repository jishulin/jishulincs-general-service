<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrowseLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('browse_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('log_k', 60)->default('')->comment('服务类型');
            $table->string('log_type', 60)->default('')->comment('操作类型');
            $table->string('log_type_origin', 60)->default('')->comment('原始类型');
            $table->string('log_type_trans', 100)->default('')->comment('转换类型');
            $table->bigInteger('log_user_id')->default(0)->comment('用户ID');
            $table->string('log_user_name', 100)->default('')->comment('用户名');
            $table->bigInteger('log_plate_id')->default(0)->comment('平台ID');
            $table->string('log_plate_name', 100)->default('')->comment('平台名');
            $table->string('log_app', 100)->default('')->comment('app');
            $table->string('log_controller', 100)->default('')->comment('controller');
            $table->string('log_action', 100)->default('')->comment('action');
            $table->string('log_ip', 50)->default('')->comment('IP');
            $table->string('log_created_at', 20)->default('')->comment('创建日期');
            $table->longText('log_opt_message')->comment('操作信息');
            $table->text('log_red_message')->comment('日志信息');
            $table->longText('log_sys_message')->comment('系统日志信息');
        });
        DB::statement("ALTER TABLE `yw_browse_logs` comment '系统日志表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('browse_logs');
    }
}
