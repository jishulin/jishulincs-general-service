<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfDictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_dicts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->default(0)->comment('所属平台(单位)');
            $table->bigInteger('parent_id')->default(0)->comment('字典类型ID');
            $table->string('name', 100)->default('')->comment('名称');
            $table->tinyInteger('status')->default(1)->comment('状态【1=启用，0=禁用】');
            $table->integer('orders')->default(10)->comment('排序');
            $table->bigInteger('created_id')->default(0)->comment('创建人ID');
            $table->string('created_by', 100)->default('')->comment('创建人');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_dicts` comment '技术林-流程引擎-字典详情表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_dicts');
    }
}
