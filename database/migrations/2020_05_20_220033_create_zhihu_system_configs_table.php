<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuSystemConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_system_configs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('k', 60)->unique()->comment('配置键');
            $table->text('v')->comment('配置值/序列化存储');
            $table->string('title', 100)->default('')->comment('描述');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_system_configs` comment '知乎应用-系统配置表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_system_configs');
    }
}
