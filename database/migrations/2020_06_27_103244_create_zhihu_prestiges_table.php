<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuPrestigesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_prestiges', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('preskey', 64)->unique()->comment('分值标识Key');
            $table->string('name', 100)->comment("描述");
            $table->integer('score')->default(0)->comment('分值');
            $table->tinyInteger('status')->default(1)->comment('状态[1=启用,0=禁用]');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_prestiges` comment '知乎应用-声望分值设置表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_prestiges');
    }
}
