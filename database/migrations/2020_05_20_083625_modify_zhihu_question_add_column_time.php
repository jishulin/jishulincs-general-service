<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyZhihuQuestionAddColumnTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            $table->timestamp('created_at')->nullable()->comment('发布时间');
            $table->timestamp('updated_at')->nullable()->comment('更新时间');
            $table->timestamp('removed_at')->nullable()->comment('删除时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            //
        });
    }
}
