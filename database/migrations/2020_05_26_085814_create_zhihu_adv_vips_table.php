<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuAdvVipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_system_advs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('advcomname', 100)->default('')->comment('投放广告公司名称');
            $table->string('advcomcontactname', 100)->default('')->comment('投放广告公司联系人姓名');
            $table->string('advcomcontact', 13)->default('')->comment('投放广告公司联系人电话');
            $table->string('title', 20)->default('')->comment('广告标题');
            $table->string('description', 20)->default('')->comment('广告简述');
            $table->string('imageurl', 200)->default('')->comment('广告图片本地');
            $table->string('imagelink', 200)->default('')->comment('广告图片链接');
            $table->string('blanklinkurl', 200)->default('')->comment('广告点击链接');
            $table->integer('orderby')->default(10)->comment('排序');
            $table->tinyInteger('status')->default(1)->comment('状态[1=正常，0=删除]');
            $table->string('type')->default('general')->comment('广告类型[general=普通，vip=VIP，welfare=公益]');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `yw_zhihu_system_advs` comment '知乎应用-系统广告表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_system_advs');
    }
}
