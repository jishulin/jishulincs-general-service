<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_collections', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index()->comment("被收藏的问题的id");
            $table->unsignedBigInteger('user_id')->index()->comment("收藏问题人id");
            $table->tinyInteger('status')->default(1)->comment('收藏及取消[0=取消，1=关注]');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `yw_zhihu_collections` comment '知乎应用-我的博文收藏表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_collections');
    }
}
