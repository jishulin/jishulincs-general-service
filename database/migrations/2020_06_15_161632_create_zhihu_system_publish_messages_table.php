<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuSystemPublishMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_system_publish_messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('title', '100')->default('')->comment('消息标题');
            $table->string('message', '500')->default('')->comment('消息内容');
            $table->string('message_type', '60')->default('info')->comment('消息类型【success,warning,info,error】');
            $table->string('position', '60')->default('bottom-right')->comment('消息通知位置【top-right,top-left,bottom-right,bottom-left】');
            $table->integer('duration')->default(0)->comment('通知停留时间s【0=不自动关闭】');
            $table->tinyInteger('is_removed')->default(0)->comment('是否删除【0=否，1=是】');
            $table->tinyInteger('is_published')->default(0)->comment('是否已经发布【0=否，1=是】');
            $table->tinyInteger('types')->default(0)->comment('消息任务类型【0=及时，1=时间之后，2=指定日期】');
            $table->string('times', '100')->default('')->comment('时间信息');
            $table->timestamp('removed_at')->nullable()->comment("删除时间");
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_system_publish_messages` comment '知乎应用-系统消息发布表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_system_publish_messages');
    }
}
