<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOfQuestions2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            $table->mediumText('editormd-html-code')->comment('内容HTML');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            //
        });
    }
}
