<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnZhihuMessagesType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_messages', function (Blueprint $table) {
            $table->tinyInteger('status')->default(0)->comment('消息状态[0=未读，1=已读，2=删除]')->after('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_messages', function (Blueprint $table) {
            //
        });
    }
}
