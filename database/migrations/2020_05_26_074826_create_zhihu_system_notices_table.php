<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuSystemNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_system_notices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('content', 500)->default('')->comment('公告内容');
            $table->tinyInteger('status')->default(1)->comment('状态[1=正常，0=删除]');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_system_notices` comment '知乎应用-系统公告表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_system_notices');
    }
}
