<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArterTableAddColumnOfZhihuConfigSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_config_sources', function (Blueprint $table) {
            $table->tinyInteger('is_system')->default(1)->comment('系统配置源【1=是，0=否】');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_config_sources', function (Blueprint $table) {
            //
        });
    }
}
