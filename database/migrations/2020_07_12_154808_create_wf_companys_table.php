<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfCompanysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_companys', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->default(0)->comment('上级ID');
            $table->string('name', 100)->default('')->comment('单位(平台)名称');
            $table->string('short_name', 100)->default('')->comment('单位(平台)简称');
            $table->string('code', 100)->unique()->default('')->comment('单位(平台)编码');
            $table->string('tag', 60)->unique()->default('')->comment('单位(平台)唯一标识');
            $table->string('address', 500)->default('')->comment('单位(平台)地址');
            $table->string('logo', 100)->default('')->comment('单位(平台)LOGO');
            $table->tinyInteger('status')->default(0)->comment('状态[1=启用，0=禁用]');
            $table->string('legal', 100)->default('')->comment('单位(平台)法人');
            $table->string('mobile', 13)->default('')->comment('法人联系电话');
            $table->string('idcard', 18)->default('')->comment('法人身份证号');
            $table->text('auth')->nullable()->comment('授权信息');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_companys` comment '技术林-流程引擎-单位平台表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_companys');
    }
}
