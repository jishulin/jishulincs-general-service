<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuTopicQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_topic_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->bigInteger('question_id')->unsigned()->index()->comment('问题ID');
                $table->bigInteger('topic_id')->unsigned()->index()->comment('话题ID');
                $table->timestamps();
            });
            DB::statement("ALTER TABLE `yw_zhihu_topic_questions` comment '知乎应用-话题问题关系表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_topic_questions');
    }
}
