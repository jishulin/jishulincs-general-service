<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_followers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->comment("被关注的用户的id");
            $table->unsignedBigInteger('follower_id')->index()->comment("粉丝用户的id");
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `yw_zhihu_followers` comment '知乎应用-用户关注（粉丝）表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_followers');
    }
}
