<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->comment("回答问题用户");
            $table->unsignedBigInteger('question_id')->index()->comment("回答的问题");
            $table->text('content')->comment("答案的具体内容");
            $table->integer('votes_count')->default(0)->comment("点赞总数");
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_answers` comment '知乎应用-问题回答表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_answers');
    }
}
