<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeWfUserRealname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_users', function (Blueprint $table) {
            $table->dropColumn('realname');
            // $table->string('realname', 200)->default('')->comment('真实姓名');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_users', function (Blueprint $table) {
            //
        });
    }
}
