<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_menus', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('title', 100)->default('')->comment('标题');
            $table->integer('parent_id')->default(0)->comment('上级菜单ID');
            $table->tinyInteger('status')->default(1)->comment('启用禁用[1=启用，0=禁用]');
            $table->integer('order_num')->default(10)->comment('排序');
            $table->tinyInteger('show')->default(1)->comment('是否显示[1=显示，0=不显示]');
            $table->string('path', 100)->default('')->comment('前端组件路径');
            $table->string('component', 100)->default('')->comment('前端组件');
            $table->string('icon', 100)->default('')->comment('前端菜单图标');
            $table->string('type', 30)->default('')->comment('菜单类型[menu,button]');
            $table->string('remark', 500)->default('')->comment('备注');
            $table->string('authtype', 100)->default('')->comment('授权类型');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_menus` comment '技术林-流程引擎-菜单表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_menus');
    }
}
