<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuFavouriteQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_favourite_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index()->comment("被喜欢的问题的id");
            $table->unsignedBigInteger('user_id')->index()->comment("喜欢问题人id");
            $table->tinyInteger('status')->default(1)->comment('喜欢及取消[0=取消，1=关注]');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_favourite_questions` comment '知乎应用-用户喜欢问题表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_favourite_questions');
    }
}
