<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOfZhihuAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_answers', function (Blueprint $table) {
			$table->tinyInteger('status')->default(1)->comment('状态【1=正常，0=删除】');
            $table->timestamp('removed_at')->nullable()->comment('删除时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_answers', function (Blueprint $table) {
            //
        });
    }
}
