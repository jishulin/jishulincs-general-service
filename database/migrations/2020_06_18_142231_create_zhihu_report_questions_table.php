<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuReportQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_report_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index()->comment("被举报的问题的id");
            $table->unsignedBigInteger('question_user_id')->index()->comment("问题所属人id");
            $table->string('question_title', 100)->comment("问题标题");
            $table->mediumText('question_content')->comment("问题内容");
            $table->unsignedBigInteger('report_user_id')->index()->comment("举报人ID");
            $table->string('report_type', 100)->default('')->comment("举报类型");
            $table->text('report_descrption')->comment('举报描述');
            $table->tinyInteger('status')->default(0)->comment('复议核销状态[0=举报中，1=举报通过，2=举报撤销]');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_report_questions` comment '知乎应用-问题被举报表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_report_questions');
    }
}
