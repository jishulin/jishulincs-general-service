<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeZhihuUserTableClomunsOfEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            $table->smallInteger('email_verified_pass')->default(0)->change();
            $table->string('avatar', 100)->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            //
        });
    }
}
