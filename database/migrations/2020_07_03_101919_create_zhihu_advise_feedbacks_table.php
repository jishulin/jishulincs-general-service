<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuAdviseFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_advise_feedbacks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->comment("反馈人id");
            $table->string('type', 120)->default('')->comment('建议反馈类型');
            $table->string('elsedesc', 210)->default('')->comment("其他【描述】");
            $table->mediumText('content')->comment("反馈内容");
            $table->tinyInteger('status')->default(0)->comment('反馈状态[0=受理中，1=接收，2=不予处理]');
            $table->string('deal_message', 500)->default('')->comment("反馈处理意见");
            $table->timestamp('deal_at')->nullable()->comment('反馈处理时间');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_advise_feedbacks` comment '知乎应用-建议反馈收录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_advise_feedbacks');
    }
}
