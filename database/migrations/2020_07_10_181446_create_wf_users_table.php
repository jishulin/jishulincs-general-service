<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 100)->unique()->comment('用户名');
            $table->string('email', 60)->unique()->deault('')->comment('注册邮箱');
            $table->string('idcard', 18)->unique()->deault('')->comment('身份证号码');
            $table->string('mobile', 11)->unique()->deault('')->comment('手机号码');
            $table->string('realname', 200)->unique()->deault('')->comment('真实姓名');
            $table->string('password')->default('')->comment('登录密码');
            $table->string('solt', 100)->default('')->comment('密码加盐');
            $table->string('avatar')->default('')->comment('用户头像');
            $table->smallInteger('status')->default(1)->comment('用户状态【1=启用，0=禁用】');
            $table->tinyInteger('user_type')->default(2)->comment('用户类型[0=超管,1=管理员,2=普通用户]');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_users` comment '技术林-流程引擎-用户信息表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_users');
    }
}
