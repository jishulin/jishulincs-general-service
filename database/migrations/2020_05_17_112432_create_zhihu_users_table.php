<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateZhihuUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name')->unique()->comment('用户名');
            $table->string('email')->unique()->comment('注册邮箱');
            $table->timestamp('email_verified_pass')->smallInteger()->comment('邮箱校验是否通过【1=通过，0=未通过】');
            $table->string('password')->comment('登录密码');
            $table->string('avatar')->comment('用户头像');
            $table->integer('questions_count')->default(0)->comment('总提问数目');
            $table->integer('answers_count')->default(0)->comment('总回答数目');;
            $table->integer('comments_count')->default(0);
            $table->integer('favorites_count')->default(0);
            $table->integer('likes_count')->default(0);
            $table->integer('followers_count')->default(0);
            $table->integer('followings_count')->default(0);
            $table->json('settings')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_users` comment '知乎应用-用户信息表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_users');
    }
}
