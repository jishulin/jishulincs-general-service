<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuSystemCoverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_system_covers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('title', 100)->default('')->comment('标题');
            $table->string('description', 500)->default('')->comment('描述');
            $table->string('coverimg', 200)->default('')->comment('封面图片');
            $table->tinyInteger('status')->default(0)->comment('状态[1=设置，0=取消]');
            $table->integer('start_date')->default(0)->comment('自动启用时间');
            $table->integer('end_date')->default(0)->comment('自动结束时间');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_system_covers` comment '知乎应用-封面设置管理'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_system_covers');
    }
}
