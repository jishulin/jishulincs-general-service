<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuUserPrestigesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_user_prestiges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index()->comment("用户id");
            $table->string('conent', 1024)->default('')->comment('内容');
            $table->tinyInteger('types')->default(0)->comment("记录类型【0=新增，1=减】");
            $table->integer('score')->default(0)->comment('分值');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_user_prestiges` comment '知乎应用-用户声望记录表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_user_prestiges');
    }
}
