<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfDictTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_dict_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 100)->default('')->comment('类型名称');
            $table->string('code', 100)->default('')->comment('类型编码');
            $table->string('remark', 500)->default('')->comment('备注');
            $table->bigInteger('created_id')->default(0)->comment('创建人ID');
            $table->string('created_by', 100)->default('')->comment('创建人');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_dict_types` comment '技术林-流程引擎-字典类型表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_dict_types');
    }
}
