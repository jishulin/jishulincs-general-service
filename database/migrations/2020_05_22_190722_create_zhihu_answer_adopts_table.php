<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuAnswerAdoptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_answer_adopts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('answer_id')->index()->comment("采纳问题ID");
            $table->unsignedBigInteger('question_id')->index()->comment("问题ID");
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_answer_adopts` comment '知乎应用-问题回答采纳表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_answer_adopts');
    }
}
