<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOfUsersDel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_users', function (Blueprint $table) {
            $table->tinyInteger('isdel')->default(0)->comment('是否删除[1=删除，0=正常]')->after('company_id');
            $table->integer('deleted_at')->default(0)->comment('删除时间');
            $table->tinyInteger('ismod')->default(0)->comment('是否修改账号[0=未修改，1=已修改]');
            $table->bigInteger('parent_id')->default(0)->comment('上级ID');
            $table->text('role')->nullable()->comment('拥有角色');
            $table->text('auth')->nullable()->comment('授权信息');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_users', function (Blueprint $table) {
            //
        });
    }
}
