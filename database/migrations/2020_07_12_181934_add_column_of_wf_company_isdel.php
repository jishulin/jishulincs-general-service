<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOfWfCompanyIsdel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wf_companys', function (Blueprint $table) {
            $table->tinyInteger('isdel')->default(0)->comment('是否删除[1=删除，0=正常]')->after('status');
            $table->integer('deleted_at')->default(0)->comment('删除时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wf_companys', function (Blueprint $table) {
            //
        });
    }
}
