<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuConfigSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_config_sources', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->tinyInteger('status')->default(1)->comment("状态【1=启用，0=禁用】");
            $table->tinyInteger('is_removed')->default(0)->comment("是否删除【1=是，0=否】");
            $table->string('key_name', 100)->unique()->default('')->comment("配置key");
            $table->string('key_field', 500)->default('')->comment("配置限制字段");
            $table->timestamp('removed_at')->nullable()->comment("删除时间");
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_config_sources` comment '知乎应用-系统配置源管理表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_config_sources');
    }
}
