<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateZhihuQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->comment('用户ID');
            $table->string('title', 100)->comment('问题标题');
            $table->mediumText('content')->comment('内容');
            $table->integer('comments_count')->default(0)->comment('评论数目');
            $table->integer('followers_count')->default(1)->comment('问题关注数目');
            $table->integer('answers_count')->default(0)->comment('问题回答数目');
            $table->tinyInteger('close_comment')->default(1)->comment('是否关闭评论【1=否，0=是】');
            $table->tinyInteger('is_hidden')->default(1)->comment('是否隐藏问题【1=否，0=是】');
        });

        DB::statement("ALTER TABLE `yw_zhihu_questions` comment '知乎应用-问题表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_questions');
    }
}
