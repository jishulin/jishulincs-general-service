<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewZhihuFollowerQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_follower_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index()->comment("被关注的问题的id");
            $table->unsignedBigInteger('user_id')->index()->comment("关注问题人id");
            $table->tinyInteger('status')->default(1)->comment('关注及取消[0=取消，1=关注]');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `yw_zhihu_follower_questions` comment '知乎应用-用户关注问题表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_follower_questions');
    }
}
