<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnQuestionsTypesCream extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            $table->string('cates', 64)->default('else')->comment("分类");
            $table->tinyInteger('is_cream')->default(0)->comment('是否精华【1=是，0=否】');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            //
        });
    }
}
