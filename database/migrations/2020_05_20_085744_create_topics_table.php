<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_topics', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name', 100)->unique()->comment('话题名');
            $table->tinyInteger('status')->default(1)->comment('状态【1=启用，0=禁用】');
            $table->bigInteger('questions_count')->default(0)->comment('话题问题总数');
            $table->bigInteger('followers_count')->default(0)->comment('话题关注总数');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_zhihu_topics` comment '知乎应用-话题表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_topics');
    }
}
