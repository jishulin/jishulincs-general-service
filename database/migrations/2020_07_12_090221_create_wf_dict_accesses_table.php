<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfDictAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_dict_accesses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->bigIncrements('id');
			$table->bigInteger('company_id')->default(0)->comment('所属平台(单位)');
			$table->string('dicts')->default('')->comment('字典类型ID,字符拼接');
			$table->timestamps();
		});
		DB::statement("ALTER TABLE `yw_wf_dict_accesses` comment '技术林-流程引擎-字典授权表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_dict_accesses');
    }
}
