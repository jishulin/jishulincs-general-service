<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnGuestviewOfZhihuQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            $table->bigInteger('is_guest_view')->default(1)->comment('是否游客可见')->after('is_dirty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_questions', function (Blueprint $table) {
            //
        });
    }
}
