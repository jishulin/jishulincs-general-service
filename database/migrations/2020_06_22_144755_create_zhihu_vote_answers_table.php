<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuVoteAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_vote_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('answer_id')->index()->comment("答案的id");
            $table->unsignedBigInteger('user_id')->index()->comment("点赞人id");
            $table->tinyInteger('status')->default(1)->comment('点赞及取消[0=取消，1=关注]');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `yw_zhihu_vote_answers` comment '知乎应用-答案点赞表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_vote_answers');
    }
}
