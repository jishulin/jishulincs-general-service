<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLolumnOfZhihuUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            $table->tinyInteger('user_type')->default(1)->comment('用户类型【0=管理员，1=普通用户】');
            $table->string('nickname', 100)->default('')->comment('用户昵称');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            //
        });
    }
}
