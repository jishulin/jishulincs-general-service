<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_codes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('code', 100)->default('')->comment('代码编码');
            $table->string('name', 100)->default('')->comment('代码名称');
            $table->string('vals', 100)->default('')->comment('代码值');
            $table->tinyInteger('status')->default(1)->comment('状态[1=启用,0=禁用]');
            $table->integer('order')->default(10)->comment('排序');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `yw_wf_codes` comment '技术林-流程引擎-代码表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_codes');
    }
}
