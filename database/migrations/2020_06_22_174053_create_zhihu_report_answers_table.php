<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuReportAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_report_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id')->index()->comment("被举报的答案所属问题的id");
            $table->string('question_title', 100)->comment("被举报的答案所属问题标题");
            $table->unsignedBigInteger('answer_id')->index()->comment("被举报的答案的id");
            $table->unsignedBigInteger('answer_user_id')->index()->comment("答案所属人id");
            $table->mediumText('answer_content')->comment("评论内容");
            $table->unsignedBigInteger('report_user_id')->index()->comment("举报人ID");
            $table->string('report_type', 100)->default('')->comment("举报类型");
            $table->text('report_descrption')->comment('举报描述');
            $table->tinyInteger('status')->default(0)->comment('复议核销状态[0=举报中，1=举报通过，2=举报撤销]');
            $table->string('report_exam_message', 500)->default('')->comment("审核意见");
            $table->timestamp('examed_at')->nullable()->comment('审核时间');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_report_answers` comment '知乎应用-评论被举报表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_report_answers');
    }
}
