<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeZhihuUserTableClomunsChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            $table->string('solt', 100)->comment('密码加盐')->after('password');
            $table->dropColumn('email_verified_pass');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zhihu_users', function (Blueprint $table) {
            //
        });
    }
}
