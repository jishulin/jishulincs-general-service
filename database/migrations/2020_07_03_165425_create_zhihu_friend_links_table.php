<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZhihuFriendLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zhihu_friend_links', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('tags', 100)->default('')->comment('标识');
            $table->string('name', 100)->default('')->comment('链接名称');
            $table->longText('imagesrc')->comment('链接图片');
            $table->string('links', 500)->default('')->comment('跳转链接');
            $table->text('content')->comment("描述备注");
            $table->integer('startdate')->default(0)->comment("起始时间");
            $table->integer('enddate')->default(0)->comment("截止时间");
            $table->tinyInteger('status')->default(1)->comment('状态[0=禁用，1=启用]');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE `yw_zhihu_friend_links` comment '知乎应用-友情链接表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhihu_friend_links');
    }
}
