<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class ExampleTest extends TestCase
{
    // use RefreshDatabase;
    public function testExample()
    {
        $response = $this->json('POST', '/api/test', []);
        $response
            ->assertStatus(200)
            ->assertJsonPath('aaa', 'aaa');
        // $this->assertTrue(true);
    }

    /* public function testUpload()
    {
        Storage::fake('local');
        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->json('POST', '/api/imageUploadStore', [
            'file' => $file,
        ]);
        Storage::disk('local')->assertExists($file->hashName());
        // Storage::disk('avatars')->assertMissing('missing.jpg');
    } */

    /* public function testImageUpload()
    {
        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->withHeaders([
            'uid' => '4',
            'token' => '04c801a0a71211ea9d8c4b1f66569602',
        ])->json('POST', '/api/zhihu/imageUpload', [
            'file' => $file,
        ]);
        dump($response);exit;
        $response->assertOk();
        $response->assertJsonPath('code', 10000);
    } */

    public function testGetUserList()
    {

        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->withHeaders([
            'uid' => '4',
            'token' => '04c801a0a71211ea9d8c4b1f66569602',
        ])->json('POST', '/api/zhihu/userManager/list');
        dump($response);exit;
        $response->assertOk();
        $response->assertJsonPath('code', 10000);
    }

}
