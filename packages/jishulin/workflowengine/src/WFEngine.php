<?php

namespace Jishulin\WorkFlowEngine;

use Illuminate\Config\Repository;
use Jishulin\WorkFlowEngine\Config\Config;

class WFEngine
{
    public function __construct()
    {
    }

    public function test()
    {
        $configMsg = Config::VERSION;
        return "config:" . $configMsg;
    }
}
