<?php
namespace Jishulin\WorkFlowEngine\Config;

final class Config
{
    const VERSION = '1.0.0';
    const REDISPRIFIX = 'jishulin_workflow_';
    const EXAMRESNAME = [
        '0' => '不通过',
        '1' => '通过',
        '2' => '回退'
    ];
    const CURRENTNODE = '当前节点';
    const JOBSTATUS = '任务状态';
    const JOBSTATUSNAME = [
        '-100' => '待发起',
        '1' => '执行中',
        '100' => '待决策',
    ];
    const JOBCLOMUNSHEADER = [
        ['value' => '字段名', 'width' => 20],
        ['value' => '字段描述', 'width' => 20],
        ['value' => '字段类型', 'width' => 20],
        ['value' => '代码类型', 'width' => 20],
        ['value' => '系统编码', 'width' => 20],
        ['value' => '公司编码', 'width' => 20],
    ];
    const JOBCLOMUNSCODETYPE = [
        '1' => '系统字典',
        '2' => '公司字典',
    ];
    const CUSTOMERNAME = '客户列表';
    const SKIPJOB = ['divider'];
    const ERRORMSG = '操作失败';
    const SUCCESSMSG = '操作成功';
    const ERRORFLOW = '流程不存在';
    const ERRORBIRT = '报表不存在';
    const ERROR_FLOW_CANNOT_NODE = '流程还未绘制，不允许启动';
    const ERROR_PARAMS = '参数异常';
    const ERROR_EMPTY_FLOW = '流程ID或流程名不能为空';
    const ERROR_EMPTY_NODE = '节点数据不能为空';
    const ERROR_EMPTY_LINK = '节点连接不能为空';
    const ERROR_FROMNOTEXISTS = '表单不存在';
    const TRANSPDFERROR = '转换PDF失败';
    const TRANSPDFSUCCESS = '转换PDF成功';

    const JOBTABLEEXISTS = '业务表已经存在了';
    const JOBTABLERULEERROR = '业务表规则不正确，必须以[zgjob]开头';
    const NAMECHS = '姓名';
    const NUMBERCHS = '订单号';
    const KEYWORCHS = '快捷搜索';
    const KEYWORDHOLDERCHS = '姓名/订单号';
    const JOBCREATEDCHS = '新建';
    const JOBEXAMEDCHS = '审核';
    const JOBSUBMITEDCHS = '提交';
    const JOBEXPORTEDCHS = '导出';
    const JOBPRINTEDCHS = '打印';
    const ERRORFILENOTEXISTS = '文件不存在';
    const BIRTLISTERROR = '导出列表已经配置，但没有配置模板';
    const ERRORFILEFORMAT = '导出模板文件格式不正确，只允许上传excel文件，且后缀为xls或xlsx';

    // 流程相关
    const PROCESSHASOPT = '该订单已经被操作过了，请返回页面刷新...';
    const PROCESSLOCKED = '该订单号已被锁定，请@@@TTL@@@秒之后再试...';
    const PROCESSMETHODNOTEXISTS = '方法名不存在【@@@METHODNAME@@@】';
    const PROCESSCLASSNOTEXISTS = '类不存在【@@@CLASSNAME@@@】';
    const PROCESSERRORINJOB = '自定义前后置方法出现错误，请确认';
    const PROCESSCRDJOBERROR = '数据保存出错';
    const PROCESSPARAMSERROR = '参数异常';
    const PROCESSCURRENTNOTEXISTS = '当前流程节点不存在';
    const PROCESSREQUIREDFAILED = '订单未填写完整不允许提交';
    const PROCESSSYSEXAMPASS = '非审核节点，系统默认通过';
    const PROCESSNOTEXITS = '进程不存在，请确认';
    const PROCESSIGEAL = '无对应的进程状态，请确认';
    const PROCESSEXAMCONFIGERROR = '请先配置审核节点信息';
    const PROCESSEXAMNOT = '订单还未审核，请确认';
    const PROCESSEXAMREBACKERROR = '该订单审核退回异常，请重新审核';
    const PROCESSNOTEXITSEXAMTYPE = '请确认审核结果类型，只支持[0=不通过，1=通过，2=退回]';
    const PROCESSCURRENTNOTOFNEXTNOTEXIST = '当前节点审核到下一节点不存在，请确认';
    const PROCESSERRORWORKFLOW = '流程网关设计异常';
    const PROCESSERRORG = '异常合流节点';
    const PROCESSVERIFYRULEERROR = '规则校验不通过';

    // 统计相关
    const JOBTOP = 6;
    const JOBTOTALCREATED = '录单总数';
    const JOBTOTALCOMPLETED = '成单总数';
    const JOBTOTALSCRAP = '废单总数';
    const JOBTOTALWAIT = '待定总数';

    const JOBTODAYCREATED = '今日录单';
    const JOBTODAYCOMPLETED = '今日成单';
    const JOBTODAYSCRAP = '今日废单';
    const JOBTODAYWAIT = '今日待定';
    const JOBWEEKCREATED = '本周录单';
    const JOBWEEKCOMPLETED = '本周成单';
    const JOBWEEKSCRAP = '本周废单';
    const JOBWEEKWAIT = '本周待定';
    const JOBMONTHCREATED = '本月录单';
    const JOBMONTHCOMPLETED = '本月成单';
    const JOBMONTHSCRAP = '本月废单';
    const JOBMONTHWAIT = '本月待定';
    const JOBYEARCREATED = '年度录单';
    const JOBYEARCOMPLETED = '年度成单';
    const JOBYEARSCRAP = '年度废单';
    const JOBYEARWAIT = '年度待定';

    const JOBTODAYCREATEDTRANS = '同比昨日录单：';
    const JOBTODAYCOMPLETEDTRANS = '同比昨日成单：';
    const JOBTODAYSCRAPTRANS = '同比昨日废单：';
    const JOBWEEKCREATEDTRANS = '同比上周录单';
    const JOBWEEKCOMPLETEDTRANS = '同比上周成单';
    const JOBWEEKSCRAPTRANS = '同比上周废单';
    const JOBMONTHCREATEDTRANS = '同比上月录单';
    const JOBMONTHCOMPLETEDTRANS = '同比上月成单';
    const JOBMONTHSCRAPTRANS = '同比上月废单';
    const JOBYEARCREATEDTRANS = '同比上年录单';
    const JOBYEARCOMPLETEDTRANS = '同比上年成单';
    const JOBYEARSCRAPTRANS = '同比上年废单';

    const JOBTOTALCOMPLETEDCOLON = '成单总数：';
    const JOBTOTALCOMPLETEDRATIO = '成单比率：';
    const JOBTOTALCOMPLETEDRANKING = '排名：';
    const JOBTOTALCOMPLETEDFLOW = '成单量：';
    const JOBTOTALCREATEDFLOW = '录单量：';
    const JOBTOTALSCRAPFLOW = '废单量：';
    const JOBCURRENTNODEORFDERS = '当前业务订单数：';
    const JOBNODEPASSRATIO = '当前通过率：';
    const JOBNODEUNPASSRATIO = '当前拒单率：';
    const JOBNODEBACKRATIO = '当前回退率：';
    const JOBWORKDETAIL = '业务详情(录单，成单，废单)';
    const JOBFLOWEXAMPASS = '通过率：';
    const JOBFLOWEXAMUNPASS = '拒单率：';
    const JOBFLOWEXAMREBACK = '回退率：';
    const JOBORDERSNUM = '当前业务数：';
    const JOBTRANSCHARTS = '同比新增率';
    const JOBTRANSSUB = '(负增长)';
}
