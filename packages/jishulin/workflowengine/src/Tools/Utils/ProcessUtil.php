<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Jishulin\WorkFlowEngine\Config\Config;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;

class ProcessUtil
{
    public static function getProcessesId($flowId, $nodeId, $jobId)
    {
        return DB::table('wf_workprocesses')
            ->where('wf12001', $flowId)
            ->where('wf12004', $nodeId)
            ->where('wf12002', $jobId)
            ->value('id');
    }

    public static function systemDefaultFabsExecute($tag, $flowId, $nodeId, $jobId, $list, $apiUser)
    {
        $map['flowId'] = $flowId;
        $map['nodeId'] = $nodeId;
        $map['jobId'] = $jobId;
        $map['data'] = $list;
        $map['apiUser'] = $apiUser;

        $res = DB::table('wf_workfbas')
            ->where('wf99001', $flowId)
            ->where('wf99002', $nodeId)
            ->where('wf99003', 1)
            ->where('wf88006', $tag)
            ->select(['wf99004 as ctl', 'wf99005 as act'])
            ->get()
            ->toArray();
        if (empty($res)) return ['error' => 0];

        return static::executeJobFbasAction($res, $map);
    }

    protected static function executeJobFbasAction($jsonData, $params)
    {
        // 方法校验
        foreach ($jsonData as $data) {
            $data =(array)$data;
            $verfiyResult = static::functionVerify($data['ctl'], $data['act']);
            if ($verfiyResult['error']) return $verfiyResult;
        }

        // 方法执行
        foreach ($jsonData as $exec) {
            $exec =(array)$exec;
            $class = new $exec['ctl']();
            $method = $exec['act'];
            $res = $class->$method($params);
            if ($res['error']) {
                return ['error' => 1, 'message' => $res['message'] ?? Config::PROCESSERRORINJOB];
            }
        }

        return ['error' => 0];
    }

    protected static function functionVerify($space, $fun)
    {
        try {
            $object = new \ReflectionClass($space);
            $methods = $object->getMethods();
            $assert = 0;
            foreach ($methods as $method) {
                if ($method->getName() == $fun) {
                    $assert = 1;
                    break;
                }
            }

            if (!$assert) {
                return ['error' => 1, 'message' => str_replace('@@@METHODNAME@@@', $fun, Config::PROCESSMETHODNOTEXISTS)];
            }
            return ['error' => 0];
        } catch (\Exception $e) {
            return ['error' => 1, 'message' => str_replace('@@@CLASSNAME@@@', $space, Config::PROCESSCLASSNOTEXISTS)];
        }
    }

    public static function generateNumberRuleByFlowId($flowId)
    {
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'ordernumber')
            ->where('wf08004', 1)
            ->value('wf08005');
        if (!empty($globalConfig)) {
            $globalConfig = json_decode($globalConfig, true);
            $type = $globalConfig['type'] ?? '';
            $number = '';
            switch ($type) {
                case 'sysnum':
                    $number = Util::generateSystemNumber();
                    break;
                case 'sysstr':
                    $number = strtoupper(Uuid::uuid1()->getHex());
                    break;
                case 'syspoint':
                    $prefix = $globalConfig['prefix'] ?? '';
                    $connect = ['0' => '', '1' => '-', '2' => '_'];
                    $connectp = $connect[($globalConfig['connectp'] ?? '0')];
                    $date = $globalConfig['date'] ?? 'ymd';
                    $connectd = $connect[($globalConfig['connectd'] ?? '0')];
                    $randnum = $globalConfig['randnum'] ?? 3;

                    $strdate = '';
                    $strsingle = '';
                    $strtype = '';
                    switch ($date) {
                        case 'ymd':
                            $strdate = date('Ymd');
                            $strsingle = date('d');
                            $strtype = '0';
                            break;
                        case 'y-m-d':
                            $strdate = date('Y-m-d');
                            $strsingle = date('d');
                            $strtype = '0';
                            break;
                        case 'y_m_d':
                            $strdate = str_replace('-', '_', date('Y-m-d'));
                            $strsingle = date('d');
                            $strtype = '0';
                            break;
                        case 'ym':
                            $strdate = date('Ym');
                            $strsingle = date('m');
                            $strtype = '1';
                            break;
                        case 'y-m':
                            $strdate = date('Y-m');
                            $strsingle = date('m');
                            $strtype = '1';
                            break;
                        case 'y_m':
                            $strdate = str_replace('-', '_', date('Y-m'));
                            $strsingle = date('m');
                            $strtype = '1';
                            break;
                    }

                    $redisKey = Config::REDISPRIFIX . 'number_' . $flowId;
                    $redisData = [];
                    $strnum = '';
                    if (Redis::exists($redisKey)) {
                        $redisData = json_decode(Redis::get($redisKey), true);
                    }
                    if ($redisData) {
                        if (
                            $redisData['strtype'] == $strtype &&
                            $redisData['strsingle'] == $strsingle &&
                            $redisData['randnum'] == $randnum
                        ) {
                            $strnum = str_pad((int)$redisData['strnum'] + 1, $randnum, '0', STR_PAD_LEFT);
                        } else {
                            $strnum = str_pad('1', $randnum, '0', STR_PAD_LEFT);
                        }
                    } else {
                        $strnum = str_pad('1', $randnum, '0', STR_PAD_LEFT);
                    }

                    Redis::set($redisKey, json_encode([
                        'strtype' => $strtype,
                        'strsingle' => $strsingle,
                        'randnum' => $randnum,
                        'strnum' => (int)$strnum,
                    ]));

                    $number = $prefix . $connectp . $strdate . $connectd . $strnum;
                    break;
                default:
                    $number = Util::generateSystemNumber();
                    break;
            }

            return $number;
        }

        return Util::generateSystemNumber();
    }

    public static function createJobGetId($table, $data)
    {
        return DB::table($table)->insertGetId($data);
    }

    public static function createJob($table, $data)
    {
        $insertData = [];
        foreach ($data as $rmk => $rmv) {
            $list = [];
            foreach ($rmv as $k => $v) {
                if (strpos($k, "$") === 0) continue;
                if ($k === 'index') continue;
                $list[$k] = $v === null ? '' : $v;
            }
            $insertData[] = $list;
            // if (strpos($rmk, "$") === 0) continue;
            // $insertData[$rmk] = $rmv === null ? '' : $rmv;
        }
        return DB::table($table)->insertOrIgnore($insertData);
    }

    public static function updateJobByPrimary($primary, $table, $data)
    {
        return DB::table($table)->where('id', $primary)->update($data);
    }

    public static function getJobFieldByPrimary($primary, $table, $field)
    {
        return DB::table($table)->where('id', $primary)->value($field);
    }

    public static function getFieldByUniqid($table, $where, $field)
    {
        return DB::table($table)->where($where)->value($field);
    }

    public static function getSubJobDataByPid($table, $pid)
    {
        return DB::table($table)->where('PID', $pid)->get()->toArray();
    }

    public static function getJobFieldsByPrimary($primary, $table, $fields = null)
    {
        $query = DB::table($table)->where('id', $primary);
        if (!is_null($fields)) {
            $query->select($fields);
        }

        return (array)$query->first();
    }

    public static function dealWithSubJobData($table, $data, $pid)
    {
        // 获取原始存在数据记录
        $originId = DB::table($table)->where('PID', $pid)->pluck('id')->toArray();

        $hasExistsId = [];
        foreach ($data as $k => $v) {
            $primary = $v['id'] ?? 0;
            if ($primary) {
                array_push($hasExistsId, $primary);
                unset($v['id']);
            }
            $vFinal = [];
            foreach ($v as $rmk => $rmv) {
                if (strpos($rmk, "$") === 0) continue;
                if ($rmk === 'index') continue;
                $vFinal[$rmk] = $rmv === null ? '' : $rmv;
            }
            $v = $vFinal;
            $primary ?
                DB::table($table)->where('id', $primary)->update($v) :
                DB::table($table)->insert($v);
        }
        $diffId = array_values(array_diff($originId, $hasExistsId));
        if (count($diffId)) {
            DB::table($table)->whereIn('id', $diffId)->delete();
        }
    }

    public static function getJobColumnSystemCode($flowId, $table)
    {
        return DB::table('wf_workjobcolumns')
            ->where('wf06001', $flowId)
            ->where('wf06002', $table)
            ->where('wf06008', 1)
            ->pluck('wf06003')
            ->toArray();
    }

    // $tag = 0表示只获取自定义表不含系统内置表，1=表示获取含内置表，2表示获取内置表，0和1表示不含主表
    // ，-1表示获取全部数据表，-2表示获取不含内置表其他表
    public static function getSubJobTables($flowId, $tag = 0)
    {
        $query = DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05005', 1);
        if ($tag === 0) {
            $query->where('wf05004', 0)->where('wf05006', 0);
        } elseif ($tag === 1) {
            $query->where('wf05004', 0);
        } elseif ($tag === 2) {
            $query->where('wf05004', 0)->where('wf05006', 1);
        } elseif ($tag === -1) {

        } elseif ($tag === -2) {
            $query->where('wf05006', 0);
        }

        return $query->pluck('wf05003')->toArray();
    }

    public static function getMainJobTableByFlowId($flowId)
    {
        return DB::table('wf_workjobtables')
            ->where('wf05001', $flowId)
            ->where('wf05004', 1)
            ->where('wf05005', 1)
            ->value('wf05003');
    }

    public static function createProcess($processData)
    {
        $processKeys = [
            'flowId' => 'wf12001', 'jobId' => 'wf12002', 'companyId' => 'wf12003',
            'nodeId' => 'wf12004', 'nextNodeId' => 'wf12005', 'decisionTag' => 'wf12006',
            'operatorId' => 'wf12007', 'operator' => 'wf12008', 'operatorDate' => 'wf12009',
            'ownerId' => 'wf12010', 'owner' => 'wf12011'
        ];

        $saveData = [];
        foreach ($processKeys as $key => $val) {
            if (!isset($processData[$key]) || $processData[$key] === '') {
                return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];
            }
            $saveData[$val] = $processData[$key];
        }
        $saveData['created_at'] = Carbon::now();

        DB::table('wf_workprocesses')->insert($saveData);

        return ['error' => 0];
    }

    public static function getProcessIdByJob($flowId, $nodeId, $jobId)
    {
        return DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12002', $jobId)->where('wf12004', $nodeId)->value('id');
    }

    public static function judgeCurrentNodeIsExam($flowId, $nodeId)
    {
        return DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $nodeId)->value('wf02005');
    }

    public static function getFlowNodeName($flowId, $nodeId)
    {
        return DB::table('wf_worknodes')
            ->where('wf02001', $flowId)
            ->where('wf02002', $nodeId)
            ->value('wf02003');
    }

    public static function getWorkLinksForExam($flowId, $nodeId)
    {
        return DB::table('wf_worklinks')
            ->select(['wf03003 as toId', 'wf03008 as step'])
            ->where('wf03001', $flowId)
            ->where('wf03002', $nodeId)
            ->where('wf03006', 0)
            ->get()
            ->toArray();
    }

    public static function jobProcessSystemExamRecordSet($flowId, $nodeId, $jobId, $examData)
    {
        // 获取审核记录表
        $table = DB::table('wf_workjobtables')
            ->where('wf05005', 1)
            ->where('wf05001', $flowId)
            ->where('wf05006', 1)
            ->where('wf05007', 1)
            ->value('wf05003');
        if (!$table) return true;
        // 获取组装审核基类配置字段信息
        $fieldFill = (array)DB::table('wf_worksysexams')
            ->where('wf09001', $flowId)
            ->where('wf09009', 1)
            ->first();
        if (empty($fieldFill)) return true;

        // 获取节点名称
        $nodeName = static::getFlowNodeName($flowId, $nodeId);
        // DB::table('wf_worknodes')
        //     ->where('wf02001', $flowId)
        //     ->where('wf02002', $nodeId)
        //     ->value('wf02003');
        // 审核数据记录
        $insertData = [
            'PID' => $jobId,
            $fieldFill['wf09002'] => $examData['examResult'],
            $fieldFill['wf09003'] => Carbon::now(),
            $fieldFill['wf09004'] => $examData['examReason'],
            $fieldFill['wf09005'] => $examData['examId'],
            $fieldFill['wf09006'] => $examData['examName'],
            $fieldFill['wf09007'] => $nodeName,
            $fieldFill['wf09008'] => $examData['examTag'] ?? 0,
        ];

        DB::table($table)->insert($insertData);

        return true;
    }

    public static function jobProcessSystemNodeTimeRecordSet($flowId, $nodeId, $jobId, $data)
    {
        // 获取审核记录表
        $table = DB::table('wf_workjobtables')
            ->where('wf05005', 1)
            ->where('wf05001', $flowId)
            ->where('wf05006', 1)
            ->where('wf05008', 1)
            ->value('wf05003');
        if (!$table) return true;

        $fieldFill = (array)DB::table('wf_worksysnodetimes')
            ->where('wf10001', $flowId)
            ->where('wf10008', 1)
            ->first();
        if (empty($fieldFill)) return true;
        // 获取节点名称
        $nodeName = static::getFlowNodeName($flowId, $nodeId);
        // DB::table('wf_worknodes')
        //     ->where('wf02001', $flowId)
        //     ->where('wf02002', $nodeId)
        //     ->value('wf02003');

        $model = (array)DB::table($table)
            ->where('PID', $jobId)
            ->where($fieldFill['wf10002'], $nodeId)
            ->first();
        if (!$model) {
            $insertData = [
                'PID' => $jobId,
                $fieldFill['wf10002'] => $nodeId,
                $fieldFill['wf10003'] => $nodeName,
                $fieldFill['wf10004'] => Carbon::now(),
                $fieldFill['wf10005'] => Carbon::now(),
                $fieldFill['wf10006'] => $data['optId'],
                $fieldFill['wf10007'] => $data['optName']
            ];
            DB::table($table)->insert($insertData);
        } else {
            DB::table($table)->where('id', $model['id'])->update([
                $fieldFill['wf10005'] => Carbon::now(),
                $fieldFill['wf10006'] => $data['optId'],
                $fieldFill['wf10007'] => $data['optName']
            ]);
        }

        return true;
    }

    public static function jobProcessExamDealSave($flowId, $nodeId, $jobId, $exam, $apiUser)
    {
        // 获取配置审核信息
        $examConfig = DB::table('wf_worknodecolumns')
            ->where('wf07001', $flowId)
            ->where('wf07002', $nodeId)
            ->value('wf07005');
        if (!$examConfig) return ['error' => 1, 'message' => Config::PROCESSEXAMCONFIGERROR];

        // 获取主表信息
        $table = static::getMainJobTableByFlowId($flowId);
        // DB::table('wf_workjobtables')
        //     ->where('wf05005', 1)
        //     ->where('wf05001', $flowId)
        //     ->where('wf05004', 1)
        //     ->value('wf05003');

        // 更新主表信息字段
        $examResult = (int)$exam['examResult'];
        $examReason = $exam['examReason'] ?? '';

        $examConfig = json_decode($examConfig, true);
        $updateData[$examConfig['examResult']] = $examResult;
        $updateData[$examConfig['examDate']] = Carbon::now();
        $updateData[$examConfig['examReason']] = $examReason;
        $updateData[$examConfig['examPerson']] = $apiUser['realname'] ?? '';
        static::updateJobByPrimary($jobId, $table, $updateData);
        // 审核退回
        $redisKey = Config::REDISPRIFIX . 'exam_' . $flowId . '_' . $nodeId . '_' . $jobId;
        if ($examResult == 2) {
            Redis::set($redisKey, $exam['examReback']);
        // 去除缓存记录信息
        } elseif (Redis::exists($redisKey)) {
            Redis::del($redisKey);
        }

        return ['error' => 0, 'data' => ['mainTable' => $table]];
    }

    public static function jobProcessExamCheck($flowId, $nodeId, $jobId)
    {
        $examConfig = DB::table('wf_worknodecolumns')
            ->where('wf07001', $flowId)
            ->where('wf07002', $nodeId)
            ->value('wf07005');
        if (!$examConfig) return ['error' => 1, 'message' => Config::PROCESSEXAMCONFIGERROR];
        // 获取主表信息
        $table = static::getMainJobTableByFlowId($flowId);
        // DB::table('wf_workjobtables')
        //     ->where('wf05005', 1)
        //     ->where('wf05001', $flowId)
        //     ->where('wf05004', 1)
        //     ->value('wf05003');
        $examConfig = json_decode($examConfig, true);
        $examReback = '';
        if (isset($examConfig['examResult']) && $examConfig['examResult']) {
            $examResult = static::getJobFieldByPrimary($jobId, $table, $examConfig['examResult']);
            if ($examResult == '' || $examResult == null) return ['error' => 1, 'message' => Config::PROCESSEXAMNOT];
            if ($examResult == 2) {
                $redisKey = Config::REDISPRIFIX . 'exam_' . $flowId . '_' . $nodeId . '_' . $jobId;
                if (!Redis::exists($redisKey)) {
                    return ['error' => 1, 'message' => Config::PROCESSEXAMREBACKERROR];
                } else {
                    $examReback = Redis::get($redisKey);
                }
            }
        } else {
            return ['error' => 1, 'message' => Config::PROCESSEXAMCONFIGERROR];
        }

        $jobData = static::getJobFieldsByPrimary($jobId, $table, [
            $examConfig['examResult'],
            $examConfig['examReason'],
            $examConfig['examDate'],
            $examConfig['examPerson'],
        ]);

        return ['error' => 0, 'data' => [
            'value' => [
                'mainTable' => $table,
                'examResult' => $jobData[$examConfig['examResult']],
                'examReason' => $jobData[$examConfig['examReason']],
                'examDate' => $jobData[$examConfig['examDate']],
                'examPerson' => $jobData[$examConfig['examPerson']],
                'examReback' => $examReback
            ],
            'key' => [
                'examResult' => $examConfig['examResult'],
                'examReason' => $examConfig['examReason'],
                'examDate' => $examConfig['examDate'],
                'examPerson' => $examConfig['examPerson'],
            ],
        ]];
    }

    public static function jobProcessHistoriesRecords($flowId, $jobId, $processData, $examData)
    {
        // 判断是否启用历史记录
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemworkflow')
            ->where('wf08004', 1)
            ->value('wf08005');

        if (empty($globalConfig)) return;
        $globalConfig = json_decode($globalConfig, true);
        if (isset($globalConfig['history']) && $globalConfig['history'] == 1) {
            // 获取主表及业务数据
            $table = static::getMainJobTableByFlowId($flowId);
            // DB::table('wf_workjobtables')
            //     ->where('wf05005', 1)
            //     ->where('wf05001', $flowId)
            //     ->where('wf05004', 1)
            //     ->value('wf05003');
            $mainData = (array)DB::table($table)->where('id', $jobId)->first();

            $jsonData = [];
            $jsonData['NUMBER'] = $mainData['NUMBER'];
            $jsonData['NAME'] = $mainData['NAME'];
            $jsonData['FLOWID'] = $flowId;
            $jsonData['NODEID'] = $processData['wf12004'];
            $jsonData['JOBID'] = $jobId;
            $jsonData['COMPANYID'] = $processData['wf12003'];
            $jsonData['ORDERBELONGID'] = $processData['wf12010'];
            $jsonData['ORDERBELONG'] = $processData['wf12011'];
            $jsonData['EXAMID'] = $examData['examId'];
            $jsonData['EXAMNAME'] = $examData['examName'];
            $jsonData['EXAMDATE'] = Carbon::now();
            $jsonData['EXAMRESULT'] = $examData['examResult'];
            $jsonData['EXAMREASON'] = $examData['examReason'];
            $jsonData['OPTID'] = $processData['wf12007'];
            $jsonData['OPTNAME'] = $processData['wf12008'];
            $jsonData['created_at'] = Carbon::now();

            $JOBDATA = [];
            $JOBDATA['main'][$table] = $mainData;
            $subTables = static::getSubJobTables($flowId, 1);
            foreach ($subTables as $k => $v) {
                $JOBDATA[$v] = DB::table($v)->where('PID', $jobId)->get()->toArray();
            }
            $jsonData['JOBDATA'] = json_encode($JOBDATA);

            return DB::table('wf_workhistories')->insert($jsonData);
        }

        return;
    }

    public static function jobProcessOverRecords($processData, $overtype)
    {
        $jsonData = [];
        $jsonData['FLOWID'] = $processData['wf12001'];
        $jsonData['NODEID'] = $processData['wf12004'];
        $jsonData['JOBID'] = $processData['wf12002'];
        $jsonData['COMPANYID'] = $processData['wf12003'];
        $jsonData['ORDERBELONGID'] = $processData['wf12010'];
        $jsonData['ORDERBELONG'] = $processData['wf12011'];
        $jsonData['OVERTYPE'] = $overtype;
        $jsonData['created_at'] = Carbon::now();

        return DB::table('wf_workprocessovers')->insert($jsonData);
    }

    public static function getProcessQueryFieldsAlias()
    {
        return [
            'wf12001 as flowId', 'wf12002 as jobId', 'wf12003 as companyId',
            'wf12004 as nodeId', 'wf12005 as nextNodeId', 'wf12006 as decisionTag',
            'wf12007 as operatorId', 'wf12008 as operator', 'wf12009 as operatorDate',
            'wf12010 as ownerId', 'wf12011 as owner', 'wf_workprocesses.id as processId'
        ];
    }

    public static function getJobQueryFields($flowId, $nodeId)
    {
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemname')
            ->where('wf08004', 1)
            ->value('wf08005');

        $gridColumns = [];
        if (empty($globalConfig)) {
            $gridColumns = ['NUMBER', 'NAME'];
        } else {
            $globalConfig = json_decode($globalConfig, true);
            if ($globalConfig['numberstatus']) {
                array_push($gridColumns, 'NUMBER');
            }
            if ($globalConfig['namestatus']) {
                array_push($gridColumns, 'NAME');
            }
        }
        // 获取配置列
        $nodeColumnsConfig = DB::table('wf_worknodecolumns')
            ->where('wf07001', $flowId)
            ->where('wf07002', $nodeId)
            ->value('wf07004');
        if ($nodeColumnsConfig) {
            $nodeColumnsConfig = json_decode($nodeColumnsConfig, true);
            foreach ($nodeColumnsConfig as $k => $v) {
                array_push($gridColumns, $v['key']);
            }
        }

        return $gridColumns;
    }

    public static function dealWorkAnalysisSearchConditoin(&$condition, $searchArr)
    {
        $AND = 'AND';
        $seachHas = [];
        foreach ($searchArr as $key => $val) {
            if ($val[0] == 'keyword') continue;
            if (!in_array($val[0], $seachHas)) {
                switch ($val[1]) {
                    case '≠':
                        $condition[] = [$val[0], '<>', $val[2], $AND];
                        break;
                    case 'like':
                        $condition[] = [$val[0], 'like', "%$val[2]%", $AND];
                        break;
                    case '>':
                        $condition[] = [$val[0], '>', $val[2], $AND];
                        break;
                    case '≥':
                        $condition[] = [$val[0], '>=', $val[2], $AND];
                        break;
                    case '<':
                        $condition[] = [$val[0], '<', $val[2], $AND];
                        break;
                    case '≤':
                        $condition[] = [$val[0], '<=', $val[2], $AND];
                        break;
                    case '∈':
                        $condition[] = [DB::raw("{$val[0]} in ({$val[2]})"), '1'];
                        break;
                    default:
                        $condition[] = [$val[0], '=', $val[2], $AND];
                        break;
                }
                array_push($seachHas, $val[0]);
            }
        }

        return $condition;
    }

    public static function getJobListForBirt($flowId, $nodeId, $apiUser, $fields = null)
    {
        // 获取主表信息
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);

        $where[] = ['wf12001', '=', $flowId, 'AND'];
        $where[] = ['wf12003', '=', $apiUser['company_id'] ?? 0, 'AND'];
        $where[] = ['wf12004', '=', $nodeId, 'AND'];
        if (is_null($fields) || empty($fields)) {
            $fields = $table . '.*';
            $columns = static::getJobColumnSystemCode($flowId, $table);
        } else {
            $columns = $fields;
        }

        $listData = DB::table('wf_workprocesses')
            ->select($fields)
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->get()
            ->toArray();

        $codes = static::getBeTranslateSystemCode($flowId, $table, $columns);
        if (count($codes)) static::jobTranslateCodeToName($listData, $codes);

        return $listData;
    }

    public static function getJobTodoListByWorkStep(
        $offest,
        $limit,
        $flowId,
        $nodeId,
        $keyword,
        $search,
        $apiUser
    )
    {
        // 获取主表信息
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);
        // where条件处理
        $orWhere = [];
        $where[] = ['wf12001', '=', $flowId, 'AND'];
        $where[] = ['wf12003', '=', $apiUser['company_id'] ?? 0, 'AND'];
        $where[] = ['wf12004', '=', $nodeId, 'AND'];
        if ($keyword) {
            $orWhere[] = ['NAME', 'like', "%{$keyword}%", 'AND'];
            $orWhere[] = ['NUMBER', 'like', "%{$keyword}%", 'OR'];
        }

        if ($search) {
            $where = static::dealWorkAnalysisSearchConditoin($where, $search);
        }

        // 获取进程字段信息
        $queryProcessFields = static::getProcessQueryFieldsAlias();
        $queryJobFields = static::getJobQueryFields($flowId, $nodeId);
        $queryFields = array_merge($queryProcessFields, $queryJobFields);
        // 需要代码处理值
        $codes = static::getBeTranslateSystemCode($flowId, $table, $queryJobFields);

        $listData = DB::table('wf_workprocesses')
            ->select($queryFields)
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })
            ->offset($offest)
            ->limit($limit)
            ->get()
            ->toArray();
        if (count($codes)) static::jobTranslateCodeToName($listData, $codes);

        $totalCount = DB::table('wf_workprocesses')
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })->count();

        $jsonData = [
            'list' => $listData,
            'count' => $totalCount
        ];

        return ['error' => 0, 'data' => $jsonData];
    }

    public static function getBeTranslateSystemCode($flowId, $table, $columns)
    {
        $pluckArr = DB::table('wf_workjobcolumns')
            ->where('wf06001', $flowId)
            ->where('wf06002', $table)
            ->whereIn('wf06003', $columns)
            ->where('wf06008', 1)
            ->pluck('wf06009', 'wf06003');

        return $pluckArr;
    }

    public static function jobTranslateCodeToName(&$listData, $codes, $single = false)
    {
        if ($single) {
            foreach ($codes as $name => $code) {
                if (!isset($listData[$name]) || $listData[$name] == '') continue;
                $vals = $listData[$name];
                $valsName = static::getFieldByUniqid('wf_codes', [
                    ['code', '=', $code, 'AND'],
                    ['vals', '=', $vals, 'AND'],
                ], 'name');
                if ($valsName != '' && !is_null($valsName)) {
                    $listData[$name] = $valsName;
                }
            }
        } else {
            foreach ($listData as $key => $val) {
                $val = (array)$val;
                foreach ($codes as $name => $code) {
                    if (!isset($val[$name]) || $val[$name] == '') continue;
                    $vals = $val[$name];
                    $valsName = static::getFieldByUniqid('wf_codes', [
                        ['code', '=', $code, 'AND'],
                        ['vals', '=', $vals, 'AND'],
                    ], 'name');
                    if ($valsName != '' && !is_null($valsName)) {
                        $val[$name] = $valsName;
                    }
                }

                $listData[$key] = $val;
            }
        }
    }

    public static function getJobFormDefaultConfig($flowId, $nodeId)
    {
        $res = (array)DB::table('wf_workforms')
            ->where('wf11001', $flowId)
            ->where('wf11002', $nodeId)
            ->where('wf11005', 1)
            ->where('wf11006', 1)
            ->first();
        if (empty($res)) {
            $res = (array)DB::table('wf_workforms')
                ->where('wf11001', $flowId)
                ->where('wf11002', $nodeId)
                ->where('wf11005', 0)
                ->where('wf11006', 1)
                ->orderBy('id', 'asc')
                ->first();
        }

        $jsonData = [];
        if (empty($res)) {
            return $jsonData;
        }

        $jsonData = empty($res['wf11004']) ? [] : json_decode($res['wf11004'], true);

        $formConfigs = DB::table('wf_workforms')
                ->where('wf11001', $flowId)
                ->where('wf11002', $nodeId)
                ->where('wf11006', 1)
                ->orderBy('wf11005', 'desc')
                ->orderBy('id', 'asc')
                ->get()
                ->toArray();
        $formConfigsTemp = [];
        foreach ($formConfigs as $key => $val) {
            $val = (array)$val;
            $formConfigsTemp[] = [
                'formId' => $val['id'],
                'formName' => $val['wf11003'],
                // 'formConfig' => empty($val['wf11004']) ? [] : json_decode($val['wf11004'], true)
            ];
        }
        $formConfigs = $formConfigsTemp;

        $finalData = [];
        $finalData['default'] = $jsonData;
        $finalData['currentConfig'] = $res ? [
            'formId' => $res['id'],
            'formName' => $res['wf11003'],
            // 'formConfig' => $jsonData
        ] : [];
        $finalData['formConfigs'] = $formConfigs;

        return $finalData;
    }

    public static function dealIllegalInput($flowId, $table, $column)
    {
        $type = DB::table('wf_workjobcolumns')
            ->where('wf06001', $flowId)
            ->where('wf06002', $table)
            ->where('wf06003', $column)
            ->value('wf06005');
        $val = '';
        switch ($type) {
            case 'text':
                $val = null;
                break;
            case 'decimal':
            case 'bigint':
            case 'int':
            case 'tinyint':
                $val = 0;
                break;
        }

        return $val;
    }

    public static function judgeNodeIsLastEnd($flowId, $nodeId)
    {
        $linksData = DB::table('wf_worklinks')
            ->where('wf03001', $flowId)
            ->where('wf03002', $nodeId)
            ->where('wf03006', 0)
            ->count();
            // ->toArray();
        return $linksData ? false : true;
        // if (count($linksData) > 1) return false;
        // if (!count($linksData)) return true;
        // if (count($linksData) == 1) {
        //     $next = $linksData[0]['wf03003'];
        //     if (DB::table('wf_worklinks')->where('wf03001', $flowId)->where('wf03002', $next)->where('wf03006', 0)->count()) {
        //         return false;
        //     } else {
        //         return true;
        //     }
        // }
    }

    public static function resetJobProcessExam($flowId, $nodeId, $jobId)
    {
        $isExam = DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $nodeId)->value('wf02005');
        if (!$isExam) return;
        $examConfig = DB::table('wf_worknodecolumns')
            ->where('wf07001', $flowId)
            ->where('wf07002', $nodeId)
            ->value('wf07005');
        if (!$examConfig) return;
        // 获取主表信息
        $table = static::getMainJobTableByFlowId($flowId);
        // DB::table('wf_workjobtables')
        //     ->where('wf05005', 1)
        //     ->where('wf05001', $flowId)
        //     ->where('wf05004', 1)
        //     ->value('wf05003');
        $examConfig = json_decode($examConfig, true);

        static::updateJobByPrimary($jobId, $table, [
            $examConfig['examResult'] => '',
            $examConfig['examDate'] => '',
            $examConfig['examPerson'] => '',
            $examConfig['examReason'] => ''
        ]);
        $redisKey = Config::REDISPRIFIX . 'exam_' . $flowId . '_' . $nodeId . '_' . $jobId;
        if (Redis::exists($redisKey)) {
            Redis::del($redisKey);
        }
        return;
    }

    public static function recordJobProcessCharts($recordChartData, $type)
    {
        switch ($type) {
            case -100:
                $where[] = ['FLOWID', '=', $recordChartData['FLOWID']];
                $where[] = ['NODEID', '=', $recordChartData['NODEID']];
                $where[] = ['JOBID', '=', $recordChartData['JOBID']];
                $where[] = ['COMPANYID', '=', $recordChartData['COMPANYID']];
                $count = DB::table('wf_workcharts')->where($where)->count();
                if ($count) {
                    $recordChartData['updated_at'] = Carbon::now();
                    DB::table('wf_workcharts')->where($where)->update($recordChartData);
                } else {
                    DB::table('wf_workcharts')->insert($recordChartData);
                }
                break;
            case 1:
                $where[] = ['FLOWID', '=', $recordChartData['FLOWID']];
                $where[] = ['NODEID', '=', $recordChartData['NODEID']];
                $where[] = ['JOBID', '=', $recordChartData['JOBID']];
                $where[] = ['COMPANYID', '=', $recordChartData['COMPANYID']];
                $count = DB::table('wf_workcharts')->where($where)->count();
                if ($count) {
                    $recordChartData['updated_at'] = Carbon::now();
                    unset($recordChartData['ISCREATE']);
                    DB::table('wf_workcharts')->where($where)->update($recordChartData);
                } else {
                    DB::table('wf_workcharts')->insert($recordChartData);
                }
                break;
            case 100:
                if ($recordChartData['ISEXAMRES'] == 2 || $recordChartData['ISEXAMRES'] == 0) {
                    DB::table('wf_workcharts')->insert($recordChartData);
                } else {
                    $where[] = ['FLOWID', '=', $recordChartData['FLOWID']];
                    $where[] = ['NODEID', '=', $recordChartData['NODEID']];
                    $where[] = ['JOBID', '=', $recordChartData['JOBID']];
                    $where[] = ['COMPANYID', '=', $recordChartData['COMPANYID']];
                    $where[] = ['ISEXAMRES', '=', $recordChartData['ISEXAMRES']];
                    $count = DB::table('wf_workcharts')->where($where)->count();
                    if ($count) {
                        $recordChartData['updated_at'] = Carbon::now();
                        DB::table('wf_workcharts')->where($where)->update($recordChartData);
                    } else {
                        DB::table('wf_workcharts')->insert($recordChartData);
                    }
                }
                break;
        }
    }

    public static function getNextNodeStep($flowId, $nodeId)
    {
        $currentNodeData = (array)DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $nodeId)->first();
        if (empty($currentNodeData)) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTEXISTS];

        $FR = $currentNodeData['wf02008'];
        $EXAM = $currentNodeData['wf02005'];

        // 获取走向数据
        $stepData = DB::table('wf_worklinks')
            ->where('wf03001', $flowId)
            ->where('wf03002', $nodeId)
            ->where('wf03006', 0)
            ->get()
            ->toArray();
        $stepCount = count($stepData);
        if (!$stepCount) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTEXISTS];
        $extra = [];
        switch ($FR) {
            // 当前节点属于分流节点
            case 'F':
                if (!$EXAM && $stepCount > 1) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTEXISTS];
                if ($EXAM) {
                    $nextNodeId = 0;
                    $extra = $stepData;
                } else {
                    $nextStep = (array)$stepData[0];
                    $nextNodeId = $nextStep['wf03003'];
                }
                $tag = 'F';
                break;
            // 当前节点属于合流节点
            case 'R':
                if (!$EXAM && $stepCount > 1) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTEXISTS];
                if ($EXAM) {
                    $nextNodeId = 0;
                    $extra = $stepData;
                } else {
                    $nextStep = (array)$stepData[0];
                    $nextNodeId = $nextStep['wf03003'];
                }
                $tag = 'R';
                break;
            default:
                if (!$EXAM && $stepCount > 1) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTEXISTS];
                if ($EXAM) {
                    $nextNodeId = 0;
                    $extra = $stepData;
                } else {
                    $nextStep = (array)$stepData[0];
                    $nextNodeId = $nextStep['wf03003'];
                }
                $tag = 'D';
                break;
        }

        return ['error' => 0, 'data' => ['nextNodeId' => $nextNodeId, 'nodeTag' => $tag, 'extra' => $extra]];
    }

    public static function jobProcessSystemSetWorkFlowStatus(
        $processId,
        $exam = 1,
        $examData = [],
        $apiUser = []
    )
    {
        // 获取进程数据
        $processData = (array)DB::table('wf_workprocesses')->where('id', $processId)->first();
        if (empty($processData)) return ['error' => 1, 'message' => Config::PROCESSNOTEXITS];
        // 业务ID
        $jobId = $processData['wf12002'];
        // 流程ID
        $flowId = $processData['wf12001'];
        // 当前节点ID
        $currentNodeId = $processData['wf12004'];
        // 初始判断进程状态[-100=初始状态，1=自动执行状态，100审核决策状态]
        $processStatus = $processData['wf12006'];
        switch ($processStatus) {
            case '-100':
                $res = static::dealWithProcessInitial(
                    $processId,
                    $jobId,
                    $flowId,
                    $currentNodeId,
                    $processData,
                    $apiUser
                );
                break;
            case '1':
                $res = static::dealWithProcessAuto(
                    $processId,
                    $jobId,
                    $flowId,
                    $currentNodeId,
                    $processData['wf12005'],
                    $processData,
                    $apiUser
                );
                break;
            case '100':
                $res = static::dealWithProcessDecising(
                    $processId,
                    $jobId,
                    $flowId,
                    $currentNodeId,
                    $exam,
                    $examData,
                    $processData,
                    $apiUser
                );
                break;
            default:
                $res = ['error' => 1, 'message' => PROCESSIGEAL];
                break;
        }

        $res['data'] = ['processData' => $processData];
        return $res;
    }

    protected static function dealWithProcessInitial(
        $processId,
        $jobId,
        $flowId,
        $currentNodeId,
        $processData,
        $apiUser = []
    )
    {
        // 获取当前节点的下一个节点信息
        $currentNodeJudgeData = static::getNextNodeStep($flowId, $currentNodeId);
        if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
        $currentNodeData = $currentNodeJudgeData['data'];
        // 录单记录
        $recordChartsData = [
            'FLOWID' => $flowId,
            'NODEID' => $currentNodeId,
            'JOBID' => $jobId,
            'COMPANYID' => $processData['wf12003'],
            'ORDERBELONGID' => $processData['wf12010'],
            'ORDERBELONG' => $processData['wf12011'],
            'ORDERDATEY' => date('Y'),
            'ORDERDATEM' => date('m'),
            'ORDERDATED' => date('d'),
            'ORDERDATE' => date('Y-m-d'),
            'ORDERDTIME' => date('Y-m-d H:i:s'),
            'OPTID' => $apiUser['id'] ?? 0,
            'OPTNAME' => $apiUser['realname'] ?? '',
            'created_at' => Carbon::now(),
            'ISEXAM' => 0,
            'ISEXAMRES' => 0,
            'ISCREATE' => 1,
        ];

        // 判断是否是直流节点还是分流节点（原始当前节点currentNodeId）
        if ($currentNodeData['nodeTag'] == 'D') {
            $nodeId = $currentNodeData['nextNodeId'];
            // 节点是否是结束节点
            $isEnd = static::judgeNodeIsLastEnd($flowId, $nodeId);

            $jobProcessEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData) {
                if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                    return ['error' => 1, 'message' => ERRORMSG];
                }
                // 录单记录
                $recordChartsData['ISCOMPLETE'] = 1;
                static::recordJobProcessCharts($recordChartsData, -100);

                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);
                // 订单结束记录
                static::jobProcessOverRecords($processData, 'common');

                return ['error' => 0];
            };

            $jobProcessNotEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $recordChartsData, $apiUser) {
                $nextNodeData = static::getNextNodeStep($flowId, $nodeId);
                if ($nextNodeData['error']) return $nextNodeData;
                $nextNodeId = $nextNodeData['data']['nextNodeId'];

                $processUpdateData['wf12004'] = $nodeId;
                $processUpdateData['wf12005'] = $nextNodeId;
                $processUpdateData['wf12006'] = $nextNodeId == '0' ? 100 : 1;
                $processUpdateData['wf12007'] = $apiUser['id'] ?? 0;
                $processUpdateData['wf12008'] = $apiUser['realname'] ?? '';
                $processUpdateData['wf12009'] = Carbon::now();
                $processUpdateData['updated_at'] = Carbon::now();
                DB::table('wf_workprocesses')->where('id', $processId)->update($processUpdateData);

                // 录单记录
                $recordChartsData['ISCOMPLETE'] = 0;
                static::recordJobProcessCharts($recordChartsData, -100);

                // 判断当前节点是否是审核节点，置空审核节点审核相关数据
                static::resetJobProcessExam($flowId, $nodeId, $jobId);

                // 流程走向记录
                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);

                return ['error' => 0];
            };

            return $isEnd ? $jobProcessEnd() : $jobProcessNotEnd();
        } elseif ($currentNodeData['nodeTag'] == 'F') {
            $nodeId = $currentNodeData['nextNodeId'];
            // 获取网关节点
            $G = $nodeId;
            // 获取分流节点走向
            $FNODES = DB::table('wf_worklinks')
                ->where('wf03001', $flowId)
                ->where('wf03002', $G)
                ->where('wf03006', 0)
                ->get()
                ->toArray();
            if (empty($FNODES)) return ['error' => 1, 'message' => Config::PROCESSERRORWORKFLOW];

            foreach ($FNODES as $key => $val) {
                $val = (array)$val;
                // 创建子进程节点
                $GCNODE = $val['wf03003'];
                $currentNodeJudgeData = static::getNextNodeStep($flowId, $GCNODE);
                if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
                $currentNodeData = $currentNodeJudgeData['data'];
                $GNNODE = $currentNodeData['nextNodeId'];
                // 分流节点不允许结束节点流程发生
                $process['flowId'] = $flowId;
                $process['jobId'] = $jobId;
                $process['companyId'] = $processData['wf12003'];
                $process['nodeId'] = $GCNODE;
                $process['nextNodeId'] = $GNNODE;
                $process['decisionTag'] = 1;
                $process['operatorId'] = $apiUser['id'] ?? 0;
                $process['operator'] = $apiUser['realname'] ?? '';
                $process['operatorDate'] = Carbon::now();
                $process['ownerId'] = $processData['wf12010'];
                $process['owner'] = $processData['wf12011'];
                $processRes = static::createProcess($process);
                if ($processRes['error']) return $processRes;

                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $GCNODE);
            }
            // 移除主进程
            if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                return ['error' => 1, 'message' => Config::ERRORMSG];
            }

            // 录单记录
            $recordChartsData['ISCOMPLETE'] = 0;
            static::recordJobProcessCharts($recordChartsData, -100);

            return ['error' => 0];
        }
    }

    protected static function dealWithProcessAuto(
        $processId,
        $jobId,
        $flowId,
        $currentNodeId,
        $nodeId,
        $processData,
        $apiUser = []
    )
    {
        $isEnd = static::judgeNodeIsLastEnd($flowId, $nodeId);

        // 录单记录
        $recordChartsData = [
            'FLOWID' => $flowId,
            'NODEID' => $currentNodeId,
            'JOBID' => $jobId,
            'COMPANYID' => $processData['wf12003'],
            'ORDERBELONGID' => $processData['wf12010'],
            'ORDERBELONG' => $processData['wf12011'],
            'ORDERDATEY' => date('Y'),
            'ORDERDATEM' => date('m'),
            'ORDERDATED' => date('d'),
            'ORDERDATE' => date('Y-m-d'),
            'ORDERDTIME' => date('Y-m-d H:i:s'),
            'OPTID' => $apiUser['id'] ?? 0,
            'OPTNAME' => $apiUser['realname'] ?? '',
            'created_at' => Carbon::now(),
            'ISEXAM' => 0,
            'ISEXAMRES' => 0,
            'ISCREATE' => 0,
        ];

        $jobProcessEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData) {
            if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                return ['error' => 1, 'message' => ERRORMSG];
            }

            $recordChartsData['ISCOMPLETE'] = 1;
            static::recordJobProcessCharts($recordChartsData, 1);

            static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);
            // 订单结束记录
            static::jobProcessOverRecords($processData, 'common');

            return ['error' => 0];
        };

        $jobProcessNotEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData, $apiUser) {
	    $currentNodeJudgeData = static::getNextNodeStep($flowId, $currentNodeId);
            if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
            $currentNodeData = $currentNodeJudgeData['data'];

            if ($currentNodeData['nodeTag'] == 'F') {
                $nodeId = $currentNodeData['nextNodeId'];
                    // 获取网关节点
                    $G = $nodeId;
                    // 获取分流节点走向
                    $FNODES = DB::table('wf_worklinks')
                        ->where('wf03001', $flowId)
                        ->where('wf03002', $G)
                        ->where('wf03006', 0)
                        ->get()
                        ->toArray();
                    if (empty($FNODES)) return ['error' => 1, 'message' => Config::PROCESSERRORWORKFLOW];

                    foreach ($FNODES as $key => $val) {
                        $val = (array)$val;
                        // 创建子进程节点
                        $GCNODE = $val['wf03003'];
                        $currentNodeJudgeData = static::getNextNodeStep($flowId, $GCNODE);
                        if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
                        $currentNodeData = $currentNodeJudgeData['data'];
                        $GNNODE = $currentNodeData['nextNodeId'];
                        // 分流节点不允许结束节点流程发生
                        $process['flowId'] = $flowId;
                        $process['jobId'] = $jobId;
                        $process['companyId'] = $processData['wf12003'];
                        $process['nodeId'] = $GCNODE;
                        $process['nextNodeId'] = $GNNODE;
                        $process['decisionTag'] = 1;
                        $process['operatorId'] = $apiUser['id'] ?? 0;
                        $process['operator'] = $apiUser['realname'] ?? '';
                        $process['operatorDate'] = Carbon::now();
                        $process['ownerId'] = $processData['wf12010'];
                        $process['owner'] = $processData['wf12011'];
                        $processRes = static::createProcess($process);
                        if ($processRes['error']) return $processRes;

                        static::jobProcessRecords($flowId, $jobId, $currentNodeId, $GCNODE);
                    }
                    // 移除主进程
                    if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                        return ['error' => 1, 'message' => Config::ERRORMSG];
                    }

                    $recordChartsData['ISCOMPLETE'] = 0;
                    static::recordJobProcessCharts($recordChartsData, 1);
            } else {
                $nextNodeData = static::getNextNodeStep($flowId, $nodeId);
                $toFirst = substr($nodeId, 0, 1);
                if ($nextNodeData['error'] && $toFirst != 'G') {
                    return $nextNodeData;
                } elseif ($toFirst != 'G') {
                    $nextNodeId = $nextNodeData['data']['nextNodeId'];
                }

                if ($toFirst == 'G') {
                    return static::jobProcessDealWorkSubG($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData, $apiUser);
                }

                $processUpdateData['wf12004'] = $nodeId;
                $processUpdateData['wf12005'] = $nextNodeId;
                $processUpdateData['wf12006'] = $nextNodeId == '0' ? 100 : 1;
                $processUpdateData['wf12007'] = $apiUser['id'] ?? 0;
                $processUpdateData['wf12008'] = $apiUser['realname'] ?? '';
                $processUpdateData['wf12009'] = Carbon::now();
                $processUpdateData['updated_at'] = Carbon::now();
                DB::table('wf_workprocesses')->where('id', $processId)->update($processUpdateData);

                $recordChartsData['ISCOMPLETE'] = 0;
                static::recordJobProcessCharts($recordChartsData, 1);

                // 判断当前节点是否是审核节点，置空审核节点审核相关数据
                static::resetJobProcessExam($flowId, $nodeId, $jobId);

                // 流程走向记录
                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);
            }

            return ['error' => 0];
        };

        return $isEnd ? $jobProcessEnd() : $jobProcessNotEnd();
    }

    protected static function dealWithProcessDecising(
        $processId,
        $jobId,
        $flowId,
        $currentNodeId,
        $exam,
        $examData,
        $processData,
        $apiUser = []
    )
    {
        $currentNodeJudgeData = static::getNextNodeStep($flowId, $currentNodeId);
        $currentNodeData = $currentNodeJudgeData['data'];

        // 录单记录
        $recordChartsData = [
            'FLOWID' => $flowId,
            'NODEID' => $currentNodeId,
            'JOBID' => $jobId,
            'COMPANYID' => $processData['wf12003'],
            'ORDERBELONGID' => $processData['wf12010'],
            'ORDERBELONG' => $processData['wf12011'],
            'ORDERDATEY' => date('Y'),
            'ORDERDATEM' => date('m'),
            'ORDERDATED' => date('d'),
            'ORDERDATE' => date('Y-m-d'),
            'ORDERDTIME' => date('Y-m-d H:i:s'),
            'OPTID' => $apiUser['id'] ?? 0,
            'OPTNAME' => $apiUser['realname'] ?? '',
            'created_at' => Carbon::now(),
            'ISEXAM' => 1,
            'ISEXAMRES' => (int)$exam,
            'ISCREATE' => 0,
        ];

        if ($currentNodeData['nodeTag'] == 'D' || $currentNodeData['nodeTag'] == 'R') {
            $stepNodeData = $currentNodeData['extra'];
            $nodeData = [];
            foreach ($stepNodeData as $key => $val) {
                $val = (array)$val;
                if ($exam == $val['wf03008']) {
                    $nodeData = $val;
                    break;
                }
            }
            if (empty($nodeData)) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTOFNEXTNOTEXIST];
            if ($exam == 2) {
                $nodeId = $examData['examReback'];
            } else {
                $nodeId = $nodeData['wf03003'];
            }

            $isEnd = static::judgeNodeIsLastEnd($flowId, $nodeId);

            $jobProcessEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $exam, $processData, $recordChartsData) {
                if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                    return ['error' => 1, 'message' => ERRORMSG];
                }

                $recordChartsData['ISCOMPLETE'] = ($exam == 0 ? 0 : 1);
                static::recordJobProcessCharts($recordChartsData, 100);

                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);
                // 订单结束记录
                static::jobProcessOverRecords($processData, ($exam == 0 ? 'error' : 'common'));
                // 黑名单处理TODO
                return ['error' => 0];
            };

            $jobProcessNotEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData, $apiUser) {
                $nextNodeData = static::getNextNodeStep($flowId, $nodeId);
                $toFirst = substr($nodeId, 0, 1);
                if ($nextNodeData['error'] && $toFirst != 'G') {
                    return $nextNodeData;
                } elseif ($toFirst != 'G') {
                    $nextNodeId = $nextNodeData['data']['nextNodeId'];
                }

                if ($toFirst == 'G') {
                    return static::jobProcessDealWorkSubG($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData, $apiUser);
                }

                $processUpdateData['wf12004'] = $nodeId;
                $processUpdateData['wf12005'] = $nextNodeId;
                $processUpdateData['wf12006'] = $nextNodeId == '0' ? 100 : 1;
                $processUpdateData['wf12007'] = $apiUser['id'] ?? 0;
                $processUpdateData['wf12008'] = $apiUser['realname'] ?? '';
                $processUpdateData['wf12009'] = Carbon::now();
                $processUpdateData['updated_at'] = Carbon::now();
                DB::table('wf_workprocesses')->where('id', $processId)->update($processUpdateData);

                $recordChartsData['ISCOMPLETE'] = 0;
                static::recordJobProcessCharts($recordChartsData, 100);

                static::resetJobProcessExam($flowId, $nodeId, $jobId);
                // 流程走向记录
                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);

                return ['error' => 0];
            };

            return $isEnd ? $jobProcessEnd() : $jobProcessNotEnd();
        } elseif ($currentNodeData['nodeTag'] == 'F') {
            $stepNodeData = $currentNodeData['extra'];
            $nodeData = [];
            foreach ($stepNodeData as $key => $val) {
                $val = (array)$val;
                if ($exam == $val['wf03008']) {
                    $nodeData = $val;
                    break;
                }
            }
            if (empty($nodeData)) return ['error' => 1, 'message' => Config::PROCESSCURRENTNOTOFNEXTNOTEXIST];
            if ($exam == 2) {
                $nodeId = $examData['examReback'];
            } else {
                $nodeId = $nodeData['wf03003'];
            }
            $isEnd = static::judgeNodeIsLastEnd($flowId, $nodeId);

            $jobProcessEnd = function () use ($processId, $flowId, $nodeId, $jobId, $currentNodeId, $exam, $processData, $recordChartsData) {
                if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                    return ['error' => 1, 'message' => Config::ERRORMSG];
                }

                $recordChartsData['ISCOMPLETE'] = ($exam == 0 ? 0 : 1);
                static::recordJobProcessCharts($recordChartsData, 100);

                static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);
                // 订单结束记录
                static::jobProcessOverRecords($processData, ($exam == 0 ? 'error' : 'common'));
                // 黑名单处理TODO
                return ['error' => 0];
            };

            $jobProcessNotEnd = function () use ($processId, $flowId, $nodeId, $jobId, $exam, $currentNodeId, $processData, $recordChartsData, $apiUser) {
                // 回退
                if ($exam == 2) {
                    $nextNodeData = static::getNextNodeStep($flowId, $nodeId);
                    if ($nextNodeData['error']) return $nextNodeData;
                    $nextNodeId = $nextNodeData['data']['nextNodeId'];
                    $processUpdateData['wf12004'] = $nodeId;
                    $processUpdateData['wf12005'] = $nextNodeId;
                    $processUpdateData['wf12006'] = $nextNodeId == '0' ? 100 : 1;
                    $processUpdateData['wf12007'] = $apiUser['id'] ?? 0;
                    $processUpdateData['wf12008'] = $apiUser['realname'] ?? '';
                    $processUpdateData['wf12009'] = Carbon::now();
                    $processUpdateData['updated_at'] = Carbon::now();
                    DB::table('wf_workprocesses')->where('id', $processId)->update($processUpdateData);

                    $recordChartsData['ISCOMPLETE'] = 0;
                    static::recordJobProcessCharts($recordChartsData, 100);

                    static::resetJobProcessExam($flowId, $nodeId, $jobId);
                    // 流程走向记录
                    static::jobProcessRecords($flowId, $jobId, $currentNodeId, $nodeId);

                    return ['error' => 0];
                } else {
                    // 获取网关节点
                    $G = $nodeId;
                    // 获取分流节点走向
                    $FNODES = DB::table('wf_worklinks')
                        ->where('wf03001', $flowId)
                        ->where('wf03002', $G)
                        ->where('wf03006', 0)
                        ->get()
                        ->toArray();
                    if (empty($FNODES)) return ['error' => 1, 'message' => Config::PROCESSERRORWORKFLOW];

                    foreach ($FNODES as $key => $val) {
                        $val = (array)$val;
                        // 创建子进程节点
                        $GCNODE = $val['wf03003'];
                        $currentNodeJudgeData = static::getNextNodeStep($flowId, $GCNODE);
                        if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
                        $currentNodeData = $currentNodeJudgeData['data'];
                        $GNNODE = $currentNodeData['nextNodeId'];
                        // 分流节点不允许结束节点流程发生
                        $process['flowId'] = $flowId;
                        $process['jobId'] = $jobId;
                        $process['companyId'] = $processData['wf12003'];
                        $process['nodeId'] = $GCNODE;
                        $process['nextNodeId'] = $GNNODE;
                        $process['decisionTag'] = 1;
                        $process['operatorId'] = $apiUser['id'] ?? 0;
                        $process['operator'] = $apiUser['realname'] ?? '';
                        $process['operatorDate'] = Carbon::now();
                        $process['ownerId'] = $processData['wf12010'];
                        $process['owner'] = $processData['wf12011'];
                        $processRes = static::createProcess($process);
                        if ($processRes['error']) return $processRes;

                        static::jobProcessRecords($flowId, $jobId, $currentNodeId, $GCNODE);
                    }
                    // 移除主进程
                    if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                        return ['error' => 1, 'message' => Config::ERRORMSG];
                    }

                    $recordChartsData['ISCOMPLETE'] = 0;
                    static::recordJobProcessCharts($recordChartsData, 100);

                    return ['error' => 0];
                }
            };

            return $isEnd ? $jobProcessEnd() : $jobProcessNotEnd();
        }
    }

    protected static function jobProcessDealWorkSubG($processId, $flowId, $nodeId, $jobId, $currentNodeId, $processData, $recordChartsData, $apiUser)
    {
        $GR = DB::table('wf_worklinks')
            ->where('wf03001', $flowId)
            ->where('wf03002', $nodeId)
            ->where('wf03006', 0)
            ->get()
            ->toArray();
        if (count($GR) != 1) return ['error' => 1, 'message' => Config::PROCESSERRORG];
        // 合流节点
        $GRTONODE = $GR[0]->wf03003;
        if ('R' != DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $GRTONODE)->value('wf02008')) {
            return ['error' => 1, 'message' => Config::PROCESSERRORG];
        }
        // 获取总合流
        $GCOUNT = DB::table('wf_worklinks')
            ->where('wf03001', $flowId)
            ->where('wf03003', $nodeId)
            ->where('wf03006', 0)
            ->count();
        // 获取当前待合流分支
        $WAITGBRANCH = DB::table('wf_workprocesses')
            ->where('wf12001', $flowId)
            ->where('wf12002', $jobId)
            ->where('wf12004', $nodeId)
            ->where('wf12006', -1)
            ->get()
            ->toArray();
        // 判断当前子节点有无完成节点
        if (count($WAITGBRANCH) == ($GCOUNT - 1)) {
            foreach ($WAITGBRANCH as $key => $val) {
                $val = (array)$val;
                // 移除子进程
                if (!DB::table('wf_workprocesses')->where('id', $val['id'])->delete()) {
                    return ['error' => 1, 'message' => Config::ERRORMSG];
                }
            }
            // 移除当前进程
            if (!DB::table('wf_workprocesses')->where('id', $processId)->delete()) {
                return ['error' => 1, 'message' => Config::ERRORMSG];
            }
            // 创建主进程
            $currentNodeJudgeData = static::getNextNodeStep($flowId, $GRTONODE);

            if ($currentNodeJudgeData['error']) return $currentNodeJudgeData;
            $currentNodeData = $currentNodeJudgeData['data'];
            $RNNODE = $currentNodeData['nextNodeId'];
            // 分流节点不允许结束节点流程发生
            $process['flowId'] = $flowId;
            $process['jobId'] = $jobId;
            $process['companyId'] = $processData['wf12003'];
            $process['nodeId'] = $GRTONODE;
            $process['nextNodeId'] = $RNNODE;
            $process['decisionTag'] = $RNNODE == '0' ? 100 : 1;
            $process['operatorId'] = $apiUser['id'] ?? 0;
            $process['operator'] = $apiUser['realname'] ?? '';
            $process['operatorDate'] = Carbon::now();
            $process['ownerId'] = $processData['wf12010'];
            $process['owner'] = $processData['wf12011'];
            $processRes = static::createProcess($process);
            if ($processRes['error']) return $processRes;

            static::resetJobProcessExam($flowId, $GRTONODE, $jobId);
            static::jobProcessRecords($flowId, $jobId, $currentNodeId, $GRTONODE);
        } else {
            $processUpdateData['wf12004'] = $nodeId;
            $processUpdateData['wf12005'] = $nodeId;
            $processUpdateData['wf12006'] = -1;
            $processUpdateData['wf12007'] = $apiUser['id'] ?? 0;
            $processUpdateData['wf12008'] = $apiUser['realname'] ?? '';
            $processUpdateData['wf12009'] = Carbon::now();
            $processUpdateData['updated_at'] = Carbon::now();
            DB::table('wf_workprocesses')->where('id', $processId)->update($processUpdateData);

            // 流程走向记录
            static::jobProcessRecords($flowId, $jobId, $currentNodeId, $GRTONODE);
        }

        $recordChartsData['ISCOMPLETE'] = 0;
        static::recordJobProcessCharts($recordChartsData, ($recordChartsData['ISEXAM'] ? 100 : 1));

        return ['error' => 0];
    }

    public static function jobProcessRecords($flowId, $jobId, $preNodeId, $nextNodeId)
    {
        return DB::table('wf_workprocessrecords')->insert([
            'wf98001' => $flowId,
            'wf98002' => $jobId,
            'wf98003' => $preNodeId,
            'wf98004' => $nextNodeId,
            'created_at' => Carbon::now()
        ]);
    }

    public static function getJobAnalysisForTotal($flowId, $companyId)
    {
        $jsonData = [];
        if (!$flowId) {
            // 录单总数
            $todayCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->count();
            $jsonData[] = ['title' => Config::JOBTOTALCREATED, 'count' => (int)$todayCreated, 'icon' => 'el-icon-document-add', 'color' => '#409EFF'];
            // 成单总数
            $todayCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->count();
            $jsonData[] = ['title' => Config::JOBTOTALCOMPLETED, 'count' => (int)$todayCompleted, 'icon' => 'el-icon-document-checked', 'color' => '#67C23A'];
            // 废单总数
            $todayScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->count();
            $jsonData[] = ['title' => Config::JOBTOTALSCRAP, 'count' => (int)$todayScrap, 'icon' => 'el-icon-document-delete', 'color' => '#F56C6C'];
            // 待定总数
            $todayWait = DB::table('wf_workprocesses')->where('wf12003', $companyId)->where('wf12006', -100)->count();
            $jsonData[] = ['title' => Config::JOBTOTALWAIT, 'count' => (int)$todayWait, 'icon' => 'el-icon-document', 'color' => '#909399'];
        } else {
            // 录单总数
            $todayCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->count();
            $jsonData[] = ['title' => Config::JOBTOTALCREATED, 'count' => (int)$todayCreated, 'icon' => 'el-icon-document-add', 'color' => '#409EFF'];
            // 成单总数
            $todayCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->count();
            $jsonData[] = ['title' => Config::JOBTOTALCOMPLETED, 'count' => (int)$todayCompleted, 'icon' => 'el-icon-document-checked', 'color' => '#67C23A'];
            // 废单总数
            $todayScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->count();
            $jsonData[] = ['title' => Config::JOBTOTALSCRAP, 'count' => (int)$todayScrap, 'icon' => 'el-icon-document-delete', 'color' => '#F56C6C'];
            // 待定总数
            $todayWait = DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->where('wf12006', -100)->count();
            $jsonData[] = ['title' => Config::JOBTOTALWAIT, 'count' => (int)$todayWait, 'icon' => 'el-icon-document', 'color' => '#909399'];
        }

        return $jsonData;
    }

    public static function getJobAnalysisForDateTotal($flowId, $companyId)
    {
        $jsonData = [];
        if (!$flowId) {
            // 当日
            $currentDate = date('Y-m-d');
            // 今日录单总数
            $todayCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYCREATED, 'count' => (int)$todayCreated, 'icon' => 'el-icon-document-add'];
            // 今日成单总数
            $todayCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYCOMPLETED, 'count' => (int)$todayCompleted, 'icon' => 'el-icon-document-checked'];
            // 今日废单总数
            $todayScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYSCRAP, 'count' => (int)$todayScrap, 'icon' => 'el-icon-document-delete'];
            // 今日待定总数
            $todayWait = DB::table('wf_workprocesses')->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) = \'' . $currentDate . '\'')->count();
            $jsonData[] = ['title' => Config::JOBTODAYWAIT, 'count' => (int)$todayWait, 'icon' => 'el-icon-document'];

            // 本周
            $week = Util::getWeekDateByCurrentTime();
            $fisrtWeek = $week[0];
            $lastWeek = $week[6];
            $weekCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKCREATED, 'count' => (int)$weekCreated, 'icon' => 'el-icon-document-add'];
            $weekCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKCOMPLETED, 'count' => (int)$weekCompleted, 'icon' => 'el-icon-document-checked'];
            $weekScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKSCRAP, 'count' => (int)$weekScrap, 'icon' => 'el-icon-document-delete'];
            $weekWait = DB::table('wf_workprocesses')->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $fisrtWeek . '\' and \'' . $lastWeek . '\'')->count();
            $jsonData[] = ['title' => Config::JOBWEEKWAIT, 'count' => (int)$weekWait, 'icon' => 'el-icon-document'];

            // 本月
            $firstMonth = date('Y-m-01', strtotime($currentDate));
            $lastMonth = date('Y-m-d', strtotime($currentDate));
            $monthCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHCREATED, 'count' => (int)$monthCreated, 'icon' => 'el-icon-document-add'];
            $monthCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHCOMPLETED, 'count' => (int)$monthCompleted, 'icon' => 'el-icon-document-checked'];
            $monthScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHSCRAP, 'count' => (int)$monthScrap, 'icon' => 'el-icon-document-delete'];
            $monthWait = DB::table('wf_workprocesses')->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $firstMonth . '\' and \'' . $lastMonth . '\'')->count();
            $jsonData[] = ['title' => Config::JOBMONTHWAIT, 'count' => (int)$monthWait, 'icon' => 'el-icon-document'];

            // 年度
            $firstYear = date('Y-01-01', strtotime($currentDate));
            $lastYear = date('Y-m-d', strtotime($currentDate));
            $yearCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARCREATED, 'count' => (int)$yearCreated, 'icon' => 'el-icon-document-add'];
            $yearCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARCOMPLETED, 'count' => (int)$yearCompleted, 'icon' => 'el-icon-document-checked'];
            $yearScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARSCRAP, 'count' => (int)$yearScrap, 'icon' => 'el-icon-document-delete'];
            $yearWait = DB::table('wf_workprocesses')->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $firstYear . '\' and \'' . $firstYear . '\'')->count();
            $jsonData[] = ['title' => Config::JOBYEARWAIT, 'count' => (int)$yearWait, 'icon' => 'el-icon-document'];
        } else {
            // 当日
            $currentDate = date('Y-m-d');
            $todayCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYCREATED, 'count' => (int)$todayCreated, 'icon' => 'el-icon-document-add'];
            $todayCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYCOMPLETED, 'count' => (int)$todayCompleted, 'icon' => 'el-icon-document-checked'];
            $todayScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $currentDate)->count();
            $jsonData[] = ['title' => Config::JOBTODAYSCRAP, 'count' => (int)$todayScrap, 'icon' => 'el-icon-document-delete'];
            $todayWait = DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) = \'' . $currentDate . '\'')->count();
            $jsonData[] = ['title' => Config::JOBTODAYWAIT, 'count' => (int)$todayWait, 'icon' => 'el-icon-document'];

            // 本周
            $week = Util::getWeekDateByCurrentTime();
            $fisrtWeek = $week[0];
            $lastWeek = $week[6];
            $weekCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKCREATED, 'count' => (int)$weekCreated, 'icon' => 'el-icon-document-add'];
            $weekCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKCOMPLETED, 'count' => (int)$weekCompleted, 'icon' => 'el-icon-document-checked'];
            $weekScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $jsonData[] = ['title' => Config::JOBWEEKSCRAP, 'count' => (int)$weekScrap, 'icon' => 'el-icon-document-delete'];
            $weekWait = DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $fisrtWeek . '\' and \'' . $lastWeek . '\'')->count();
            $jsonData[] = ['title' => Config::JOBWEEKWAIT, 'count' => (int)$weekWait, 'icon' => 'el-icon-document'];

            // 本月
            $firstMonth = date('Y-m-01', strtotime($currentDate));
            $lastMonth = date('Y-m-d', strtotime($currentDate));
            $monthCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHCREATED, 'count' => (int)$monthCreated, 'icon' => 'el-icon-document-add'];
            $monthCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHCOMPLETED, 'count' => (int)$monthCompleted, 'icon' => 'el-icon-document-checked'];
            $monthScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $jsonData[] = ['title' => Config::JOBMONTHSCRAP, 'count' => (int)$monthScrap, 'icon' => 'el-icon-document-delete'];
            $monthWait = DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $firstMonth . '\' and \'' . $lastMonth . '\'')->count();
            $jsonData[] = ['title' => Config::JOBMONTHWAIT, 'count' => (int)$monthWait, 'icon' => 'el-icon-document'];

            // 年度
            $firstYear = date('Y-01-01', strtotime($currentDate));
            $lastYear = date('Y-m-d', strtotime($currentDate));
            $yearCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARCREATED, 'count' => (int)$yearCreated, 'icon' => 'el-icon-document-add'];
            $yearCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARCOMPLETED, 'count' => (int)$yearCompleted, 'icon' => 'el-icon-document-checked'];
            $yearScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $jsonData[] = ['title' => Config::JOBYEARSCRAP, 'count' => (int)$yearScrap, 'icon' => 'el-icon-document-delete'];
            $yearWait = DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->where('wf12006', -100)->whereRaw('left(created_at, 10) between\'' . $firstYear . '\' and \'' . $firstYear . '\'')->count();
            $jsonData[] = ['title' => Config::JOBYEARWAIT, 'count' => (int)$yearWait, 'icon' => 'el-icon-document'];
        }

        return $jsonData;
    }

    public static function getJobAnalysisForBusmanTop($companyId, $userArray, $share)
    {
        // 成单总数
        $totalCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->count();
        // 获取排名人员信息
        $topData = DB::table('wf_workcharts')->select(DB::raw('count(ORDERBELONGID) as NUMS'), 'ORDERBELONGID')
            ->where('COMPANYID', $companyId)
            ->where('ISCOMPLETE', 1)
            ->groupBy('ORDERBELONGID')
            ->orderBy(DB::raw('count(ORDERBELONGID)'), 'desc')
            ->offset(0)
            ->limit(Config::JOBTOP)
            ->get()
            ->toArray();

        $jsonData = [];
        $userTable = $userArray['table'];
        foreach ($topData as $key => $val) {
            $val = (array)$val;
            $user = (array)DB::table($userTable)->select($userArray['column'])->where($userArray['primary'], $val['ORDERBELONGID'])->first();
            if ($user) {
                $jsonData[] = [
                    'name' => $user['realname'],
                    'src' => $user['avatar'] == '' ? '' : $share::getInstance()->getSrcByKey($user['avatar']),
                    'text' => Config::JOBTOTALCOMPLETEDRANKING . ($key + 1) . '，' . Config::JOBTOTALCOMPLETEDCOLON . $val['NUMS'] . '，' . Config::JOBTOTALCOMPLETEDRATIO . bcmul(bcdiv($val['NUMS'], $totalCompleted, 6), 100, 2) . '%'
                ];
            }
        }

        return $jsonData;
    }

    public static function getJobAnalysisForTransRadio($flowId, $companyId)
    {
        $jsonData = [];
        if (!$flowId) {
            $currentDate = date('Y-m-d');
            $yesterdayDate = date("Y-m-d", strtotime("-1 day"));
            // 同比昨日录单
            $todayCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $currentDate)->count();
            $yesterdayCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYCREATEDTRANS, 'point' => bcmul(bcdiv($todayCreated - $yesterdayCreated, ($yesterdayCreated ? $yesterdayCreated : 1), 6), 100, 2), 'title' => Config::JOBTODAYCREATEDTRANS . bcmul(bcdiv($todayCreated - $yesterdayCreated, ($yesterdayCreated ? $yesterdayCreated : 1), 6), 100, 2) . '%', 'count' => ($todayCreated - $yesterdayCreated), 'color' => '#409EFF'];
            // 同比昨日成单
            $todayCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $currentDate)->count();
            $yesterdayCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYCOMPLETEDTRANS, 'point' => bcmul(bcdiv($todayCompleted - $yesterdayCompleted, ($yesterdayCompleted ? $yesterdayCompleted : 1), 6), 100, 2), 'title' => Config::JOBTODAYCOMPLETEDTRANS . bcmul(bcdiv($todayCompleted - $yesterdayCompleted, ($yesterdayCompleted ? $yesterdayCompleted : 1), 6), 100, 2) . '%', 'count' => ($todayCompleted - $yesterdayCompleted), 'color' => '#67C23A'];
            // 同比昨日废单
            $todayScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $currentDate)->count();
            $yesterdayScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYSCRAPTRANS, 'point' => bcmul(bcdiv($todayScrap - $yesterdayScrap, ($yesterdayScrap ? $yesterdayScrap : 1), 6), 100, 2), 'title' => Config::JOBTODAYSCRAPTRANS . bcmul(bcdiv($todayScrap - $yesterdayScrap, ($yesterdayScrap ? $yesterdayScrap : 1), 6), 100, 2) . '%', 'count' => ($todayScrap - $yesterdayScrap), 'color' => '#F56C6C'];

            $week = Util::getWeekDateByCurrentTime();
            $fisrtWeek = $week[0];
            $lastWeek = $week[6];
            $previousWeekEnd = date("Y-m-d", strtotime("-1 day", strtotime($fisrtWeek)));
            $previousWeekStart = date("Y-m-d", strtotime("-6 day", strtotime($previousWeekEnd)));
            $weekCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKCREATEDTRANS, 'point' => bcmul(bcdiv($weekCreated - $previousweekCreated, ($previousweekCreated ? $previousweekCreated : 1), 6), 100, 2), 'title' => Config::JOBWEEKCREATEDTRANS . bcmul(bcdiv($weekCreated - $previousweekCreated, ($previousweekCreated ? $previousweekCreated : 1), 6), 100, 2) . '%', 'count' => ($weekCreated - $previousweekCreated), 'color' => '#409EFF'];
            $weekCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKCOMPLETEDTRANS, 'point' => bcmul(bcdiv($weekCompleted - $previousweekCompleted, ($previousweekCompleted ? $previousweekCompleted : 1), 6), 100, 2), 'title' => Config::JOBWEEKCOMPLETEDTRANS . bcmul(bcdiv($weekCompleted - $previousweekCompleted, ($previousweekCompleted ? $previousweekCompleted : 1), 6), 100, 2) . '%', 'count' => ($weekCompleted - $previousweekCompleted), 'color' => '#67C23A'];
            $weekScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKSCRAPTRANS, 'point' => bcmul(bcdiv($weekScrap - $previousweekScrap, ($previousweekScrap ? $previousweekScrap : 1), 6), 100, 2), 'title' => Config::JOBWEEKSCRAPTRANS . bcmul(bcdiv($weekScrap - $previousweekScrap, ($previousweekScrap ? $previousweekScrap : 1), 6), 100, 2) . '%', 'count' => ($weekScrap - $previousweekScrap), 'color' => '#F56C6C'];

            $firstMonth = date('Y-m-01', strtotime($currentDate));
            $lastMonth = date('Y-m-d', strtotime($currentDate));
            $previousMonthStart = date('Y-m-d', strtotime(date('Y-m-01 00:00:00',strtotime('-1 month'))));
            $previousMonthEnd = date('Y-m-d', strtotime(date("Y-m-d 23:59:59", strtotime(-date('d').'day'))));
            $monthCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHCREATEDTRANS, 'point' => bcmul(bcdiv($monthCreated - $previousmonthCreated, ($previousmonthCreated ? $previousmonthCreated : 1), 6), 100, 2), 'title' => Config::JOBMONTHCREATEDTRANS . bcmul(bcdiv($monthCreated - $previousmonthCreated, ($previousmonthCreated ? $previousmonthCreated : 1), 6), 100, 2) . '%', 'count' => ($monthCreated - $previousmonthCreated), 'color' => '#409EFF'];
            $monthCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHCOMPLETEDTRANS, 'point' => bcmul(bcdiv($monthCompleted - $previousmonthCompleted, ($previousmonthCompleted ? $previousmonthCompleted : 1), 6), 100, 2), 'title' => Config::JOBMONTHCOMPLETEDTRANS . bcmul(bcdiv($monthCompleted - $previousmonthCompleted, ($previousmonthCompleted ? $previousmonthCompleted : 1), 6), 100, 2) . '%', 'count' => ($monthCompleted - $previousmonthCompleted), 'color' => '#67C23A'];
            $monthScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHSCRAPTRANS, 'point' => bcmul(bcdiv($monthScrap - $previousmonthScrap, ($previousmonthScrap ? $previousmonthScrap : 1), 6), 100, 2), 'title' => Config::JOBMONTHSCRAPTRANS . bcmul(bcdiv($monthScrap - $previousmonthScrap, ($previousmonthScrap ? $previousmonthScrap : 1), 6), 100, 2) . '%', 'count' => ($monthScrap - $previousmonthScrap), 'color' => '#67C23A'];

            $firstYear = date('Y-01-01', strtotime($currentDate));
            $lastYear = date('Y-m-d', strtotime($currentDate));
            $previousYearStart = ((int)date('Y') - 1) . '-01-01';
            $previousYearEnd = ((int)date('Y') - 1) . '-12-31';
            $yearCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearCreated = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARCREATEDTRANS, 'point' => bcmul(bcdiv($yearCreated - $previousyearCreated, ($previousyearCreated ? $previousyearCreated : 1), 6), 100, 2), 'title' => Config::JOBYEARCREATEDTRANS . bcmul(bcdiv($yearCreated - $previousyearCreated, ($previousyearCreated ? $previousyearCreated : 1), 6), 100, 2) . '%', 'count' => ($yearCreated - $previousyearCreated), 'color' => '#409EFF'];
            $yearCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearCompleted = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARCOMPLETEDTRANS, 'point' => bcmul(bcdiv($yearCompleted - $previousyearCompleted, ($previousyearCompleted ? $previousyearCompleted : 1), 6), 100, 2), 'title' => Config::JOBYEARCOMPLETEDTRANS . bcmul(bcdiv($yearCompleted - $previousyearCompleted, ($previousyearCompleted ? $previousyearCompleted : 1), 6), 100, 2) . '%', 'count' => ($yearCompleted - $previousyearCompleted), 'color' => '#67C23A'];
            $yearScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearScrap = DB::table('wf_workcharts')->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARSCRAPTRANS, 'point' => bcmul(bcdiv($yearScrap - $previousyearScrap, ($previousyearScrap ? $previousyearScrap : 1), 6), 100, 2), 'title' => Config::JOBYEARSCRAPTRANS . bcmul(bcdiv($yearScrap - $previousyearScrap, ($previousyearScrap ? $previousyearScrap : 1), 6), 100, 2) . '%', 'count' => ($yearScrap - $previousyearScrap), 'color' => '#F56C6C'];
        } else {
            $currentDate = date('Y-m-d');
            $yesterdayDate = date("Y-m-d", strtotime("-1 day"));
            // 同比昨日录单
            $todayCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $currentDate)->count();
            $yesterdayCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYCREATEDTRANS, 'point' => bcmul(bcdiv($todayCreated - $yesterdayCreated, ($yesterdayCreated ? $yesterdayCreated : 1), 6), 100, 2), 'title' => Config::JOBTODAYCREATEDTRANS . bcmul(bcdiv($todayCreated - $yesterdayCreated, ($yesterdayCreated ? $yesterdayCreated : 1), 6), 100, 2) . '%', 'count' => ($todayCreated - $yesterdayCreated), 'color' => '#409EFF'];
            // 同比昨日成单
            $todayCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $currentDate)->count();
            $yesterdayCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYCOMPLETEDTRANS, 'point' => bcmul(bcdiv($todayCompleted - $yesterdayCompleted, ($yesterdayCompleted ? $yesterdayCompleted : 1), 6), 100, 2), 'title' => Config::JOBTODAYCOMPLETEDTRANS . bcmul(bcdiv($todayCompleted - $yesterdayCompleted, ($yesterdayCompleted ? $yesterdayCompleted : 1), 6), 100, 2) . '%', 'count' => ($todayCompleted - $yesterdayCompleted), 'color' => '#67C23A'];
            // 同比昨日废单
            $todayScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $currentDate)->count();
            $yesterdayScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->where('ORDERDATE', $yesterdayDate)->count();
            $jsonData[] = ['charts' => Config::JOBTODAYSCRAPTRANS, 'point' => bcmul(bcdiv($todayScrap - $yesterdayScrap, ($yesterdayScrap ? $yesterdayScrap : 1), 6), 100, 2), 'title' => Config::JOBTODAYSCRAPTRANS . bcmul(bcdiv($todayScrap - $yesterdayScrap, ($yesterdayScrap ? $yesterdayScrap : 1), 6), 100, 2) . '%', 'count' => ($todayScrap - $yesterdayScrap), 'color' => '#F56C6C'];

            $week = Util::getWeekDateByCurrentTime();
            $fisrtWeek = $week[0];
            $lastWeek = $week[6];
            $previousWeekEnd = date("Y-m-d", strtotime("-1 day", strtotime($fisrtWeek)));
            $previousWeekStart = date("Y-m-d", strtotime("-6 day", strtotime($previousWeekEnd)));
            $weekCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKCREATEDTRANS, 'point' => bcmul(bcdiv($weekCreated - $previousweekCreated, ($previousweekCreated ? $previousweekCreated : 1), 6), 100, 2), 'title' => Config::JOBWEEKCREATEDTRANS . bcmul(bcdiv($weekCreated - $previousweekCreated, ($previousweekCreated ? $previousweekCreated : 1), 6), 100, 2) . '%', 'count' => ($weekCreated - $previousweekCreated), 'color' => '#409EFF'];
            $weekCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKCOMPLETEDTRANS, 'point' => bcmul(bcdiv($weekCompleted - $previousweekCompleted, ($previousweekCompleted ? $previousweekCompleted : 1), 6), 100, 2), 'title' => Config::JOBWEEKCOMPLETEDTRANS . bcmul(bcdiv($weekCompleted - $previousweekCompleted, ($previousweekCompleted ? $previousweekCompleted : 1), 6), 100, 2) . '%', 'count' => ($weekCompleted - $previousweekCompleted), 'color' => '#67C23A'];
            $weekScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$fisrtWeek, $lastWeek])->count();
            $previousweekScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousWeekStart, $previousWeekEnd])->count();
            $jsonData[] = ['charts' => Config::JOBWEEKSCRAPTRANS, 'point' => bcmul(bcdiv($weekScrap - $previousweekScrap, ($previousweekScrap ? $previousweekScrap : 1), 6), 100, 2), 'title' => Config::JOBWEEKSCRAPTRANS . bcmul(bcdiv($weekScrap - $previousweekScrap, ($previousweekScrap ? $previousweekScrap : 1), 6), 100, 2) . '%', 'count' => ($weekScrap - $previousweekScrap), 'color' => '#F56C6C'];

            $firstMonth = date('Y-m-01', strtotime($currentDate));
            $lastMonth = date('Y-m-d', strtotime($currentDate));
            $previousMonthStart = date('Y-m-d', strtotime(date('Y-m-01 00:00:00',strtotime('-1 month'))));
            $previousMonthEnd = date('Y-m-d', strtotime(date("Y-m-d 23:59:59", strtotime(-date('d').'day'))));
            $monthCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHCREATEDTRANS, 'point' => bcmul(bcdiv($monthCreated - $previousmonthCreated, ($previousmonthCreated ? $previousmonthCreated : 1), 6), 100, 2), 'title' => Config::JOBMONTHCREATEDTRANS . bcmul(bcdiv($monthCreated - $previousmonthCreated, ($previousmonthCreated ? $previousmonthCreated : 1), 6), 100, 2) . '%', 'count' => ($monthCreated - $previousmonthCreated), 'color' => '#409EFF'];
            $monthCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHCOMPLETEDTRANS, 'point' => bcmul(bcdiv($monthCompleted - $previousmonthCompleted, ($previousmonthCompleted ? $previousmonthCompleted : 1), 6), 100, 2), 'title' => Config::JOBMONTHCOMPLETEDTRANS . bcmul(bcdiv($monthCompleted - $previousmonthCompleted, ($previousmonthCompleted ? $previousmonthCompleted : 1), 6), 100, 2) . '%', 'count' => ($monthCompleted - $previousmonthCompleted), 'color' => '#67C23A'];
            $monthScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstMonth, $lastMonth])->count();
            $previousmonthScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousMonthStart, $previousMonthEnd])->count();
            $jsonData[] = ['charts' => Config::JOBMONTHSCRAPTRANS, 'point' => bcmul(bcdiv($monthScrap - $previousmonthScrap, ($previousmonthScrap ? $previousmonthScrap : 1), 6), 100, 2), 'title' => Config::JOBMONTHSCRAPTRANS . bcmul(bcdiv($monthScrap - $previousmonthScrap, ($previousmonthScrap ? $previousmonthScrap : 1), 6), 100, 2) . '%', 'count' => ($monthScrap - $previousmonthScrap), 'color' => '#67C23A'];

            $firstYear = date('Y-01-01', strtotime($currentDate));
            $lastYear = date('Y-m-d', strtotime($currentDate));
            $previousYearStart = ((int)date('Y') - 1) . '-01-01';
            $previousYearEnd = ((int)date('Y') - 1) . '-12-31';
            $yearCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearCreated = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARCREATEDTRANS, 'point' => bcmul(bcdiv($yearCreated - $previousyearCreated, ($previousyearCreated ? $previousyearCreated : 1), 6), 100, 2), 'title' => Config::JOBYEARCREATEDTRANS . bcmul(bcdiv($yearCreated - $previousyearCreated, ($previousyearCreated ? $previousyearCreated : 1), 6), 100, 2) . '%', 'count' => ($yearCreated - $previousyearCreated), 'color' => '#409EFF'];
            $yearCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearCompleted = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARCOMPLETEDTRANS, 'point' => bcmul(bcdiv($yearCompleted - $previousyearCompleted, ($previousyearCompleted ? $previousyearCompleted : 1), 6), 100, 2), 'title' => Config::JOBYEARCOMPLETEDTRANS . bcmul(bcdiv($yearCompleted - $previousyearCompleted, ($previousyearCompleted ? $previousyearCompleted : 1), 6), 100, 2) . '%', 'count' => ($yearCompleted - $previousyearCompleted), 'color' => '#67C23A'];
            $yearScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$firstYear, $lastYear])->count();
            $previousyearScrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->whereBetween('ORDERDATE', [$previousYearStart, $previousYearEnd])->count();
            $jsonData[] = ['charts' => Config::JOBYEARSCRAPTRANS, 'point' => bcmul(bcdiv($yearScrap - $previousyearScrap, ($previousyearScrap ? $previousyearScrap : 1), 6), 100, 2), 'title' => Config::JOBYEARSCRAPTRANS . bcmul(bcdiv($yearScrap - $previousyearScrap, ($previousyearScrap ? $previousyearScrap : 1), 6), 100, 2) . '%', 'count' => ($yearScrap - $previousyearScrap), 'color' => '#F56C6C'];
        }

        return $jsonData;
    }

    public static function getJobCompletedByUserId($userId, $companyId)
    {
        return DB::table('wf_workcharts')
            ->where('COMPANYID', $companyId)
            ->where('ORDERBELONGID', $userId)
            ->where('ISCOMPLETE', 1)
            ->count();
    }

    public static function getJobCreatedByUserId($userId, $companyId)
    {
        return DB::table('wf_workcharts')
            ->where('COMPANYID', $companyId)
            ->where('ORDERBELONGID', $userId)
            ->where('ISCREATE', 1)
            ->count();
    }

    public static function getJobCompletedByFlowId($flowId, $companyId)
    {
        return DB::table('wf_workcharts')
            ->where('COMPANYID', $companyId)
            ->where('FLOWID', $flowId)
            ->where('ISCOMPLETE', 1)
            ->count();
    }

    public static function getJobCreatedByflowId($flowId, $companyId)
    {
        return DB::table('wf_workcharts')
            ->where('COMPANYID', $companyId)
            ->where('FLOWID', $flowId)
            ->where('ISCREATE', 1)
            ->count();
    }

    public static function getJobTopDataByUserId($userId, $companyId)
    {
        $topData = DB::table('wf_workcharts')->select(DB::raw('count(ORDERBELONGID) as NUMS'), 'ORDERBELONGID')
            ->where('COMPANYID', $companyId)
            ->where('ISCOMPLETE', 1)
            ->groupBy('ORDERBELONGID')
            ->orderBy(DB::raw('count(ORDERBELONGID)'), 'desc')
            ->get()
            ->toArray();

        $res['topcount'] = count($topData);
        $res['top'] = 0;
        $index = array_search($userId, array_column($topData, 'ORDERBELONGID'));
        if ($index) $res['top'] = $index + 1;

        return $res;
    }

    public static function getWorkFlowByBelongEnabled($flowBelong)
    {
        $where = [['wf01003', '=', $flowBelong], ['wf01004', '=', 1]];
        $workFlowLists = DB::table('wf_workflows')
            ->select('wf01001 as flowId', 'wf01002 as flowName')
            ->where($where)
            ->get()
            ->toArray();

        return $workFlowLists;
    }

    public static function getWorkFlowByFlowId($flowId)
    {
        return (array)DB::table('wf_workflows')
            ->select('wf01001 as flowId', 'wf01002 as flowName')
            ->where('wf01001', $flowId)
            ->first();
    }

    public static function getWorkNodeByFlowIdEnabled($flowId)
    {
        return DB::table('wf_worknodes')
            ->where('wf02001', $flowId)
            ->orderby('wf02009', 'ASC')
            ->get()
            ->toArray();
    }

    public static function getJobTotalInProcessByNodeId($flowId, $nodeId, $companyId)
    {
        return DB::table('wf_workprocesses')
            ->where('wf12001', $flowId)
            ->where('wf12004', $nodeId)
            ->where('wf12003', $companyId)
            ->count();
    }

    public static function getJobNodeStrtipsByNodeId($flowId, $nodeData, $companyId)
    {
        $nodeId = $nodeData['wf02002'];
        $string = '';
        $isExam = DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $nodeId)->value('wf02005');
        if (!$isExam) return $string;
        // 节点信息
        $jobDataTotal = static::getJobTotalInProcessByNodeId($flowId, $nodeId, $companyId);
        $string .= '<h6> ' . $nodeData['wf02003'] . ' - <small>' . Config::JOBCURRENTNODEORFDERS . $jobDataTotal . '</small></h6> <br />';
        $examCount = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('NODEID', $nodeId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->count();
        // 通过率
        $pass = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('NODEID', $nodeId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 1)->count();
        // 拒单率
        $unpass = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('NODEID', $nodeId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->count();
        // 回退率
        $reback = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('NODEID', $nodeId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 2)->count();
        $string .= '<p>' . Config::JOBFLOWEXAMPASS . bcmul(bcdiv($pass, ($examCount ? $examCount : 1), 6), 100, 2) . '%</p>';
        $string .= '<p>' . Config::JOBFLOWEXAMUNPASS . bcmul(bcdiv($unpass, ($examCount ? $examCount : 1), 6), 100, 2) . '%</p>';
        $string .= '<p>' . Config::JOBFLOWEXAMREBACK . bcmul(bcdiv($reback, ($examCount ? $examCount : 1), 6), 100, 2) . '%</p>';

        return $string;
    }

    public static function getJobFlowStrtipsByFlowId($flowId, $companyId)
    {
        $string = '';

        $string .= '<p>' . Config::JOBORDERSNUM . DB::table('wf_workprocesses')->where('wf12001', $flowId)->where('wf12003', $companyId)->count() . '</p>';
        // 录单总数
        $created = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCREATE', 1)->count();
        $string .= '<p>' . Config::JOBTOTALCREATEDFLOW . $created . '</p>';
        // 成单总数
        $completed = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISCOMPLETE', 1)->count();
        $string .= '<p>' . Config::JOBTOTALCOMPLETEDFLOW . $completed . '</p>';
        // 废单总数
        $scrap = DB::table('wf_workcharts')->where('FLOWID', $flowId)->where('COMPANYID', $companyId)->where('ISEXAM', 1)->where('ISEXAMRES', 0)->count();
        $string .= '<p>' . Config::JOBTOTALSCRAPFLOW . $scrap . '</p>';

        return $string;
    }

    public static function getHistoryJobListByFlowId(
        $offest,
        $limit,
        $flowId,
        $keyword,
        $outWhere,
        $apiUser
    )
    {
        $orWhere = [];
        $where[] = ['FLOWID', '=', $flowId, 'AND'];
        $where[] = ['COMPANYID', '=', $apiUser['company_id'] ?? 0, 'AND'];
        if ($apiUser['user_type'] == 2) {
            $where[] = ['ORDERBELONGID', '=', $apiUser['id'] ?? 0, 'AND'];
        }

        if ($keyword) {
            $orWhere[] = ['NAME', 'like', "%{$keyword}%", 'AND'];
            $orWhere[] = ['NUMBER', 'like', "%{$keyword}%", 'OR'];
        }

        $queryFields = [
            'id', 'NUMBER', 'NAME', 'FLOWID', 'NODEID', 'JOBID', 'COMPANYID', 'ORDERBELONGID',
            'ORDERBELONG', 'EXAMID', 'EXAMNAME', 'EXAMDATE', 'EXAMRESULT', 'EXAMREASON'
        ];

        $listData = DB::table('wf_workhistories')
            ->select($queryFields)
            ->where($where)
            ->where(function ($query) use ($outWhere) {
                if ($outWhere) $query->where($outWhere);
            })
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })
            ->offset($offest)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();

        foreach ($listData as $k => $v) {
            if (is_object($v)) {
                $listData[$k]->EXAMRESULTNAME = Config::EXAMRESNAME[(string)$v->EXAMRESULT];
                $listData[$k]->NODENAME = static::getFlowNodeName($flowId, $v->NODEID);
            } else {
                $listData[$k]['EXAMRESULTNAME'] = Config::EXAMRESNAME[(string)$v['EXAMRESULT']];
                $listData[$k]['NODENAME'] = static::getFlowNodeName($flowId, $v['NODEID']);
            }
        }

        $totalCount = DB::table('wf_workhistories')
            ->where($where)
            ->where(function ($query) use ($outWhere) {
                if ($outWhere) $query->where($outWhere);
            })
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })->count();

        $jsonData = [
            'list' => $listData,
            'count' => $totalCount
        ];

        return ['error' => 0, 'data' => $jsonData];
    }

    public static function getHistoryJobFieldsByPrimary($primaryId)
    {
        return (array)DB::table('wf_workhistories')
            ->where('id', $primaryId)
            ->first();
    }

    public static function getJobCustomerQueryFields($flowId, $userId)
    {
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemname')
            ->where('wf08004', 1)
            ->value('wf08005');

        $gridColumns = [];
        if (empty($globalConfig)) {
            $gridColumns = ['NUMBER', 'NAME'];
        } else {
            $globalConfig = json_decode($globalConfig, true);
            if ($globalConfig['numberstatus']) {
                array_push($gridColumns, 'NUMBER');
            }
            if ($globalConfig['namestatus']) {
                array_push($gridColumns, 'NAME');
            }
        }
        // 获取配置列
        $customerColumnsConfig = DB::table('wf_workcustomercolumns')
            ->where('wf97001', $flowId)
            ->where('wf97003', 1)
            ->where('wf97002', $userId)
            ->value('wf97005');
        if ($customerColumnsConfig) {
            $customerColumnsConfig = json_decode($customerColumnsConfig, true);
            foreach ($customerColumnsConfig as $k => $v) {
                array_push($gridColumns, $v['key']);
            }
        }

        return $gridColumns;
    }

    public static function getJobCustomerListByFlowId(
        $offest,
        $limit,
        $flowId,
        $keyword,
        $outWhere,
        $apiUser
    )
    {
        // 获取主表信息
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);
        // where条件处理
        $orWhere = [];
        $where[] = ['wf12001', '=', $flowId, 'AND'];
        $where[] = ['wf12003', '=', $apiUser['company_id'] ?? 0, 'AND'];
        if ($keyword) {
            $orWhere[] = ['NAME', 'like', "%{$keyword}%", 'AND'];
            $orWhere[] = ['NUMBER', 'like', "%{$keyword}%", 'OR'];
        }

        // 获取进程字段信息
        $queryProcessFields = static::getProcessQueryFieldsAlias();
        $queryJobFields = [];
        $queryJobFields = static::getJobCustomerQueryFields($flowId, $apiUser['id']);
        $queryFields = array_merge($queryProcessFields, $queryJobFields);
        // 需要代码处理值
        $codes = static::getBeTranslateSystemCode($flowId, $table, $queryJobFields);

        $listData = DB::table('wf_workprocesses')
            ->select($queryFields)
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })
            ->where(function ($query) use ($outWhere) {
                if ($outWhere) $query->where($outWhere);
            })
            ->offset($offest)
            ->limit($limit)
            ->get()
            ->toArray();
        if (count($codes)) static::jobTranslateCodeToName($listData, $codes);

        // 数据处理
        foreach ($listData as $k => $v) {
            if (is_object($v)) {
                $listData[$k]->JOBSTATUS = Config::JOBSTATUSNAME[(string)$v->decisionTag];
                $listData[$k]->CURRENTNODE = static::getFlowNodeName($flowId, $v->nodeId);
            } else {
                $listData[$k]['JOBSTATUS'] = Config::JOBSTATUSNAME[(string)$v['decisionTag']];
                $listData[$k]['CURRENTNODE'] = static::getFlowNodeName($flowId, $v['nodeId']);
            }
        }

        $totalCount = DB::table('wf_workprocesses')
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->where(function ($query) use ($orWhere) {
                if ($orWhere) $query->orWhere($orWhere);
            })->where(function ($query) use ($outWhere) {
                if ($outWhere) $query->where($outWhere);
            })->count();

        $jsonData = [
            'list' => $listData,
            'count' => $totalCount
        ];

        return ['error' => 0, 'data' => $jsonData];
    }

    public static function getJobCustomerListForExport($flowId, $outWhere, $apiUser, $fields = null)
    {
        // 获取主表信息
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);

        $where[] = ['wf12001', '=', $flowId, 'AND'];
        $where[] = ['wf12003', '=', $apiUser['company_id'] ?? 0, 'AND'];
        if (is_null($fields) || empty($fields)) {
            $fields = $table . '.*';
            $columns = static::getJobColumnSystemCode($flowId, $table);
        } else {
            $columns = $fields;
        }

        $listData = DB::table('wf_workprocesses')
            ->select($fields)
            ->leftJoin($table, 'wf_workprocesses.wf12002', '=', $table . '.id')
            ->where($where)
            ->where(function ($query) use ($outWhere) {
                if ($outWhere) $query->where($outWhere);
            })
            ->get()
            ->toArray();

        // 数据处理
        foreach ($listData as $k => $v) {
            if (is_object($v)) {
                $listData[$k]->JOBSTATUS = Config::JOBSTATUSNAME[(string)$v->JOBSTATUS];
                $listData[$k]->CURRENTNODE = static::getFlowNodeName($flowId, $v->CURRENTNODE);
            } else {
                $listData[$k]['JOBSTATUS'] = Config::JOBSTATUSNAME[(string)$v['JOBSTATUS']];
                $listData[$k]['CURRENTNODE'] = static::getFlowNodeName($flowId, $v['CURRENTNODE']);
            }
        }

        $codes = static::getBeTranslateSystemCode($flowId, $table, $columns);
        if (count($codes)) static::jobTranslateCodeToName($listData, $codes);

        return $listData;
    }
}
