<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

class FakerSeed
{
    private $faker = null;
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    public function fakerBirtData($config)
    {
        $mapData = [];
        foreach ($config as $k => $v) {
            if ($k == 'single') {
                $single = [];
                foreach ($v as $sk => $sv) {
                    $single[$sv[1]] = $this->gennerSimlerValue($sv[0], $sv[1]);
                }
                $mapData[$k] = $single;
            } else {
                if (!count($v)) {
                    continue;
                }
                $table = [];
                foreach ($v as $tk => $tv) {
                    $listData = [];
                    $times = mt_rand(1, 5);
                    for ($i = 0; $i <= $times; $i++) {
                        foreach ($tv as $ttk => $ttv) {
                            $listData[$i][$ttv[1]] = $this->gennerSimlerValue($ttv[0], $ttv[1]);
                        }
                    }
                    $table[$tk] = $listData;
                }
                $mapData[$k] = $table;
            }
        }
        
        return $mapData;
    }

    public function gennerSimlerValue($type, $col)
    {
        $val = '';
        if ($type == 1) {
            $val = './tmp/' . mt_rand(1, 5) . '.jpg';
        } else {
            $val = substr($this->faker->sha256, 0, 6);
        }

        return $val;
    }
}
