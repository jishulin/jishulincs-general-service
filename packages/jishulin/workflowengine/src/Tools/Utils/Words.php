<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use PhpOffice\PhpWord\Shared\Converter;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;
use Jishulin\WorkFlowEngine\Tools\Utils\WordProcessor;

class Words
{
    /**
     * Defined unique instance variables
     *
     * @var $_instance
     */
    private	static $_instance;

    /**
     * Defined unique instance variables of \PhpOffice\PhpWord\PhpWord
     *
     * @var $_obj
     */
    private static $_obj;

    /**
     * Prohibition of external instantiation
     *
     * @return void
     */
    private function __construct()
    {

    }

    /**
     * Prevention of cloning
     *
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * \PhpOffice\PhpWord\SimpleType\Jc 文本位置
     *
     * @var  $JC
     */
    public $JC = [
        'center' => Jc::CENTER,
        'left' => Jc::LEFT,
        'right' => Jc::RIGHT
    ];

    /**
     * Provide external static invocation method
     *
     * @return \app\common\controller\Words
     */
    public static function getInstance()
    {
        if (! (self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        self::$_obj = self::$_instance->init();

        return self::$_instance;
    }

    /**
     * Initialization PhpWord
     *
     * @return \PhpOffice\PhpWord\PhpWord
     */
    protected function init()
    {
        self::$_obj = new PhpWord();
        return self::$_obj;
    }

    /**
     * 通过模板生成文档
     *
     * @param array $map
     * $map = [
     * 		//对应模板值，全路径
     *		'config' => [
     *          'key' => './mdfile/words/template.docx',
     *          'fields' => [
     *              //图片为1，有三个参数，第三个参数设置图片的大小
     *              'single' => [[0, 'username'], [0, 'gender'], [1, 'images']],
     *              'table' => [
     *                  'list0' => [[0, 'v1'], [0, 'v2'], [0, 'v3'], [0, 'v4'], [1, 'v5']],
     *                  'list1' => [[0, 'a1'], [0, 'a2'], [0, 'a3'], [0, 'a4'], [1, 'a5']],
     *              ],
     *          ],
     *      ],
     * 		//下载类型D,L返回文件路径
     * 		'downType' => 'D',
     * 		//文件名--无后缀
     * 		'fileName' => '文件模板测试',
     * 		//非多记录表格数据配置single为定值不能修改
     *		'single' => [
     *			'username' => '丽丽',
     *			'gender' => 'Female',
     *			'images' => './resouce/logo.png'
     *		],
     * 		//多记录表格数据
     *		'table' => [
     * 			//list0与配置文件中的相对应，可修改，里面是二维多记录
     *			'list0' => [
     *				['v1' => 'aaa', 'v2' => 'bbb', 'v3' => 'ccc', 'v4' => 'ddd', 'v5' => './resouce/logo.png'],
     *				['v1' => '111', 'v2' => '222', 'v3' => '333', 'v4' => '444', 'v5' => './resouce/logo.png'],
     *			],
     * 			'list1' => [
     * 				[],[],[]
     * 			],
     *		],
     *	];
     *
     * @return mixed
     */
    public function createWordUseTemplate(array $map = [])
    {
        $configWords = $map['config'];
        $templateDocx = $configWords['key'];

        $templateProcessor = new WordProcessor($templateDocx);

        $templateFields = $configWords['fields'];
        foreach ($templateFields as $key => $val) {
            switch ($key) {
                case 'single':
                    $single = $map['single'] ?? [];
                    $this->fillSingleToWord($templateProcessor, $val, $single);
                    break;
                case 'table':
                    $table = $map['table'] ?? [];
                    $this->fillTableToWord($templateProcessor, $val, $table);
                    break;
                default:
                    break;
            }
        }

        $downType = strtoupper($map['downType'] ?? 'D');
        $fileName = urlencode(($map['fileName'] ?? (uniqid() . time()))) . '.docx';

        $filePath = Util::createStorage('word');

        $returnUrl = $filePath . $fileName;

        $templateProcessor->saveAs($returnUrl);

        if ($downType == 'D') {
            ob_clean();
            ob_start();
            $fp = fopen($returnUrl, "r");
            $file_size = filesize($returnUrl);
            Header("Content-type:application/octet-stream");
            Header("Accept-Ranges:bytes");
            Header("Accept-Length:" . $file_size);
            header('Content-Disposition:attchment;filename="' . $fileName . '"');
            $buffer = 1024;
            $file_count = 0;
            while (!feof($fp) && $file_count < $file_size){
                $file_con = fread($fp, $buffer);
                $file_count += $buffer;
                echo $file_con;
            }
            fclose($fp);
            ob_end_flush();
            @unlink($returnUrl);
            exit;
            // ob_end_clean();
            // header("Content-Type: application/force-download");
            // header("Content-Type: application/octet-stream");
            // header("Content-Type: application/download");
            // header('Content-Disposition:inline;filename="' . $fileName . '"');
            // header("Content-Transfer-Encoding: binary");
            // header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            // header("Pragma: no-cache");
            // header('location:http://' . $_SERVER['HTTP_HOST'] . '/' . $returnUrl);
            // exit;
        } else {
            return ['status' => true, 'type' => 'L', 'data' => ['filepath' => $returnUrl]];
        }
    }

    /**
     * 填充替换模板中的table
     *
     * @param \app\common\controller\WordProcessor $templateProcessor
     * @param array $fields
     * @param array $data
     *
     * @return void
     */
    private function fillTableToWord(
        WordProcessor $templateProcessor,
        array &$fields = [],
        array &$data = []
    )
    {
        foreach ($fields as $fieldkey => $fieldval) {
            if (!isset($data[$fieldkey]) || !count($data[$fieldkey])) continue;

            $final = $data[$fieldkey];
            $count = count($final);
            $templateProcessor->cloneRow($fieldval[0][1], $count);

            $index = 1;
            foreach ($final as $fin) {
                $this->fillSingleToWord($templateProcessor, $fieldval, $fin, true, $index);
                $index++;
            }
        }
    }

    /**
     * 填充替换模板中的单值
     *
     * @param \app\common\controller\WordProcessor $templateProcessor
     * @param array $fields
     * @param array $data
     *
     * @return void
     */
    private function fillSingleToWord(
        WordProcessor $templateProcessor,
        array &$fields = [],
        array &$data = [],
        $cloneRow = false,
        $index = 1
    )
    {
        foreach ($fields as $field) {
            $fieldKey = $cloneRow === true ? ($field[1] . '#' . $index) : $field[1];
            if ($field[0] == 0) {
                $templateProcessor->setValue($fieldKey, $data[$field[1]]);
            } elseif ($field[0] == 1 && !empty($data[$field[1]])) {
                $swh = 	isset($field[2]) && $field[2] != '' && is_numeric($field[2]) ? strval($field[2]) : '250';
                $arrImagenes = ['src' => $data[$field[1]], 'swh' => $swh];
                $templateProcessor->setImg($fieldKey, $arrImagenes);
            } elseif ($field[0] == 1) {
                $templateProcessor->setValue($fieldKey, '');
            }
        }
    }
}
