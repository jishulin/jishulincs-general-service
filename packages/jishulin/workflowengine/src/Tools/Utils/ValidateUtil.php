<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

use Illuminate\Support\Facades\DB;
use Jishulin\WorkFlowEngine\Config\Config;

class ValidateUtil
{
    public static function _valNullParams(...$args)
    {
        $len = count($args);
        if (!$len) return true;
        foreach ($args as $arg) {
            if ($arg === '0' || $arg === 0 || $arg === false) continue;
            if (is_array($arg) && empty($arg)) return false;
            if ($arg === '' || $arg === null) return false;
        }
        return true;
    }

    public static function basicJobProcessDealWork($list)
    {
        $flowId = $list['flowId'] ?? '';
        $nodeId = $list['nodeId'] ?? '';
        if (!static::_valNullParams($flowId, $nodeId)) return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];

        // 细节校验

        return ['error' => 0];
    }

    public static function basicJobProcessDealExam($list)
    {
        $flowId = $list['flowId'] ?? '';
        $nodeId = $list['nodeId'] ?? '';
        $jobId = $list['jobId'] ?? '';
        $processId = $list['processId'] ?? '';
        $exam = $list['exam'] ?? [];
        if (!static::_valNullParams($flowId, $nodeId, $jobId, $processId, $exam)) return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];

        $examResult = $exam['examResult'] ?? '';
        $examReason = $exam['examReason'] ?? '';
        $examReback = $exam['examReback'] ?? '';
        if (!static::_valNullParams($examResult)) return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];
        if ($examResult != 1 && $examReason == '') return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];
        if ($examResult == 2 && $examReback == '') return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];

        return ['error' => 0];
    }
    
    public static function basicJobProcessSubmit($list)
    {
        $flowId = $list['flowId'] ?? '';
        $nodeId = $list['nodeId'] ?? '';
        $jobId = $list['jobId'] ?? '';
        $processId = $list['processId'] ?? '';
        if (!static::_valNullParams($flowId, $nodeId, $jobId, $processId)) return ['error' => 1, 'message' => Config::PROCESSPARAMSERROR];
        
        return ['error' => 0];
    }
    
}
