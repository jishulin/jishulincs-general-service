<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

use Jishulin\WorkFlowEngine\Tools\Utils\Util;

class Exports
{
    /**
     * 单页导出--不计数量--可以导出多个sheet工作区
     *
     * @param array $data 需要的相关数据
     * $data = [
     * 		//表示sheet0对应需要导出的数据 数据是否存在根据key中的keys个数判断
     * 		'sheet0'=>['data' => $dataInfos],
     * 		//表示sheet1对应需要导出的数据
     * 		'sheet1'=>['data' => $dataInfos2],
     * 		//excel中对应的key
     * 		'key' => 'common_export',
     * 		//导出的文件名
     * 		'fileInfo' => [
     * 				//文件名
     * 				'fileName' => '多页导出计数测试',
     * 				//工作表名替换key中设置的，不设置将使用key中设置的
     * 				'sheetName0' => '测试替换',
     * 				'sheetName1' => '配置不设置'
     *		]
     * ];
     *
     * @return void
     */
    // public static function common_excel_export(array $data, $tag = 0)
    // {
    //     @ini_set('memory_limit', '2048M');
    //     set_time_limit(0);
    //     error_reporting(E_ALL);

    //     $key = $data['key'];

    //     $excelInfo = Config::get('excel.' . $key);

    //     if (isset($data['fileInfo']['fileName'])) {
    //         $fileName = $data['fileInfo']['fileName'];
    //     } else {
    //         if (isset($excelInfo['fileName'])) {
    //             $fileName = $excelInfo['fileName'];
    //         } else {
    //             $fileName = uniqid().time();
    //         }
    //     }

    //     // $fileName = $data['fileInfo']['fileName'] ?? uniqid().time();

    //     unset($data['key']);

    //     unset($data['fileInfo']);

    //     ksort($data);

    //     $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel5')->load($excelInfo['tempExcel']);

    //     $index = 0;

    //     foreach ($excelInfo['keys'] as $v) {
    //         if (isset($v['sheetName'])) {
    //             $objPHPExcel->getSheet($v['index'])->setTitle($v['sheetName']);
    //         }
    //         $dataInfos = $data['sheet' . $index]['data'];
    //         $num = $v['num'];
    //         $keys = $v['key'];
    //         foreach ($dataInfos as $info) {
    //             $column = count($keys);
    //             $temp = 0;
    //             for ($n = 0; $n < $column; $n++) {
    //                 if ($temp == $column) {
    //                     break;
    //                 } else {
    //                     $pcoordinate = \PHPExcel_Cell::stringFromColumnIndex($n) . '' . $num;
    //                     $keys[$temp] == 'index_id' ? (
    //                         $objPHPExcel->setActiveSheetIndex($v['index'])
    //                                     ->setCellValue($pcoordinate, ($num-1))
    //                     ) : (
    //                         $objPHPExcel->setActiveSheetIndex($v['index'])
    //                                     ->setCellValueExplicit($pcoordinate, $info[$keys[$temp]] ?? '' . "\t")
    //                     );
    //                     $temp++;
    //                 }
    //             }
    //             $num++;
    //         }
    //         $index++;
    //     }

    //     $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

    //     if ($tag) {
    //         $return_path = Rules::createExcelUploadRule(false);
    //         $true_file_path = $return_path . $fileName . '.xls';
    //         $objWriter->save($true_file_path);
    //         return $true_file_path;
    //     } else {
    //         header ( 'Content-Type: application/vnd.ms-excel;charset=utf-8;' );
    //         header ( 'Content-Disposition: attachment;filename="' . urlencode($fileName) . '.xls"');
    //         header ( 'Cache-Control: max-age=0' );
    //         $objWriter->save('php://output');
    //         exit;
    //     }
    // }

    /**
     * 多页导出--计数量--将相同数据按每个工作区的量导入多个工作区即设置每个工作的上限---只支持无表头
     *
     * @param array $data 需要的相关数据
     *$data = [
     * 		//表示sheet0对应需要导出的数据 数据是否存在根据key中的keys个数判断
     * 		'sheet0'=>['data' => $dataInfos],
     * 		//表示sheet1对应需要导出的数据
     * 		'sheet1'=>['data' => $dataInfos2],
     * 		'filePath' => '文件路径',
     * 		'keys' => [],
     * 		//导出的文件名
     * 		'fileInfo' => [
     * 				//文件名
     * 				'fileName' => '多页导出计数测试',
     * 				//工作表名替换key中设置的，不设置将使用key中设置的
     * 				'sheetName0' => '测试替换',
     * 				'sheetName1' => '配置不设置'
     *		]
     * ];
     *
     * @return void
     */
    public static function caculatExcelExport(array $data)
    {
        @ini_set('memory_limit', '2048M');
        set_time_limit(0);
        error_reporting(E_ALL);

        $fileInfo = $data['fileInfo'];
        $fileName = $fileInfo['fileName'] ?? uniqid() . time();
        $filePath = $data['filePath'];
        unset($data['filePath']);
        unset($data['fileInfo']);
        if (isset($data['sheetSize'])) {
            $sheetSize = (int)$data['sheetSize'] > 0 ? (int)$data['sheetSize'] : 40000;
            unset($data['sheetSize']);
        } else {
            $sheetSize = 40000;
        }
        $colKeys = $data['keys'];
        unset($data['keys']);
        if (isset($data['downType'])) {
            $downType = $data['downType'];
            unset($data['downType']);
        } else {
            $downType = 'D';
        }

        ksort($data);

        $suffix = pathinfo($filePath)['extension'];
        $loadReader = $suffix == 'xls' ? 'Excel5' : 'Excel2007';
        $objPHPExcel = \PHPExcel_IOFactory::createReader($loadReader)->load($filePath);
        $sheetIndex = 0;
        foreach ($colKeys as $v) {
            $dataIndex = $v['index'];
            $dataInfos = $data['sheet' . $dataIndex]['data'];
            $num = $v['num'];
            $keys = $v['key'];
            $rowSize = (isset($v['sheetSize']) && is_numeric($v['sheetSize'])) ? intval($v['sheetSize']) : $sheetSize;
            $tempSub = 0;
            $dataLen = count($dataInfos);
            $sheetCounts = ceil(bcdiv($dataLen, $rowSize, 6));
            $sheetTemp = 0;
            $objPHPExcel->getSheet($sheetIndex)->setTitle(($fileInfo['sheetName'.$dataIndex] ?? $v['sheetName']) . $sheetTemp);
            foreach ($dataInfos as $info) {
                if (is_object($info)) $info = (array)$info;
                $column = count($keys);
                $temp = 0;
                for ($n = 0; $n < $column; $n++) {
                    if ($temp == $column) {
                        break;
                    } else {
                        $pcoordinate = \PHPExcel_Cell::stringFromColumnIndex($n) . '' . $num;
                        $keys[$temp] == 'indexId' ? (
                            $objPHPExcel->setActiveSheetIndex($sheetIndex)->setCellValue($pcoordinate, $num)
                        ) : (
                            $objPHPExcel->setActiveSheetIndex($sheetIndex)->setCellValue($pcoordinate, ($info[$keys[$temp]] ?? '') . "\t")
                        );
                        $temp++;
                    }
                }
                $num++;
                $tempSub ++;
                if ($tempSub % $rowSize == 0 && $tempSub <= $dataLen) {
                    $objPHPExcel->createSheet();
                    $sheetindex++;
                    $sheetTemp++;
                    if ($sheetTemp < $sheetCounts) {
                        $sheetName = $fileInfo['sheetName' . $dataIndex] ?? $v['sheetName'];
                        $objPHPExcel->getSheet($sheetindex)->setTitle($sheetName . $sheetTemp);
                    }
                    $num = $v['num'];
                }
            }
        }

        if ($downType == 'D') {
            header ( 'Content-Type: application/vnd.ms-excel' );
            header ( 'Content-Disposition: attachment;filename="' . urlencode($fileName) . '.' . $suffix . '"');
            header ( 'Cache-Control: max-age=0' );
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $loadReader);
            $objWriter->save ('php://output');
            exit;
        } else {
            $fileOutPath = Util::createStorage('export') . '/' . urlencode($fileName) . '.' . $suffix;
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $loadReader);
            $objWriter->save($fileOutPath);
            return ['status' => true, 'type' => 'L', 'data' => ['filepath' => $fileOutPath]];
        }
    }

    /**
     * 生成表头导出数据
     *
     * @param array $data
     * $data = [
     * 		'head' => [
     * 			[
     * 				'value' => '表头名',
     * 				'col' => 2,	//占据多少列
     * 				'row' => 2, //占据多少行
     * 				'width' => 20, //单元格宽度
     * 				//单元格下拉格式 list=下拉选择，range=范围选择
     * 				'type' => 'list',
     * 				//下拉数据，以英文逗号分隔
     * 				'allowarray' => 'aa,bb',
     * 				'content' => '备注信息'
     * 			],
     * 			[
     * 				'value' => '表头名',
     * 				'col' => 2,
     * 				'row' => 1,
     *  			'width' => 20,
     * 				'content' => '备注信息',
     * 				//下一行数据
     * 				'children' => [
     * 					[
     * 						'value' => '表头名',
     * 						'col' => 1,
     * 						'width' => 20,
     * 						//单元格范围设置
     * 						'type' => 'range',
     * 						//范围数据，以英文逗号分隔，仅支持最大最小值设置
     * 						'allowarray' => '10,100'
     * 					],
     * 					[
     * 						'value' => '',
     * 						'width' => 20
     * 					],
     * 				],
     * 			],
     * 			[],
     * 			[]
     * 		],
     * 		//需要插入的数据
     * 		'data' => [
     * 			[],[],[],[]
     * 		],
     * 		//文件名
     * 		'fileName' => '',
     * 		//从第几行开始插入数据
     * 		'row' => 2
     * ];
     *
     * @return void
     */
    public static function recursionCreateExcel(array $map)
    {
        $PHPExecl = new \PHPExcel();
        $objWriter = \PHPExcel_IOFactory::createWriter($PHPExecl, 'Excel2007');
        $PHPExecl->getProperties()
           ->setCreator("4399om")
           ->setTitle("Office 2007 XLSX Test Document")
           ->setSubject("Office 2007 XLSX Test Document")
           ->setDescription("Generate document for Office 2007 XLSX, generated using PHP classes.")
           ->setKeywords("office 2007 openxml php")
           ->setCategory("Test result file");

        $counts = count($map['datas']);
        for ($i = 0; $i < $counts; $i++) {
            $data = $map['datas'][$i];
            $sheetIndex = $data['sheet']['index'] ?? 0;
            $sheetName = $data['sheet']['name'] ?? '';

            $PHPExecl->createSheet($sheetIndex);
            $PHPExecl->setActiveSheetIndex($sheetIndex);
            $PHPExecl->getActiveSheet()->getDefaultRowDimension()->setRowHeight(30);
            $sheet = $PHPExecl->getActiveSheet($sheetIndex);

            if (!empty($sheetName)) $PHPExecl->getSheet($sheetIndex)->setTitle($sheetName);

            static::generateExcelHeader($sheet, $data['head'], 1, 0, 0);
            static::summerInsertDataToExcel(
                $sheet,
                $data['head'],
                $data['data'],
                $data['row']
            );

            // start add by lijl 20200226 新增需要合并单元格合集--一维数组
            if (isset($data['isMerge']) && count($data['isMerge']) > 0) {
                foreach ($data['isMerge'] as $mk => $mv) $sheet->mergeCells($mv);
            }
        }

        static::outInputHeader($objWriter, $map['fileName']);
    }

    /**
     * 生成表头
     *
     * @param object $sheet
     * @param array $head 表头数据
     * @param integer $beginRow 起始行
     * @param integer $col 起始列
     * @param integer $startCol 开始列
     * @return void
     */
    private static function generateExcelHeader(
        $sheet,
        $head,
        $beginRow,
        $col,
        $startCol
    )
    {
        foreach ($head as $key => $cells) {
            $row = $beginRow;
            $beginCol = \PHPExcel_Cell::stringFromColumnIndex($col) . $row;
            $sheet->getCell($beginCol)->setValue($cells['value']);
            // 设置表格样式
            $sheet->getStyle($beginCol)->applyFromArray([
                //字体加粗
                'font' => ['bold' => true],
                'alignment' => [
                    //水平居中
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    //垂直居中
                    'vertical'=> \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation' => 0,
                    'wrap' => true,
                ],
            ]);

            // 设置单元格的宽度
            if(isset($cells['width'])) {
                $Cell = $sheet->getColumnDimension(\PHPExcel_Cell::stringFromColumnIndex($col));
                $Cell->setWidth($cells['width']);
            }
            // 元素打上标记
            if (isset($cells['content'])) static::setComment($sheet, $beginCol, $cells['content']);
            // 合并单元格
            $merge = false;
            if (isset($cells['col'])) {
                $col += $cells['col'] - 1;
                $merge = true;
            }
            if (isset($cells['row'])) {
                $row += $cells['row'] - 1;
                $merge = true;
            }
            if ($merge) {
                $endCol = \PHPExcel_Cell::stringFromColumnIndex($col) . $row;
                $sheet->mergeCells($beginCol . ':' . $endCol);
            }
            $row ++;
            $col ++;
            if (isset($cells['children']) && is_array($cells['children'])) {
                $cols = $startCol;
                if (!static::isExistChilren($cells['children'])) {
                    $cols = $col - 2;
                    $startCol = $col;
                }
                static::generateExcelHeader(
                    $sheet,
                    $cells['children'],
                    $row,
                    $cols,
                    $startCol
                );
            } else {
                $startCol = $col;
            }
        }
    }

    /**
     * 判断自己的孩子节点中是否存在孙子节点
     *
     * @param array $data
     * @return bool
     */
    private static function isExistChilren($data)
    {
        foreach ($data as $key => $value) {
            if (isset($value['children']) && is_array($value['children'])) return true;
        }
        return false;
    }

    /**
     * 生成Execl单元格备注
     *
     * @param object $sheet 当前的工作簿对象
     * @param integer $cell 需要设置属性的单元格
     * @param string $content 备注内容
     * @return void
     */
    private static function setComment($sheet, $cell, $content)
    {
        $sheet->getComment($cell)->setAuthor('4399om');
        $objCommentRichText = $sheet->getComment($cell)->getText()->createTextRun('4399om:');
        $objCommentRichText->getFont()->setBold(true);
        $sheet->getComment($cell)->getText()->createTextRun("\r\n");
        $sheet->getComment($cell)->getText()->createTextRun($content);
        $sheet->getComment($cell)->setWidth('100pt');
        $sheet->getComment($cell)->setHeight('100pt');
        $sheet->getComment($cell)->setMarginLeft('150pt');
        $sheet->getComment($cell)->getFillColor()->setRGB('EEEEEE');
    }

    /**
     * 将数据写入到数据表中
     *
     * @param object $sheet
     * @param array $head
     * @param array $data	要插入进Execl数据
     * @param integer $n 表示从第几行起的插入数据
     * @param array $ruleData  表示数据格式的规则数组
     * @return void
     */
    public static function summerInsertDataToExcel(
        $sheet,
        $head,
        $data,
        $n = 3,
        array $ruleData = []
    )
    {
        $simpleHead = static::getHeader($head);
        $row = $n;
        foreach ($data as $key => $valueArr) {
            $m = 0;
            foreach ($valueArr as $k=>$v) {
                $startCol = \PHPExcel_Cell::stringFromColumnIndex($m) . $row;
                $sheet->getCell($startCol)->setValue($v . "\t");
                $sheet->getStyle($startCol)->getAlignment()->applyFromArray([
                    'horizontal'=> \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation' => 0,
                    'wrap' => true,
                ]);
                if (isset($simpleHead[$k]['col'])) {
                    $m = $m + $simpleHead[$k]['col'] - 1;
                    $endCol = \PHPExcel_Cell::stringFromColumnIndex($m) . $row;
                    $sheet->mergeCells($startCol . ':' . $endCol);
                }
                $m++;
                $type = false;
                if (isset($simpleHead[$k]['type'])) {
                    $type = $simpleHead[$k]['type'];
                    $allowArray = $simpleHead[$k]['allowarray'];
                }
                // 设置单元格的数据验证
                if ($type) {
                    switch ($type) {
                        case 'list':
                            static::setSelectionRange($sheet, $startCol, $allowArray);
                            break;
                        case 'range':
                            static::setValueRange($sheet, $startCol, $allowArray);
                            break;
                        default:
                            break;
                    }
                }
            }
            $row ++;
        }
    }

    /**
     * 获取底层数据
     *
     * @param array $head
     * @param array $node
     * @return array
     */
    private static function getHeader($head, &$node = [])
    {
        foreach ($head as $key => $value) {
            if (isset($value['children']) && is_array($value['children'])) {
                static::getHeader($value['children'], $node);
            } else {
                $node[] = $value;
            }
        }

        return $node;
    }

    /**
     * 数据控制，设置单元格数据在一个可选方位类
     *
     * @param object $sheet
     * @param integer $cell
     * @param string $rangeStr
     * @param string $title
     * @return void
     */
    private static function setSelectionRange(
        $sheet,
        $cell,
        $rangeStr,
        $title = '数据类型'
    )
    {
        $objValidation = $sheet->getCell($cell)->getDataValidation();
        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST)
            ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP)
            ->setAllowBlank(true)
            ->setShowInputMessage(true)
            ->setShowErrorMessage(true)
            ->setShowDropDown(true)
            ->setErrorTitle('输入的值有误')
            ->setError('您输入的值不在下拉框列表内.')
            ->setPromptTitle('"'.$title.'"')
            ->setFormula1('"'.$rangeStr.'"');
    }

    /**
     * 现在单元格的有效数据范围，暂时仅限于数字
     * @param object $sheet 当前的工作簿对象
     * @param integer $cell 需要设置属性的单元格
     * @param string $valueRange 允许输入数组的访问
     * @return void
     */
    private static function setValueRange($sheet, $cell, $valueRange)
    {
        //设置单元格的的数据类型是数字，并且保留有效位数
        $sheet->getStyle($cell)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
        $valueRange = explode(',', $valueRange);

        //开始数值有效访问设定
        $objValidation = $sheet->getCell($cell)->getDataValidation();
        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE );
        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP );
        $objValidation->setAllowBlank(true);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setErrorTitle('输入错误');
        $objValidation->setError('请输入数据范围在从' . $valueRange[0] . '到' . $valueRange[1] . '之间的所有值');
        $objValidation->setPromptTitle('允许输入');
        $objValidation->setPrompt('请输入数据范围在从' . $valueRange[0] . '到' . $valueRange[1] . '之间的所有值');
        $objValidation->setFormula1($valueRange['0']);
        $objValidation->setFormula2($valueRange['1']);
    }

    /**
     * 输出
     *
     * @param object $objWriter
     * @return void
     */
    private static function outInputHeader($objWriter, $fileName = '')
    {
        $fileName = $fileName == '' ? time() : $fileName;
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="' . urlencode($fileName) . '.xlsx"');
        header("Content-Transfer-Encoding: binary");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: no-cache");
        $objWriter->save('php://output');
        exit;
    }

    /**
     * 排版模板导出
     *
     * @name	readerXslByTemplet
     */
    public static function readerXslByTemplet($map = [], $tag = 0)
    {
        $key = $map['key'];
        $excelInfo = Config::get('excel.' . $key);
        $tempExcel = $excelInfo['tempExcel'];
        $keys = $excelInfo['keys'];
        $fileName = $map['fileInfo']['fileName'] ?? uniqid().time();

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        $spreadsheet = $reader->load($tempExcel);

        foreach ($keys as $k => $v) {
            $pretty = $v['pretty'];
            if ($pretty) {
                $stratRow = $v['rowrange'][0];
                $endRow = $v['rowrange'][1];
                $sheetName = $v['sheetname'];
                $startCol = $v['colrange'][0];
                $endCol = $v['colrange'][1];
                $reader->setReadFilter( new ReadFilter($stratRow, $endRow, range($startCol, $endCol), $sheetName) );
                $data =  $map['sheet' . $v['index']]['data'][0];

                $index = $v['index'];
                $reg = '/^\$.*?\$$/';
                $sheet = $spreadsheet->getSheet($index);
                for ($row = $stratRow; $row <= $endRow; $row++) {
                    for ($col = $startCol; $col <= $endCol; $col++) {
                        $cellVal = $sheet->getCell($col . $row)->getValue();
                        if (!empty($cellVal) && preg_match($reg, $cellVal)) {
                            $dk = trim($cellVal, "$");
                            $sheet->getCell($col . $row)->setValue(($data[$dk] ?? '') . "\t");
                        }
                    }
                }
            } else {
                $index = $v['index'];
                $data =  $map['sheet' . $v['index']]['data'];
                $num = $v['num'];
                $keys = $v['key'];
                $sheet = $spreadsheet->getSheet($index);
                $len = count($data);
                $klen = count($keys);
                for ($i = 0; $i < $len; $i++) {
                    $n = $i + $num;
                    for ($j = 0; $j < $klen; $j++) {
                        if ($keys[$j] == 'index_id') {
                            $sheet->setCellValueByColumnAndRow(($j+1), $n, ($i+1) . "\t");
                        } else {
                            $sheet->setCellValueByColumnAndRow(($j+1), $n, $data[$i][$keys[$j]] . "\t");
                        }
                    }
                }
            }
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        if ($tag) {
            $return_path = Rules::createExcelUploadRule(false);
            $true_file_path = $return_path . $fileName . '.xls';
            $writer->save($true_file_path);
            return $true_file_path;
        } else {
            header ( 'Content-Type: application/vnd.ms-excel' );
            header ( 'Content-Disposition: attachment;filename="' . $fileName . '.xls"');
            header ( 'Cache-Control: max-age=0' );
            $writer->save('php://output');
            exit;
        }
    }

}
