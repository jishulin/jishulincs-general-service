<?php

namespace Jishulin\WorkFlowEngine\Tools\Utils;

class Util
{
    public static function strBC($str, $dot = 2)
    {
        bcscale($dot);
        $argv = func_get_args();
        $string = str_replace(' ', '', '(' . $argv[0] . ')');
        $string = preg_replace_callback('/\$([0-9\.]+)/', function ($matches) {
            return '$argv[$1]';
        }, $string);

        while (preg_match('/(()?)\(([^\)\(]*)\)/', $string, $match)) {
            while (preg_match('/([0-9\.]+)(\^)([0-9\.]+)/', $match[3], $m) || preg_match('/([0-9\.]+)([\*\/\%])([0-9\.]+)/', $match[3], $m) || preg_match('/([0-9\.]+)([\+\-])([0-9\.]+)/', $match[3], $m)) {
                switch ($m[2]) {
                    case '+':
                        $result = bcadd($m[1], $m[3]);
                        break;
                    case '-':
                        $result = bcsub($m[1], $m[3]);
                        break;
                    case '*':
                        $result = bcmul($m[1], $m[3]);
                        break;
                    case '/':
                        $result = bcdiv($m[1], $m[3]);
                        break;
                    case '%':
                        $result = bcmod($m[1], $m[3]);
                        break;
                    case '^':
                        $result = bcpow($m[1], $m[3]);
                        break;
                }

                $match[3] = str_replace($m[0], $result, $match[3]);
            }
            if (!empty($match[1]) && function_exists($func = 'strBC' . $match[1])) {
                $match[3] = $func($match[3], $dot);
            }
            $string = str_replace($match[0], $match[3], $string);
        }
        return $string;
    }

    /**
     * 创建目录
     *
     * @name 	mkdirs
     * @param 	string 		$dir 		创建路径
     * @param 	integer 	$mode 		设置目录的权限
     * @return 	bool 					是否创建成功
     */
    public static function mkdirs($dir, $mode = 0777)
    {
        if (is_dir($dir) || @mkdir($dir, $mode)) {
            return true;
        }

        if (!self::mkdirs(dirname($dir), $mode)) {
            return false;
        }

        return @mkdir($dir, $mode);
    }

    public static function createStorage($dir, $tag = false)
    {
        $path = './uploads/' . $dir . '/';

        if ($tag) {
            $path = public_path() . '/uploads/' . $dir . '/';
        }

        static::mkdirs($path);

        return $path;
    }

    public static function generateSystemNumber($prefix = '')
    {
        $order_id_main = date('YmdHis') . rand(10000000, 99999999);
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for ($i = 0; $i < $order_id_len; $i++) {
            $order_id_sum += (int)(substr($order_id_main, $i, 1));
        }

        $number = $prefix . $order_id_main . str_pad((100 - $order_id_sum % 100) % 100, 2, '0', STR_PAD_LEFT);

        return $number;
    }

    public static function asynicLogSql($logs, $type, $table, $data, $extra = [])
    {
        switch ($type) {
            case 'insertMulti':
                $insertSql= " INSERT INTO  ". $table . " ";
                $key = array_keys($data[0]);
                $insertSql .= " ( ".implode(',', $key)." ) VALUES " . "\n";
                $len = count($data);
                foreach($data as $k => $d){
                    $value = array_values($data[$k]);
                    $insertSql .= " ( '" . implode("','", $value) . "' ),";
                    if ($k != ($len - 1)) {
                        $insertSql .= "\n";
                    }
                }
                $insertSql = rtrim($insertSql, ',') . ";";
                $logs->info($insertSql);
                break;
            case 'insertSingle':
                $insertSql= " INSERT INTO  ". $table . " ";
                $key = array_keys($data);
                $insertSql .= " ( ".implode(',', $key)." ) VALUES " . "\n";
                $value = array_values($data);
                $insertSql .= " ( '" . implode("','", $value) . "' );";
                $logs->info($insertSql);
                break;
            case 'updateSingle':
                $updateSql = " UPDATE `" . $table . "` SET";
                $setData = $data['data'];
                foreach ($setData as $k => $v) {
                    $updateSql .= " `" . $k . "` = '" . $v . "',";
                }
                $updateSql = rtrim($updateSql, ',');
                if (!isset($data['where'])) {
                    $updateSql .= ';';
                } else {
                    $where = $data['where'];
                    $updateSql .= ' WHERE ';
                    foreach ($where as $wk => $wv) {
                        $updateSql .= " `" . $wv[0] . "` " . $wv[1] . " '" . $wv[2] . "' AND";
                    }
                    $updateSql = rtrim($updateSql, 'AND') . ';';
                }
                $logs->info($updateSql);
                break;
            case 'deleteSingle':
                $deleteSql = " DELETE FROM `" . $table . "`";
                if (!isset($data['where'])) {
                    $deleteSql .= ';';
                } else {
                    $where = $data['where'];
                    $deleteSql .= ' WHERE ';
                    foreach ($where as $wk => $wv) {
                        $deleteSql .= " `" . $wv[0] . "` " . $wv[1] . " '" . $wv[2] . "' AND";
                    }

                    if ($extra && isset($extra['whereIn'])) {
                        $whereIn = $extra['whereIn'];
                        foreach ($whereIn as $wink => $winv) {
                            $deleteSql .= " `" . $winv[0] . "` in " . " ( '" . implode("','", $winv[1]) . "' )" . " AND";
                        }
                    }

                    $deleteSql = rtrim($deleteSql, 'AND') . ';';
                }
                $logs->info($deleteSql);
                break;
        }
    }

    public static function getSystemInnerDate()
    {
        $systemDate = [
            'SYSTEMDATE0' => date('Y-m-d'),
            'SYSTEMDATE1' => date('Y/m/d'),
            'SYSTEMDATE2' => date('Ymd'),
            'SYSTEMDATE3' => date('Y') . ' 年 ' . date('m') . ' 月 ' . date('d') . ' 日',
            'SYSTEMYEAR' => date('Y'),
            'SYSTEMMONTH' => date('m'),
            'SYSTEMDAY' => date('d'),
            'SYSTEMYEARMONTH0' => date('Y-m'),
            'SYSTEMYEARMONTH1' => date('Y/m'),
            'SYSTEMYEARMONTH2' => date('Ym'),
            'SYSTEMYEARMONTH3' => date('Y') . ' 年 ' . date('m') . ' 月',
            'SYSTEMTIME0' => date('Y-m-d H:i:s'),
            'SYSTEMTIME1' => date('Y') . ' 年 ' . date('m') . ' 月 ' . date('d') . ' 日 ' . date('H') . ' 时 ' . date('i') . ' 分 ' . date('s') . ' 秒',
        ];

        return $systemDate;
    }

    public static function getWeekDateByCurrentTime($time = '', $format='Y-m-d')
    {
        $time = $time != '' ? $time : time();
        $week = date('w', $time);
        $date = [];
        for ($i = 1; $i <= 7; $i++){
            $date[$i] = date($format, strtotime( '+' . $i - $week . ' days', $time));
        }

        return array_values($date);
    }
}
