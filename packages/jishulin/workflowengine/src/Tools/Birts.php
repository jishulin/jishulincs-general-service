<?php

namespace Jishulin\WorkFlowEngine\Tools;

use Jishulin\WorkFlowEngine\Config\Config;
use Illuminate\Support\Facades\Config as APP;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Jishulin\WorkFlowEngine\Tools\Utils\Words;
use Jishulin\WorkFlowEngine\Tools\Utils\Exports;
use Jishulin\WorkFlowEngine\Tools\Utils\FakerSeed;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;
use Jishulin\WorkFlowEngine\Tools\Utils\ProcessUtil;
use Jishulin\WorkFlowEngine\Tools\Tools;
use Jishulin\WorkFlowEngine\Log\LLog;

class Birts
{
    use \Jishulin\WorkFlowEngine\Traits\ReturnJson;

    /**
     * @var $_instance
     */
    private	static $_instance;

    private $logtypes = 'sql';

    private $prefix;

    /**
     * @return void
     */
    private function __construct()
    {
        $this->prefix = APP::get('database.connections.mysql.prefix');
    }

    /**
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * Provide external static invocation method
     *
     * @return \Jishulin\WorkFlowEngine\Tools\Birts
     */
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * 创建报表
     *
     * @name    birtCrd
     *
     * @param   array       $list           请求参数
     *
     * @return  array                       返回结果
     */
    public function birtCrd($list)
    {
        // 基础参数校验
        $insertData['wf04001'] = $list['flowId'];
        $insertData['wf04002'] = $list['nodeId'];
        $insertData['wf04003'] = !isset($list['uuid']) || $list['uuid'] == '' ? Uuid::uuid1()->getHex() : $list['uuid'];
        $insertData['wf04004'] = $list['birtName'] ?? '';
        $insertData['wf04005'] = $list['birtSrc'] ?? '';
        $insertData['wf04006'] = isset($list['birtConfig']) ? json_encode($list['birtConfig']) : '';
        $insertData['wf04007'] = $list['birtType'] ?? 0;
        $insertData['wf04008'] = $list['birtTransPdf'] ?? 1;
        $insertData['wf04009'] = $list['birtIsSingleDetail'] ?? 0;
        $insertData['wf04010'] = $list['birtStatus'] ?? 1;
        $insertData['wf04011'] = $list['birtOrders'] ?? 10;
        $insertData['created_at'] = $list['birtCreatedAt'] ?? Carbon::now();

        try {
            $id = DB::table('wf_workbirts')->insertGetId($insertData);

            $this->logs = LLog::init($this->logtypes, $list['flowId']);
            Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workbirts', $insertData);

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', ['id' => $id]);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 编辑报表
     *
     * @name    birtCrd
     *
     * @param   string 	    $uuid           UUID
     * @param   array       $list           请求参数
     *
     * @return  array                       返回结果
     */
    public function birtEdt($uuid, $list)
    {
        if (isset($list['birtName'])) {
            $data['wf04004'] = $list['birtName'];
        }
        if (isset($list['birtSrc'])) {
            $data['wf04005'] = $list['birtSrc'];
        }
        if (isset($list['birtConfig'])) {
            $data['wf04006'] = json_encode($list['birtConfig']);
        }
        if (isset($list['birtType'])) {
            $data['wf04007'] = $list['birtType'];
        }
        if (isset($list['birtTransPdf'])) {
            $data['wf04008'] = $list['birtTransPdf'];
        }
        if (isset($list['birtIsSingleDetail'])) {
            $data['wf04009'] = $list['birtIsSingleDetail'];
        }
        if (isset($list['birtStatus'])) {
            $data['wf04010'] = $list['birtStatus'];
        }
        if (isset($list['birtOrders'])) {
            $data['wf04011'] = $list['birtOrders'];
        }
        if (isset($list['birtUpdatedAt']) && $list['birtUpdatedAt']) {
            $data['updated_at'] = $list['birtUpdatedAt'];
        } else {
            $data['updated_at'] = Carbon::now();
        }

        try {
            $flowId = $this->getBirtFlowIdByUuid($uuid);
            $where = [['wf04003', '=', $uuid]];
            DB::table('wf_workbirts')->where($where)->update($data);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workbirts', [
                'where' => $where,
                'data' => $data
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 删除报表
     *
     * @name    birtRmvByUuid
     *
     * @param   string      $uuid           请求参数
     *
     * @return  array                       返回结果
     */
    public function birtRmvByUuid($uuid)
    {
        $flowId = $this->getBirtFlowIdByUuid($uuid);
        try {
            $where = [['wf04003', '=', $uuid]];
            DB::table('wf_workbirts')
                ->where($where)
                ->delete();

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_workbirts', [
                'where' => $where
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取流程节点下的报表信息
     *
     * @name    getBirtListByFlowNodeId
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return  array                       返回结果
     */
    public function getBirtListByFlowNodeId($flowId, $nodeId)
    {
        try {
            $birts = DB::table('wf_workbirts')
                ->where('wf04001', $flowId)
                ->where('wf04002', $nodeId)
                ->orderby('wf04011', 'ASC')
                ->get()
                ->toArray();

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $birts);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 启用报表
     *
     * @name    birtEnabled
     *
     * @param   string      $uuid           UUID
     *
     * @return  array                       返回结果
     */
    public function birtEnabled($uuid)
    {
        $flowId = $this->getBirtFlowIdByUuid($uuid);
        try {
            $where = [['wf04003', '=', $uuid]];
            $updateData = ['wf04010' => 1, 'updated_at' => Carbon::now()];
            DB::table('wf_workbirts')->where($where)->update($updateData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workbirts', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 禁用报表
     *
     * @name    birtDisabled
     *
     * @param   string      $uuid           UUID
     *
     * @return  array                       返回结果
     */
    public function birtDisabled($uuid)
    {
        $flowId = $this->getBirtFlowIdByUuid($uuid);
        try {
            $where = [['wf04003', '=', $uuid]];
            $updateData = ['wf04010' => 0, 'updated_at' => Carbon::now()];
            DB::table('wf_workbirts')->where('wf04003', $uuid)->update($updateData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workbirts', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 根据UUID获取报表信息
     *
     * @name    getBirtByUuid
     *
     * @param   string      $uuid           UUID
     *
     * @return  array                       返回结果
     */
    public function getBirtByUuid($uuid)
    {
        try {
            // 数据查询
            if (!$birtData = DB::table('wf_workbirts')->where('wf04003', $uuid)->first()) {
                return $this->jsonReturns(1, Config::ERRORBIRT);
            }
            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $birtData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 根据UUID获取报表名称
     *
     * @name    getBirtNameByUuid
     *
     * @param   string      $uuid           UUID
     *
     * @return  array                       返回结果
     */
    public function getBirtNameByUuid($uuid) {
        return DB::table('wf_workbirts')->where('wf04003', $uuid)->value('wf04004');
    }

    /**
     * 根据UUID获取报表流程ID
     *
     * @name    getBirtFlowIdByUuid
     *
     * @param   string      $uuid           UUID
     *
     * @return  array                       返回结果
     */
    public function getBirtFlowIdByUuid($uuid) {
        return DB::table('wf_workbirts')->where('wf04003', $uuid)->value('wf04001');
    }

    /**
     * word转pdf调用
     *
     * @name    word2pdf
     * @param   string          $docx
     * @param   string          $pdf
     * @return  array
     */
    public function word2pdf($docx, $pdf)
    {
        $command = APP::get('app.javajar') . " -jar " . APP::get('app.jodconverter') . " " . $docx . " " . $pdf;
        exec($command, $output, $return_var);
        if ($return_var != 0) {
            return ['error' => 1, 'message' => Config::TRANSPDFERROR];
        } else {
            return ['error' => 0, 'message' => Config::TRANSPDFSUCCESS];
        }
    }

    public function downLoadPdf($originFile)
    {
        $originFileData = pathinfo($originFile);
        $targetFile = $originFileData['dirname'] . '/' . $originFileData['filename'] . '.pdf';

        $originRealPath = realpath($originFile);
        $targetRealPath = str_replace($originFileData['basename'], $originFileData['filename'] . '.pdf', $originRealPath);

        $transRes = $this->word2pdf($originRealPath, $targetRealPath);

        if ($transRes['error']) {
            unlink($originFile);
            return $this->jsonReturns(1, $transRes['message']);
        }

        header('Content-Type: application/pdf');
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Disposition:inline;filename="' . $targetFile . '"');
        header("Content-Transfer-Encoding: binary");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: no-cache");
        echo file_get_contents($targetFile);
        unlink($targetFile);
        unlink($originFile);
        exit;
    }

    /**
     * 打印报表通过UUID
     *
     * @name    birtPrint
     *
     * @param   string      $uuid           UUID
     * @param   string      $mapData        数据信息
     *
     * @return  array                       返回结果
     */
    public function birtPrintByUuid($uuid, $mapData = [], $verify = 1)
    {
        $birtData = (array)DB::table('wf_workbirts')->where('wf04003', $uuid)->first();
        if (empty($birtData)) return $this->jsonReturns(1, Config::ERRORBIRT);
        if (!$birtData['wf04005']) return $this->jsonReturns(1, Config::ERRORFILENOTEXISTS);
        $filePath = public_path() . $birtData['wf04005'];
        $config['key'] = $filePath;
        $config['fields'] = json_decode($birtData['wf04006'], true);
        $mapData['config'] = $config;

        // 报表名称
        $mapData['fileName'] = $mapData['fileName'] ?? ($birtData['wf04004'] . time());
        // 数据校验
        if ($verify === 1) {
            if (!is_file($filePath) || !file_exists($filePath)) return $this->jsonReturns(1, Config::ERRORFILENOTEXISTS);
        }

        $mapData['downType'] = $birtData['wf04008'] == 1 ? 'L' : 'D';

        // 是否是需要模拟
        if (isset($mapData['simulation']) && $mapData['simulation'] === true) {
            // 生成模拟数据
            $faker = new FakerSeed();
            $fakerData = $faker->fakerBirtData($config['fields']);
            $mapData = array_merge($mapData, $fakerData);
        }

        $res = Words::getInstance()->createWordUseTemplate($mapData);
        $file = $res['data']['filepath'];

        return $this->downLoadPdf($file);
    }

    /**
     * 报表列表导出
     *
     * @name    birtExportJobList
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   array       $apiUser        当前用户信息
     * @param   string      $fileName       文件名
     *
     * @return  array                       返回结果
     */
    public function birtExportJobList($flowId, $nodeId, $apiUser, $fileName = null)
    {
        $birtData = (array)DB::table('wf_workbirts')
            ->where('wf04001', $flowId)
            ->where('wf04002', $nodeId)
            ->where('wf04007', 1)
            ->where('wf04010', 1)
            ->first();

        $exportCustTemplateJob = function () use ($flowId, $nodeId, $birtData, $fileName, $apiUser) {
            $birtTemplate = $birtData['wf04005'];
            if (!$birtTemplate) return $this->jsonReturns(1, Config::BIRTLISTERROR);
            $filePath = public_path() . $birtData['wf04005'];
            if (!is_file($filePath) || !file_exists($filePath)) return $this->jsonReturns(1, Config::ERRORFILENOTEXISTS);
            // 模板文件校验(是否是excel)
            $extension = pathinfo($filePath, PATHINFO_EXTENSION);
            if (!in_array($extension, ['xls', 'xlsx'])) return $this->jsonReturns(1, Config::ERRORFILEFORMAT);

            $birtName = static::getBirtNameByUuid($birtData['wf04003']);
            $birtExportKey = [];
            if ($birtData['wf04006']) {
                $keyConfig = json_decode($birtData['wf04006'], true)['single'] ?? [];
                if ($keyConfig) {
                    foreach ($keyConfig as $k => $v) {
                        array_push($birtExportKey, ($v[1] ? trim($v[1]) : (uniqid() . $k)));
                    }
                }
            }

            $dataList = ProcessUtil::getJobListForBirt($flowId, $nodeId, $apiUser);

            $mapData = [
                'keys' => [['key' => $birtExportKey, 'num' => 2, 'index' => 0]],
                'filePath' => $filePath,
                'sheet0' => ['data' => $dataList],
                'fileInfo' => [
                    'sheetName0' => $birtName,
                    'fileName' => is_null($fileName) || empty($fileName) ? ($birtName . time()) : $fileName
                ],
            ];
            $mapData['downType'] = $birtData['wf04008'] == 1 ? 'L' : 'D';

            $res = Exports::caculatExcelExport($mapData);
            $file = $res['data']['filepath'];

            return $this->downLoadPdf($file);
        };

        $exportSystemDefaultJob = function () use ($flowId, $nodeId, $fileName, $apiUser) {
            // 文件名处理
            if (is_null($fileName) || empty($fileName)) {
                $fileName = ProcessUtil::getFlowNodeName($flowId, $nodeId) . time();
            }

            $mapData = [];
            // 获取列表字段信息(字段名及显示名称)
            $columnData = Tools::getInstance()->getWorkJobGridColumns($flowId, $nodeId);
            $header = [];
            $columns = [];
            foreach ($columnData as $key => $val) {
                if ($val['prop'] !== 'keyword') {
                    $header[] = ['value' => $val['label'], 'width' => 20];
                    array_push($columns, $val['prop']);
                }
            }
            // 获取业务数据
            $dataList = ProcessUtil::getJobListForBirt($flowId, $nodeId, $apiUser, $columns);

            $mapData['datas'] = [['row' => 2, 'head' => $header, 'data' => $dataList]];
            $mapData['fileName'] = $fileName;

            // 生成表头excel并导出
            Exports::recursionCreateExcel($mapData);
        };

        return $birtData ? $exportCustTemplateJob() : $exportSystemDefaultJob();
    }

    /**
     * 客户列表导出
     *
     * @name    exportCustomerListByFlowId
     *
     * @param   string      $flowId         流程ID
     * @param   array       $outWhere       外部条件
     * @param   array       $apiUser        当前用户信息
     * @param   string      $fileName       文件名
     *
     * @return  array                       返回结果
     */
    public function exportCustomerListByFlowId($flowId, $outWhere, $apiUser, $fileName = null)
    {
        // 文件名处理
        if (is_null($fileName) || empty($fileName)) {
            $fileName = Config::CUSTOMERNAME . time();
        }

        $mapData = [];
        // 获取列表字段信息(字段名及显示名称)
        $columnData = Tools::getInstance()->getCustomerGridColumns($flowId, $apiUser);
        $header = [];
        $columns = [];
        foreach ($columnData as $key => $val) {
            if ($val['prop'] !== 'keyword') {
                $header[] = ['value' => $val['label'], 'width' => 20];
                if ($val['prop'] == 'JOBSTATUS') {
                    array_push($columns, 'wf12006 as ' . $val['prop']);
                } elseif ($val['prop'] == 'CURRENTNODE') {
                    array_push($columns, 'wf12004 as ' . $val['prop']);
                } else {
                    array_push($columns, $val['prop']);
                }
            }
        }
        // 获取业务数据
        $dataList = ProcessUtil::getJobCustomerListForExport($flowId, $outWhere, $apiUser, $columns);
        $mapData['datas'] = [['row' => 2, 'head' => $header, 'data' => $dataList]];
        $mapData['fileName'] = $fileName;

        // 生成表头excel并导出
        Exports::recursionCreateExcel($mapData);

        return $birtData ? $exportCustTemplateJob() : $exportSystemDefaultJob();
    }

    /**
     * 流程业务字段导出
     *
     * @name    exportCustomerListByFlowId
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function exportWorkFlowJobCloumnsByFlowId($flowId)
    {
        $header = Config::JOBCLOMUNSHEADER;
        $fields = ['wf06003', 'wf06004', 'wf06005', 'wf06008', 'wf06009', 'wf06010'];
        $jobTables = ProcessUtil::getSubJobTables($flowId, -2);
        $datas = [];
        foreach ($jobTables as $k => $v) {
            $datas[$k] = [
                'row' => 2,
                'head' => $header,
                'data' => [],
                'sheet' => [],
            ];
            $sheet = [
                'index' => $k,
                'name' => ProcessUtil::getFieldByUniqid('wf_workjobtables', [['wf05001', '=', $flowId], ['wf05003', '=', $v]], 'wf05002')
            ];
            $datas[$k]['sheet'] = $sheet;
            $jobColumns = DB::table('wf_workjobcolumns')->select($fields)->where('wf06001', $flowId)->where('wf06002', $v)->get()->toArray();
            foreach ($jobColumns as $key => $val) {
                $jobColumns[$key]->wf06008 = $val->wf06008 ? Config::JOBCLOMUNSCODETYPE[(string)$val->wf06008] : '';
            }

            $datas[$k]['data'] = $jobColumns;
        }

        $mapData['datas'] = $datas;
        $mapData['fileName'] = ProcessUtil::getFieldByUniqid('wf_workflows', [['wf01001', '=', $flowId]], 'wf01002') . time();

        // 生成表头excel并导出
        Exports::recursionCreateExcel($mapData);
    }
}
