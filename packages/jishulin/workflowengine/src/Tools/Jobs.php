<?php

namespace Jishulin\WorkFlowEngine\Tools;

use Jishulin\WorkFlowEngine\Config\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config as APP;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Jishulin\WorkFlowEngine\Log\LLog;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;

class Jobs
{
    use \Jishulin\WorkFlowEngine\Traits\ReturnJson;

    /**
     * @var $_instance
     */
    private	static $_instance;

    private $logtypes = 'sql';

    // private $columns = 'ZGJOB01';

    /**
     * @return void
     */
    private function __construct()
    {
        $this->prefix = APP::get('database.connections.mysql.prefix');
    }

    /**
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * Provide external static invocation method
     *
     * @return \Jishulin\WorkFlowEngine\Tools\Jobs
     */
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * 通过流程ID获取流程配置业务表树-elementui
     *
     * @name    getJobConfigsTreeByFlowId
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function getJobConfigsTreeByFlowId($flowId)
    {
        try {
            $jsonData = [];
            $parent = (array)DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05004', 1)->first();
            if (empty($parent)) {
                return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
            }

            $jsonData = [];
            $jsonData['id'] = $parent['id'];
            $jsonData['flowId'] = $parent['wf05001'];
            $jsonData['table'] = $parent['wf05003'];
            $jsonData['label'] = $parent['wf05002'];
            $jsonData['system'] = $parent['wf05006'];
            $jsonData['ismain'] = $parent['wf05004'];

            $chilren = DB::table('wf_workjobtables')
                ->select('id', 'wf05001 as flowId', 'wf05003 as table', 'wf05002 as label', 'wf05006 as system', 'wf05004 as ismain')
                ->where('wf05001', $flowId)
                ->where('wf05004', 0)
                ->orderby('created_at', 'ASC')
                ->get()
                ->toArray();
            $jsonData['children'] = $chilren;

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', [$jsonData]);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务表创建
     *
     * @name    jobConfigCrd
     *
     * @param   string      $flowId         流程ID
     * @param   array       $mapData        业务表信息
     *
     * @return  array                       返回结果
     */
    public function jobConfigCrd($flowId, $mapData)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            // 数据表字段信息校验

            $tableNameOrigin = $mapData['table'];
            $tableName = $this->prefix . $tableNameOrigin;
            $tableComment = $mapData['comment'];

            // 表名规则校验
            if (substr($tableNameOrigin, 0, 5) !== 'zgjob') {
                return $this->jsonReturns(1, Config::JOBTABLERULEERROR);
            }

            $sql = "SELECT count(*) AS count FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME=? LIMIT 1";
            $model = (array)DB::select($sql, [APP::get('database.connections.mysql.database'), $tableName]);
            if ($model[0]->count > 0) {
                return $this->jsonReturns(1, Config::JOBTABLEEXISTS);
            }

            // 存在即副表，否则即主表
            if (DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05004', 1)->count()) {
                $createJobSubSql = "CREATE TABLE `" . $tableName . "` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `PID` bigint(20) NOT NULL,
                        PRIMARY KEY (`id`) USING BTREE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='" . $tableComment . "';";
                DB::statement($createJobSubSql);
                $this->logs->info($createJobSubSql);

                $insertData = ['wf05001' => $flowId, 'wf05002' => $tableComment, 'wf05003' => $tableNameOrigin, 'wf05004' => 0, 'wf05005' => 1, 'wf05006' => 0, 'wf05007' => 0, 'wf05008' => 0, 'created_at' => Carbon::now()];
                DB::table('wf_workjobtables')->insert($insertData);

                Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workjobtables', $insertData);
            } else {
                $tableNameCol =strtoupper($tableNameOrigin);
                // 主表
                $createJobMainsql = "CREATE TABLE `" . $tableName . "` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `REQUIRED` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'REQUIRED[完整=1，不完整=0]',
                        `NUMBER` varchar(100) NOT NULL COMMENT 'NUMBER',
                        `NAME` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'NAME',
                        `COMPANYID` bigint(20) NOT NULL DEFAULT '0' COMMENT 'COMPANYID',
                        PRIMARY KEY (`id`) USING BTREE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='" . $tableComment . "';";
                DB::statement($createJobMainsql);

                $createExamSql = "CREATE TABLE `" . ($tableName . '01') . "` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `PID` bigint(20) NOT NULL,
                        `" . $tableNameCol . "010001` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核结果',
                        `" . $tableNameCol . "010002` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核时间',
                        `" . $tableNameCol . "010003` text COLLATE utf8mb4_unicode_ci COMMENT '审核意见',
                        `" . $tableNameCol . "010004` bigint(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '审核人ID',
                        `" . $tableNameCol . "010005` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核人',
                        `" . $tableNameCol . "010006` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '审核类型',
                        `" . $tableNameCol . "010007` tinyint(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '审核类型标识[0=审核节点，1=非审核节点]',
                        PRIMARY KEY (`id`) USING BTREE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='审核记录(副表)';";
                DB::statement($createExamSql);

                $createNodeSql = "CREATE TABLE `" . ($tableName . '02') . "` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `PID` bigint(20) NOT NULL,
                        `" . $tableNameCol . "020001` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点ID',
                        `" . $tableNameCol . "020002` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点名称',
                        `" . $tableNameCol . "020003` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点首次提交时间',
                        `" . $tableNameCol . "020004` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点最后一次提交时间',
                        `" . $tableNameCol . "020005` bigint(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '节点提交人ID',
                        `" . $tableNameCol . "020006` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点提交人',
                        PRIMARY KEY (`id`) USING BTREE
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='时间节点记录(副表)';";
                DB::statement($createNodeSql);
                $this->logs->info($createJobMainsql);
                $this->logs->info($createExamSql);
                $this->logs->info($createNodeSql);
                $insertData = [
                    ['wf05001' => $flowId, 'wf05002' => $tableComment, 'wf05003' => $tableNameOrigin, 'wf05004' => 1, 'wf05005' => 1, 'wf05006' => 0, 'wf05007' => 0, 'wf05008' => 0, 'created_at' => Carbon::now()],
                    ['wf05001' => $flowId, 'wf05002' => '审核记录表', 'wf05003' => $tableNameOrigin . '01', 'wf05004' => 0, 'wf05005' => 1, 'wf05006' => 1, 'wf05007' => 1, 'wf05008' => 0, 'created_at' => Carbon::now()],
                    ['wf05001' => $flowId, 'wf05002' => '节点提交时间记录表', 'wf05003' => $tableNameOrigin . '02', 'wf05004' => 0, 'wf05005' => 1, 'wf05006' => 1, 'wf05007' => 0, 'wf05008' => 1, 'created_at' => Carbon::now()],
                ];
                DB::table('wf_workjobtables')->insert($insertData);
                Util::asynicLogSql($this->logs, 'insertMulti', $this->prefix . 'wf_workjobtables', $insertData);

                $examConfigInsertData = [
                    'wf09001' => $flowId,
                    'wf09002' => $tableNameCol . '010001',
                    'wf09003' => $tableNameCol . '010002',
                    'wf09004' => $tableNameCol . '010003',
                    'wf09005' => $tableNameCol . '010004',
                    'wf09006' => $tableNameCol . '010005',
                    'wf09007' => $tableNameCol . '010006',
                    'wf09008' => $tableNameCol . '010007',
                    'wf09009' => 1,
                    'created_at' => Carbon::now()
                ];
                DB::table('wf_worksysexams')->insert($examConfigInsertData);
                Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_worksysexams', $examConfigInsertData);

                $nodeTimeConfigInsertData = [
                    'wf10001' => $flowId,
                    'wf10002' => $tableNameCol . '020001',
                    'wf10003' => $tableNameCol . '020002',
                    'wf10004' => $tableNameCol . '020003',
                    'wf10005' => $tableNameCol . '020004',
                    'wf10006' => $tableNameCol . '020005',
                    'wf10007' => $tableNameCol . '020006',
                    'wf10008' => 1,
                    'created_at' => Carbon::now()
                ];
                DB::table('wf_worksysnodetimes')->insert($nodeTimeConfigInsertData);
                Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_worksysnodetimes', $nodeTimeConfigInsertData);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务表编辑
     *
     * @name    jobConfigEdt
     *
     * @param   string      $flowId         流程ID
     * @param   array       $mapData        业务表信息
     *
     * @return  array                       返回结果
     */
    public function jobConfigEdt($flowId, $mapData)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $tableNameOrigin = $mapData['table'];
            $tableName = $this->prefix . $tableNameOrigin;
            $tableComment = $mapData['comment'];

            // 修改表注释
            $alterSql = " ALTER TABLE `" . $tableName . "` COMMENT= '" . $tableComment . "';";
            DB::statement($alterSql);
            $this->logs->info($alterSql);

            // 数据更新操作
            $where = [['wf05001', '=', $flowId], ['wf05003', '=', $tableNameOrigin]];
            $update = ['wf05002' => $tableComment, 'updated_at' => Carbon::now()];
            DB::table('wf_workjobtables')->where($where)->update($update);

            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workjobtables', [
                'where' => $where,
                'data' => $update
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取业务表最大字段
     *
     * @name    getJobConfigsMaxColumn
     *
     * @param   string      $flowId         流程ID
     * @param   string      $table          业务表
     *
     * @return  array                       返回结果
     */
    public function getJobConfigsMaxColumn($flowId, $table)
    {
        try {
            $maxnum = DB::table('wf_workjobcolumns')
                ->where('wf06001', $flowId)
                ->where('wf06002', $table)
                ->max('wf06003');

            $columns = strtoupper($table);

            if ($maxnum) {
                $strNum = substr($maxnum, strlen($columns), strlen($maxnum));
                $strpadLen = strlen($strNum);
                $strNum = str_pad(((int)$strNum + 1), $strpadLen, '0', STR_PAD_LEFT);
            } else {
                $strNum = '0001';
            }
            $jsonData = [
                'prefix' => $columns,
                'maxnum' => $strNum
            ];

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取业务表字段信息
     *
     * @name    getJobConfigsList
     *
     * @param   string      $flowId         流程ID
     * @param   string      $table          业务表
     *
     * @return  array                       返回结果
     */
    public function getJobConfigsList($flowId, $table)
    {
        try {
            $data = DB::table('wf_workjobcolumns')
                ->select(
                    'id', 'wf06001 as flowId', 'wf06002 as table', 'wf06003 as columnName',
                    'wf06004 as comment', 'wf06005 as columnType', 'wf06006 as columnLength',
                    'wf06007 as columnDig', 'wf06008 as codeType', 'wf06009 as sysNumber', 'wf06010 as comNumber'
                )
                ->where('wf06001', $flowId)
                ->where('wf06002', $table)
                ->get()
                ->toArray();

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $data);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务表字段信息保存
     *
     * @name    jobConfigSave
     *
     * @param   array       $mapData        业务字段信息
     *
     * @return  array                       返回结果
     */
    public function jobConfigSave($mapData)
    {
        // 数据校验TODO
        $flowId = $mapData['flowId'];
        $tableNameOrigin = $mapData['table'];
        $tableName = $this->prefix . $tableNameOrigin;

        // 判断字段是否存在数据表中
        $descSql = " DESCRIBE `" . $tableName .  "` `" . $mapData['columnName'] . "`;";
        if (count(DB::select($descSql))) {
            return $this->jobConfigModifySave($mapData);
        }

        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $alterAddSql = " ALTER TABLE `" . $tableName . "` ADD COLUMN `" . $mapData['columnName'] . "` ";
            $insertData = [
                'wf06001' => $flowId,
                'wf06002' => $tableNameOrigin,
                'wf06003' => $mapData['columnName'],
                'wf06004' => $mapData['comment'],
                'wf06005' => $mapData['columnType'],
            ];
            switch ($mapData['columnType']) {
                case 'varchar':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ? $mapData['columnLength'] : 100;
                    $alterAddSql .= "varchar(" . $len . ") NOT NULL DEFAULT '' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'text':
                    $len = 100;
                    $alterAddSql .= "text NULL COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'decimal':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 20 ? 20 : $mapData['columnLength']) : 15;
                    $dig = (isset($mapData['columnDig']) && !empty($mapData['columnDig'])) ?
                        ($mapData['columnDig'] >= $len ? ($len - 2) : $mapData['columnDig']) : 2;
                    $default = "0." . str_pad(0, $dig, '0', STR_PAD_LEFT);
                    $alterAddSql .= "decimal(" . $len . ", " . $dig . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'bigint':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 20 ? 20 : $mapData['columnLength']) : 20;
                    $dig = 0;
                    $default = 0;
                    $alterAddSql .= "bigint(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'int':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 11 ? 11 : $mapData['columnLength']) : 11;
                    $dig = 0;
                    $default = 0;
                    $alterAddSql .= "int(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'tinyint':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 4 ? 4 : $mapData['columnLength']) : 4;
                    $dig = 0;
                    $default = 0;
                    $alterAddSql .= "tinyint(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
            }
            DB::statement($alterAddSql);
            $this->logs->info($alterAddSql);

            $insertData['wf06006'] = isset($len) ? $len : $mapData['columnLength'];
            $insertData['wf06007'] = isset($dig) ? $dig : ($mapData['columnDig'] ?? 0);
            $insertData['wf06008'] = $mapData['codeType'] ?? '';
            $insertData['wf06009'] = $mapData['sysNumber'] ?? '';
            $insertData['wf06010'] = $mapData['comNumber'] ?? '';

            DB::table('wf_workjobcolumns')->insert($insertData);

            Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workjobcolumns', $insertData);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务字段修改保存
     *
     * @name    jobConfigModifySave
     *
     * @param   array       $mapData        业务字段信息
     *
     * @return  array                       返回结果
     */
    public function jobConfigModifySave($mapData)
    {
        $flowId = $mapData['flowId'];
        $tableNameOrigin = $mapData['table'];
        $tableName = $this->prefix . $tableNameOrigin;

        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $alterSql = " ALTER TABLE `" . $tableName . "` MODIFY COLUMN `" . $mapData['columnName'] . "` ";
            $updateData = [
                'wf06004' => $mapData['comment'],
                'wf06005' => $mapData['columnType'],
            ];
            switch ($mapData['columnType']) {
                case 'varchar':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ? $mapData['columnLength'] : 100;
                    $alterSql .= "varchar(" . $len . ") CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'text':
                    $len = 100;
                    $alterSql .= "text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'decimal':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 20 ? 20 : $mapData['columnLength']) : 15;
                    $dig = (isset($mapData['columnDig']) && !empty($mapData['columnDig'])) ?
                        ($mapData['columnDig'] >= $len ? ($len - 2) : $mapData['columnDig']) : 2;
                    $default = "0." . str_pad(0, $dig, '0', STR_PAD_LEFT);
                    $alterSql .= "decimal(" . $len . ", " . $dig . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'bigint':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 20 ? 20 : $mapData['columnLength']) : 20;
                    $dig = 0;
                    $default = 0;
                    $alterSql .= "bigint(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'int':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 11 ? 11 : $mapData['columnLength']) : 11;
                    $dig = 0;
                    $default = 0;
                    $alterSql .= "int(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
                case 'tinyint':
                    $len = (isset($mapData['columnLength']) && !empty($mapData['columnLength'])) ?
                        ($mapData['columnLength'] > 4 ? 4 : $mapData['columnLength']) : 4;
                    $dig = 0;
                    $default = 0;
                    $alterSql .= "tinyint(" . $len . ") NOT NULL DEFAULT '" . $default . "' COMMENT '" . $mapData['comment'] . "';";
                    break;
            }
            DB::statement($alterSql);
            $this->logs->info($alterSql);

            $updateData['wf06006'] = isset($len) ? $len : $mapData['columnLength'];
            $updateData['wf06007'] = isset($dig) ? $dig : ($mapData['columnDig'] ?? 0);
            $updateData['wf06008'] = $mapData['codeType'] ?? '';
            $updateData['wf06009'] = $mapData['sysNumber'] ?? '';
            $updateData['wf06010'] = $mapData['comNumber'] ?? '';
            $where = [['wf06001', '=', $flowId], ['wf06002', '=', $tableNameOrigin], ['wf06003', '=', $mapData['columnName']]];
            DB::table('wf_workjobcolumns')->where($where)->update($updateData);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workjobcolumns', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务字段删除
     *
     * @name    jobConfigRmv

     * @param   string      $flowId         流程ID
     * @param   string      $table          业务表
     * @param   string      $column         业务字段
     *
     * @return  array                       返回结果
     */
    public function jobConfigRmv($flowId, $table, $column)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $tableNameOrigin = $table;
            $tableName = $this->prefix . $tableNameOrigin;

            // 修改表注释
            $alterDelSql = " ALTER TABLE `" . $tableName . "` DROP COLUMN `" . $column . "`;";
            DB::statement($alterDelSql);
            $this->logs->info($alterDelSql);

            $where = [['wf06001', '=', $flowId], ['wf06002', '=', $tableNameOrigin], ['wf06003', '=', $column]];
            DB::table('wf_workjobcolumns')->where($where)->delete();

            Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_workjobcolumns', [
                'where' => $where
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 业务表删除
     *
     * @name    jobTableRmv
     *
     * @param   string      $flowId         流程ID
     * @param   string      $table          业务表
     *
     * @return  array                       返回结果
     */
    public function jobTableRmv($flowId, $table)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $tableNameOrigin = $table;
            $tableName = $this->prefix . $tableNameOrigin;

            // 删除表
            $dropSql = " DROP TABLE `" . $tableName . "`;";
            DB::statement($dropSql);
            $this->logs->info($dropSql);

            // 删除表配置
            $condition = [['wf05001', '=', $flowId], ['wf05003', '=', $tableNameOrigin]];
            DB::table('wf_workjobtables')->where($condition)->delete();
            Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_workjobtables', [
                'where' => $condition
            ]);

            // 删除表字段配置
            $where = [['wf06001', '=', $flowId], ['wf06002', '=', $tableNameOrigin]];
            DB::table('wf_workjobcolumns')->where($where)->delete();
            Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_workjobcolumns', [
                'where' => $where
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取节点字段配置
     *
     * @name    jobTableRmv
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return  array                       返回结果
     */
    public function getNodeColumnsConfig($flowId, $nodeId)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            // 获取主数据业务表
            $mainJob = DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05004', 1)->value('wf05003');
            // dump($mainJob);exit;
            // 获取主业务表字段信息
            $jobColumns = DB::table('wf_workjobcolumns')
                ->where('wf06001', $flowId)
                ->where('wf06002', $mainJob)
                ->select('wf06003 as key', 'wf06004 as label')
                ->get()
                ->toArray();
            array_map(function ($key, $label) use (&$jobColumnsSingle) {
                $jobColumnsSingle[$key] = $label;
            }, array_column($jobColumns, 'key'), array_column($jobColumns, 'label'));
            // 获取选择的列表显示字段
            $where = [
                ['wf07001', '=', $flowId],
                ['wf07002', '=', $nodeId],
            ];
            $nodeColumns = (array)DB::table('wf_worknodecolumns')
                ->where($where)
                ->first();
            if (empty($nodeColumns)) {
                $tag = 0;
                $target = [];
            } else {
                $tag = 1;
                $target = is_null($nodeColumns['wf07003']) || empty($nodeColumns['wf07003']) ? [] : json_decode($nodeColumns['wf07003'], true);
            }

            // target数据校验剔除操作
            if ($target && $tag) {
                $jobColumnsFlip = array_flip(array_column($jobColumns, 'key'));
                $check = 0;
                foreach ($target as $k => $v) {
                    if (!isset($jobColumnsFlip[$v])) {
                        unset($target[$v]);
                        $check = 1;
                    }
                }
                if ($check) {
                    $target = array_values($target);
                    $targetUpdate['updated_at'] = Carbon::now();
                    $targetUpdate['wf07003'] = empty($target) ? null : json_encode($target);
                    DB::table('wf_worknodecolumns')->where($where)->update($targetUpdate);
                    Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worknodecolumns', [
                        'where' => $where,
                        'data' => $targetUpdate
                    ]);
                }
            }

            $targetList = [];
            if (empty($target) && $tag == 0) {
                $targetList = [];
            } elseif (empty($target) && $tag == 1) {
                $targetList = [];
                // 更新为空
                if ($nodeColumns['wf07004']) {
                    $dealData['updated_at'] = Carbon::now();
                    $dealData['wf07004'] = null;
                    DB::table('wf_worknodecolumns')->where($where)->update($dealData);
                    Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worknodecolumns', [
                        'where' => $where,
                        'data' => $dealData
                    ]);
                }
            // 存在数据
            } else {
                $targetList = is_null($nodeColumns['wf07004']) || empty($nodeColumns['wf07004']) ? [] : json_decode($nodeColumns['wf07004'], true);
                // 数据处理
                $diffTag = 0;
                if (empty($targetList)) {
                    $diffTag = 1;
                    foreach ($target as $targetK => $targetV) {
                        $targetList[] = [
                            'origin' => $jobColumnsSingle[$targetV],
                            'key' => $targetV,
                            'label' => ''
                        ];
                    }
                    $updateData['wf07004'] = empty($targetList) ? null : json_encode($targetList);
                    $updateData['updated_at'] = Carbon::now();
                // 判断是否存在差异数据-差异数据剔除
                } else {
                    $targetListKey = array_column($targetList, 'key');
                    $diffAdd = array_diff($target, $targetListKey);
                    $diffRemove = array_diff($targetListKey, $target);
                    if ($diffRemove) {
                        foreach ($diffRemove as $drK => $drV) {
                            unset($targetList[$drK]);
                        }
                        $diffTag = 1;
                        $updateData['wf07004'] = empty($targetList) ? null : json_encode($targetList);
                        $updateData['updated_at'] = Carbon::now();
                    }

                    if ($diffAdd) {
                        $diffAddNode = [];
                        foreach ($diffAdd as $daK => $daV) {
                            $diffAddNode[] = [
                                'origin' => $jobColumnsSingle[$daV],
                                'key' => $daV,
                                'label' => ''
                            ];
                        }

                        $diffTag = 1;
                        $targetList = array_merge($targetList, $diffAddNode);
                        $updateData['wf07004'] = empty($targetList) ? null : json_encode($targetList);
                        $updateData['updated_at'] = Carbon::now();
                    }
                }
                // 更新操作
                if ($diffTag) {
                    DB::table('wf_worknodecolumns')->where($where)->update($updateData);
                    Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worknodecolumns', [
                        'where' => $where,
                        'data' => $updateData
                    ]);
                }
            }

            $jsonData = [
                'source' => $jobColumns,
                'target' => $target,
                'targetList' => $targetList
            ];
            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 节点配置信息保存
     *
     * @name    nodeColumnsConfigSave
     *
     * @param   integer     $type           保存类型
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   mixed       $target         目标数据-待处理数据
     *
     * @return  array                       返回结果
     */
    public function nodeColumnsConfigSave($type, $flowId, $nodeId, $target)
    {
        $this->logs = LLog::init($this->logtypes, $flowId);
        try {
            $type = (int)$type;

            $where = [
                ['wf07001', '=', $flowId],
                ['wf07002', '=', $nodeId],
            ];
            // 判断是否需要新增或插入
            $count = DB::table('wf_worknodecolumns')
                ->where('wf07001', $flowId)
                ->where('wf07002', $nodeId)
                ->count();
            switch ($type) {
                case 1:
                    $dealData['wf07003'] = !$target ? null : json_encode($target);
                    break;
                case 2:
                    if (!$target) {
                        $targetData = null;
                    } else {
                        $dealTarget = function () use ($target) {
                            foreach ($target as $k => $v) {
                                yield [
                                    'origin' => $v['origin'],
                                    'key' => $v['key'],
                                    'label' => $v['label']
                                ];
                            }
                        };
                        $targetData = json_encode(iterator_to_array($dealTarget()));
                    }
                    $dealData['wf07004'] = $targetData;
                    break;
                case 4:
                    $dealData['wf07005'] = !$target ? null : json_encode($target);
                    break;
            }

            // 更新
            if ($count) {
                $dealData['updated_at'] = Carbon::now();
                DB::table('wf_worknodecolumns')->where($where)->update($dealData);
                Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worknodecolumns', [
                    'where' => $where,
                    'data' => $dealData
                ]);
            // 新增
            } else {
                $dealData['wf07001'] = $flowId;
                $dealData['wf07002'] = $nodeId;
                $dealData['created_at'] = Carbon::now();
                DB::table('wf_worknodecolumns')->insert($dealData);
                Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_worknodecolumns', $dealData);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取审核节点配置信息
     *
     * @name    getNodeColumnExamConfig
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return  array                       返回结果
     */
    public function getNodeColumnExamConfig($flowId, $nodeId)
    {
        try {
            $examConfig = DB::table('wf_worknodecolumns')
                ->where('wf07001', $flowId)
                ->where('wf07002', $nodeId)
                ->value('wf07005');

            $jsonData = [];
            $jsonData = !$examConfig ? [] : json_decode($examConfig, true);
            if (!$jsonData) {
                $jsonData = [
                    'examResult' => '', 'examDate' => '', 'examReason' => '', 'examPerson' => ''
                ];
            }
            $finalData['examConfig'] = $jsonData;

            // 获取主表字段
            $table = DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05004', 1)->value('wf05003');
            $columnsData = DB::table('wf_workjobcolumns')
                ->select(['wf06003', 'wf06004'])
                ->where('wf06001', $flowId)
                ->where('wf06002', $table)
                ->get()
                ->toArray();
            $columns = [];
            foreach ($columnsData as $key => $val) {
                $val = (array)$val;
                $columns[] = ['value' => $val['wf06003'], 'label' => $val['wf06004'] . '[' . $val['wf06003'] . ']'];
            }
            $finalData['examColumns'] = $columns;

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $finalData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取客户列表字段配置
     *
     * @name    jobTableRmv
     *
     * @param   string      $flowId         流程ID
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function getCustomerColumnsSet($flowId, $apiUser)
    {
        try {
            // 获取主数据业务表
            $mainJob = DB::table('wf_workjobtables')->where('wf05001', $flowId)->where('wf05004', 1)->value('wf05003');
            // 获取主业务表字段信息
            $jobColumns = DB::table('wf_workjobcolumns')
                ->where('wf06001', $flowId)
                ->where('wf06002', $mainJob)
                ->select('wf06003 as key', 'wf06004 as label')
                ->get()
                ->toArray();
            array_map(function ($key, $label) use (&$jobColumnsSingle) {
                $jobColumnsSingle[$key] = $label;
            }, array_column($jobColumns, 'key'), array_column($jobColumns, 'label'));
            // 获取选择的列表显示字段
            $where = [
                ['wf97001', '=', $flowId],
                ['wf97002', '=', $apiUser['id']],
                // ['wf97003', '=', 1],
            ];
            $customerColumns = (array)DB::table('wf_workcustomercolumns')
                ->where($where)
                ->first();
            if (empty($customerColumns)) {
                $tag = 0;
                $target = [];
            } else {
                $tag = 1;
                $target = is_null($customerColumns['wf97004']) || empty($customerColumns['wf97004']) ? [] : json_decode($customerColumns['wf97004'], true);
            }

            // target数据校验剔除操作
            if ($target && $tag) {
                $jobColumnsFlip = array_flip(array_column($jobColumns, 'key'));
                $check = 0;
                foreach ($target as $k => $v) {
                    if (!isset($jobColumnsFlip[$v])) {
                        unset($target[$v]);
                        $check = 1;
                    }
                }
                if ($check) {
                    $target = array_values($target);
                    $targetUpdate['updated_at'] = Carbon::now();
                    $targetUpdate['wf97004'] = empty($target) ? null : json_encode($target);
                    DB::table('wf_workcustomercolumns')->where($where)->update($targetUpdate);
                }
            }

            $targetList = [];
            if (empty($target) && $tag == 0) {
                $targetList = [];
            } elseif (empty($target) && $tag == 1) {
                $targetList = [];
                // 更新为空
                if ($customerColumns['wf97005']) {
                    $dealData['updated_at'] = Carbon::now();
                    $dealData['wf97005'] = null;
                    DB::table('wf_workcustomercolumns')->where($where)->update($dealData);
                }
            // 存在数据
            } else {
                $targetList = is_null($customerColumns['wf97005']) || empty($customerColumns['wf97005']) ? [] : json_decode($customerColumns['wf97005'], true);
                // 数据处理
                $diffTag = 0;
                if (empty($targetList)) {
                    $diffTag = 1;
                    foreach ($target as $targetK => $targetV) {
                        $targetList[] = [
                            'origin' => $jobColumnsSingle[$targetV],
                            'key' => $targetV,
                            'label' => ''
                        ];
                    }
                    $updateData['wf97005'] = empty($targetList) ? null : json_encode($targetList);
                    $updateData['updated_at'] = Carbon::now();
                // 判断是否存在差异数据-差异数据剔除
                } else {
                    $targetListKey = array_column($targetList, 'key');
                    $diffAdd = array_diff($target, $targetListKey);
                    $diffRemove = array_diff($targetListKey, $target);
                    if ($diffRemove) {
                        foreach ($diffRemove as $drK => $drV) {
                            unset($targetList[$drK]);
                        }
                        $diffTag = 1;
                        $updateData['wf97005'] = empty($targetList) ? null : json_encode($targetList);
                        $updateData['updated_at'] = Carbon::now();
                    }

                    if ($diffAdd) {
                        $diffAddNode = [];
                        foreach ($diffAdd as $daK => $daV) {
                            $diffAddNode[] = [
                                'origin' => $jobColumnsSingle[$daV],
                                'key' => $daV,
                                'label' => ''
                            ];
                        }

                        $diffTag = 1;
                        $targetList = array_merge($targetList, $diffAddNode);
                        $updateData['wf97005'] = empty($targetList) ? null : json_encode($targetList);
                        $updateData['updated_at'] = Carbon::now();
                    }
                }
                // 更新操作
                if ($diffTag) {
                    DB::table('wf_workcustomercolumns')->where($where)->update($updateData);
                }
            }

            $jsonData = [
                'source' => $jobColumns,
                'target' => $target,
                'targetList' => $targetList
            ];
            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 客户列表配置保存
     *
     * @name    customerColumnsSetSave
     *
     * @param   integer     $type           保存类型
     * @param   string      $flowId         流程ID
     * @param   mixed       $target         目标数据-待处理数据
     * @param   integer     $userId         用户ID
     *
     * @return  array                       返回结果
     */
    public function customerColumnsSetSave($type, $flowId, $target, $userId)
    {
        try {
            $type = (int)$type;

            $where = [
                ['wf97001', '=', $flowId],
                ['wf97002', '=', $userId],
            ];
            // 判断是否需要新增或插入
            $count = DB::table('wf_workcustomercolumns')
                ->where($where)
                ->count();
            switch ($type) {
                case 1:
                    $dealData['wf97004'] = !$target ? null : json_encode($target);
                    break;
                case 2:
                    if (!$target) {
                        $targetData = null;
                    } else {
                        $dealTarget = function () use ($target) {
                            foreach ($target as $k => $v) {
                                yield [
                                    'origin' => $v['origin'],
                                    'key' => $v['key'],
                                    'label' => $v['label']
                                ];
                            }
                        };
                        $targetData = json_encode(iterator_to_array($dealTarget()));
                    }
                    $dealData['wf97005'] = $targetData;
                    break;
            }

            // 更新
            if ($count) {
                $dealData['updated_at'] = Carbon::now();
                DB::table('wf_workcustomercolumns')->where($where)->update($dealData);
            // 新增
            } else {
                $dealData['wf97001'] = $flowId;
                $dealData['wf97002'] = $userId;
                $dealData['wf97003'] = 1;
                $dealData['created_at'] = Carbon::now();
                DB::table('wf_workcustomercolumns')->insert($dealData);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }
}
