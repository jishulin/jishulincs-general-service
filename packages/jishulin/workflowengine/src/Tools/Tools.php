<?php

namespace Jishulin\WorkFlowEngine\Tools;

use Jishulin\WorkFlowEngine\Config\Config;
use Illuminate\Support\Facades\Config as APP;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Jishulin\WorkFlowEngine\Log\LLog;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;

class Tools
{
    use \Jishulin\WorkFlowEngine\Traits\ReturnJson;

    /**
     * @var $_instance
     */
    private	static $_instance;

    private $logtypes = 'sql';

    private $prefix;

    /**
     * @return void
     */
    private function __construct()
    {
        $this->prefix = APP::get('database.connections.mysql.prefix');
    }

    /**
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * Provide external static invocation method
     *
     * @return \Jishulin\WorkFlowEngine\Tools\Tools
     */
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * 创建流程记录
     *
     * @name    workFlowCrd
     *
     * @param   string      $flowId         流程ID
     * @param   string      $flowName       流程名称
     * @param   mixed       $flowBelong     流程所有者
     *
     * @return  array                       返回结果
     */
    public function workFlowCrd($flowId, $flowName, $flowBelong)
    {
        try {
            $insertData = [
                'wf01001' => $flowId,
                'wf01002' => $flowName,
                'wf01003' => $flowBelong,
                'created_at' => Carbon::now()
            ];
            $id = DB::table('wf_workflows')->insertGetId($insertData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workflows', $insertData);

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', ['id' => $id]);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 修改流程记录
     *
     * @name    workFlowEdt
     *
     * @param   string      $flowId         流程ID
     * @param   string      $flowName       流程名称
     *
     * @return  array                       返回结果
     */
    public function workFlowEdt($flowId, $flowName)
    {
        try {
            $where = [['wf01001', '=', $flowId]];
            if (!$flowData = DB::table('wf_workflows')->where($where)->first()) {
                return $this->jsonReturns(1, Config::ERRORFLOW);
            }

            $updateData = ['wf01002' => $flowName, 'updated_at' => Carbon::now()];
            DB::table('wf_workflows')->where($where)->update($updateData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workflows', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 启用流程
     *
     * @name    workFlowEnabled
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function workFlowEnabled($flowId)
    {
        try {
            $where = [['wf01001', '=', $flowId]];

            // 数据查询
            if (!$flowData = DB::table('wf_workflows')->where($where)->first()) {
                return $this->jsonReturns(1, Config::ERRORFLOW);
            }
            // 判断是否满足启动条件
            if (!$flowData->wf01005) {
                return $this->jsonReturns(1, Config::ERROR_FLOW_CANNOT_NODE);
            }
            // 判断流程是否属于闭合流程TODO

            $updateData = ['wf01004' => 1, 'wf01007' => 1, 'updated_at' => Carbon::now()];
            DB::table('wf_workflows')->where($where)->update($updateData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workflows', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 禁用流程
     *
     * @name    workFlowDisabled
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function workFlowDisabled($flowId)
    {
        try {
            $where = [['wf01001', '=', $flowId]];

            // 数据查询
            if (!$flowData = DB::table('wf_workflows')->where($where)->first()) {
                return $this->jsonReturns(1, Config::ERRORFLOW);
            }

            $updateData = ['wf01004' => 0, 'wf01007' => 0, 'updated_at' => Carbon::now()];
            DB::table('wf_workflows')->where($where)->update($updateData);

            $this->logs = LLog::init($this->logtypes, $flowId);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workflows', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取分页列表
     *
     * @name    getWorkFlowListPagination
     *
     * @param   integer         $offest
     * @param   integer         $limit
     * @param   array           $where
     * $where = [
     *    'arr' => [['a', '=', '1'], ['b', '>', '2']],
     *    'in' => [['c', [1, 2, 3]], ['c', ['1', '2']]]
     * ]
     * @param   array           $orderBy
     *
     * @return  array           返回结果
     */
    public function getWorkFlowListPagination($offest, $limit, $where, $orderBy)
    {
        try {
            $query = DB::table('wf_workflows');
            if (isset($where['arr']) && !empty($where['arr']))
                $query->where($where['arr']);
            if (isset($where['in']) && !empty($where['in'])) {
                foreach ($where['in'] as $k => $v) $query->whereIn($v[0], $v[1]);
            }
            if (!$orderBy && count($orderBy)) {
                foreach ($orderBy as $k => $v) $query->orderBy($v[0], $v[1]);
            }

            $queryCount = clone($query);
            $list = $query->offset($offest)->limit($limit)->get()->toArray();
            $count = $queryCount->count();

            $jsonData = [
                'list' => $list,
                'count' => $count
            ];

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取所有流程通过所有者
     *
     * @name    getWorkFlowByBelong
     *
     * @param   mixed       $flowBelong     流程所有者
     *
     * @return  array                       返回结果
     */
    public function getWorkFlowByBelong($flowBelong, $tag = 0)
    {
        try {
            if ($tag) {
                $where = [['wf01003', '=', $flowBelong], ['wf01004', '=', 1]];
            } else {
                $where = [['wf01003', '=', $flowBelong]];
            }

            $workFlowLists = DB::table('wf_workflows')->where($where)->get()->toArray();

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $workFlowLists);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取所有流程ID获取流程信息
     *
     * @name    getWorkFlowByFlowId
     *
     * @param   mixed 	    $flowId     流程ID
     *
     * @return 	array                   返回结果
     */
    public function getWorkFlowByFlowId($flowId)
    {
        try {
            // 数据查询
            if (!$flowData = DB::table('wf_workflows')->where('wf01001', $flowId)->first()) {
                return $this->jsonReturns(1, Config::ERRORFLOW);
            }
            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $flowData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 流程保存
     *
     * @name    workFlowSave
     *
     * @param   array|json     $workFlowData   流程数据
     * @param   string         $workFlowImg    流程图片
     *
     * @return 	array                          返回结果
     */
    public function workFlowSave($workFlowData = [], $workFlowImg = '')
    {
        if (!$workFlowData) {
            return $this->jsonReturns(1, Config::ERROR_PARAMS);
        }

        // 获取流程数据
        $flowData = $workFlowData['flowDataArray'];
        // 获取节点数据
        $nodeData = $workFlowData['nodeDataArray'];
        // 获取节点连线数据
        $linkData = $workFlowData['linkDataArray'];
        // 数据值判断
        $flowId = $flowData['flowId'] ?? '';
        $flowName = $flowData['flowName'] ?? '';
        if (!$flowId || !$flowName) {
            return $this->jsonReturns(1, Config::ERROR_EMPTY_FLOW);
        }
        if (!count($nodeData)) {
            return $this->jsonReturns(1, Config::ERROR_EMPTY_NODE);
        }
        if (!count($linkData)) {
            return $this->jsonReturns(1, Config::ERROR_EMPTY_LINK);
        }
        // 流程完整性判断TODO

        $this->logs = LLog::init($this->logtypes, $flowId);
        $where = [['wf01001', '=', $flowId]];
        // 根据流程KEY获取流程是否属于更新，连线数据是否进行删替备份操作
        if (!$workFlowInfo = DB::table('wf_workflows')->where($where)->first()) {
            return $this->jsonReturns(1, Config::ERRORFLOW);
        }
        $orginDraw = [
            'nodeDataArray' => $nodeData,
            'linkDataArray' => $linkData
        ];
        try {
            $wf01007 = $workFlowInfo->wf01007;
            // 更新流程源数据，更新流程附件数据
            $updateData = [
                'wf01002' => $flowName,
                'wf01005' => json_encode($orginDraw, 256),
                'wf01007' => 0,
                'updated_at' => Carbon::now()
            ];
            DB::table('wf_workflows')->where($where)->update($updateData);
            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workflows', ['where' => $where, 'data' => $updateData]);

            // 获取起始点，数据按需按序组装
            $workFlowAnalysisRes = $this->analysisLinkAndNodeData($flowId, $nodeData, $linkData);
            // 节点数据更新或插入，更新需进行数据对比替换含，新增，删除，更新，通过KEY值进行操作
            // 操作成功后进行批量更新，新增，删除动作
            if ($workFlowAnalysisRes['updating']) {
                foreach ($workFlowAnalysisRes['updating'] as $k => $v) {
                    $subWhere = [['wf02001', '=', $v['wf02001']], ['wf02002', '=', $v['wf02002']]];
                    DB::table('wf_worknodes')->where($subWhere)->update($v);
                    Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worknodes', ['where' => $subWhere, 'data' => $v]);
                }
            }
            if ($workFlowAnalysisRes['adding']) {
                DB::table('wf_worknodes')->insert($workFlowAnalysisRes['adding']);
                Util::asynicLogSql($this->logs, 'insertMulti', $this->prefix . 'wf_worknodes', $workFlowAnalysisRes['adding']);
            }
            if ($workFlowAnalysisRes['removing']) {
                $nodeWhere = [['wf02001', '=', $flowId]];
                DB::table('wf_worknodes')->where($nodeWhere)->whereIn('wf02002', $workFlowAnalysisRes['removing'])->delete();
                Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_worknodes', [
                    'where' => $nodeWhere
                ], [
                    'whereIn' => [['wf02002', $workFlowAnalysisRes['removing']]]
                ]);
            }
            // 流程线批量更新，删，增操作
            // 数据处理
            $getLinkDataArrangeAction = function () use ($linkData, $flowId) {
                foreach ($linkData as $k => $v) {
                    $links = [];
                    $links['wf03001'] = $flowId;
                    $links['wf03002'] = $v['from'];
                    $links['wf03003'] = $v['to'];
                    $links['wf03004'] = $v['routerId'];
                    $links['wf03005'] = $v['label'] ?? '';
                    $links['wf03006'] = 0;
                    $links['wf03007'] = 0;
                    // 待处理TODO
                    if ($links['wf03005'] == '通过' || $links['wf03005'] == 'pass') {
                        $exam = 1;
                    } elseif ($links['wf03005'] == '不通过' || $links['wf03005'] == 'unpass') {
                        $exam = 0;
                    } elseif ($links['wf03005'] == '回退' || $links['wf03005'] == 'back') {
                        $exam = 2;
                    } else {
                        $exam = 1;
                    }
                    $links['wf03008'] = $exam;
                    $links['updated_at'] = Carbon::now();

                    yield $v['routerId'] => $links;
                }
            };
            $linkDataArrange = array_values(iterator_to_array($getLinkDataArrangeAction()));
            // 更新
            if ($wf01007 == 1) {
                $linkWhere = [['wf03001', '=', $flowId], ['wf03006', '=', '0']];
                $linkUpdateData = [
                    'wf03006' => 1,
                    'wf03007' => time(),
                    'updated_at' => Carbon::now()
                ];
                DB::table('wf_worklinks')->where($linkWhere)->update($linkUpdateData);
                Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_worklinks', ['where' => $linkWhere, 'data' => $linkUpdateData]);
            // 删除
            } else {
                $linkWhere = [['wf03001', '=', $flowId], ['wf03006', '=', '0']];
                DB::table('wf_worklinks')->where($linkWhere)->delete();
                Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_worklinks', [
                    'where' => $linkWhere
                ]);
            }
            // 增
            DB::table('wf_worklinks')->insert($linkDataArrange);
            Util::asynicLogSql($this->logs, 'insertMulti', $this->prefix . 'wf_worklinks', $linkDataArrange);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 流程节点数据整理
     *
     * @name    analysisLinkAndNodeData
     *
     * @param   string          $flowId   流程ID
     * @param   array           $nodes    待整理节点数据
     * @param   array           $links    节点关系
     *
     * @return 	array                     返回结果
     */
    protected function analysisLinkAndNodeData($flowId, $nodes, $links)
    {
        // 获取起始节点标识
        $startKey = 'E00001';
        foreach ($nodes as $k => $v) {
            if ($v['nodeType'] == 'start') {
                $startKey = $v['key'];
                break;
            }
        }
        // 获取有效节点信息
        $getActivtyNodeAction = function () use ($nodes) {
            $activityNodeTypes = ['comm', 'freedom', 'innerChildFlow', 'outerChildFlow'];
            foreach ($nodes as $k => $v) {
                if (in_array($v['nodeType'], $activityNodeTypes)) {
                    yield $v['key'] => $v;
                }
            }
        };
        $activtyNode = iterator_to_array($getActivtyNodeAction());
        // 获取起始节点
        foreach ($links as $k => $v) {
            if ($v['from'] == $startKey) {
                $startNodeKey = $v['to'];
                $activtyNode[$startNodeKey]['tag'] = 'start';
                unset($links[$k]);
                break;
            }
        }
        $links = array_values($links);

        $checkCount = function ($toKey, $type) use ($links) {
            $count = 0;
            $tag = $type == 1 ? 'from' : 'to';
            foreach ($links as $k => $v) {
                if ($v[$tag] == $toKey) $count++;
            }

            return $count;
        };

        array_map(function ($v) use (&$activtyNode, $checkCount) {
            $toKey = $v['to'];
            $toFirst = substr($toKey, 0, 1);
            // 子流程处理标识
            $toFirst == 'S' ? $activtyNode[$toKey]['tag'] = 'sub' : (
                $toFirst == 'G' ? (
                    $checkCount($toKey, 0) == 1 ? $activtyNode[$v['from']]['G'] = 'F' : $activtyNode[$v['from']]['RI'] = 'E'
                ) : (
                    (substr($v['from'], 0, 1) == 'G' && $checkCount($v['from'], 0) > 1) ? $activtyNode[$toKey]['G'] = 'R' : null
                )
            );
            (substr($v['from'], 0, 1) == 'G' && $checkCount($v['from'], 0) == 1) ? $activtyNode[$toKey]['FO'] = 'S' : null;

            // if ($toFirst == 'S') {
            //     $activtyNode[$toKey]['tag'] = 'sub';
            // } elseif ($toFirst == 'G') {
            //     $checkInOutRes = $checkCount($toKey, 0);
            //     if ($checkInOutRes == 1) {
            //         // 分流节点
            //         $activtyNode[$v['from']]['G'] = 'F';
            //     } else {
            //         // 子流程-分流结束点
            //         $activtyNode[$v['from']]['RI'] = 'E';
            //     }
            // } elseif (substr($v['from'], 0, 1) == 'G' && $checkCount($v['from'], 0) > 1) {
            //     // 合流节点
            //     $activtyNode[$toKey]['G'] = 'R';
            // }
            // if (substr($v['from'], 0, 1) == 'G' && $checkCount($v['from'], 0) == 1) {
            //     // 子流程-分流开始节点
            //     $activtyNode[$toKey]['FO'] = 'S';
            // }
        }, $links);

        // 数据整理
        $activtyNodeArrange = function ($nodes) use ($flowId, $checkCount) {
            foreach ($nodes as $k => $v) {
                $arrange = [];
                $arrange['wf02001'] = $flowId;
                $arrange['wf02002'] = $v['key'];
                $arrange['wf02003'] = $v['text'];
                $arrange['wf02004'] = $v['tag'] ?? '';
                $arrange['wf02005'] = $checkCount($v['key'], 1) > 1 ? 1 : 0;
                $arrange['wf02006'] = $v['RI'] ?? '';
                $arrange['wf02007'] = $v['FO'] ?? '';
                $arrange['wf02008'] = $v['G'] ?? '';
                $arrange['wf02009'] = $v['order'] ?? 10;

                yield $v['key'] => $arrange;
            }
        };
        $nodeArrange = array_values(iterator_to_array($activtyNodeArrange(array_values($activtyNode))));

        // 获取原始数据
        $originNode = DB::table('wf_worknodes')->where('wf02001', $flowId)->get()->toArray();
        // 对比新老数据
        // 通过主要键进行处理
        $newKey = array_column($nodeArrange, 'wf02002');
        $oldKey = array_column($originNode, 'wf02002');
        // 获取更新交集
        $intersect = array_intersect_key(array_flip($newKey), array_flip($oldKey));
        // 获取新增差集
        $diffAdding = array_diff($newKey, $oldKey);
        // 获取删除差集
        $diffRemovong = array_diff($oldKey, $newKey);

        $getNodeArrange = function ($keys, $type) use ($nodeArrange) {
            $types = $type == 0 ? 'created_at' : 'updated_at';
            foreach ($keys as $k => $v) {
                foreach ($nodeArrange as $key => $val) {
                    if ($val['wf02002'] == $v) {
                        $val[$types] = Carbon::now();
                        yield $v => $val;
                    }
                }
            }
        };

        // 更新数据从新数据获取
        $updating = [];
        if ($intersect) {
            $updating = array_values(iterator_to_array($getNodeArrange(array_values(array_flip($intersect)), 1)));
        }
        // 新增数据从新数据获取
        $adding = [];
        if ($diffAdding) {
            $adding = array_values(iterator_to_array($getNodeArrange(array_values($diffAdding), 0)));
        }

        return [
            'updating' => $updating,
            'adding' => $adding,
            'removing' => array_values($diffRemovong)
        ];
    }

    /**
     * 通过流程ID获取流程有效节点信息
     *
     * @name    getWorkNodeByFlowId
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function getWorkNodeByFlowId($flowId)
    {
        try {
            $node = DB::table('wf_worknodes')->where('wf02001', $flowId)->orderby('wf02009', 'ASC')->get()->toArray();

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $node);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 通过流程ID获取流程有效节点信息-字典显示
     *
     * @name    getWorkNodeByFlowIdForDict
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function getWorkNodeByFlowIdForDict($flowId)
    {
        return DB::table('wf_worknodes')
            ->select('wf02002 as value', 'wf02003 as label')
            ->where('wf02001', $flowId)
            ->orderby('wf02009', 'ASC')
            ->get()
            ->toArray();
    }

    /**
     * 通过流程ID获取流程有效节点树-elementui
     *
     * @name    getWorkNodesTreeByFlowId
     *
     * @param   string      $flowId         流程ID
     *
     * @return  array                       返回结果
     */
    public function getWorkNodesTreeByFlowId($flowId)
    {
        try {
            if (!$workFlowInfo = DB::table('wf_workflows')->where('wf01001', $flowId)->first()) {
                return $this->jsonReturns(1, Config::ERRORFLOW);
            }

            $jsonData = [];
            $jsonData['id'] = $workFlowInfo->wf01001;
            $jsonData['flowId'] = $workFlowInfo->wf01001;
            $jsonData['label'] = $workFlowInfo->wf01002;
            $jsonData['type'] = 'flow';

            $node = DB::table('wf_worknodes')
                ->select('wf02001 as flowId', 'wf02002 as id', 'wf02003 as label', DB::raw("'node' as type"))
                ->where('wf02001', $flowId)
                ->orderby('wf02009', 'ASC')
                ->get()
                ->toArray();
            $jsonData['children'] = $node;

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', [$jsonData]);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 通过流程ID和Key获取配置信息
     *
     * @name    getWrokFlowGlobalByKey
     *
     * @param   string      $flowId         流程ID
     * @param   string      $key            配置KEY
     *
     * @return  array                       返回结果
     */
    public function getWrokFlowGlobalByKey($flowId, $key)
    {
        try {
            $globalConfig = DB::table('wf_workglobal')
                ->where('wf08001', $flowId)
                ->where('wf08002', $key)
                ->where('wf08004', 1)
                ->value('wf08005');

            $jsonData = [];
            $jsonData = !$globalConfig ? [] : json_decode($globalConfig, true);

            if (!$jsonData && $key == 'systemname') {
                $jsonData = [
                    'namestatus' => '1', 'namekey' => 'NAME', 'nameshow' => Config::NAMECHS,
                    'numberstatus' => '1', 'numberkey' => 'NUMBER', 'numbershow' => Config::NUMBERCHS,
                    'keywordstatus' => '1', 'keywordplaceholder' => Config::KEYWORDHOLDERCHS,
                ];
            } elseif (!$jsonData && $key == 'systemworkflow') {
                $jsonData = ['history' => '0', 'blackname' => '0'];
            } elseif (!$jsonData && $key == 'ordernumber') {
                $jsonData = ['type' => 'sysnum', 'prefix' => '', 'connectp' => '0', 'date' => 'ymd', 'connectd' => '0', 'randnum' => '3'];
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 全局配置设置保存
     *
     * @name    workFlowGlobalConfigSave
     *
     * @param   array      $mapData         待保存数据
     *
     * @return 	array                       返回结果
     */
    public function workFlowGlobalConfigSave($mapData = [])
    {
        // 参数校验
        try {
            $flowId = $mapData['flowId'];
            $key = $mapData['key'];
            $keyTitle = $mapData['keyTitle'];
            $config = $mapData['config'];

            $this->logs = LLog::init($this->logtypes, $flowId);

            $where = [['wf08001', '=', $flowId], ['wf08002', '=', $key]];
            if (DB::table('wf_workglobal')->where($where)->count()) {
                $updateData = [
                    'wf08003' => $keyTitle,
                    'wf08005' => !$config ? null : json_encode($config),
                    'updated_at' => Carbon::now()
                ];
                DB::table('wf_workglobal')->where($where)->update($updateData);

                Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workglobal', [
                    'where' => $where,
                    'data' => $updateData
                ]);
            } else {
                $insertData = [
                    'wf08001' => $flowId,
                    'wf08002' => $key,
                    'wf08003' => $keyTitle,
                    'wf08004' => 1,
                    'wf08005' => !$config ? null : json_encode($config),
                    'created_at' => Carbon::now()
                ];
                DB::table('wf_workglobal')->insert($insertData);
                Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workglobal', $insertData);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取待办列表配置信息
     *
     * @name    getWorkFlowTodoConfig
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return 	array                       返回结果
     */
    public function getWorkFlowTodoConfig($flowId, $nodeId)
    {
        try {
            // 列表字段
            $gridColumns = $this->getWorkJobGridColumns($flowId, $nodeId);
            $jsonData['gridColumns'] = $gridColumns;

            // 列表功能按钮
            $gridBtns = $this->getWorkJobGridBtns($flowId, $nodeId);
            $jsonData['gridBtns'] = $gridBtns;

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取待办列表功能按钮
     *
     * @name    getWorkJobGridBtns
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return 	array                       返回结果
     */
    protected function getWorkJobGridBtns($flowId, $nodeId)
    {
        $gridBtnToolBars = [];
        $gridBtnOptBars = [];
        $printConfigList = [];
        $nodes = (array)DB::table('wf_worknodes')->where('wf02001', $flowId)->where('wf02002', $nodeId)->first();
        if ($nodes['wf02004'] == 'start') {
            $gridBtnToolBars[] = ['type' => 'primary', 'funName' => 'jobCreated', 'label' => Config::JOBCREATEDCHS];
        }
        if ($nodes['wf02005'] == 1) {
            $gridBtnOptBars[] = ['type' => 'warningbtn-color', 'funName' => 'jobExamed', 'label' => Config::JOBEXAMEDCHS];
        }
        $gridBtnOptBars[] = ['type' => 'dangerbtn-color', 'funName' => 'jobSubmited', 'label' => Config::JOBSUBMITEDCHS];

        // 报表，导出
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemname')
            ->where('wf08004', 1)
            ->value('wf08005');
        if (!empty($globalConfig)) {
            $globalConfig = json_decode($globalConfig, true);
            if ($globalConfig['printstatus']) {
                $gridBtnOptBars[] = ['type' => 'primary', 'funName' => 'jobPrinted', 'label' => Config::JOBPRINTEDCHS];
                // 获取配置信息
                $printConfigList = DB::table('wf_workbirts')
                    ->select('wf04003 as uuId', 'wf04004 as birtName')
                    ->where('wf04001', $flowId)
                    ->where('wf04002', $nodeId)
                    ->where('wf04010', 1)
                    ->where('wf04007', 0)
                    ->orderBy('wf04011', 'ASC')
                    ->get()
                    ->toArray();
            }
            if ($globalConfig['exportstatus']) {
                $gridBtnToolBars[] = ['type' => 'primary', 'funName' => 'jobExported', 'label' => Config::JOBEXPORTEDCHS];
            }
        }

        $jsonData['toolbar'] = $gridBtnToolBars;
        $jsonData['optbar'] = $gridBtnOptBars;
        $jsonData['prints'] = $printConfigList;

        return $jsonData;
    }

    /**
     * 获取待办列表显示字段
     *
     * @name    getWorkJobGridColumns
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return 	array                       返回结果
     */
    public function getWorkJobGridColumns($flowId, $nodeId)
    {
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemname')
            ->where('wf08004', 1)
            ->value('wf08005');

        $gridColumns = [];
        if (empty($globalConfig)) {
            $gridColumns[] = ['prop' => 'NUMBER', 'label' => Config::NUMBERCHS];
            $gridColumns[] = ['prop' => 'NAME', 'label' => Config::NAMECHS];
        } else {
            $globalConfig = json_decode($globalConfig, true);
            if ($globalConfig['numberstatus']) {
                $gridColumns[] = ['prop' => 'NUMBER', 'label' => $globalConfig['numbershow'] ?? Config::NUMBERCHS];
            }
            if ($globalConfig['namestatus']) {
                $gridColumns[] = ['prop' => 'NAME', 'label' => $globalConfig['nameshow'] ?? Config::NAMECHS];
            }
        }
        // 获取配置列
        $nodeColumnsConfig = DB::table('wf_worknodecolumns')
            ->where('wf07001', $flowId)
            ->where('wf07002', $nodeId)
            ->value('wf07004');
        if ($nodeColumnsConfig) {
            $nodeColumnsConfig = json_decode($nodeColumnsConfig, true);
            foreach ($nodeColumnsConfig as $k => $v) {
                $gridColumns[] = ['prop' => $v['key'], 'label' => $v['label'] ? $v['label'] : $v['origin']];
            }
        }
        // 加入快捷搜索
        if (!$globalConfig) {
            $gridColumns[] = ['prop' => 'keyword', 'label' => Config::KEYWORCHS, 'hide' => true, 'search' => true, 'searchPlaceholder' => Config::KEYWORDHOLDERCHS];
        } elseif ($globalConfig['keywordstatus']) {
            $gridColumns[] = ['prop' => 'keyword', 'label' => Config::KEYWORCHS, 'hide' => true, 'search' => true, 'searchPlaceholder' => $globalConfig['keywordplaceholder']];
        }

        return $gridColumns;
    }

    /**
     * 通过流程ID获取表单信息列表
     *
     * @name    getFormDesignList
     *
     * @param   string      $flowId         流程ID
     *
     * @return 	array                       返回结果
     */
    public function getFormDesignList($flowId)
    {
        try {
            $where[] = ['wf11001', '=', $flowId];
            $fields = [
                'wf_workforms.id as formId', 'wf02002 as nodeName', 'wf11003 as formName', 'wf_workforms.created_at',
                'wf11001 as flowId', 'wf11002 as nodeId', 'wf11005 as firstShow', 'wf11006 as formStatus'
            ];
            $list = DB::table('wf_workforms')
                ->select($fields)
                ->leftJoin('wf_worknodes', 'wf11002', '=', 'wf02001')
                ->where($where)
                ->get()
                ->toArray();

            $jsonData = [
                'list' => $list,
                'nodeSelection' => $this->getWorkNodeByFlowIdForDict($flowId)
            ];


            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 表单设计添加
     *
     * @name    formDesignConfigCrd
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   string      $formName       表单名称
     * @param   integer     $firstShow      是否默认显示
     * @param   integer     $formStatus     状态
     *
     * @return 	array                       返回结果
     */
    public function formDesignConfigCrd($flowId, $nodeId, $formName, $firstShow, $formStatus)
    {
        try {
            $this->logs = LLog::init($this->logtypes, $flowId);
            $insertData = [
                'wf11001' => $flowId,
                'wf11002' => $nodeId,
                'wf11003' => $formName,
                'wf11005' => $firstShow,
                'wf11006' => $formStatus,
                'created_at' => Carbon::now()
            ];
            // 其他置为
            if ($firstShow == 1) {
                $updateData = ['wf11005' => 0, 'updated_at' => Carbon::now()];
                $where = [['wf11001', '=', $flowId], ['wf11002', '=', $nodeId], ['wf11005', '=', 1]];
                DB::table('wf_workforms')->where($where)->update($updateData);

                Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workforms', [
                    'where' => $where,
                    'data' => $updateData
                ]);
            }

            DB::table('wf_workforms')->insert($insertData);
            Util::asynicLogSql($this->logs, 'insertSingle', $this->prefix . 'wf_workforms', $insertData);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 表单设计编辑
     *
     * @name    formDesignConfigEdt
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   string      $formName       表单名称
     * @param   integer     $firstShow      是否默认显示
     * @param   integer     $formStatus     状态
     *
     * @return 	array                       返回结果
     */
    public function formDesignConfigEdt($formId, $flowId, $nodeId, $formName, $firstShow, $formStatus)
    {
        try {
            $this->logs = LLog::init($this->logtypes, $flowId);
            $updateData = [
                'wf11002' => $nodeId,
                'wf11003' => $formName,
                'wf11005' => $firstShow,
                'wf11006' => $formStatus,
                'updated_at' => Carbon::now()
            ];
            // 其他置为
            if ($firstShow == 1) {
                $updateDataShow = ['wf11005' => 0, 'updated_at' => Carbon::now()];
                $whereShow = [['wf11001', '=', $flowId], ['wf11002', '=', $nodeId], ['wf11005', '=', 1]];
                DB::table('wf_workforms')->where($whereShow)->update($updateDataShow);

                Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workforms', [
                    'where' => $whereShow,
                    'data' => $updateDataShow
                ]);
            }

            $where = [['id', '=', $formId]];
            DB::table('wf_workforms')->where($where)->update($updateData);

            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workforms', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 表单设计启用禁用
     *
     * @name    formDesignConfigEdt
     *
     * @param   string      $formId         表单ID
     * @param   string      $flowId         流程ID
     * @param   integer     $formStatus     状态
     *
     * @return 	array                       返回结果
     */
    public function formDesignConfigSts($formId, $flowId, $formStatus)
    {
        try {
            $this->logs = LLog::init($this->logtypes, $flowId);
            $updateData = [
                'wf11006' => $formStatus,
                'updated_at' => Carbon::now()
            ];

            $where = [['id', '=', $formId]];
            DB::table('wf_workforms')->where($where)->update($updateData);

            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workforms', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 表单设计删除
     *
     * @name    formDesignConfigRmv
     *
     * @param   string      $formId         表单ID
     * @param   string      $flowId         流程ID
     *
     * @return 	array                       返回结果
     */
    public function formDesignConfigRmv($formId, $flowId)
    {
        try {
            $this->logs = LLog::init($this->logtypes, $flowId);

            $where = [['id', '=', $formId]];
            DB::table('wf_workforms')->where($where)->delete();
            Util::asynicLogSql($this->logs, 'deleteSingle', $this->prefix . 'wf_workforms', [
                'where' => $where
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取表单配置通过表单ID
     *
     * @name    getFormDesignConfigByFormId
     *
     * @param   string      $formId         表单ID
     *
     * @return 	array                       返回结果
     */
    public function getFormDesignConfigByFormId($formId)
    {
        try {
            if (!$formData = (array)DB::table('wf_workforms')->where('id', $formId)->first()) {
                return $this->jsonReturns(1, Config::ERROR_FROMNOTEXISTS);
            }
            $formConfig = empty($formData['wf11004']) || $formData['wf11004'] == '[]' ? [] : json_decode($formData['wf11004'], true);

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $formConfig);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取表单配置保存
     *
     * @name    formDesignConfigSave
     *
     * @param   string      $formId         表单ID
     * @param   string      $flowId         流程ID
     * @param   array       $formConfig     表单配置
     * @return 	array                       返回结果
     */
    public function formDesignConfigSave($flowId, $formId, $formConfig)
    {
        try {
            $this->logs = LLog::init($this->logtypes, $flowId);
            $updateData = [
                'wf11004' => json_encode($formConfig, 256),
                'updated_at' => Carbon::now()
            ];

            $where = [['id', '=', $formId]];
            DB::table('wf_workforms')->where($where)->update($updateData);

            Util::asynicLogSql($this->logs, 'updateSingle', $this->prefix . 'wf_workforms', [
                'where' => $where,
                'data' => $updateData
            ]);

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 判断流程是否启用历史记录
     *
     * @name    checkWorkFlowEnableHistoryRecord
     *
     * @param   string      $flowId         流程ID
     * @return 	boolean                     返回结果
     */
    public function checkWorkFlowEnableHistoryRecord($flowId)
    {
        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemworkflow')
            ->where('wf08004', 1)
            ->value('wf08005');

        $res = !$globalConfig ? [] : json_decode($globalConfig, true);

        if (!$res) return false;

        $history = isset($res['history']) && $res['history'] == 1 ? true : false;

        return $history;
    }

    /**
     * 获取客户列表字段配置信息
     *
     * @name    getCustomerColumnsConfig
     *
     * @param   string      $flowId         流程ID
     * @param   array       $apiUser        用户信息
     *
     * @return 	array                       返回结果
     */
    public function getCustomerColumnsConfig($flowId, $apiUser)
    {
        try {
            // 列表字段
            $gridColumns = $this->getCustomerGridColumns($flowId, $apiUser);
            $jsonData['gridColumns'] = $gridColumns;

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
        } catch (\Exception $e) {
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 获取待办列表显示字段
     *
     * @name    getCustomerGridColumns
     *
     * @param   string      $flowId         流程ID
     * @param   array       $apiUser        用户信息
     *
     * @return 	array                       返回结果
     */
    public function getCustomerGridColumns($flowId, $apiUser)
    {
        $gridColumns = [];
        // 加入当前节点
        $gridColumns[] = ['prop' => 'CURRENTNODE', 'label' => Config::CURRENTNODE];
        // 所属任务性质状态
        $gridColumns[] = ['prop' => 'JOBSTATUS', 'label' => Config::JOBSTATUS];

        $globalConfig = DB::table('wf_workglobal')
            ->where('wf08001', $flowId)
            ->where('wf08002', 'systemname')
            ->where('wf08004', 1)
            ->value('wf08005');

        if (empty($globalConfig)) {
            $gridColumns[] = ['prop' => 'NUMBER', 'label' => Config::NUMBERCHS];
            $gridColumns[] = ['prop' => 'NAME', 'label' => Config::NAMECHS];
        } else {
            $globalConfig = json_decode($globalConfig, true);
            if ($globalConfig['numberstatus']) {
                $gridColumns[] = ['prop' => 'NUMBER', 'label' => $globalConfig['numbershow'] ?? Config::NUMBERCHS];
            }
            if ($globalConfig['namestatus']) {
                $gridColumns[] = ['prop' => 'NAME', 'label' => $globalConfig['nameshow'] ?? Config::NAMECHS];
            }
        }
        // 获取配置列
        $customerColumnsConfig = DB::table('wf_workcustomercolumns')
            ->where('wf97001', $flowId)
            ->where('wf97003', 1)
            ->where('wf97002', $apiUser['id'])
            ->value('wf97005');
        if ($customerColumnsConfig) {
            $customerColumnsConfig = json_decode($customerColumnsConfig, true);
            foreach ($customerColumnsConfig as $k => $v) {
                $gridColumns[] = ['prop' => $v['key'], 'label' => $v['label'] ? $v['label'] : $v['origin']];
            }
        }
        // 加入快捷搜索
        if (!$globalConfig) {
            $gridColumns[] = ['prop' => 'keyword', 'label' => Config::KEYWORCHS, 'hide' => true, 'search' => true, 'searchPlaceholder' => Config::KEYWORDHOLDERCHS];
        } elseif ($globalConfig['keywordstatus']) {
            $gridColumns[] = ['prop' => 'keyword', 'label' => Config::KEYWORCHS, 'hide' => true, 'search' => true, 'searchPlaceholder' => $globalConfig['keywordplaceholder']];
        }

        return $gridColumns;
    }
}
