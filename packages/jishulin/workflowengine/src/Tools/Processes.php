<?php

namespace Jishulin\WorkFlowEngine\Tools;

use Jishulin\WorkFlowEngine\Config\Config;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Jishulin\WorkFlowEngine\Tools\Utils\Util;
use Jishulin\WorkFlowEngine\Tools\Utils\ProcessUtil;
use Jishulin\WorkFlowEngine\Tools\Utils\ValidateUtil;
use Jishulin\WorkFlowEngine\Log\LLog;
use Jishulin\RulesEngine\Lrules\Lrules;

class Processes
{
    use \Jishulin\WorkFlowEngine\Traits\ReturnJson;

    private $logs;
    /**
     * @var $_instance
     */
    private	static $_instance;

    /**
     * @return void
     */
    private function __construct()
    {
        $this->logs = LLog::init();
    }

    /**
     * @return void
     */
    private function __clone()
    {

    }

    /**
     * Provide external static invocation method
     *
     * @return \Jishulin\WorkFlowEngine\Tools\Birts
     */
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * 流程审核处理
     *
     * @name    jobProcessDealExam
     *
     * @param   array       $list           请求参数
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function jobProcessDealExam($list, $apiUser = [])
    {
        // 基本参数校验
        $validateRes = ValidateUtil::basicJobProcessDealExam($list);
        if ($validateRes['error']) return $validateRes;

        try {
            // 进程ID
            $processId = $list['processId'];
            // 业务ID
            $jobId = $list['jobId'];
            // 流程ID
            $flowId = $list['flowId'];
            // 节点ID
            $nodeId = $list['nodeId'];
            // 审核保存提交标识[0=审核保存，1=审核并提交]
            $saveOrSubmit = $list['saveOrSubmit'] ?? 0;
            // 审核信息
            $exam = $list['exam'];

            // 判断该进程是否被操作过
            if (!ProcessUtil::getProcessesId($flowId, $nodeId, $jobId)) {
                return $this->jsonReturns(1, Config::PROCESSHASOPT);
            }

            // 审核结果处理（保存，redis缓存）
            $examDealSaveRes = ProcessUtil::jobProcessExamDealSave($flowId, $nodeId, $jobId, $exam, $apiUser);
            if ($examDealSaveRes['error']) return $examDealSaveRes;

            // 提交进程
            if ($saveOrSubmit) {
                $map['processId'] = $processId;
                $map['jobId'] = $jobId;
                $map['flowId'] = $flowId;
                $map['nodeId'] = $nodeId;
                $map['REQUIRED'] = ProcessUtil::getJobFieldByPrimary($jobId, $examDealSaveRes['data']['mainTable'], 'REQUIRED');
                return $this->jobProcessSubmit($map, $apiUser);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG);
        } catch (\Exception $e) {
            $this->logs->info('jobProcessDealExam ------> Try Catch Error: ' . $e->getMessage());
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 流程处理
     *
     * @name    jobProcessDealWork
     *
     * @param   array       $list           请求参数
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function jobProcessDealWork($list, $apiUser = [])
    {
        // 基本参数校验
        $validateRes = ValidateUtil::basicJobProcessDealWork($list);
        if ($validateRes['error']) return $validateRes;

        try {
            // 业务ID
            $jobId = $list['jobId'] ?? '';
            // 流程ID
            $flowId = $list['flowId'];
            // 节点ID
            $nodeId = $list['nodeId'];
            // 保存提交标识[0=保存，1=提交]
            $saveOrSubmit = $list['saveOrSubmit'] ?? 0;

            // 已知数据剔除
            $unsetCol = ['jobId', 'flowId', 'nodeId', 'saveOrSubmit'];
            $map = [];
            foreach ($unsetCol as $key => $val) {
                if (isset($list[$val])) {
                    $map[$val] = $list[$val];
                    unset($list[$val]);
                }
            }

            // 判断该进程是否被操作过
            if ($jobId && !ProcessUtil::getProcessesId($flowId, $nodeId, $jobId)) {
                return $this->jsonReturns(1, Config::PROCESSHASOPT);
            }
            // 判断进程是否被锁定
            if ($jobId && $saveOrSubmit) {
                // 获取全局配置锁定时间TODO
                $ttl = 3;
                $redisKey = Config::REDISPRIFIX . $flowId . $nodeId . $jobId;
                if (Redis::exists($redisKey)) {
                    return $this->jsonReturns(1, str_replace('@@@TTL@@@', $ttl, Config::PROCESSLOCKED));
                } else {
                    Redis::setex($redisKey, $ttl, 1);
                }
            }
            // 进程保存前置动作
            $beforeSaveRes = ProcessUtil::systemDefaultFabsExecute('beforeSave', $flowId, $nodeId, $jobId, $list, $apiUser);
            if ($beforeSaveRes['error']) return $this->jsonReturns(1, $beforeSaveRes['message']);

            // 保存或提交数据校验（保存进行错误校验，提交进行错误及必填校验）[业务参数校验]
            $ruleKey = $flowId . '_' . $nodeId;
            $ruleVerifyRes = Lrules::getInstance()->satisfies($ruleKey, $list, $map, false, false);
            if ($ruleVerifyRes['error']) return $this->jsonReturns(1, $ruleVerifyRes['message'] ?? Config::PROCESSVERIFYRULEERROR);

            // 提交数据校验(控制数据是否完整)
            $ruleVerifySubmitRes = Lrules::getInstance()->satisfies($ruleKey, $list, $map, true, false);
            $REQUIRED = $ruleVerifySubmitRes['error'] ? 0 : 1;
            $map['REQUIRED'] = $REQUIRED;

            // 创建业务进程或更新业务进程
            $jobRes = !$jobId ?
                $this->jobProcessCreated($REQUIRED, $flowId, $nodeId, $list, $apiUser) :
                $this->jobProcessUpdated($REQUIRED, $flowId, $nodeId, $jobId, $list, $apiUser);
            if ($jobRes['error']) return $this->jsonReturns(1, $jobRes['message']);
            if (!$jobId) {
                $jobId = $jobRes['data']['jobId'];
                $map['jobId'] = $jobId;
            }

            // 进程保存后置动作
            $afterSaveRes = ProcessUtil::systemDefaultFabsExecute('afterSave', $flowId, $nodeId, $jobId, $list, $apiUser);
            if ($afterSaveRes['error']) return $this->jsonReturns(1, $afterSaveRes['message']);

            // 提交进程
            if ($saveOrSubmit) {
                $map['processId'] = ProcessUtil::getProcessIdByJob($map['flowId'], $map['nodeId'], $map['jobId']);
                return $this->jobProcessSubmit($map, $apiUser);
            }

            return $this->jsonReturns(0, Config::SUCCESSMSG, '', ['jobId' => $jobId]);
        } catch (\Exception $e) {
            $this->logs->info('jobProcessDealWork ------> Try Catch Error: ' . $e->getMessage());
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    /**
     * 流程提交
     *
     * @name    jobProcessSubmit
     *
     * @param   array       $list           请求参数
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function jobProcessSubmit($list, $apiUser = [])
    {
        // 基本参数校验
        $validateRes = ValidateUtil::basicJobProcessSubmit($list);
        if ($validateRes['error']) return $validateRes;

        try {
            // 提交前置动作
            $beforeSubmitRes = ProcessUtil::systemDefaultFabsExecute(
                'beforeSubmit',
                $list['flowId'],
                $list['nodeId'],
                $list['jobId'],
                $list,
                $apiUser
            );
            if ($beforeSubmitRes['error']) return $this->jsonReturns(1, $beforeSubmitRes['message']);

            if (!isset($list['REQUIRED'])) {
                $list['REQUIRED'] = ProcessUtil::getJobFieldByPrimary(
                    $list['jobId'],
                    ProcessUtil::getMainJobTableByFlowId($list['flowId']),
                    'REQUIRED'
                );
            }

            // 待处理业务数据
            $verfiyData = [];
            $table = ProcessUtil::getMainJobTableByFlowId($list['flowId']);
            $verfiyData['main'][$table] = ProcessUtil::getJobFieldsByPrimary($list['jobId'], $table);
            // 副表待处理TODO

            $ruleKey = $list['flowId'] . '_' . $list['nodeId'];
            $ruleVerifySubmitRes = Lrules::getInstance()->satisfies($ruleKey, $verfiyData, $list, true, false);
            if ($ruleVerifySubmitRes['error']) {
                return $this->jsonReturns(1, $ruleVerifySubmitRes['message'] ?? Config::PROCESSREQUIREDFAILED);
            }

            // 必填校验标识控制（剔除，仅做页面显示提示）
            // 提交前进行规则必填校验（TODO）
            // if (!$list['REQUIRED']) {
            //     return $this->jsonReturns(1, Config::PROCESSREQUIREDFAILED);
            // }

            // 判断当前节点是否是审核环节
            $isExamNodeTag = ProcessUtil::judgeCurrentNodeIsExam($list['flowId'], $list['nodeId']);
            $exam = [];
            $examResult = '';
            $examKey = [];
            // 审核节点获取相关数据及校验
            if ($isExamNodeTag) {
                // 校验数据是否审核，获取审核配置，校验业务表是否审核操作过
                $examCheckRes = ProcessUtil::jobProcessExamCheck($list['flowId'], $list['nodeId'], $list['jobId']);
                if ($examCheckRes['error']) return $this->jsonReturns(1, $examCheckRes['message']);
                $exam = $examCheckRes['data']['value'];
                $examKey = $examCheckRes['data']['key'];
                $examResult = $exam['examResult'];
            }

            // 审核环节
            $jobProcessExamSubmit = function () use (&$list, &$apiUser, $exam, &$jobSubmitRes, &$examData) {
                // 审核前置动作
                $beforeExamRes = ProcessUtil::systemDefaultFabsExecute(
                    'beforeExam',
                    $list['flowId'],
                    $list['nodeId'],
                    $list['jobId'],
                    $list,
                    $apiUser
                );
                if ($beforeExamRes['error']) {
                    $jobSubmitRes = $this->jsonReturns(1, $beforeExamRes['message']);
                    return $jobSubmitRes;
                }
                $examResult = (int)$exam['examResult'];
                switch ($examResult) {
                    case 1:
                    case 0:
                    case 2:
                        $processDealRes = ProcessUtil::jobProcessSystemSetWorkFlowStatus($list['processId'], $examResult, $exam, $apiUser);
                        break;
                    default:
                        $jobSubmitRes = $this->jsonReturns(1, Config::PROCESSNOTEXITSEXAMTYPE);
                        return $jobSubmitRes;
                        break;
                }

                // 成功
                if (!$processDealRes['error']) {
                    // 审核提交后置业务处理
                    $afterExamRes = ProcessUtil::systemDefaultFabsExecute(
                        'afterExam',
                        $list['flowId'],
                        $list['nodeId'],
                        $list['jobId'],
                        $list,
                        $apiUser
                    );
                    if ($afterExamRes['error']) {
                        $jobSubmitRes = $this->jsonReturns(1, $afterExamRes['message']);
                        return $jobSubmitRes;
                    }

                    // 默认处理下一个节点业务数据未不完整数据
                    ProcessUtil::updateJobByPrimary($list['jobId'], $exam['mainTable'], ['REQUIRED' => 0]);

                    // 设置默认审核记录信息(不做强制错误处理)
                    $examData = [
                        'examResult' => $exam['examResult'],
                        'examReason' => $exam['examReason'],
                        'examId' => $apiUser['id'],
                        'examName' => $exam['examPerson'],
                        'examTag' => 0
                    ];
                    ProcessUtil::jobProcessSystemExamRecordSet($list['flowId'], $list['nodeId'], $list['jobId'], $examData);
                }

                $jobSubmitRes = $processDealRes;

                return $jobSubmitRes;
            };

            // 非审核环节
            $jobProcessCommonSubmit = function () use (&$list, &$apiUser, &$jobSubmitRes, &$examData) {
                // 处理流程进程信息
                $processDealRes = ProcessUtil::jobProcessSystemSetWorkFlowStatus(
                    $list['processId'],
                    1,
                    [],
                    $apiUser
                );
                // 成功
                if (!$processDealRes['error']) {
                    // 设置默认审核记录信息(不做强制错误处理)
                    $examData = [
                        'examResult' => 1,
                        'examReason' => Config::PROCESSSYSEXAMPASS,
                        'examId' => $apiUser['id'],
                        'examName' => $apiUser['realname'],
                        'examTag' => 1
                    ];
                    ProcessUtil::jobProcessSystemExamRecordSet($list['flowId'], $list['nodeId'], $list['jobId'], $examData);
                }

                $jobSubmitRes = $processDealRes;

                return $jobSubmitRes;
            };

            $isExamNodeTag ? $jobProcessExamSubmit() : $jobProcessCommonSubmit();

            // 处理成功
            if (!$jobSubmitRes['error']) {
                // 设置系统提交节点默认值处理(不做强制错误处理)
                ProcessUtil::jobProcessSystemNodeTimeRecordSet($list['flowId'], $list['nodeId'], $list['jobId'], [
                    'optId' => $apiUser['id'],
                    'optName' => $apiUser['realname']
                ]);

                // 提交后置动作
                $afterSubmitRes = ProcessUtil::systemDefaultFabsExecute(
                    'afterSubmit',
                    $list['flowId'],
                    $list['nodeId'],
                    $list['jobId'],
                    $list,
                    $apiUser
                );
                if ($afterSubmitRes['error']) return $this->jsonReturns(1, $afterSubmitRes['message']);
                // 系统历史备份记录
                ProcessUtil::jobProcessHistoriesRecords($list['flowId'], $list['jobId'], $jobSubmitRes['data']['processData'], $examData);
                if (isset($jobSubmitRes['data']['processData'])) {
                    unset($jobSubmitRes['data']['processData']);
                }
                // 更新剔除相关审核数据（业务数据）
                if ($isExamNodeTag && $examResult == 2) {
                    $redisKey = Config::REDISPRIFIX . 'exam_' . $list['flowId'] . '_' . $list['nodeId'] . '_' . $list['jobId'];
                    if (Redis::exists($redisKey)) {
                        Redis::del($redisKey);
                    }
                }
            }

            return $jobSubmitRes;
        } catch (\Exception $e) {
            $this->logs->info('jobProcessSubmit ------> Try Catch Error: ' . $e->getMessage());
            return $this->jsonReturns(1, Config::ERRORMSG, $e->getMessage());
        }
    }

    // 业务创建
    protected function jobProcessCreated($REQUIRED, $flowId, $nodeId, $list, $apiUser)
    {
        // 获取下一节点信息
        $nextNodeRes = ProcessUtil::getNextNodeStep($flowId, $nodeId);
        if ($nextNodeRes['error']) return $nextNodeRes;
        $nextNodeId = $nextNodeRes['data']['nextNodeId'];

        // 处理主表数据
        $jobMain = $list['main'];
        $mainTable = array_keys($jobMain)[0];
        $mainValue = array_values($jobMain)[0];
        $mainValue['REQUIRED'] = $REQUIRED;

        // 获取副表业务表
        $subJobTables = ProcessUtil::getSubJobTables($flowId, 0);

        // 兼容VUE中‘$’参数
        $mainValueFinal = [];
        foreach ($mainValue as $rmk => $rmv) {
            // if (strpos($rmk, "$") === 0) unset($mainValue[$rmk]);
            if (strpos($rmk, "$") === 0) continue;
            // if ($rmv === null) $mainValue[$rmk] = '';
            $mainValueFinal[$rmk] = $rmv === null ? '' : $rmv;
            // 处理副表数据-移除转移
            if (in_array($rmk, $subJobTables)) {
                $list[$rmk] = $mainValue[$rmk];
                unset($mainValueFinal[$rmk]);
                unset($list['main'][$mainTable][$rmk]);
            }
        }
        $mainValue = $mainValueFinal;

        // 生成订单号
        $NUMBER = ProcessUtil::generateNumberRuleByFlowId($flowId);
        $mainValue['NUMBER'] = $NUMBER;
        $mainValue['COMPANYID'] = $apiUser['company_id'];
        // 创建业务信息（主表）
        $jobId = ProcessUtil::createJobGetId($mainTable, $mainValue);
        if (!$jobId) return ['error' => 1, 'message' => Config::PROCESSCRDJOBERROR];
        // 移除主表数据信息
        unset($list['main']);
        // 处理副表数据-逐条处理
        if (count($subJobTables) && $list) {
            $dealSubJob = function () use ($list, $subJobTables, $jobId) {
                foreach ($list as $key => $val) {
                    if (!in_array($key, $subJobTables)) continue;
                    if (!count($val)) continue;
                    if (count($val) == count($val, 1)) $val = [$val];
                    $assert = false;
                    foreach ($val as $k => $v) {
                        foreach ($v as $colK => $colV) {
                            if ($colV != '') {
                                $assert = true;
                                break;
                            }
                        }
                        if ($assert === true) {
                            $val[$k]['PID'] = $jobId;
                        } else {
                            unset($val[$k]);
                        }
                    }
                    count($val) ? ProcessUtil::createJob($key, array_values($val)) : null;
                }
            };
            $dealSubJob();
        }

        // 创建进程表信息
        $process['flowId'] = $flowId;
        $process['jobId'] = $jobId;
        $process['companyId'] = $apiUser['company_id'];
        $process['nodeId'] = $nodeId;
        $process['nextNodeId'] = $nextNodeId;
        $process['decisionTag'] = -100;
        $process['operatorId'] = $apiUser['id'];
        $process['operator'] = $apiUser['realname'];
        $process['operatorDate'] = Carbon::now();
        $process['ownerId'] = $apiUser['id'];
        $process['owner'] = $apiUser['realname'];
        $processRes = ProcessUtil::createProcess($process);
        if ($processRes['error']) return $processRes;

        // 返回处理结果
        return ['error' => 0, 'data' => ['jobId' => $jobId]];
    }

    // 业务更新
    protected function jobProcessUpdated($REQUIRED, $flowId, $nodeId, $jobId, $list, $apiUser)
    {
        // 处理主表数据
        $jobMain = $list['main'];
        $mainTable = array_keys($jobMain)[0];
        $mainValue = array_values($jobMain)[0];
        $mainValue['REQUIRED'] = $REQUIRED;

        // 获取副表业务表
        $subJobTables = ProcessUtil::getSubJobTables($flowId, 1);

        // 兼容VUE中‘$’参数
        $mainValueFinal = [];
        foreach ($mainValue as $rmk => $rmv) {
            if (strpos($rmk, "$") === 0) continue;
            $mainValueFinal[$rmk] = $rmv === null ? '' : $rmv;
            // 处理副表数据-移除转移
            if (in_array($rmk, $subJobTables)) {
                $list[$rmk] = $mainValue[$rmk];
                unset($mainValueFinal[$rmk]);
                unset($list['main'][$mainTable][$rmk]);
            }
        }
        $mainValue = $mainValueFinal;

        // 更新业务信息（主表）
        $updateJobRes = ProcessUtil::updateJobByPrimary($jobId, $mainTable, $mainValue);
        // if (!$updateJobRes) return ['error' => 1, 'message' => Config::PROCESSCRDJOBERROR];
        // 移除主表数据信息
        unset($list['main']);
        $subJobUnSystemTables = ProcessUtil::getSubJobTables($flowId, 0);
        // 处理副表数据-逐条处理
        if (count($subJobUnSystemTables) && $list) {
            $dealSubJob = function () use ($list, $subJobUnSystemTables, $jobId) {
                foreach ($list as $key => $val) {
                    if (!in_array($key, $subJobUnSystemTables)) continue;
                    if (!count($val)) continue;
                    if (count($val) == count($val, 1)) $val = [$val];
                    $assert = false;
                    foreach ($val as $k => $v) {
                        foreach ($v as $colK => $colV) {
                            if ($colV != '') {
                                $assert = true;
                                break;
                            }
                        }
                        if ($assert === true) {
                            $val[$k]['PID'] = $jobId;
                            $val[$k]['id'] = $v['id'] ?? 0;
                        } else {
                            unset($val[$k]);
                        }
                    }
                    count($val) ? ProcessUtil::dealWithSubJobData($key, array_values($val), $jobId) : null;
                }
            };
            $dealSubJob();
        }

        return ['error' => 0, 'data' => ['jobId' => $jobId]];
    }

    /**
     * 获取业务待办列表
     *
     * @name    getJobTodoList
     *
     * @param   integer     $offest
     * @param   integer     $limit
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   mixed       $keyword        快捷搜索
     * @param   array       $search         高级过滤
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function getJobTodoList(
        $offest,
        $limit,
        $flowId,
        $nodeId,
        $keyword,
        $search,
        $apiUser
    )
    {
        $res = ProcessUtil::getJobTodoListByWorkStep(
            $offest,
            $limit,
            $flowId,
            $nodeId,
            $keyword,
            $search,
            $apiUser
        );
        if ($res['error']) {
            return $this->jsonReturns(50000, $res['message'] ?? '');
        }
        $jsonData = $res['data'];

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 获取业务报表业务数据
     *
     * @name    getBirtJobData
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   string      $uuId           UUID
     *
     * @return  array                       返回结果
     */
    public function getBirtJobData($flowId, $jobId, $uuId)
    {
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);
        $singleData = ProcessUtil::getJobFieldsByPrimary($jobId, $table);
        $columns = ProcessUtil::getJobColumnSystemCode($flowId, $table);
        $codes = ProcessUtil::getBeTranslateSystemCode($flowId, $table, $columns);
        if (count($codes)) ProcessUtil::jobTranslateCodeToName($singleData, $codes, true);

        // 获取系统内置日期参数
        $systemDateData = Util::getSystemInnerDate();
        $single = array_merge($singleData, $systemDateData);

        // 获取多记录配置
        $britConfig = ProcessUtil::getFieldByUniqid('wf_workbirts', [['wf04003', '=', $uuId]], 'wf04006');
        $tableConfig = [];
        if (!empty($britConfig)) {
            $keyConfig = json_decode($britConfig, true)['table'] ?? [];
            if ($keyConfig) {
                $subTable = [];
                $subJobTables = ProcessUtil::getSubJobTables($flowId, 1);
                foreach ($keyConfig as $key => $val) {
                    if (in_array($key, $subJobTables)) array_push($subTable, $key);
                }
                if ($subTable) {
                    $subTable = array_values(array_unique($subTable));
                    foreach ($subTable as $k => $v) {
                        $tableConfig[$v] = ProcessUtil::getSubJobDataByPid($v, $jobId);
                    }
                }
            }
        }

        $jsonData = [
            'single' => $single,
            'table' => $tableConfig
        ];

        return $jsonData;
    }

    /**
     * 获取业务审核信息
     *
     * @name    getJobTodoList
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   integer     $jobId          业务ID
     *
     * @return  array                       返回结果
     */
    public function getJobProcessExamDetail($flowId, $nodeId, $jobId)
    {
        $examResult = ProcessUtil::getWorkLinksForExam($flowId, $nodeId);
        $examResSelection = [];
        $examRebackSelection = [];
        foreach ($examResult as $key => $val) {
            $val = (array)$val;
            $examResSelection[] = ['label' => Config::EXAMRESNAME[(string)$val['step']], 'value' => (string)$val['step']];
            if ((string)$val['step'] == '2') {
                $examRebackSelection[] = [
                    'label' => ProcessUtil::getFieldByUniqid('wf_worknodes', [['wf02001', '=', $flowId], ['wf02002', '=', $val['toId']]], 'wf02003'),
                    'value' => $val['toId']
                ];
            }
        }

        $jsonData['selection'] = [
            'examResSelection' => $examResSelection,
            'examRebackSelection' => $examRebackSelection
        ];

        // 获取审核配置
        $examConfig = ProcessUtil::getFieldByUniqid('wf_worknodecolumns', [['wf07001', '=', $flowId], ['wf07002', '=', $nodeId]], 'wf07005');
        $examConfig = json_decode($examConfig, true);
        // 获取主表信息
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);
        // 获取业务数据
        $jobData = ProcessUtil::getJobFieldsByPrimary($jobId, $table, [
            $examConfig['examResult'] . ' as examResult', $examConfig['examReason'] . ' as examReason'
        ]);
        $exam['examResult'] = is_null($jobData['examResult']) ? '' : $jobData['examResult'];
        $exam['examReason'] = is_null($jobData['examReason']) ? '' : $jobData['examReason'];
        $exam['examReback'] = '';
        $redisKey = Config::REDISPRIFIX . 'exam_' . $flowId . '_' . $nodeId . '_' . $jobId;
        if (Redis::exists($redisKey)) {
            $exam['examReback'] = Redis::get($redisKey);
        }

        $jsonData['exam'] = $exam;

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 获取业务数据详情
     *
     * @name    getJobDataDetail
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     * @param   integer     $jobId          业务ID
     *
     * @return  array                       返回结果
     */
    public function getJobDataDetail($flowId, $nodeId, $jobId)
    {
        $jsonData = [];
        $table = ProcessUtil::getMainJobTableByFlowId($flowId);
        $mainData = ProcessUtil::getJobFieldsByPrimary($jobId, $table);
        // 主表数据
        $jsonData = $mainData;
        // 副表数据
        // 获取副表业务表
        $subJobTables = ProcessUtil::getSubJobTables($flowId, 1);
        foreach ($subJobTables as $k => $v) {
            $jsonData[$v] = ProcessUtil::getSubJobDataByPid($v, $jobId);
        }

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 获取业务表单配置信息
     *
     * @name    getJobFormDefaultConfig
     *
     * @param   string      $flowId         流程ID
     * @param   string      $nodeId         节点ID
     *
     * @return  array                       返回结果
     */
    public function getJobFormDefaultConfig($flowId, $nodeId)
    {
        $jsonData = ProcessUtil::getJobFormDefaultConfig($flowId, $nodeId);

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 判断流程是否启用历史记录
     *
     * @name    getWorkFlowAnalysis
     *
     * @param   string      $flowId         流程ID
     * @param   array       $apiUser        用户信息
     * @param   array       $usertable      用户表信息
     * @param   object      $share          公共方法
     * @return 	boolean                     返回结果
     */
    public function getWorkFlowAnalysis($flowId, $apiUser, $usertable, $share)
    {
        $jsonData = [];
        // 获取相关总数统计
        $jsonTotals = ProcessUtil::getJobAnalysisForTotal($flowId, $apiUser['company_id']);
        $jsonData['jobTotal'] = $jsonTotals;
        $jsonData = array_merge($jsonData, $this->transChartsUsedJobTotal($jsonTotals));

        // 获取今日，本周，本月，年度相关总数
        $jobDateTotal = ProcessUtil::getJobAnalysisForDateTotal($flowId, $apiUser['company_id']);
        $jsonData['jobDateTotal'] = $jobDateTotal;
        $jsonData = array_merge($jsonData, $this->transChartsUsedJobDateTotal($jobDateTotal));

        // 获取成单排行版
        $jobTopBusman = ProcessUtil::getJobAnalysisForBusmanTop($apiUser['company_id'], $usertable, $share);
        $jsonData['jobTopBusman'] = $jobTopBusman;
        // 同比转化率
        $jobTransRatio = ProcessUtil::getJobAnalysisForTransRadio($flowId, $apiUser['company_id']);
        $jsonData['jobTransRatio'] = $jobTransRatio;
        $jsonData = array_merge($jsonData, $this->transChartsTransedJobTransRatio($jobTransRatio));

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }
    
    /**
     * 获取用户统计相关信息
     *
     * @name    getWorkFlowForUser
     *
     * @param   string      $flowId         流程ID
     * @param   array       $apiUser        用户信息
     * @param   array       $usertable      用户表信息
     * @return 	boolean                     返回结果
     */
    public function getWorkFlowForUser($apiUser, $usertable)
    {
        $jsonData = [];
        // 获取成单量
        $jsonData['completed'] = (int)ProcessUtil::getJobCompletedByUserId($apiUser['id'], $apiUser['company_id']);
        // 获取录单量
        $jsonData['created'] = (int)ProcessUtil::getJobCreatedByUserId($apiUser['id'], $apiUser['company_id']);
        // 成单率
        $jsonData['completedratio'] = bcmul(bcdiv($jsonData['completed'], ($jsonData['created'] ? $jsonData['created'] : 1), 6), 100, 2) . '%';
        // 公司排名及总数
        $topData = ProcessUtil::getJobTopDataByUserId($apiUser['id'], $apiUser['company_id']);

        $jsonData['top'] = $topData['top'];
        $jsonData['topcount'] = $topData['topcount'];

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    public function getJobWorkFlowNodeAnalysis($flowId, $apiUser)
    {
        $jsonData = [];
        // 获取流程数据
        if (!$flowId) {
            $workFlowData = ProcessUtil::getWorkFlowByBelongEnabled($apiUser['company_id']);
            $count = count($workFlowData);
            $jsonData['span'] = (int)(24 / $count);

        } else {
            $workFlowData = ProcessUtil::getWorkFlowByFlowId($flowId);
            $workFlowData = [$workFlowData];
            $jsonData['span'] = 24;
        }

        $workNodeData = [];
        foreach ($workFlowData as $key => $val) {
            $val = (array)$val;
            $list = [];
            $workNodeData[$key]['title'] = $val['flowName'];
            $workNodeData[$key]['color'] = sprintf( "#%06X", mt_rand(0, 0xFFFFFF));
            $workNodeData[$key]['subtext'] = ProcessUtil::getJobCompletedByFlowId($val['flowId'], $apiUser['company_id']) . '/' . ProcessUtil::getJobCreatedByflowId($val['flowId'], $apiUser['company_id']);

            // 获取节点信息
            $nodeDatas = ProcessUtil::getWorkNodeByFlowIdEnabled($val['flowId']);

            $flowNodeTips = ProcessUtil::getJobFlowStrtipsByFlowId($val['flowId'], $apiUser['company_id']);
            $list[] = [
                'title' => Config::JOBWORKDETAIL,
                'check' => true,
                'tip' => $flowNodeTips,
            ];

            foreach ($nodeDatas as $k => $v) {
                $v = (array)$v;
                $list[($k + 1)]['title'] = $v['wf02003'];
                $list[($k + 1)]['check'] = true;
                $tip = ProcessUtil::getJobNodeStrtipsByNodeId($val['flowId'], $v, $apiUser['company_id']);
                $list[($k + 1)]['tip'] = $tip;
            }
            $workNodeData[$key]['list'] = $list;
        }
        $jsonData['data'] = $workNodeData;

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    public function transChartsUsedJobTotal($list)
    {
        $jsonData = [];
        $categories = array_column($list, 'title');

        $seriesColumnQData = [];
        $seriesColumnFData = [];
        array_map(function ($v) use (&$seriesColumnQData, &$seriesColumnFData) {
            array_push($seriesColumnQData, (int)$v['count']);
            array_push($seriesColumnFData, (int)$v['count']);
        }, $list);

        $seriesData[] = ['type' => 'column', 'name' => '', 'showInLegend' => false, 'data' => $seriesColumnQData];
        $seriesData[] = ['type' => 'spline', 'name' => '', 'showInLegend' => false, 'data' => $seriesColumnFData];

        $jsonData = [
            'jobTotalCategories' => $categories,
            'jobTotalSeriesData' => $seriesData,
        ];

        return $jsonData;
    }

    public function transChartsUsedJobDateTotal($list)
    {
        $jsonData = [];
        $categories = array_column($list, 'title');

        $seriesColumnQData = [];
        $seriesColumnFData = [];
        array_map(function ($v) use (&$seriesColumnQData, &$seriesColumnFData) {
            array_push($seriesColumnQData, (int)$v['count']);
            array_push($seriesColumnFData, (int)$v['count']);
        }, $list);

        $seriesData[] = ['type' => 'column', 'name' => '', 'showInLegend' => false, 'data' => $seriesColumnQData];
        $seriesData[] = ['type' => 'spline', 'name' => '', 'showInLegend' => false, 'data' => $seriesColumnFData];

        $jsonData = [
            'jobDateTotalCategories' => $categories,
            'jobDateTotalSeriesData' => $seriesData,
        ];

        return $jsonData;
    }

    public function transChartsTransedJobTransRatio($list)
    {
        $jsonData = [];

        $seriesColumnData = [];
        array_map(function ($v) use (&$seriesColumnData) {
            $seriesColumnData[] = ['name' => ($v['point'] < 0 ? Config::JOBTRANSSUB : '') . $v['charts'], 'y' => floatval($v['point'] < 0 ? -$v['point'] : $v['point'])];
        }, $list);

        $seriesData[] = ['type' => 'pie', 'name' => Config::JOBTRANSCHARTS, 'data' => $seriesColumnData];

        $jsonData = [
            'jobTransRatioSeriesData' => $seriesData,
        ];

        return $jsonData;
    }

    /**
     * 获取历史业务列表
     *
     * @name    getHistoryJobList
     *
     * @param   integer     $offest
     * @param   integer     $limit
     * @param   string      $flowId         流程ID
     * @param   mixed       $keyword        快捷搜索
     * @param   array       $outWhere       外部搜索
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function getHistoryJobList(
        $offest,
        $limit,
        $flowId,
        $keyword,
        $outWhere,
        $apiUser
    )
    {
        $res = ProcessUtil::getHistoryJobListByFlowId(
            $offest,
            $limit,
            $flowId,
            $keyword,
            $outWhere,
            $apiUser
        );
        if ($res['error']) {
            return $this->jsonReturns(50000, $res['message'] ?? '');
        }
        $jsonData = $res['data'];

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 获取历史业务数据详情
     *
     * @name    getHistoryJobDataDetail
     *
     * @param   integer     $primaryId      历史记录主键ID
     *
     * @return  array                       返回结果
     */
    public function getHistoryJobDataDetail($primaryId)
    {
        $historyJData = ProcessUtil::getHistoryJobFieldsByPrimary($primaryId);
        $flowId = $historyJData['FLOWID'];
        $historyJobData = $historyJData['JOBDATA'];

        $historyJobData = json_decode($historyJobData, true);
        if (!$historyJobData) return $historyJobData;

        $jsonData = array_values($historyJobData['main'])[0];
        // 获取副表业务表
        $subJobTables = ProcessUtil::getSubJobTables($flowId, 1);
        foreach ($subJobTables as $k => $v) {
            if (isset($historyJobData[$v])) {
                $jsonData[$v] = $historyJobData[$v];
            }
        }
        
        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }

    /**
     * 获取业务客户列表
     *
     * @name    getCustomerJobList
     *
     * @param   integer     $offest
     * @param   integer     $limit
     * @param   string      $flowId         流程ID
     * @param   mixed       $keyword        快捷搜索
     * @param   array       $outWhere       外部条件
     * @param   array       $apiUser        用户信息
     *
     * @return  array                       返回结果
     */
    public function getCustomerJobList(
        $offest,
        $limit,
        $flowId,
        $keyword,
        $outWhere,
        $apiUser
    )
    {
        $res = ProcessUtil::getJobCustomerListByFlowId(
            $offest,
            $limit,
            $flowId,
            $keyword,
            $outWhere,
            $apiUser
        );
        if ($res['error']) {
            return $this->jsonReturns(50000, $res['message'] ?? '');
        }
        $jsonData = $res['data'];

        return $this->jsonReturns(0, Config::SUCCESSMSG, '', $jsonData);
    }
}
