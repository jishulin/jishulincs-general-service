<?php

namespace Jishulin\WorkFlowEngine;

use Illuminate\Support\ServiceProvider;

class WFServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        // $this->publishes([
        //     __DIR__.'/config/wf.php' => config_path('wf.php'),
        // ]);
    }
}
