<?php
class ToolsTest extends \PHPUnit\Framework\TestCase
{
    protected $tools;
    public function setUp()
    {
        $this->tools = \Jishulin\WorkFlowEngine\Tools\Tools::getInstance();
    }

    public function testWorkFlowCrd()
    {
        $res = $this->tools->workFlowCrd('aaaa', 'test work flow');
        return $this->assertEquals(0, $res['error']);
    }

}
