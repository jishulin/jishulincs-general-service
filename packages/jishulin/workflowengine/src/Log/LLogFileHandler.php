<?php

namespace Jishulin\WorkFlowEngine\Log;

define("LLOG_ROOT", __DIR__);
define("LLOG_DIR", realpath(LLOG_ROOT) . "/logs/");

class LLogFileHandler
{
    private $handle = null;

    public function __construct($type, $filePrefix)
    {
        if ($type == 'sql') {
            $logPathInit = LLOG_DIR  . '/' . $type;
        } else {
            $logPathInit = LLOG_DIR  . '/' . $type . '/' . date('Ym');
        }
        
        if (!is_dir($logPathInit)) {
            mkdir($logPathInit, 0777, true);
        }
        
        if ($type == 'sql') {
            $file = $logPathInit . '/' . $filePrefix . '.sql';
        } else {
            $file = $logPathInit . '/' . $filePrefix . '_' . date('Y-m-d') . '.log';
        }

        $this->handle = fopen($file, 'a');
    }

    public function write($msg)
    {
        fwrite($this->handle, $msg, 8192);
    }

    public function __destruct()
    {
        fclose($this->handle);
    }
}
