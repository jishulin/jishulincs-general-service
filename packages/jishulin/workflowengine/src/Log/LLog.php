<?php

namespace Jishulin\WorkFlowEngine\Log;

use Jishulin\WorkFlowEngine\Log\LLogFileHandler;

class LLog
{
    private $handler = null;

    private $level = 15;

    private static $instance = null;

    private static $handlerStatic = null;

    static $type;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function init($type = 'log', $filePrefix = 'debug')
    {
        static::$type = $type;
        if (!(self::$handlerStatic instanceof LLogFileHandler)) {
            self::$handlerStatic  = new LLogFileHandler($type, $filePrefix);
        }

        if (!self::$instance instanceof self) {
            self::$instance = new self();
            self::$instance->__setHandle(self::$handlerStatic);
            self::$instance->__setLevel(15);
        }

        return self::$instance;
    }


    private function __setHandle($handler)
    {
        $this->handler = $handler;
    }

    private function __setLevel($level)
    {
        $this->level = $level;
    }

    public function debug($msg, $tag = 0)
    {
        self::$instance->write(1, $msg, $tag);
    }

    public function warn($msg, $tag = 0)
    {
        self::$instance->write(4, $msg, $tag);
    }

    public function error($msg, $tag = 0)
    {
        $debugInfo = debug_backtrace();
        $stack = "[";
        foreach ($debugInfo as $key => $val) {
            if (array_key_exists("file", $val)) {
                $stack .= ",file:" . $val["file"];
            }
            if (array_key_exists("line", $val)) {
                $stack .= ",line:" . $val["line"];
            }
            if (array_key_exists("function", $val)) {
                $stack .= ",function:" . $val["function"];
            }
        }
        $stack .= "]";
        self::$instance->write(8, $stack . $msg, $tag);
    }

    public function info($msg, $tag = 0)
    {
        self::$instance->write(2, $msg, $tag);
    }

    private function getLevelStr($level)
    {
        switch ($level) {
            case 1:
                return 'debug';
                break;
            case 2:
                return 'info';
                break;
            case 4:
                return 'warn';
                break;
            case 8:
                return 'error';
                break;
            default:
                break;
        }
    }

    protected function write($level, $msg, $tag = 0)
    {
        if (($level & $this->level) == $level) {
            if (static::$type == 'sql') {
                $msg = '-- [' . date('Y-m-d H:i:s') . ']' . "\n" . $msg . "\n\n";
            } else {
                $msg =  ($tag ? "\n\n\n" : '') . '[' . date('Y-m-d H:i:s') . '][' . $this->getLevelStr($level) . '] ' . $msg . "\n";
            }
            $this->handler->write($msg);
        }
    }
}
