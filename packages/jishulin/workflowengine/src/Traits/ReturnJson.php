<?php

namespace Jishulin\WorkFlowEngine\Traits;

trait ReturnJson
{
    /**
     * 返回信息列表处理
     * 
     * @name    jsonReturns
     * @param 	integer     $tag            代码值[1=失败，0=成功]
     * @param   string 	    $message        返回消息
     * @param   string 	    $errmsg         具体异常
     * @param   array       $data           额外数据信息
     * @return 	array                       返回结果 
     */
    public function jsonReturns($tag, $message, $errmsg = '', $map = [])
    {
        $tag = intval($tag);
        
        $res = ['error' => $tag, 'message' => $message, 'errmsg' => $errmsg, 'data' => []];
        if($map) $res['data'] = $map;
        
        return $res;
    }
}
