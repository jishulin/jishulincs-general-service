<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkprocessoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workprocessovers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('FLOWID', 100)->default('')->comment('流程ID');
            $table->string('NODEID', 100)->default('')->comment('节点ID');
            $table->integer('JOBID')->default(0)->comment('业务ID');
            $table->string('COMPANYID', 100)->default('')->comment('所属[单位/平台]');
            $table->integer('ORDERBELONGID')->default(0)->comment('订单所属人ID');
            $table->string('ORDERBELONG', 100)->default('')->comment('订单所属人');
            $table->string('OVERTYPE', 100)->default('')->comment('结束类型[common=正常结束,error=审核不通过结束]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workprocessovers');
    }
}
