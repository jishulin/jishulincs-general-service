<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkpsysexamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_worksysexams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf09001', 100)->default('')->comment('流程ID');
            $table->string('wf09002', 50)->default('')->comment('审核结果');
            $table->string('wf09003', 50)->default('')->comment('审核时间');
            $table->string('wf09004', 50)->default('')->comment('审核意见');
            $table->string('wf09005', 50)->default('')->comment('审核人ID');
            $table->string('wf09006', 50)->default('')->comment('审核人');
            $table->string('wf09007', 50)->default('')->comment('审核类型');
            $table->string('wf09008', 50)->default('')->comment('审核标识');
            $table->tinyInteger('wf09009')->default('1')->comment('状态[1=启用，0=禁用]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_worksysexams');
    }
}
