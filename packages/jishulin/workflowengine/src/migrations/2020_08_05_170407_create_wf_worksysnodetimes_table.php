<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorksysnodetimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_worksysnodetimes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf10001', 100)->default('')->comment('流程ID');
            $table->string('wf10002', 50)->default('')->comment('节点ID');
            $table->string('wf10003', 50)->default('')->comment('节点名称');
            $table->string('wf10004', 50)->default('')->comment('节点首次提交时间');
            $table->string('wf10005', 50)->default('')->comment('节点最后一次提交时间');
            $table->string('wf10006', 50)->default('')->comment('节点提交人ID');
            $table->string('wf10007', 50)->default('')->comment('节点提交人');
            $table->tinyInteger('wf10008')->default('1')->comment('状态[1=启用，0=禁用]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_worksysnodetimes');
    }
}
