<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workhistories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NUMBER', 100)->default('')->comment('订单号');
            $table->string('NAME', 100)->default('')->comment('姓名');
            $table->string('FLOWID', 100)->default('')->comment('流程ID');
            $table->string('NODEID', 100)->default('')->comment('节点ID');
            $table->integer('JOBID')->default(0)->comment('节点ID');
            $table->string('COMPANYID', 100)->default('')->comment('所属[单位/平台]');
            $table->integer('ORDERBELONGID')->default(0)->comment('订单所属人ID');
            $table->string('ORDERBELONG', 100)->default('')->comment('订单所属人');
            $table->integer('EXAMID')->default(0)->comment('审核人ID');
            $table->string('EXAMNAME', 100)->default('')->comment('审核人');
            $table->string('EXAMDATE', 20)->default('')->comment('审核时间');
            $table->string('EXAMRESULT', 64)->default('')->comment('审核结果');
            $table->text('EXAMREASON')->nullable()->comment('审核意见');
            $table->integer('OPTID')->default(0)->comment('操作人ID');
            $table->string('OPTNAME', 100)->default('')->comment('操作人');
            $table->longText('JOBDATA')->nullable()->comment('业务数据');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workhistories');
    }
}
