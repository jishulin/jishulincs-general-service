<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkfbasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workfbas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wf99001', 100)->default('')->comment('流程ID');
            $table->string('wf99002', 100)->default('')->comment('节点ID');
            $table->tinyInteger('wf99003')->default(1)->comment('是否启用【1=启用，0=不启用】');
            $table->string('wf99004', 100)->default('')->comment('命名空间');
            $table->string('wf99005', 100)->default('')->comment('方法名');
            $table->enum('wf88006', ['afterExam', 'beforeExam','afterSubmit','beforeSubmit','afterSave','beforeSave','afterDelete','beforeDelete'])->comment('动作类型【删除前后，保存前后，提交前后，审核前后】');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workfbas');
    }
}
