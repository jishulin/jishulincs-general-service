<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkcustomercolumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workcustomercolumns', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf97001', 100)->default('')->comment('流程ID');
            $table->integer('wf97002')->default(0)->comment('用户ID');
            $table->tinyInteger('wf97003')->default(1)->comment('启用禁用[1=启用，0=禁用]');
            $table->text('wf97004')->nullable()->comment('字段配置[原始]');
            $table->text('wf97005')->nullable()->comment('字段配置[排序，别名]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workcustomercolumns');
    }
}
