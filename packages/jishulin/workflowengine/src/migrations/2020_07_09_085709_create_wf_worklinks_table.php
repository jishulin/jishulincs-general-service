<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorklinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_worklinks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf03001', 100)->comment('流程唯一标识');
            $table->string('wf03002', 60)->default('')->comment('from节点');
            $table->string('wf03003', 60)->default('')->comment('to节点');
            $table->string('wf03004', 60)->default('')->comment('linkID');
            $table->string('wf03005', 100)->default('')->comment('link名称');
            $table->tinyInteger('wf03006')->default(0)->comment('是否删除[0=正常，1=删除]');
            $table->integer('wf03007')->default(0)->comment('删除时间');
            $table->tinyInteger('wf03008')->default(1)->comment('节点走向条件值[1=通过，0=不通过，2=回退]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_worklinks');
    }
}
