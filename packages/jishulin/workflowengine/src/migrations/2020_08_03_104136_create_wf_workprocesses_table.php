<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkprocessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workprocesses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf12001', 100)->default('')->comment('流程ID');
            $table->bigInteger('wf12002')->default(0)->comment('业务ID');
            $table->string('wf12003', 100)->default('')->comment('所属[单位/平台]');
            $table->string('wf12004', 100)->default('')->comment('当前节点ID');
            $table->string('wf12005', 100)->default('')->comment('下一节点ID');
            $table->integer('wf12006')->default('-100')->comment('决策标识【-100=初始状态，100=待决策，1=执行下一节点】');
            $table->integer('wf12007')->default(0)->comment('操作人ID');
            $table->string('wf12008', 100)->default('')->comment('操作人');
            $table->string('wf12009', 20)->default('')->comment('操作时间');
            $table->integer('wf12010')->default(0)->comment('订单所属人ID');
            $table->string('wf12011', 100)->default('')->comment('订单所属人');
            $table->text('wf12012')->nullable()->comment('系统备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workprocesses');
    }
}
