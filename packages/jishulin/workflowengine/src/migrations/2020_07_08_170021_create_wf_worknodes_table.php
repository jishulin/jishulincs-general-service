<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorknodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_worknodes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf02001', 100)->comment('流程唯一标识');
            $table->string('wf02002', 100)->default('')->comment('节点标识');
            $table->string('wf02003', 100)->default('')->comment('节点名');
            $table->string('wf02004', 100)->default('')->comment('节点属性[start=开始，sub=子流程]');
            $table->tinyInteger('wf02005')->default(0)->comment('是否审核节点[0=否，1=是]');
            $table->string('wf02006', 20)->default('')->comment('分流到子流程节点标识[S]');
            $table->string('wf02007', 20)->default('')->comment('合流前一个节点标识[E]');
            $table->string('wf02008', 20)->default('')->comment('非子流程节点是否是分流或合流[F=分流，R=合流]');
            $table->tinyInteger('wf02009')->default(10)->comment('排序');
            $table->index(['wf02001', 'wf02002']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_worknodes');
    }
}
