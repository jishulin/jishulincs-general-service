<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorknodecolumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_worknodecolumns', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf07001', 100)->default('')->comment('流程ID');
            $table->string('wf07002', 100)->default('')->comment('节点ID');
            $table->text('wf07003')->nullable()->comment("列表选择显示字段");
            $table->text('wf07004')->nullable()->comment("列表显示字段【排序，别名】");
            $table->text('wf07005')->nullable()->comment("系统内置字段【NUMBER,NAME】【显用，别名】");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_worknodecolumns');
    }
}
