<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkchartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workcharts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('FLOWID', 100)->default('')->comment('流程ID');
            $table->string('NODEID', 100)->default('')->comment('节点ID');
            $table->integer('JOBID')->default(0)->comment('业务ID');
            $table->string('COMPANYID', 100)->default('')->comment('所属[单位/平台]');
            $table->integer('ORDERBELONGID')->default(0)->comment('订单所属人ID');
            $table->string('ORDERBELONG', 100)->default('')->comment('订单所属人');
            $table->string('ORDERDATEY', 4)->default('')->comment('生成日期(年)');
            $table->string('ORDERDATEM', 2)->default('')->comment('生成日期(月)');
            $table->string('ORDERDATED', 2)->default('')->comment('生成日期(月)');
            $table->string('ORDERDATE', 10)->default('')->comment('生成日期(年月日)');
            $table->string('ORDERDTIME', 20)->default('')->comment('生成时间');
            $table->tinyInteger('ISEXAM')->default(0)->comment('是否是审核节点');
            $table->tinyInteger('ISEXAMRES')->default(1)->comment('审核结果');
            $table->tinyInteger('ISCREATE')->default(0)->comment('是否是录单');
            $table->tinyInteger('ISCOMPLETE')->default(0)->comment('是否是成单');
            $table->integer('OPTID')->default(0)->comment('操作人ID');
            $table->string('OPTNAME', 100)->default('')->comment('操作人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workcharts');
    }
}
