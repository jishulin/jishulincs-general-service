<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkprocessrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workprocessrecords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wf98001', 100)->default('')->comment('流程ID');
            $table->integer('wf98002')->default(0)->comment('业务ID');
            $table->string('wf98003', 100)->default('')->comment('开始节点ID');
            $table->string('wf98004', 100)->default('')->comment('结束节点ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workprocessrecords');
    }
}
