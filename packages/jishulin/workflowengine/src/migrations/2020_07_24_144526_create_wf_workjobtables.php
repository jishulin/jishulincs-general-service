<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkjobtables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workjobtables', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf05001', 100)->default('')->comment('流程ID');
            $table->string('wf05002', 100)->default('')->comment('业务表名称(注释)');
            $table->string('wf05003', 100)->default('')->comment('业务表名(无前缀)');
            $table->tinyInteger('wf05004')->default(0)->comment('主表副表[主=1，副=0]');
            $table->tinyInteger('wf05005')->default(1)->comment('是否启用[1=启用，0=禁用]');
            $table->tinyInteger('wf05006')->default(1)->comment('系统内置[1=内置，0=非内置]');
            $table->tinyInteger('wf05007')->default(0)->comment('是否是审核记录表【1=是，0=否】有且只有一个，系统内置');
            $table->tinyInteger('wf05008')->default(0)->comment('是否是节点时间记录表【1=是，0=否】有且只有一个，系统内置');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workjobtables');
    }
}
