<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkjobcolumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workjobcolumns', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf06001', 100)->default('')->comment('流程ID');
            $table->string('wf06002', 100)->default('')->comment('业务表名(无前缀)');
            $table->string('wf06003', 100)->default('')->comment('字段名');
            $table->string('wf06004', 100)->default('')->comment('字段注释');
            $table->string('wf06005', 60)->default('')->comment('字段类型');
            $table->string('wf06006', 60)->default('')->comment('字段长度');
            $table->string('wf06007', 60)->default('')->comment('小数点');
            $table->string('wf06008', 10)->default('')->comment('代码类型');
            $table->string('wf06009', 60)->default('')->comment('系统编码');
            $table->string('wf06010', 60)->default('')->comment('公司编码');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workjobcolumns');
    }
}
