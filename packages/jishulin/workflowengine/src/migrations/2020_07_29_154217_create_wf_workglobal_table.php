<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkglobalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workglobal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wf08001', 100)->default('')->comment('流程ID');
            $table->string('wf08002', 100)->default('')->comment('KEY');
            $table->string('wf08003', 100)->default('')->comment('KEY标题');
            $table->tinyInteger('wf08004')->default(1)->comment('是否启用[1=启用，0=禁用]');
            $table->text('wf08005')->nullable()->comment("配置信息");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workglobal');
    }
}
