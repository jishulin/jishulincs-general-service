<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workforms', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf11001', 100)->default('')->comment('流程ID');
            $table->string('wf11002', 50)->default('')->comment('节点ID');
            $table->string('wf11003', 100)->default('')->comment('表单名称');
            $table->longText('wf11004')->nullable()->comment('表单配置');
            $table->tinyInteger('wf11005')->default('1')->comment('默认显示[1=是，0=否]');
            $table->tinyInteger('wf11006')->default('1')->comment('状态[1=启用，0=禁用]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workforms');
    }
}
