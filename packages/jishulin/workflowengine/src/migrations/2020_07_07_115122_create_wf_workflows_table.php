<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workflows', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf01001', 100)->unique()->comment('流程唯一标识');
            $table->string('wf01002', 100)->default('')->comment('流程名称');
            $table->string('wf01003', 100)->default('')->comment('流程所有者[单位/平台],根据业务需求而定');
            $table->tinyInteger('wf01004')->default(0)->comment('流程状态[0=禁用，1=启用]');
            $table->text('wf01005')->nullable()->comment("流程渲染数据");
            $table->string('wf01006', 100)->default('')->comment('流程图片key[云存储]，本地附件存储长度不够自定义修改');
            $table->tinyInteger('wf01007')->default(0)->comment('辅助字段[0=无需重置，1=需要重置]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workflows');
    }
}
