<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWfWorkbirtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_workbirts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('wf04001', 100)->default('')->comment('流程ID');
            $table->string('wf04002', 60)->default('')->comment('节点ID');
            $table->string('wf04003', 100)->default('')->comment('UUID');
            $table->string('wf04004', 100)->default('')->comment('报表名称');
            $table->string('wf04005', 100)->default('')->comment('报表地址');
            $table->text('wf04006')->nullable()->comment('配置信息');
            $table->tinyInteger('wf04007')->default(0)->comment('报表类型[列表=1，详情=0]');
            $table->tinyInteger('wf04008')->default(1)->comment('是否转PDF[1=转，0=不转]');
            $table->tinyInteger('wf04009')->default(0)->comment('是否单页详情[1=是，0=否]');
            $table->tinyInteger('wf04010')->default(1)->comment('是否启用[1=启用，0=禁用]');
            $table->integer('wf04011')->default(10)->comment('排序');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_workbirts');
    }
}
