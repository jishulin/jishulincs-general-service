<?php

namespace Jishulin\RulesEngine;

use Illuminate\Support\Facades\Config;

class RulesEngine
{
    public $foreignClass = 'App\Extend\ExpandRules';
    
    public function __construct()
    {
        $ruleConfig = Config::get('app.rule');
        if ($ruleConfig) $this->foreignClass = $ruleConfig;
    }

    public function jsonReturns($tag, $message, $errmsg = '', $map = [])
    {
        $tag = intval($tag);

        $res = ['error' => $tag, 'message' => $message, 'errmsg' => $errmsg, 'data' => []];
        if($map) $res['data'] = $map;

        return $res;
    }
}
