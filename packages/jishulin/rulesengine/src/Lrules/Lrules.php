<?php

namespace Jishulin\RulesEngine\Lrules;

use Jishulin\RulesEngine\RulesEngine;
use Jishulin\RulesEngine\Lrules\RulesCheck;
use Jishulin\RulesEngine\Lrules\RulesExpand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class Lrules extends RulesEngine
{
    /**
     * @var $_instance
     */
    private	static $_instance;

    private $ST = false;
    private $RK = '';
    private $VD = '';
    private $RQ = true;

    /**
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function satisfies(
        $ruleKey,
        $validates,
        $extra = null,
        $required = false,
        $strict = false
    )
    {
        if (!$ruleKey) {
            return $strict ? $this->jsonReturns(1, '严格模式中,规则KEY不能为空') : $this->jsonReturns(0, '非严格模式,规则KEY为空,默认校验通过');
        }
        if (empty($validates)) {
            return $strict ? $this->jsonReturns(1, '严格模式中,待校验数据不能为空') : $this->jsonReturns(0, '非严格模式,待校验数据为空,默认校验通过');
        }

        $this->ST = $strict;
        $this->RK = $ruleKey;
        $this->RQ = $required;
        $this->VD = $validates;
        $this->EXTRA = $extra;
        return $this->parseValidates();
    }

    public function parseValidates()
    {
        // 获取校验数据规则
        $rulesCol = DB::table('lrules')
            ->where('LR_KEY', $this->RK)
            ->where('LR_STATUS', 1)
            ->get()
            ->toArray();
        if (empty($rulesCol)) {
            return $this->ST ? $this->jsonReturns(1, '严格模式中,规则校验逻辑未配置') : $this->jsonReturns(0, '非严格模式中,未配置规则校验逻辑，默认校验通过');
        }
        // 数据分组
        $arrGroupByCol = function () use (&$rulesCol, &$rules) {
            foreach ($rulesCol as $key => $val) {
                $val = (array)$val;
                $rules[$val['LR_GROUP']][] = $val;
            }
        };
        $arrGroupByCol();

        foreach ($rules as $key => $rule) {
            $key = strtoupper($key);
            switch ($key) {
                // 系统内置
                case 'D':
                    $res = $this->checkSystemInnerRules($rule);
                    if ($res['error']) return $res;
                    break;
                // 客户正则
                case 'E':
                    $res = $this->checkCustRegexRules($rule);
                    if ($res['error']) return $res;
                    break;
                // 必填依赖关系
                case 'F':
                    if ($this->RQ) {
                        $res = $this->checkRequiredRelyOnRules($rule);
                        if ($res['error']) return $res;
                    }
                    break;
                // 全局校验(自定义)
                case 'G':
                    $res = $this->ckeckGlobalCustRules($rule);
                    if ($res['error']) return $res;
                    break;
                default:
                    break;
            }
        }

        return $this->jsonReturns(0, 'PASS');
    }

    // 系统内置校验
    protected function checkSystemInnerRules(&$rule)
    {
        $class = new RulesCheck();
        foreach ($rule as $key => $val) {
            $col = $val['LR_COL'];

            // 多记录校验
            if (strpos($col, '*') !== false) {
                $colValArr = data_get($this->VD, $col);
            } else {
                $colValSingle = data_get($this->VD, $col, '');
                $colValArr = [$colValSingle];
            }

            $label = $val['LR_LABEL'];
            $desc = $val['LR_DESC'];

            foreach ($colValArr as $colVal) {
                if ($this->RQ && $val['LR_REQUIRED'] && $colVal == '') return $this->jsonReturns(1, $val['LR_LABEL'] . '不能为空');
                if (!$this->RQ && $colVal == '') continue;
                if (empty($val['LR_RULES'])) continue;
                $ruleCondition = explode('|', trim($val['LR_RULES'], '|'));
                foreach ($ruleCondition as $k => $v) {
                    $limitVal = null;
                    $tMethod = '';
                    if (strpos($v, ':') === false) {
                        $tMethod = ucfirst($v);
                    } else {
                        $lV = explode(':', $v);
                        $tMethod = ucfirst($lV[0]);
                        $limitVal = $lV[1];
                    }
                    $method = 'check' . $tMethod;
                    if (method_exists($class, $method)) {
                        $ruleCheckRes = $class->$method($colVal, $label, $desc, $limitVal);
                    } elseif (class_exists($this->foreignClass)) {
                        $foreignClass = new $this->foreignClass;
                        if (method_exists($foreignClass, $method)) {
                            $ruleCheckRes = $foreignClass->$method($colVal, $label, $desc, $limitVal);
                        } else {
                            return $this->jsonReturns(1, '[' . $tMethod . ']方法不存在,请确认');
                        }
                    } else {
                        return $this->jsonReturns(1, '[' . $tMethod . ']方法不存在,请确认');
                    }

                    if ($ruleCheckRes !== true) return $ruleCheckRes;
                }
            }
        }

        return $this->jsonReturns(0, 'SYSTEM VALIDATE PASS');
    }

    // 客户自定义正则校验
    protected function checkCustRegexRules(&$rule)
    {
        foreach ($rule as $key => $val) {
            $col = $val['LR_COL'];

            // 多记录校验
            if (strpos($col, '*') !== false) {
                $colValArr = data_get($this->VD, $col);
            } else {
                $colValSingle = data_get($this->VD, $col, '');
                $colValArr = [$colValSingle];
            }

            $label = $val['LR_LABEL'];
            $desc = $val['LR_DESC'];

            foreach ($colValArr as $colVal) {
                if ($this->RQ && $val['LR_REQUIRED'] && $colVal == '') return $this->jsonReturns(1, $val['LR_LABEL'] . '不能为空');
                if (!$this->RQ && $colVal == '') continue;
                if (empty($val['LR_RULES'])) continue;
                if ($colVal == '') continue;

                $reg = $val['LR_RULES'];
                if (!preg_match($reg, $colVal)) {
                    return $this->jsonReturns(1, ($desc ? $desc : ($label . '自定义正则校验不通过')));
                }
            }
        }

        return $this->jsonReturns(0, 'CUST REGEX VALIDATE PASS');
    }

    // 全局校验自定义校验
    protected function ckeckGlobalCustRules(&$rule)
    {
        // 校验方法，严格模式直接报错，非严格模式过滤
        foreach ($rule as $key => $val) {
            $ruleVal = trim($val['LR_RULES'], '|');
            $ruleValArr = explode('|', $ruleVal);
            if (count($ruleValArr) != 2 && $this->ST) {
                return $this->jsonReturns(1, ($label . '全局校验配置有误,请确认'));
            } elseif (count($ruleValArr) != 2 && !$this->ST) {
                continue;
            }

            $space = $ruleValArr[0];
            $method = $ruleValArr[1];
            // 判断控制器是否存在
            if (!class_exists($space) && $this->ST) {
                return $this->jsonReturns(1, ($space . '不存在,请确认'));
            } elseif (!class_exists($space) && !$this->ST) {
                continue;
            }
            $object = new $space;
            if (!method_exists($object, $method) && $this->ST) {
                return $this->jsonReturns(1, ($method . '不存在,请确认'));
            } elseif (!method_exists($object, $method) && !$this->ST) {
                continue;
            }

            $res = $object->$method($this->VD, $this->EXTRA);
            if (!isset($res['error']) && $this->ST) {
                return $this->jsonReturns(1, '自定义执行方法[' . $space . '\\' . $method . ']返回格式不正确');
            } elseif (!isset($res['error']) && !$this->ST) {
                continue;
            }
            if ($res['error']) return $this->jsonReturns(1, $res['message'] ?? '执行自定义方法校验不通过');
        }

        // 返回结果
        return $this->jsonReturns(0, 'CUST GLOBAL VALIDATE PASS');
    }

    // 必填依赖关系
    protected function checkRequiredRelyOnRules(&$rule)
    {
        $preg = '/(?<={).*?(?=})+/';
        foreach ($rule as $key => $val) {
            $col = $val['LR_COL'];
            $colVal = data_get($this->VD, $col, '');
            $label = $val['LR_LABEL'];
            $desc = $val['LR_DESC'];
            $ruleStr = $val['LR_RULES'];
            if (empty($ruleStr)) continue;
            $ruleRepalceArr = [];
            preg_match_all($preg, $ruleStr, $ruleRepalceArr);
            // 替换操作
            if (isset($ruleRepalceArr[0]) && count($ruleRepalceArr[0])) {
                foreach ($ruleRepalceArr[0] as $rK => $rV) {
                    $beReplaceStr = '{' . $rV . '}';
                    $beReplaceVal = data_get($this->VD, $rV, '');
                    $ruleStr = str_replace($beReplaceStr, $beReplaceVal, $ruleStr);
                }
            }
            $exceuteRes = eval("return $ruleStr;");
            if ($exceuteRes && $colVal == '') {
                return $this->jsonReturns(1, $desc ? $desc : ($label . '不能为空'));
            }
        }

        // 返回结果
        return $this->jsonReturns(0, 'REQURIED RELY ON VALIDATE PASS');
    }

    protected function checkCustExtendRules(&$rule)
    {

    }
}
