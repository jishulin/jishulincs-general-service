<?php

namespace Jishulin\RulesEngine\Lrules;

class RulesCheck
{
    // 检验是否必填完整[$limitVal=1必填校验，否则不做校验]
    public function checkRequired($val, $label, $desc, $limitVal = null)
    {
        return ($val == '' || is_null($val)) && $limitVal == 1 ? [
            'error' => 1,
            'message' => $label . '不能为空'
        ] : true;
    }

    // 校验中文汉字
    public function checkChs($val, $label, $desc, $limitVal = null)
    {
        if ($val == '') return true;

        $reg = '/^[\x{4e00}-\x{9fa5}]+$/u';
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入中文汉字')
        ];
    }

    // 【解析中文字符为3个字符长度】判断字符长度最小值不能小于$limitVal(不含等于)
    public function checkMin($val, $label, $desc, $limitVal)
    {
        return strlen($val) < $limitVal && $val !='' ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '长度不能小于' . $limitVal)
        ] : true;
    }

    // 【解析中文字符为3个字符长度】判断字符长度最大值不能大于$limitVal(不含等于)
    public function checkMax($val, $label, $desc, $limitVal)
    {
        return strlen($val) > $limitVal && $val !='' ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '长度不能大于' . $limitVal)
        ] : true;
    }

    // 【解析中文字符为1个字符长度】判断字符长度最小值不能小于$limitVal(不含等于)
    public function checkMinone($val, $label, $desc, $limitVal)
    {
        return mb_strlen($val) < $limitVal && $val !='' ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '长度不能大于' . $limitVal)
        ] : true;
    }

    // 【解析中文字符为1个字符长度】判断字符长度最大值不能大于$limitVal(不含等于)
    public function checkMaxone($val, $label, $desc, $limitVal)
    {
        return mb_strlen($val) > $limitVal && $val !='' ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '长度不能大于' . $limitVal)
        ] : true;
    }

    // 身份证规则校验
    public function checkIdcard($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        if (strlen($val) != 18 && strlen($val) != 15) return ['error' => 1, 'message' => $desc ? $desc : ($label . '格式不正确')];
        if (strlen($val) == 15) {
            $val = array_search(substr($val, 12, 3), ['996', '997', '998', '999']) !== false ? (
                substr($val, 0, 6) . '18' . substr($val, 6, 9)
            ) : (
                substr($val, 0, 6) . '19' . substr($val, 6, 9)
            );
            $val = $val . $this->idcard_verify_number($val);
        }

        $val_ = substr($val, 0, 17);
        return $this->idcard_verify_number($val_) != strtoupper(substr($val, 17, 1)) ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '格式不正确')
        ] : true;
    }
    // 身份证规则校验加权辅助
    private function idcard_verify_number($val_)
    {
        $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        $verify_number_list = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
        $checksum = 0;
        for ($i = 0; $i < strlen($val_); $i++) $checksum += substr($val_, $i, 1) * $factor[$i];
        $mod = $checksum % 11;
        $verify_number = $verify_number_list[$mod];
        return $verify_number;
    }

    // 纯数字,没有+-符号及小数点等
    public function checkNumber($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[0-9]*$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入数字（不允许含有正负号及小数点）')
        ];
    }

    // 浮点数,最多含一位小数点
    public function checkFloat1dot($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^-?[0-9]+(.[0-9]{1})?$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入浮点数（最多一位小数点）')
        ];
    }

    // 浮点数,最多含两位小数点
    public function checkFloat2dot($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^-?[0-9]+(.[0-9]{1,2})?$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入浮点数（最多两位小数点）')
        ];
    }

    // 浮点数,指定小数点位数
    public function checkFloat($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        if ($limitVal == '' || $limitVal == 0 || $limitVal === true) {
            $reg = "/^-?[0-9]+(.[0-9]*)?$/";
            $message = $label . '只能输入浮点数';
        } else {
            $dot_tail = $limitVal == 1 ? '{1}' : '{1,' . intval($limitVal) . '}';
            $reg = "/^-?[0-9]+(.[0-9]" . $dot_tail . ")?$/";
            $message = $desc ? $desc : ($label . '只能输入浮点数（最多' . intval($limitVal) . '位小数点)');
        }

        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $message
        ];
    }

    // 身份证号码[正则校验]
    public function checkIdcardreg($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '格式不正确')
        ];
    }

    // 手机号码格式
    public function checkMobile($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^1[3-9][0-9]\d{8}$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '格式不正确')
        ];
    }

    // 只能是汉字、字母、数字和下划线_及破折号-
    public function checkChsdash($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[\x{4e00}-\x{9fa5}a-zA-Z0-9\_\-]+$/u";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入汉字、字母、数字和下划线及破折号')
        ];
    }

    // 只能是汉字、字母和数字
    public function checkChsalphanum($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[\x{4e00}-\x{9fa5}a-zA-Z0-9]+$/u";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入汉字、字母和数字')
        ];
    }

    // 是汉字、字母
    public function checkChsalpha($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[\x{4e00}-\x{9fa5}a-zA-Z]+$/u";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入汉字和字母')
        ];
    }

    // 字母和数字,下划线及破折号
    public function checkAlphadash($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[A-Za-z0-9\-\_]+$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入字母和数字下划线及破折号')
        ];
    }

    // 字母和数字
    public function checkAlphanum($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[A-Za-z0-9]*$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入字母和数字')
        ];
    }

    // 纯字母,含大小写
    public function checkAlpha($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = "/^[A-Za-z]*$/";
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能输入字母（含大小写）')
        ];
    }

    // 指定等于(限制为)某个值
    public function checkEqual($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        return $val != $limitVal ? [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能为指定值(' . $limitVal . ')')
        ] : true;
    }

    // 判断字符长度在两者之间(中文算3个字符)
    public function checkLength($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $arr = explode(',', str_replace(' ', '', rtrim($limitVal, ',')));
        $len = strlen($val);
        if ($len >= $arr[0] && $len <= $arr[1]) {
            return true;
        }

        return [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '字符长度只能在' . $arr[0] . '到' . $arr[1] . '之间')
        ];
    }

    // 判断字符长度在两者之间(中文算1个字符)
    public function checkLengthone($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $arr = explode(',', str_replace(' ', '', rtrim($limitVal, ',')));
        $len = mb_strlen($val);
        if ($len >= $arr[0] && $len <= $arr[1]) {
            return true;
        }

        return [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '字符长度只能在' . $arr[0] . '到' . $arr[1] . '之间')
        ];
    }

    // 判断数字在两者之间
    public function checkBetween($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        if (!is_numeric($val)) {
            return ['error' => 1, 'message' => $label . '不是正确的数值'];
        }
        $numArray = explode(',', str_replace(' ', '', rtrim($limitVal, ',')));
        if ($val >= $numArray[0] && $val <= $numArray[1]) {
            return true;
        }

        return [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '只能在' . $numArray[0] . '到' . $numArray[1] . '之间')
        ];
    }

    // 判断数字不在两者之间
    public function checkNotbetween($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        if (!is_numeric($val)) {
            return ['error' => 1, 'message' => $label . '不是正确的数值'];
        }
        $numArray = explode(',', str_replace(' ', '', rtrim($limitVal, ',')));
        if ($val >= $numArray[0] && $val <= $numArray[1]) {
            return [
                'error' => 1,
                'message' => $desc ? $desc : ($label . '不能在' . $numArray[0] . '到' . $numArray[1] . '之间')
            ];
        }

        return true;
    }

    // 正则校验在其中
    public function checkRegex($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = $limitVal;
        return preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '自定义正则校验不通过')
        ];
    }

    // 正则校验不在其中
    public function checkNotregex($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;

        $reg = $limitVal;
        return !preg_match($reg, $val) ? true : [
            'error' => 1,
            'message' => $desc ? $desc : ($label . '自定义正则校验不通过')
        ];
    }

    // 日期相关
    // 是否是日期格式[yyyy-mm-dd HH:ii:ss,yyyy-mm-dd HH:ii][yyyy-mm-dd,yymmdd,yyyy/mm/dd]
    public function checkDatetime($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        if (false === strtotime($val)) {
            return ['error' => 1, 'message' =>  $desc ? $desc : ($label . '不是一个合法的日期格式')];
        }

        return true;
    }

    // 日期指定格式校验[ymd,y-m-d,y/m/d]
    public function checkDateformat($val, $label, $desc, $limitVal)
    {
        if ($val == '') return true;
        if (false === strtotime($val)) {
            return ['error' => 1, 'message' =>  $desc ? $desc : ($label . '不是一个合法的日期格式')];
        }

        switch ($limitVal) {
            case 'ymd':
                $format = 'Ymd';
                break;
            case 'ymdhi':
                $format = 'Ymd H:i';
                break;
            case 'ymdhis':
                $format = 'Ymd H:i:s';
                break;
            case 'y-m-d':
                $format = 'Y-m-d';
                break;
            case 'y-m-d-h-i':
                $format = 'Y-m-d H:i';
                break;
            case 'y-m-d-h-i-s':
                $format = 'Y-m-d H:i:s';
                break;
            case 'y/m/d':
                $format = 'Y/m/d';
                break;
            case 'y/m/d/h/i':
                $format = 'Y/m/d H:i';
                break;
            case 'y/m/d/h/i/s':
                $format = 'Y/m/d H:i:s';
                break;
            default:
                return true;
                break;
        }
        $info = date_parse_from_format($format, $val);
        if (0 == $info['warning_count'] && 0 == $info['error_count']) {
            return true;
        }

        return ['error' => 1, 'message' =>  $desc ? $desc : ($label . '指定的日期格式不正确')];
    }

}
