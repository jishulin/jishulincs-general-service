<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLrulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lrules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('LR_KEY', 100)->default('')->comment('基于规则获取分组的KEY值');
            $table->string('LR_GROUP', 10)->default('')->comment('内置分组类型[D,E,F,G]');
            $table->string('LR_COL', 100)->default('')->comment('字段名称');
            $table->string('LR_LABEL', 80)->default('')->comment('标签名');
            $table->string('LR_DESC', 300)->default('')->comment('提示全称(自定义)');
            $table->tinyInteger('LR_REQUIRED')->default(0)->comment('必填[1=是，0=否]');
            $table->string('LR_RULES', 500)->default('')->comment('规则配置(混合模式,详见文档)');
            $table->tinyInteger('LR_STATUS')->default('1')->comment('状态[1=启用，0=禁用]');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lrules');
    }
}
