<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// 测试路由信息
Route::post('test', function () {
    return request();exit;
    return ['aaa' => 'aaa', 'bbb' => 'bbbb'];
    return "sssss";
});

Route::match(['post', 'get'], 'test2', 'TestController@test');
Route::match(['post', 'get'], 'testp', 'TestController@testp');
Route::match(['post', 'get'], 'testr', 'TestController@testr');

// ------------------------------------------------- start 书籍应用路由 -------------------------------------------------
// ------------------------- start 无状态方法路由 -------------------------
// API登录请求独立-POST
Route::post('signIn', 'API\SignController@signIn');
// 文章分享连接解析-无状态校验
Route::match(['post', 'get'], 'ae5e0de8771711eab205f0761cb312e', 'API\Stateless\BookStatelessController@analysisArticleShare');
// 平台接入获取书籍列表--后面添加白名单控制
Route::post('ccc713876523226d46f1798d711ee2b0', 'API\Stateless\BookStatelessController@platformAccessOfBooks');
// ------------------------- end 无状态方法路由 -------------------------

// API请求必须校验接口信息
Route::middleware(['checkApiToken'])->group(function() {
    // ------------------------- start 公共共享方法路由 -------------------------
    // 用户登出
    Route::match(['post', 'get'], 'signOut', 'API\Common\ShareController@signOut');
    // 文件上传-仅仅图片
    Route::match(['post'], 'imageUploadStore', 'API\Common\ShareController@imageUploadStore');
    // ------------------------- end 公共共享方法路由 -------------------------

    // ------------------------- start 超管相关路由 ----------------------------
    // 超管操作中间件限制
    Route::middleware(['checkApiSuperAction'])->group(function() {
        // 获取平台列表
        Route::match(['post', 'get'], 'getPlatformList', 'API\Systems\PlateController@getPlatformList');
        // 根据平台ID获取平台信息
        Route::match(['post', 'get'], 'getPlatformInfoById', 'API\Systems\PlateController@getPlatformInfoById');
        // 添加平台
        Route::match(['post'], 'platformCreated', 'API\Systems\PlateController@platformCreated');
        // 修改平台
        Route::match(['post'], 'platformModified', 'API\Systems\PlateController@platformModified');
        // 启用禁用平台
        Route::match(['post'], 'platformStatused', 'API\Systems\PlateController@platformStatused');
    });

    // 平台相关用户管理
    // 获取平台下用户
    Route::match(['post', 'get'], 'getPlatformUserList', 'API\Systems\PlateUserController@getPlatformUserList');
    // 根据用户ID获取用户信息
    Route::match(['post', 'get'], 'getPlatformUserInfoById', 'API\Systems\PlateUserController@getPlatformUserInfoById');
    // 创建用户
    Route::match(['post'], 'platformUserCreated', 'API\Systems\PlateUserController@platformUserCreated');
    // 启用禁用用户
    Route::match(['post'], 'platformUserStatused', 'API\Systems\PlateUserController@platformUserStatused');
    // 重置密码
    Route::match(['post'], 'platformUserResetPwd', 'API\Systems\PlateUserController@platformUserResetPwd');
    // ------------------------- end 超管相关路由 ----------------------------

    // ------------------------- start 书籍相关路由 ----------------------------
    // 书籍
    // 获取书籍列表信息
    Route::match(['post', 'get'], 'getBookList', 'API\Books\BookController@getBookList');
    // 获取书籍信息通过ID
    Route::match(['post', 'get'], 'getBookInfoById', 'API\Books\BookController@getBookInfoById');
    // 创建书籍
    Route::match(['post'], 'bookCreated', 'API\Books\BookController@bookCreated');
    // 修改书籍
    Route::match(['post'], 'bookModified', 'API\Books\BookController@bookModified');
    // 删除书籍
    Route::match(['post'], 'bookDeleted', 'API\Books\BookController@bookDeleted');
    // 书籍公开
    Route::match(['post', 'get'], 'bookOpenPublish', 'API\Books\BookController@bookOpenPublish');
    // 书籍文章同步操作
    Route::match(['post', 'get'], 'bookSynchronization', 'API\Books\BookController@bookSynchronization');
    // 接入平台获取公开书籍
    Route::match(['post', 'get'], 'getOpenPublishBooksByPlate', 'API\Books\BookController@getOpenPublishBooksByPlate');

    // 目录
    // 获取书籍目录信息
    Route::match(['post', 'get'], 'getArtCatesList', 'API\Books\BookArticleCateController@getArtCatesList');
    // 获取书籍目录信息通过ID
    Route::match(['post', 'get'], 'getArtCateById', 'API\Books\BookArticleCateController@getArtCateById');
    // 创建书籍目录
    Route::match(['post'], 'artCateCreated', 'API\Books\BookArticleCateController@artCateCreated');
    // 修改书籍目录
    Route::match(['post'], 'artCateModified', 'API\Books\BookArticleCateController@artCateModified');
    // 删除书籍目录
    Route::match(['post'], 'artCateDeleted', 'API\Books\BookArticleCateController@artCateDeleted');
    // 获取书籍目录树
    Route::match(['post', 'get'], 'getArticleCateBookTree', 'API\Books\BookArticleCateController@getArticleCateBookTree');

    // 文章
    // 获取文章信息通过ID
    Route::match(['post', 'get'], 'getArticleById', 'API\Books\ArticleController@getArticleById');
    // 创建文章
    Route::match(['post'], 'articleCreated', 'API\Books\ArticleController@articleCreated');
    // 修改文章
    Route::match(['post'], 'articleModified', 'API\Books\ArticleController@articleModified');
    // 删除文章
    Route::match(['post'], 'articleDeleted', 'API\Books\ArticleController@articleDeleted');
    // 获取当前文章历史版本
    Route::match(['post', 'get'], 'getArticleVeriosn', 'API\Books\ArticleController@getArticleVeriosn');
    // 生成文章分享连接信息
    Route::match(['post', 'get'], 'getArticleShares', 'API\Books\ArticleController@getArticleShares');
    // ------------------------- end 书籍相关路由 ----------------------------
});
// ------------------------------------------------- end 书籍应用路由 -------------------------------------------------

// ------------------------------------------------- start 技术林应用路由-20200518 -------------------------------------------------
Route::prefix('zhihu')->group(function () {
    // 用户注册路由
    Route::post('register', 'ZHIHU\Systems\RegisterController@register');
    // 用户注册邮箱校验激活
    Route::any('registerEmailVerifyActive', 'ZHIHU\Systems\RegisterController@registerEmailVerifyActive');
    // 用户登录
    Route::post('signIn', 'ZHIHU\Systems\SigninController@signIn');
    // 用户退出
    Route::post('signOut', 'ZHIHU\Systems\SigninController@signOut');
    // 找回密码
    Route::any('retrievePassword', 'ZHIHU\Systems\RegisterController@retrievePassword');
    // 找回密码-链接有效校验-重置密码
    Route::any('retrievePasswordLinkVerifyActiveAndReset', 'ZHIHU\Systems\RegisterController@retrievePasswordLinkVerifyActiveAndReset');
    // 上传文件
    Route::post('imageUpload', 'ZHIHU\Common\ShareController@imageUpload');
    // 获取话题列表字典
    Route::match(['post', 'get'], 'topicSelectDict', 'ZHIHU\Common\ShareController@topicSelectDict');
    // 获取脏词信息
    Route::match(['post', 'get'], 'getDirtyWords', 'ZHIHU\Common\ShareController@getDirtyWords');

    // 需登录后操作路由
    Route::middleware(['checkZhihuToken'])->group(function() {

        // ************************* start 管理员操作 *************************
        // 脏问题列表
        Route::match(['post', 'get'], 'listDirtyQuestion', 'ZHIHU\Topics\TopicController@listDirtyQuestion');
        // 脏问题标记处理
        Route::post('removeDirtyQuestion', 'ZHIHU\Topics\TopicController@removeDirtyQuestion');

        // 话题列表
        Route::match(['post', 'get'], 'listTopics', 'ZHIHU\Topics\TopicController@listTopics');
        // 创建话题
        Route::post('createTopic', 'ZHIHU\Topics\TopicController@createTopic');
        // 设置话题
        Route::post('configTopic', 'ZHIHU\Topics\TopicController@configTopic');
        // 启用禁用话题
        Route::post('statusTopic', 'ZHIHU\Topics\TopicController@statusTopic');

        // 问题举报管理-list=列表，exam=审核
        Route::post('reportManager/{name}', 'ZHIHU\Systems\SystemsConfigController@reportManager')->where('name', '[a-z]+');
        // 评论举报管理-list=列表，exam=审核
        Route::post('reportCommentManager/{name}', 'ZHIHU\Systems\SystemsConfigController@reportCommentManager')->where('name', '[a-z]+');

        // 系统设置路由
        Route::post('setting/{k}', 'ZHIHU\Systems\SystemsConfigController@setting');
        Route::post('gettingByKey/{k}', 'ZHIHU\Systems\SystemsConfigController@gettingByKey');

        // 设置配置源-edit=修改,status=启用禁用,remove=删除,create=添加,list=列表,active=有效配置源
        Route::post('configSource/{name}', 'ZHIHU\Systems\SystemsConfigController@configSource')->where('name', '[a-z]+');

        // 声望分值配置-list=列表,edit=修改,status=启用禁用,create=添加
        Route::post('prestigeScore/{name}', 'ZHIHU\Systems\SystemsConfigController@prestigeScore')->where('name', '[a-z]+');

        // 系统公告设置-edit=修改,remove=删除,create=添加,list=列表,history=历史
        Route::post('systemNotice/{name}', 'ZHIHU\Systems\SystemsConfigController@systemNotice')->where('name', '[a-z]+');

        // 广告配置操作-create=添加，remove=删除，list=列表，history=历史，edit=编辑
        Route::post('systemAdvs/{name}', 'ZHIHU\Systems\SystemsConfigController@systemAdvs')->where('name', '[a-z]+');

        // 广告设置-list=列表，create=添加，remove=删除，edit=修改
        Route::post('advsSetting/{name}', 'ZHIHU\Systems\SystemsConfigController@advsSetting')->where('name', '[a-z]+');

        // 封面管理-list=列表，create=添加，edit=修改，setting=设置为封面
        Route::post('coverManager/{name}', 'ZHIHU\Systems\SystemsConfigController@coverManager')->where('name', '[a-z]+');

        // 站内用户--list=列表，status=启用禁用
        Route::post('userManager/{name}', 'ZHIHU\Systems\WebInfomationCenterController@userManager')->where('name', '[a-z]+');

        // 建议反馈管理-list=列表，deal=处理，反馈=feedback
        Route::post('feedbackManager/{name}', 'ZHIHU\Systems\SystemsConfigController@feedbackManager')->where('name', '[a-z]+');

        // 友情链接-list=列表，create=添加，edit=修改,status=启用禁用
        Route::post('friendLink/{name}', 'ZHIHU\Systems\SystemsConfigController@friendLink')->where('name', '[a-z]+');

        // 管理员首页
        Route::match(['post', 'get'], 'getSystemRotate', 'ZHIHU\Systems\WebInfomationCenterController@getSystemRotate');
        Route::match(['post', 'get'], 'getSystemTabs', 'ZHIHU\Systems\WebInfomationCenterController@getSystemTabs');
        Route::match(['post', 'get'], 'getSystemCard', 'ZHIHU\Systems\WebInfomationCenterController@getSystemCard');
        Route::match(['post', 'get'], 'getSystemHotQuestion', 'ZHIHU\Systems\WebInfomationCenterController@getSystemHotQuestion');

        // 站内统计，用户注册量，访问量，话题量，活跃度分析
        Route::post('userRegisterCharts', 'ZHIHU\Systems\SystemStatisController@userRegisterCharts');
        Route::post('puvCharts', 'ZHIHU\Systems\SystemStatisController@puvCharts');
        Route::post('topicCharts', 'ZHIHU\Systems\SystemStatisController@topicCharts');
        Route::post('activityCharts', 'ZHIHU\Systems\SystemStatisController@activityCharts');

        // 管理员消息发布-list=消息发布列表-create=创建,edit=修改,remove=删除,publish=发布,recopy=复制
        Route::match(['post', 'get'], 'systemPublishMessage/{name}', 'ZHIHU\Systems\MessageController@systemPublishMessage')->where('name', '[a-z]+');
        // ************************* end 管理员操作 *************************

        //问题相关
        // 发布问题
        Route::post('publishQuestion', 'ZHIHU\Topics\TopicController@publishQuestion');
        // 设置问题
        Route::post('configQuestion', 'ZHIHU\Topics\TopicController@configQuestion');
        // 查看问题
        Route::match(['post', 'get'], 'viewQuestion', 'ZHIHU\Topics\TopicController@viewQuestion');
        // 删除问题
        Route::post('removeQustion', 'ZHIHU\Topics\TopicController@removeQustion');
        // 问题列表
        Route::match(['post', 'get'], 'listQuestion', 'ZHIHU\Topics\TopicController@listQuestion');
        // 设置/取消精华(管理)
        Route::match(['post', 'get'], 'configQuestionCream', 'ZHIHU\Topics\TopicController@configQuestionCream');
        // 获取最新问题及最受欢迎问题
        Route::match(['post', 'get'], 'getShowQuestionLimitList', 'ZHIHU\Topics\TopicController@getShowQuestionLimitList');

        // 站内信-私信列表-message=最新未读,readed=已读,deleted=已删除,sended=我发送的,system=系统信息,totals=获取标签未读数
        Route::match(['post', 'get'], 'listMessage/{name?}', 'ZHIHU\Systems\MessageController@listMessage')->where('name', '[a-z]+');
        // 站内信息设置操作-toread=设为已读[可批量],todelete=删除[可批量],tomessage=设为未读[可批量]
        Route::post('configMessage/{name}', 'ZHIHU\Systems\MessageController@configMessage')->where('name', '[a-z]+');

        // 用户设置
        Route::post('personalSettings', 'ZHIHU\Systems\PersonalController@personalSettings');
        // 通过用户ID获取用户信息
        Route::match(['post', 'get'], 'getPersonalInfoByUserId', 'ZHIHU\Systems\PersonalController@getPersonalInfoByUserId');
        // 获取个人信息
        Route::match(['post', 'get'], 'getPersonalInfo', 'ZHIHU\Systems\PersonalController@getPersonalInfo');
        // 用户组件信息
        Route::match(['post', 'get'], 'getPersonalComponent', 'ZHIHU\Systems\PersonalController@getPersonalComponent');
        // 发送私信
        Route::post('sendPrivateMessage', 'ZHIHU\Systems\PersonalController@sendPrivateMessage');
        // 获取声望记录
        Route::match(['post', 'get'], 'getListPrestige', 'ZHIHU\Systems\PersonalController@getListPrestige');
        // 获取用户个人中心统计数据
        Route::match(['post', 'get'], 'getPersonalTotalNums', 'ZHIHU\Systems\PersonalController@getPersonalTotalNums');
        // 修改密码
        Route::post('changePassWord', 'ZHIHU\Systems\PersonalController@changePassWord');

        // 关注用户
        Route::match(['post', 'get'], 'followerUser', 'ZHIHU\Topics\FollowerController@followerUser');
        // 取消关注用户
        Route::post('followerCancelUser', 'ZHIHU\Topics\FollowerController@followerCancelUser');
        // 我关注的用户
        Route::match(['post', 'get'], 'followerList', 'ZHIHU\Topics\FollowerController@followerList');
        // 我的粉丝
        Route::match(['post', 'get'], 'followerFanceList', 'ZHIHU\Topics\FollowerController@followerFanceList');
        // 我的收藏
        Route::match(['post', 'get'], 'getCollectionList', 'ZHIHU\Topics\FollowerController@getCollectionList');

        // 取消/关注问题
        Route::match(['post', 'get'], 'followersQuestion', 'ZHIHU\Topics\FollowerController@followersQuestion');
        // 取消/喜欢问题
        Route::match(['post', 'get'], 'favouriteQuestion', 'ZHIHU\Topics\FollowerController@favouriteQuestion');
        // 举报问题
        Route::match(['post', 'get'], 'reportQuestion', 'ZHIHU\Topics\FollowerController@reportQuestion');
        // 我关注的问题
        Route::match(['post', 'get'], 'myFollowerQuestionList', 'ZHIHU\Topics\FollowerController@myFollowerQuestionList');
        // 我喜欢的问题
        Route::match(['post', 'get'], 'myFavouriteQuestionList', 'ZHIHU\Topics\FollowerController@myFavouriteQuestionList');
        // 我评论的问题
        Route::match(['post', 'get'], 'myCommentQuestionList', 'ZHIHU\Topics\FollowerController@myCommentQuestionList');
        // 取消/收藏博文
        Route::match(['post', 'get'], 'collectionQuestion', 'ZHIHU\Topics\FollowerController@collectionQuestion');

        // 获取问题及评论信息
        Route::post('getQuestionCommentByQuestionID', 'ZHIHU\Topics\AnswersController@getQuestionCommentByQuestionID');
        // 回答问题-评论
        Route::post('answerQuestion', 'ZHIHU\Topics\AnswersController@answerQuestion');
        // 删除回答问题-评论
        Route::post('removeAnswerQuestion', 'ZHIHU\Topics\AnswersController@removeAnswerQuestion');
        // 我参与的问题-我评论过的问题
        Route::match(['post', 'get'], 'myAnswerQuestionList', 'ZHIHU\Topics\AnswersController@myAnswerQuestionList');
        // 对回答的问题点赞-评论点赞
        Route::match(['post', 'get'], 'answerQuestionVote', 'ZHIHU\Topics\AnswersController@answerQuestionVote');
        // 对回答的问题点赞-评论点赞-取消
        Route::match(['post', 'get'], 'answerQuestionCancelVote', 'ZHIHU\Topics\AnswersController@answerQuestionCancelVote');
        // 对问题进行采纳
        Route::match(['post', 'get'], 'answerQuestionAdopt', 'ZHIHU\Topics\AnswersController@answerQuestionAdopt');
        // 举报答案
        Route::match(['post', 'get'], 'reportAnswer', 'ZHIHU\Topics\AnswersController@reportAnswer');
    });

    // 网站中心
    // 获取最新问题列表
    Route::match(['post', 'get'], 'getWebLatestQuestion', 'ZHIHU\Centers\MainWebSiteController@getWebLatestQuestion');
    // 获取最热问题列表
    Route::match(['post', 'get'], 'getWebHotQuestion', 'ZHIHU\Centers\MainWebSiteController@getWebHotQuestion');
    // 获取我关注的问题列表
    Route::match(['post', 'get'], 'getFollowerQuestionList', 'ZHIHU\Centers\MainWebSiteController@getFollowerQuestionList');
    // 获取博文中心问题列表
    Route::match(['post', 'get'], 'getBlogQuestionList', 'ZHIHU\Centers\MainWebSiteController@getBlogQuestionList');
    // 获取话题问题列表
    Route::match(['post', 'get'], 'getTopicListData', 'ZHIHU\Centers\MainWebSiteController@getTopicListData');
    // 获取精华榜列表
    Route::match(['post', 'get'], 'getTopicCreamList', 'ZHIHU\Centers\MainWebSiteController@getTopicCreamList');

	// 查看问题
	Route::match(['post', 'get'], 'viewShowQuestion', 'ZHIHU\Centers\MainWebSiteController@viewShowQuestion');
    // 获取最新公告信息
    Route::match(['post', 'get'], 'getWebLatestNotice', 'ZHIHU\Centers\MainWebSiteController@getWebLatestNotice');
    // 获取广告信息
    Route::match(['post', 'get'], 'getWebAdvs', 'ZHIHU\Centers\MainWebSiteController@getWebAdvs');
    // 获取封面信息
    Route::match(['post', 'get'], 'getWebCover', 'ZHIHU\Centers\MainWebSiteController@getWebCover');
    // 获取话题统计信息
    Route::match(['post', 'get'], 'getWebTopics', 'ZHIHU\Centers\MainWebSiteController@getWebTopics');
    // 获取有效话题信息
    Route::match(['post', 'get'], 'getWebTopicsEffect', 'ZHIHU\Centers\MainWebSiteController@getWebTopicsEffect');
    // 获取友情链接
    Route::match(['post', 'get'], 'getWebFriendLinks', 'ZHIHU\Centers\MainWebSiteController@getWebFriendLinks');
});
// ------------------------------------------------- end 技术林应用路由 -------------------------------------------------

// ------------------------------------------------- start 技术林-流程引擎路由-20200710 -------------------------------------------------
Route::prefix('wf')->group(function () {
    // 用户登录
    Route::post('signIn', 'WF\Systems\SigninController@signIn');
    // 用户退出
    Route::match(['post', 'get'], 'signOut', 'WF\Systems\SigninController@signOut');
    // 需登录后操作路由
    Route::middleware(['checkWfToken'])->group(function() {
        // 公共操作
        // 对象存储上传图片
        Route::post('putObjectChannelImages', 'WF\Systems\ShareController@putObjectChannelImages');
        // 获取对象存储文件
        Route::post('getObjectChannel', 'WF\Systems\ShareController@getObjectChannel');
        // 上传本地文件
        Route::post('putObjectChannelLocalFile', 'WF\Systems\ShareController@putObjectChannelLocalFile');
        // 获取系统代码字段
        Route::match(['post', 'get'], 'getSystemDicts', 'WF\Systems\ShareController@getSystemDicts');
        // 获取公司字典
        Route::match(['post', 'get'], 'getComDicts', 'WF\Systems\ShareController@getComDicts');
        // 通过代码类型获取代码信息
        Route::match(['post', 'get'], 'getSystemDictByKey', 'WF\Systems\ShareController@getSystemDictByKey');
        // 通过字典类型获取字典信息
        Route::match(['post', 'get'], 'getComDictByKey', 'WF\Systems\ShareController@getComDictByKey');

        // 授权操作
        // 获取授权树
        Route::match(['post', 'get'], 'getAuthorizeTree', 'WF\Systems\AuthorizeController@getAuthorizeTree');
        // 公司平台授权
        Route::post('authorizeCompany', 'WF\Systems\AuthorizeController@authorizeCompany');
        // 角色授权
        Route::post('authorizeRole', 'WF\Systems\AuthorizeController@authorizeRole');
        // 用户授权
        Route::post('authorizeUser', 'WF\Systems\AuthorizeController@authorizeUser');
        // 公司字典授权
        Route::post('authorizeDict', 'WF\Systems\AuthorizeController@authorizeDict');


        // 公司平台管理操作
        // 公司列表
        Route::match(['post', 'get'], 'getCompanyList', 'WF\Systems\CompanyController@getCompanyList');
        // 获取公司详情
        Route::match(['post', 'get'], 'getCompanyById', 'WF\Systems\CompanyController@getCompanyById');
        // 获取公司架构树
        Route::match(['post', 'get'], 'getCompanyListTree', 'WF\Systems\CompanyController@getCompanyListTree');
        // 添加公司平台
        Route::post('companyCrd', 'WF\Systems\CompanyController@companyCrd');
        // 修改公司平台
        Route::post('companyEdt', 'WF\Systems\CompanyController@companyEdt');
        // 删除公司平台
        Route::post('companyRmv', 'WF\Systems\CompanyController@companyRmv');
        // 公司平台启用禁用
        Route::post('companySts', 'WF\Systems\CompanyController@companySts');

        // 架构操作
        // 添加架构
        Route::post('orgCrd', 'WF\Systems\CompanyController@orgCrd');
        // 修改架构
        Route::post('orgEdt', 'WF\Systems\CompanyController@orgEdt');
        // 删除架构
        Route::post('orgRmv', 'WF\Systems\CompanyController@orgRmv');

        // 角色操作
        // 获取角色列表
        Route::match(['post', 'get'], 'getRoleList', 'WF\Systems\RoleController@getRoleList');
        // 获取角色详情
        Route::match(['post', 'get'], 'getRoleById', 'WF\Systems\RoleController@getRoleById');
        // 获取角色字典
        Route::match(['post', 'get'], 'getRoleDicts', 'WF\Systems\RoleController@getRoleDicts');
        // 角色添加
        Route::post('roleCrd', 'WF\Systems\RoleController@roleCrd');
        // 角色编辑
        Route::post('roleEdt', 'WF\Systems\RoleController@roleEdt');
        // 角色删除
        Route::post('roleRmv', 'WF\Systems\RoleController@roleRmv');
        // 角色启用禁用
        Route::post('roleSts', 'WF\Systems\RoleController@roleSts');

        // 用户操作
        // 用户列表
        Route::match(['post', 'get'], 'getUserList', 'WF\Systems\UserController@getUserList');
        // 用户详情
        Route::match(['post', 'get'], 'getUserById', 'WF\Systems\UserController@getUserById');
        // 添加用户
        Route::post('userCrd', 'WF\Systems\UserController@userCrd');
        // 编辑用户
        Route::post('userEdt', 'WF\Systems\UserController@userEdt');
        // 删除用户
        Route::post('userRmv', 'WF\Systems\UserController@userRmv');
        // 用户启用禁用
        Route::post('userSts', 'WF\Systems\UserController@userSts');
        // 重置密码
        Route::post('userRepwd', 'WF\Systems\UserController@userRepwd');

        // 菜单操作
        Route::match(['post', 'get'], 'getAuthMenus', 'WF\Systems\MenuController@getAuthMenus');
        // 获取菜单树
        Route::match(['post', 'get'], 'getMenuListTree', 'WF\Systems\MenuController@getMenuListTree');
        // 添加菜单
        Route::post('menuCrd', 'WF\Systems\MenuController@menuCrd');
        // 修改菜单
        Route::post('menuEdt', 'WF\Systems\MenuController@menuEdt');
        // 删除菜单
        Route::post('menuRmv', 'WF\Systems\MenuController@menuRmv');

        // 公司字典操作
        // 字典类型列表
        Route::match(['post', 'get'], 'getDictTypeList', 'WF\Systems\DictsController@getDictTypeList');
        // 字典类型详情
        Route::match(['post', 'get'], 'getDictTypeById', 'WF\Systems\DictsController@getDictTypeById');
        // 字典类型添加
        Route::post('dictTypeCrd', 'WF\Systems\DictsController@dictTypeCrd');
        // 字典类型修改
        Route::post('dictTypeEdt', 'WF\Systems\DictsController@dictTypeEdt');
        // 字典类型删除
        Route::post('dictTypeRmv', 'WF\Systems\DictsController@dictTypeRmv');
        // 字典类型授权
        Route::post('dictTypeAccess', 'WF\Systems\DictsController@dictTypeAccess');
        // 字典详情列表
        Route::match(['post', 'get'], 'getDictList', 'WF\Systems\DictsController@getDictList');
        // 字典详情
        Route::match(['post', 'get'], 'getDictById', 'WF\Systems\DictsController@getDictById');
        // 添加详情
        Route::post('dictCrd', 'WF\Systems\DictsController@dictCrd');
        // 编辑详情
        Route::post('dictEdt', 'WF\Systems\DictsController@dictEdt');
        // 删除详情
        Route::post('dictRmv', 'WF\Systems\DictsController@dictRmv');
        // 详情启用禁用
        Route::post('dictSts', 'WF\Systems\DictsController@dictSts');

        // 流程管理操作
        // 获取流程列表
        Route::match(['post', 'get'], 'getWorkFlowList', 'WF\Systems\WorkFlowController@getWorkFlowList');
        // 获取流程详情
        Route::match(['post', 'get'], 'getWorkFlowByFlowId', 'WF\Systems\WorkFlowController@getWorkFlowByFlowId');
        // 创建流程
        Route::post('workFlowCrd', 'WF\Systems\WorkFlowController@workFlowCrd');
        // 编辑流程
        Route::post('workFlowEdt', 'WF\Systems\WorkFlowController@workFlowEdt');
        // Route::post('workFlowRmv', 'WF\Systems\WorkFlowController@workFlowRmv');
        // 流程启用禁用
        Route::post('workFlowSts', 'WF\Systems\WorkFlowController@workFlowSts');
        // 根据流程ID获取流程节点树
        Route::match(['post', 'get'], 'getWorkNodesTreeByFlowId', 'WF\Systems\WorkFlowController@getWorkNodesTreeByFlowId');
        // 获取报表列表
        Route::match(['post', 'get'], 'getBirtList', 'WF\Systems\WorkFlowController@getBirtList');
        // 通过UUID获取报表详情
        Route::match(['post', 'get'], 'getBirtByUuid', 'WF\Systems\WorkFlowController@getBirtByUuid');
        // 创建报表
        Route::post('birtCrd', 'WF\Systems\WorkFlowController@birtCrd');
        // 编辑报表
        Route::post('birtEdt', 'WF\Systems\WorkFlowController@birtEdt');
        // 删除报表
        Route::post('birtRmv', 'WF\Systems\WorkFlowController@birtRmv');
        // 报表启用禁用
        Route::post('birtSts', 'WF\Systems\WorkFlowController@birtSts');
        // 报表模拟
        Route::match(['post', 'get'], 'birtSimulation', 'WF\Systems\WorkFlowController@birtSimulation');
        // 通过流程ID获取业务设计表树
        Route::match(['post', 'get'], 'getJobConfigsTreeByFlowId', 'WF\Systems\WorkFlowController@getJobConfigsTreeByFlowId');
        // 创建业务表
        Route::post('jobConfigCrd', 'WF\Systems\WorkFlowController@jobConfigCrd');
        // 修改业务表
        Route::post('jobConfigEdt', 'WF\Systems\WorkFlowController@jobConfigEdt');
        // 获取最大业务字段
        Route::match(['post', 'get'], 'getJobConfigsMaxColumn', 'WF\Systems\WorkFlowController@getJobConfigsMaxColumn');
        // 获取业务字段列表
        Route::match(['post', 'get'], 'getJobConfigsList', 'WF\Systems\WorkFlowController@getJobConfigsList');
        // 保存业务字段
        Route::post('jobConfigSave', 'WF\Systems\WorkFlowController@jobConfigSave');
        // 删除业务字段
        Route::post('jobConfigRmv', 'WF\Systems\WorkFlowController@jobConfigRmv');
        // 删除业务表
        Route::post('jobTableRmv', 'WF\Systems\WorkFlowController@jobTableRmv');
        // 通过流程ID获取节点信息
        Route::match(['post', 'get'], 'getWorkNodeByFlowId', 'WF\Systems\WorkFlowController@getWorkNodeByFlowId');
        // 获取节点配置信息
        Route::match(['post', 'get'], 'getNodeColumnsConfig', 'WF\Systems\WorkFlowController@getNodeColumnsConfig');
        // 节点配置信息保存
        Route::post('nodeColumnsConfigSave', 'WF\Systems\WorkFlowController@nodeColumnsConfigSave');
        // 通过KEY获取全局配置信息
        Route::match(['post', 'get'], 'getWrokFlowGlobalByKey', 'WF\Systems\WorkFlowController@getWrokFlowGlobalByKey');
        // 全局配置保存
        Route::post('workFlowGlobalConfigSave', 'WF\Systems\WorkFlowController@workFlowGlobalConfigSave');
        // 获取流程过程升级脚本
        Route::post('getWorkFlowProcessScript', 'WF\Systems\WorkFlowController@getWorkFlowProcessScript');
        // 获取升级脚本
        Route::post('getWorkFlowUpgradeScript', 'WF\Systems\WorkFlowController@getWorkFlowUpgradeScript');
        // 获取节点审核配置信息
        Route::match(['post', 'get'], 'getNodeColumnExamConfig', 'WF\Systems\WorkFlowController@getNodeColumnExamConfig');
        // 获取表单配置列表
        Route::match(['post', 'get'], 'getFormDesignList', 'WF\Systems\WorkFlowController@getFormDesignList');
        // 获取表单配置信息
        Route::match(['post', 'get'], 'getFormDesignConfigByFormId', 'WF\Systems\WorkFlowController@getFormDesignConfigByFormId');
        // 创建表单
        Route::post('formDesignConfigCrd', 'WF\Systems\WorkFlowController@formDesignConfigCrd');
        // 修改表单
        Route::post('formDesignConfigEdt', 'WF\Systems\WorkFlowController@formDesignConfigEdt');
        // 启用禁用表单
        Route::post('formDesignConfigSts', 'WF\Systems\WorkFlowController@formDesignConfigSts');
        // 删除表单
        Route::post('formDesignConfigRmv', 'WF\Systems\WorkFlowController@formDesignConfigRmv');
        // 表单配置保存
        Route::post('formDesignConfigSave', 'WF\Systems\WorkFlowController@formDesignConfigSave');
        // 获取业务表字段信息通过流程ID-导出EXCEL
        Route::post('getJobsColumnByFlowId', 'WF\Systems\WorkFlowController@getJobsColumnByFlowId');


        // 业务开始
        // 获取相关业务配置渲染页面信息
        Route::match(['post', 'get'], 'getWorkFlowTodoConfig', 'WF\Systems\JobController@getWorkFlowTodoConfig');
        // 获取业务列表
        Route::match(['post', 'get'], 'getJobTodoList', 'WF\Systems\JobController@getJobTodoList');
        // 业务提交或保存
        Route::post('jobCommitOrSaveOfWorkFlow', 'WF\Systems\JobController@jobCommitOrSaveOfWorkFlow');
        // 业务审核提交或保存
        Route::post('jobExamOrSubmitWorkFlow', 'WF\Systems\JobController@jobExamOrSubmitWorkFlow');
        // 业务提交
        Route::post('jobProcessSubmit', 'WF\Systems\JobController@jobProcessSubmit');
        // 业务详情打印
        Route::post('printJobDetailByUuid', 'WF\Systems\JobController@printJobDetailByUuid');
        // 业务列表导出
        Route::post('exportJobListByNodeId', 'WF\Systems\JobController@exportJobListByNodeId');
        // 获取业务审核信息
        Route::match(['post', 'get'], 'getJobExamDetail', 'WF\Systems\JobController@getJobExamDetail');
        // 获取业务数据
        Route::match(['post', 'get'], 'getJobDataDetail', 'WF\Systems\JobController@getJobDataDetail');
        // 获取默认表单显示配置
        Route::match(['post', 'get'], 'getJobFormDefaultConfig', 'WF\Systems\JobController@getJobFormDefaultConfig');
        // 根据业务配置ID获取配置
        Route::match(['post', 'get'], 'getJobFormConfigByFormId', 'WF\Systems\JobController@getJobFormConfigByFormId');

        // 系统内置分析
        // 获取业务分析统计数据
        Route::match(['post', 'get'], 'getWorkFlowAnalysis', 'WF\Systems\JobController@getWorkFlowAnalysis');
        // 获取业务流程
        Route::match(['post', 'get'], 'getWorkFlowByBelongForEnabled', 'WF\Systems\JobController@getWorkFlowByBelongForEnabled');
        // 获取流程数据分析
        Route::match(['post', 'get'], 'getJobWorkFlowNodeAnalysis', 'WF\Systems\JobController@getJobWorkFlowNodeAnalysis');

        // 历史记录
        // 获取业务历史列表
        Route::match(['post', 'get'], 'getHistoryJobList', 'WF\Systems\JobController@getHistoryJobList');
        // 获取历史详情数据
        Route::match(['post', 'get'], 'getHistoryJobDataDetail', 'WF\Systems\JobController@getHistoryJobDataDetail');
        // 通过流程ID获取流程有效节点信息-字典显示
        Route::match(['post', 'get'], 'getWorkNodeByFlowIdForDict', 'WF\Systems\JobController@getWorkNodeByFlowIdForDict');

        // 客户列表
        // 客户列表字段配置获取
        Route::match(['post', 'get'], 'getCustomerColumnsConfig', 'WF\Systems\JobController@getCustomerColumnsConfig');
        // 获取客户列表数据
        Route::match(['post', 'get'], 'getCustomerJobList', 'WF\Systems\JobController@getCustomerJobList');
        // 客户列表导出
        Route::post('exportCustomerListByFlowId', 'WF\Systems\JobController@exportCustomerListByFlowId');
        // 获取客户列表字段配置
        Route::match(['post', 'get'], 'getCustomerColumnsSet', 'WF\Systems\JobController@getCustomerColumnsSet');
        // 客户列表设置保存
        Route::post('customerColumnsSetSave', 'WF\Systems\JobController@customerColumnsSetSave');

    });
});
// ------------------------------------------------- end 技术林-流程引擎路由 -------------------------------------------------
