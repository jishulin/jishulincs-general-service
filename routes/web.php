<?php

use Illuminate\Support\Facades\Route;
use App\Eloqumd\Zhihu\ZhihuSystemCover;
use Illuminate\Http\Request;
use Jishulin\WorkFlowEngine\Tools\Tools;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $currentTime = time();
    $special = ZhihuSystemCover::where('start_date', '<=', $currentTime)->where('end_date', '>=', $currentTime)->limit(1)->first();
    if ($special) {
        $coverimg = $special['coverimg'];
    } else {
        $common = ZhihuSystemCover::where('status', 1)->limit(1)->first();
        $coverimg = $common['coverimg'] ?? '';
    }

    return view('index', [
        'coverimg' => $coverimg
    ]);
});

// 流程设计器
Route::get('/designer/{flowId}', function ($flowId) {
    return view('designer/index', ['flowId' => $flowId]);
});
Route::get('/getWorkFlowByFlowId/{flowId}', function ($flowId) {
    $res = Tools::getInstance()->getWorkFlowByFlowId($flowId);

    return response()->json($res);
});
Route::post('/flowSave', function (Request $request) {
    $params = $request->all();
    $tools = Tools::getInstance();
    return $tools->workFlowSave($params);
});





Route::any('/test3', function () {
    $data = [
        'config' => [
            'key' => public_path() . '/template/test/test.docx',
            'fields' => [
                'single' => [[0, 'name'], [0, 'sex'], [1, 'image']],
            ]
        ],
        'downType' => 'D',
        'fileName' => '文件模板测试',
        'single' => [
            'name' => '丽丽',
            'sex' => '女',
            'image' => ''
        ],
    ];

    \Jishulin\WorkFlowEngine\Tools\Utils\Words::getInstance()->createWordUseTemplate($data);
});
