## 关于技术林-流程引擎【服务端】(基础开源版)

[前端开源地址](https://gitee.com/jishulin/jishulincs-workflow)

前提说明：该套流程引擎主要面向公司开发人员，实时人员进行流程配置等工作，不建议直接开放给客户使用！

内置基础功能为SAAS版本模式，附加一套社区论坛代码（含前后端），另一套为书籍平台（不含前端代码），内置规则引擎扩展包（主要用于配合流程引擎大表单使用）。

系统内置功能有：

- 系统管理>>平台架构>>单位平台管理
- 系统管理>>平台架构>>组织架构
- 系统管理>>平台架构>>用户管理
- 系统管理>>平台架构>>角色管理
- 系统管理>>运维管理>>菜单管理
- 系统管理>>运维管理>>流程管理
- 系统管理>>运维管理>>字典管理
- 工作台>>客户列表
- 工作台>>历史记录
- 工作台>>业务分析>>图表统计

关于系统详细文档请前往：[技术林-流程引擎文档(开源版)](https://www.kancloud.cn/lijianlin/jishullin_workflow_engine/1894424)

## 打赏
开源不易，熬夜不易，接收任何形式的打赏！谢谢！
![](https://img.kancloud.cn/c5/fb/c5fb2bdd7ab5b55d7e74fe5ae6c84df9_300x407.jpg) ![](https://img.kancloud.cn/8e/51/8e517c760ed85119d42633302fe833b0_300x450.jpg)

## 截图示例
![](https://img.kancloud.cn/cd/f8/cdf83701ab92a25add8720ffa7f143b9_1858x976.png)
![](https://img.kancloud.cn/c1/d1/c1d15e48ec948082276d871334a75f1c_1858x976.png)
![](https://img.kancloud.cn/03/20/032044aa754c4c2b524def6de8e3867c_1858x976.png)
![](https://img.kancloud.cn/c3/ad/c3ad94395b5e69a1deb7ad18997ada0f_1858x976.png)
![](https://img.kancloud.cn/11/7e/117eaddc530571cc8b7c6d11a5cb8fff_1858x976.png)

## 其他资源

[基于TP5.1实用案例及教程](https://www.kancloud.cn/lijianlin/ethantp51/1196443)

[技术林-社区](https://www.jishulincs.cn)

[技术林-商城(管理端)](http://rages.jishulincs.cn/backend/)
账号：JzQ32 密码：RtGrcGyZAi

[技术林-商城(商户端)](http://rages.jishulincs.cn/merchant/)
账号：test 密码：shanghutest

[技术林-商城(H5)](http://h5s.jishulincs.cn)


[技术林-报表设计器(体验)](http://139.155.47.132:8080/RDP-SERVER)
账号：user 密码：000000
测试可使用数据表：yw_zgjob01,02,03,04

[技术林-IM即时通讯(纯前端暂未开发后台服务)](http://im.jishulincs.cn)


## 高级版介绍
- 1、优化开源版相关bug
- 2、完善开源版一些查看功能
- 3、流程报表引入二维码及条形码功能
- 4、内置流程报表二维码或条形码识别搜索确认功能
- 5、嵌入报表管理工具(报表设计器)[报表，合同，大屏]功能
- 6、集成即时通讯IM服务
- 7、新增流程业务数据入库到系统数据
- 8、新增业务流程前后置数据库查询限制动作(目前内置PHP代码模式控制，含限制，拦截，扭转等所有自行控制方法)
- 9、新增系统自定义数据表功能
- 10、新增系统字段权限功能(编辑，查看)

敬请期待......

## License

MIT
