<?php

return [
    // 登录缓存时间配置
    'login_cache' => 60 * 60 * 24 * 7,
    // 错误信息换行符配置
    'broken_line' => "<br/>",
    // 图片文件后缀配置
    'image_suffix' => ['gif', 'jpg', 'jpeg', 'bmp', 'png', 'swf', 'pcx', 'tiff', 'tga', 'exif', 'fpx', 'svg', 'psd', 'cdr'],
    // 系统域名
    'server_domain' => 'http://api.ss.com',
    // 密码缓存加密加盐处理
    'pwd_cache' => '947ac6c3d0c2a033f9dfd732be2f5131',
    // 用户初始密码-平台管理员
    'init_system_pwd' => 'Aa-1234567',
    // 用户初始密码-普通用户
    'init_common_pwd' => 'Aa-1234567!@#$...',
    
    // 技术林-邮件校验缓存有效期
    'zhihu_email_verify_expr' => 60 * 15,
    // 技术林-找回密码缓存有效期
    'zhihu_retrieve_pwd_verify_expr' => 60 * 10,
    // 技术林-系统设置key配置,多个逗号隔开
    'zhihu_system_keys' => 'dirty',
    // 技术林-问题查看阅读时效控制
    'zhihu_question_read' => 60 * 30,
    // 技术林-默认头像
    'zhihu_default_img' => 'https://jishulin-1258682100.cos.ap-nanjing.myqcloud.com/2020-06-30T10%3A13%3A06%2B08%3A00-1u4ss389h15m7mkl989sf51148.jpg?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKID71g6HCnZOxKTLx8VWJtEKBoFTYNBm8F6%26q-sign-time%3D1593483127%3B1908843187%26q-key-time%3D1593483127%3B1908843187%26q-header-list%3Dhost%26q-url-param-list%3D%26q-signature%3Dc7aefc28e250a1cfad78347a87b830999f4ecb90',
    // 技术林-最新，最受欢迎显示条数控制
    'zhihu_latest_question_list' => 5,
    // 技术林-问题分类
    'zhihu_question_type' => [
        '' => '其他',
        'else' => '其他',
        'blog' => '博文',
        'discuss' => '讨论',
        'link' => '链接',
        'question' => '问答',
    ],
    
    // 技术林-流程引擎
    'jishulin_init_pwd' => 'JSL@1234567',
    
];
